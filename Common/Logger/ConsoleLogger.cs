﻿using System;

namespace Common.Logger
{
	public class ConsoleLogger : ILogger
	{
		public event Action<ErrorLoggedEventArgs> ErrorLogged;
		public string Name { get; private set; }

		public ConsoleLogger(string loggerName)
		{
			Name = loggerName;
		}

		public void Dispose()
		{
			ErrorLogged = null;
		}

		public void LogInfo(string info)
		{
			LogMessage(info, LogMessageType.Info);
		}

		public void LogError(string error)
		{
			LogMessage(CommonStrings.Error + error, LogMessageType.Error);
		}

		public void LogException(string message, Exception exception)
		{
			LogMessage(CommonStrings.Exception + message + ": " + Environment.NewLine + exception, LogMessageType.Exception);
		}

		public void LogMessage(string message, LogMessageType messageType)
		{
			var resultMessage = DateTime.Now.ToString("hh:mm:ss.fff") + ':' + Environment.NewLine + message + Environment.NewLine;
			Console.WriteLine(resultMessage);

			if (messageType == LogMessageType.Error || messageType == LogMessageType.Exception ||
				messageType == LogMessageType.ResultNegative)
			{
				RaiseErrorLogged(message, messageType);
			}
		}

		private void RaiseErrorLogged(string message, LogMessageType messageType)
		{
			var handler = ErrorLogged;
			if (handler != null)
				handler(new ErrorLoggedEventArgs(message, messageType));
		}
	}
}
