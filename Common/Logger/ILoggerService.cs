﻿namespace Common.Logger
{
	public interface ILoggerService : IService
	{
		bool HasErrors { get; }
		ILogger GetLogger(string loggerName);
		void OpenLog(LogFilter logFilter);
	}
}
