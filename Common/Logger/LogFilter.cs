﻿using System.Collections.Generic;
using System.Linq;

namespace Common.Logger
{
	public class LogFilter
	{
		public List<string> LoggersFilter { get; private set; }
		public List<LogMessageType> MessageTypeFilter { get; private set; }

		public LogFilter(params string[] loggersFilter)
		{
			LoggersFilter = loggersFilter.ToList();
			MessageTypeFilter = new List<LogMessageType>();
		}

		public LogFilter(params LogMessageType[] logMessageTypes)
		{
			LoggersFilter = new List<string>();
			MessageTypeFilter = logMessageTypes.ToList();
		}
	}
}
