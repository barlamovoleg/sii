﻿namespace Common.Logger
{
	public enum LogMessageType
	{
		Info = 0,
		ResultPositive = 1,
		ResultNegative = 2,
		Error = 3,
		Exception = 4
	}
}
