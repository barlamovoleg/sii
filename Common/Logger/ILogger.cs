﻿using System;

namespace Common.Logger
{
	public interface ILogger : IDisposable
	{
		event Action<ErrorLoggedEventArgs> ErrorLogged;
		
		string Name { get; }

		void LogInfo(string info);
		void LogError(string error);
		void LogException(string message, Exception exception);
		void LogMessage(string message, LogMessageType messageType);
	}

	public class ErrorLoggedEventArgs
	{
		public string Message { get; private set; }
		public LogMessageType MessageType { get; private set; }

		public ErrorLoggedEventArgs(string message, LogMessageType messageType = LogMessageType.Error)
		{
			Message = message;
			MessageType = messageType;
		}
	}
}
