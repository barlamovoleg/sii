﻿namespace Common.Visualizer
{
	public interface IVisualizerService<in TData, in THost> : IService
		where TData : class
		where THost : class
	{
		IVisualizer<TData, THost> CreateVisualizer(THost visualizerHost);
	}
}
