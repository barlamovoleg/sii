﻿using System;

namespace Common.Visualizer
{
	public interface IVisualizer<in TData, in THost> : IDisposable
		where TData : class
		where THost : class
	{
		bool IsPlaying { get; }
		bool IsAttached { get; }
		bool IsDataSet { get; }

		double CurrentSpeed { get; }

		void SetData(TData data);
		void RemoveData();

		void Attach(IGraphicsHostService<THost> host);
		void Detach();

		void Initialize();

		void Play();
		void Stop();
		void OneStep();

		void ChangePlaySpeed(double speed);
	}
}
