﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Common.ArrayExtensions;

// ReSharper disable once CheckNamespace
namespace Common.ObjectDeepCopy
{
	public static class ObjectsDeepCopy
	{
		private static readonly MethodInfo CloneMethod = typeof (Object).GetMethod("MemberwiseClone",
			BindingFlags.NonPublic | BindingFlags.Instance);

		public static bool IsPrimitive(this Type type)
		{
			if (type == typeof (String)) return true;
			return (type.IsValueType & type.IsPrimitive);
		}

		public static Object DeepCopy(this Object originalObject)
		{
			return InternalCopy(originalObject, new Dictionary<Object, Object>(new ReferenceEqualityComparer()));
		}

		private static Object InternalCopy(Object originalObject, IDictionary<Object, Object> visited)
		{
			if (originalObject == null) return null;
			Type typeToReflect = originalObject.GetType();
			if (IsPrimitive(typeToReflect)) return originalObject;
			if (visited.ContainsKey(originalObject)) return visited[originalObject];
			if (typeof (Delegate).IsAssignableFrom(typeToReflect)) return null;
			object cloneObject = CloneMethod.Invoke(originalObject, null);
			if (typeToReflect.IsArray)
			{
				Type arrayType = typeToReflect.GetElementType();
				if (IsPrimitive(arrayType) == false)
				{
					var clonedArray = (Array) cloneObject;
					clonedArray.ForEach(
						(array, indices) => array.SetValue(InternalCopy(clonedArray.GetValue(indices), visited), indices));
				}
			}
			visited.Add(originalObject, cloneObject);
			CopyFields(originalObject, visited, cloneObject, typeToReflect);
			RecursiveCopyBaseTypePrivateFields(originalObject, visited, cloneObject, typeToReflect);
			return cloneObject;
		}

		private static void RecursiveCopyBaseTypePrivateFields(object originalObject, IDictionary<object, object> visited,
			object cloneObject, Type typeToReflect)
		{
			if (typeToReflect.BaseType != null)
			{
				RecursiveCopyBaseTypePrivateFields(originalObject, visited, cloneObject, typeToReflect.BaseType);
				CopyFields(originalObject, visited, cloneObject, typeToReflect.BaseType,
					BindingFlags.Instance | BindingFlags.NonPublic, info => info.IsPrivate);
			}
		}

		private static void CopyFields(object originalObject, IDictionary<object, object> visited, object cloneObject,
			Type typeToReflect,
			BindingFlags bindingFlags =
				BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.FlattenHierarchy,
			Func<FieldInfo, bool> filter = null)
		{
			foreach (FieldInfo fieldInfo in typeToReflect.GetFields(bindingFlags))
			{
				if (filter != null && filter(fieldInfo) == false) continue;
				if (IsPrimitive(fieldInfo.FieldType)) continue;
				object originalFieldValue = fieldInfo.GetValue(originalObject);
				object clonedFieldValue = InternalCopy(originalFieldValue, visited);
				fieldInfo.SetValue(cloneObject, clonedFieldValue);
			}
		}

		public static T DeepCopy<T>(this T original)
		{
			return (T) DeepCopy((Object) original);
		}
	}

	public class ReferenceEqualityComparer : EqualityComparer<Object>
	{
		public override bool Equals(object x, object y)
		{
			return ReferenceEquals(x, y);
		}

		public override int GetHashCode(object obj)
		{
			if (obj == null) return 0;
			return obj.GetHashCode();
		}
	}
}