﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Common.Logger;

// ReSharper disable once CheckNamespace
namespace Common
{
	/// <summary>
	/// Helper class for managing low-level C# functions. (Reflection)
	/// </summary>
	public static class Code
	{
		/// <summary>
		/// Determines whether type is Nullable type.
		/// </summary>
		/// <param name="type">The type.</param>
		/// <returns>
		///   <c>true</c> if type is Nullable type; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsNullableType(Type type)
		{
			if (!type.IsValueType)
				return true;
			if (type.IsGenericType && (type.GetGenericTypeDefinition() == typeof(Nullable<>)))
				return true;
			return false;
		}

		/// <summary>
		/// Tries the do action.
		/// </summary>
		/// <param name="action">The action.</param>
		/// <param name="exceptionAction">The exception action.</param>
		public static void TryDo(Action action, Action<Exception> exceptionAction)
		{
			try
			{
				action();
			}
			catch (Exception exception)
			{
				exceptionAction(exception);
			}
		}

		/// <summary>
		/// Tries the do action.
		/// </summary>
		/// <param name="action">The action.</param>
		/// <param name="exceptionAction">The exception action.</param>
		/// <param name="finallyAction">The finally action.</param>
		public static void TryDo(Action action, Action<Exception> exceptionAction, Action finallyAction)
		{
			try
			{
				action();
			}
			catch (Exception exception)
			{
				exceptionAction(exception);
			}
			finally
			{
				finallyAction();
			}
		}

		/// <summary>
		/// Tries do action and get exception or null.
		/// </summary>
		/// <param name="action">The action.</param>
		public static Exception TryDoLoggingException(Action action)
		{
			try
			{
				action();
			}
			catch (Exception exception)
			{
				return exception;
			}
			return null;
		}

		/// <summary>
		/// Tries the do action with writing to log occured exception.
		/// </summary>
		/// <param name="action">The action.</param>
		/// <param name="actionDescription">The action description.</param>
		/// <param name="logger">The logger</param>
		public static void TryDoWithLogging(Action action, string actionDescription, ILogger logger)
		{
			try
			{
				action();
			}
			catch (Exception exception)
			{
				logger.LogException(actionDescription, exception);
			}
		}

		/// <summary>
		/// Converts the massive2D to massive1D.
		/// </summary>
		/// <typeparam name="T">The data type</typeparam>
		/// <param name="massive">The massive.</param>
		/// <returns></returns>
		public static T[] ConvertMassive2To1<T>(T[,] massive)
		{
			int n = massive.GetLength(0);
			int m = massive.GetLength(1);
			var data = new T[n * m];

			for (int i = 0; i < n; i++)
				for (int j = 0; j < m; j++)
					data[i * m + j] = massive[j, i];

			return data;
		}

		/// <summary>
		/// Gets the name of the member.
		/// Use it as () => <c>member</c>
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="memberExpression">The member expression.</param>
		/// <returns></returns>
		public static string GetMemberName<T>(Expression<Func<T>> memberExpression)
		{
			var expressionBody = (MemberExpression)memberExpression.Body;
			return expressionBody.Member.Name;
		}

		/// <summary>
		/// Gets the property by property name in type, or null if property with this name has not exist.
		/// </summary>
		/// <param name="ownerType">Type of the owner.</param>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="onlyPublic"></param>
		/// <returns></returns>
		public static PropertyInfo GetPropertyByName(Type ownerType, string propertyName, bool onlyPublic = true)
		{
			var props = ownerType.GetProperties().ToList();
			if (!onlyPublic)
				props.AddRange(ownerType.GetProperties(BindingFlags.NonPublic |
						 BindingFlags.Instance));
			return props.FirstOrDefault(p => p.Name == propertyName);
		}

		/// <summary>
		/// Logic OR.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="comparableValue">The comparable value.</param>
		/// <param name="values">The values.</param>
		/// <returns></returns>
		public static bool Or<T>(T comparableValue, params T[] values)
		{
			return values.Contains(comparableValue);
		}

		/// <summary>
		/// Logic AND.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="comparableValue">The comparable value.</param>
		/// <param name="values">The values.</param>
		/// <returns></returns>
		public static bool And<T>(T comparableValue, params T[] values)
		{
			return values.All(value => Equals(comparableValue, value));
		}

		/// <summary>
		/// To swap the objects.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="obj1">The obj1.</param>
		/// <param name="obj2">The obj2.</param>
		public static void Swap<T>(ref T obj1, ref T obj2)
		{
			var any = obj2;
			obj2 = obj1;
			obj1 = any;
		}

		/// <summary>
		/// Gets the description of the enum value (you must specify the attribute [Description]).
		/// </summary>
		/// <param name="value">The value.</param>
		/// <returns></returns>
		public static string GetEnumDescription(Enum value)
		{
			FieldInfo fi = value.GetType().GetField(value.ToString());

			var attributes =
				(DescriptionAttribute[])fi.GetCustomAttributes(
					typeof(DescriptionAttribute),
					false);

			if (attributes.Length > 0)
				return attributes[0].Description;
			return string.Empty;
		}

		/// <summary>
		/// Type the has attribute with specific type.
		/// This comparision only by attribute type.
		/// </summary>
		/// <param name="type">The type.</param>
		/// <param name="attributeType">Type of the attribute.</param>
		/// <param name="inherit">if set to <c>true</c> [inherit].</param>
		/// <returns></returns>
		public static bool TypeHasAttribute(Type type, Type attributeType, bool inherit = false)
		{
			return type.IsDefined(attributeType, inherit);
		}

		/// <summary>
		/// Enum of type T has flag.
		/// Type T should be Enum type and has [Flags] attribute.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="value">The value.</param>
		/// <param name="flag">The flag.</param>
		/// <returns></returns>
		public static bool EnumHasFlag<T>(T value, T flag) where T : struct, IComparable, IFormattable, IConvertible
		{
			var type = typeof(T);
			if (!type.IsEnum)
			{
				throw new AppException("attempt to get contains flag of non Enum type",
				   "{31CEDF8A-3070-4A68-81CA-F9E8FAD644FD}");
			}

			if (!TypeHasAttribute(type, typeof(FlagsAttribute)))
			{
				throw new AppException("attempt to get contains flag of Enum type without Flags attribute",
					"{F7EB51BE-9C2A-4C77-A019-F2D870088DF7}");
			}

			bool resultValue;
			var convertedValue = ConvertToEnum(EnumTryParse<T>(value, out resultValue));
			bool resultFlag;
			var convertedFlag = ConvertToEnum(EnumTryParse<T>(flag, out resultFlag));
			if (!(resultValue && resultFlag))
				throw new AppException("error while converting instances to enum type in checking for flag function",
					"{07DDD762-C670-478E-8F71-3057D8A8C27C}");

			return convertedValue.HasFlag(convertedFlag);
		}

		/// <summary>
		/// Try get instance of the spcific enum type.
		/// </summary>
		/// <typeparam name="T">Enum type</typeparam>
		/// <param name="instance">The instance.</param>
		/// <param name="result"><c>True</c> if conversion was successful; otherwise <c>False</c></param>
		/// <returns>Converted instance</returns>
		public static T EnumTryParse<T>(object instance, out bool result) where T : struct, IComparable, IFormattable, IConvertible
		{
			if (!typeof(T).IsEnum)
			{
				throw new AppException("attempt to enum try parse for non Enum type",
					"{12A1BFA0-49C9-4F25-A9E1-3E81FFA4E21D}");
			}

			T converted;
			result = Enum.TryParse(instance.ToString(), out converted);
			return converted;
		}

		/// <summary>
		/// Converts to enum type.
		/// Dont't Parse! Use it like <c>(Enum)instance</c>.
		/// Returns converted value or default(Enum) value.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="instance">The instance.</param>
		/// <returns>converted value or default(Enum)</returns>
		public static Enum ConvertToEnum<T>(T instance) where T : struct, IComparable, IFormattable, IConvertible
		{
			if (!typeof(T).IsEnum)
			{
				throw new AppException("attempt to convert to enum type non enum type",
					"{54B5876A-6FE1-4B27-9556-5EF0E6438AB3}");
			}

			return (Enum)(object)instance;
		}
	}
}
