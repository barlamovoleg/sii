﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;

// ReSharper disable once CheckNamespace
namespace Common
{
	public class FileHelper
	{
		public static void WriteLineAsync(string line, string filePath, object locker, bool append = true)
		{
			var thread = new Thread(() =>
			{
				lock (locker)
				{
					WriteLine(line, filePath, append);
				}
			});
			thread.Start();
		}

		public static void WriteLine(string line, string filePath, bool append = true)
		{
			using (var fStream = new StreamWriter(filePath, append))
			{
				fStream.WriteLine(line);
				fStream.Close();
			}
		}

		public static void SerializeToFile<T>(T obj, string filePath)
		{
			var formatter = new BinaryFormatter();
			var stream = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.None);
			formatter.Serialize(stream, obj);
			stream.Close();
		}

		public static T DeserializeFromFile<T>(string filePath)
		{
			var formatter = new BinaryFormatter();
			var stream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read);
			var obj = (T)formatter.Deserialize(stream);
			stream.Close();
			return obj;
		}
	}
}
