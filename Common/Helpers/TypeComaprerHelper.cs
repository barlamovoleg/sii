﻿namespace Common.Helpers
{
	public static class TypeComaprerHelper
	{
		public static bool IsOr<T>(object instance)
		{
			return instance is T;
		}

		public static bool IsOr<T1,T2>(object instance)
		{
			return instance is T1 || instance is T2;
		}

		public static bool IsOr<T1,T2,T3>(object instance)
		{
			return instance is T1 || instance is T2 || instance is T3;
		}

		public static bool IsOr<T1,T2,T3,T4>(object instance)
		{
			return instance is T1 || instance is T2 || instance is T3 || instance is T4;
		}
	}
}
