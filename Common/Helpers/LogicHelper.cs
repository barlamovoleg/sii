﻿using System;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace Common
{
	public static class LogicHelper
	{
		public static bool Or(params bool[] values)
		{
			if (values == null)
				throw new ArgumentNullException("values");

			return values.Any(value => value);
		}

		public static bool And(params bool[] values)
		{
			if (values == null)
				throw new ArgumentNullException("values");

			return values.All(value => value);
		}

		public static bool EqualsOr<T>(T value, params T[] objects)
		{
			if (objects == null)
				throw new ArgumentNullException("objects");

			return objects.Contains(value);
		}
	}
}
