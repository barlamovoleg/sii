﻿namespace Common.Repository
{
    public interface IRepositoryRelated<T1, in T2> : IRepository<T1>
    {
        void AddRelation(T1 entity1, T2 entity2);
        bool RemoveRelation(T1 entity1, T2 entity2);
    }
}
