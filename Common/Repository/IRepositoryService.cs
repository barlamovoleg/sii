﻿using System.Collections.Generic;


namespace Common.Repository
{
	public interface IRepositoryRelatedService<T1, in T2> : IService
    {
        IRepositoryRelated<T1, T2> GetRepository(string name);
        IEnumerable<string> GetRepositoryNames();
		void RemoveRepository(string name);
    }
}
