﻿using System.Collections.Generic;


namespace Common.Repository
{
    public interface IRepository<T>
    {
        IEnumerable<T> GetAll();
        T GetByID(int id);
        void Add(T entity);
        void Update(T entity);
        bool Delete(T entity);
    }
}
