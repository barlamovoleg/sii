﻿using System;

// ReSharper disable CSharpWarnings::CS0162
namespace Common
{
	public static class SystemConfiguration
	{
		public static bool IsDebug
		{
			get
			{
#if DEBUG
				return true;
#endif
				return false;
			}
		}
	}
}
