﻿// ReSharper disable once CheckNamespace
namespace Common
{
	/// <summary>
	/// Построитель приложения.
	/// Пробует собрать приложения из тех сервисов которые лежат в <see cref="IServiceLocator"/>
	/// и связывает их в единую структуру.
	/// </summary>
	public interface IAppBuilder
	{
		IAppInfo AppInfo { get; }

		void Run();
		void Build(IServiceLocator serviceLocator);

		void Destroy();
	}
}
