﻿// ReSharper disable once CheckNamespace
namespace Common
{
	public enum CloseReason
	{
		Unknown,
		Exception,
		System,
		User
	}
}
