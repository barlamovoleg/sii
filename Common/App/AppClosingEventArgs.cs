﻿// ReSharper disable once CheckNamespace
namespace Common
{
	public class AppClosingEventArgs
	{
		public bool CancelClose;

		public CloseReason CloseReason
		{
			get { return _closeReason; }
		}
		private readonly CloseReason _closeReason;

		public AppClosingEventArgs(CloseReason closeReason)
		{
			_closeReason = closeReason;
		}
	}
}
