﻿// ReSharper disable once CheckNamespace
namespace Common
{
	public interface IAppInfo
	{
		string AppName { get; }
		string FullVersion { get; }
		string Version { get; }
		string AppFullName { get; }
		VersionType VersionType { get; }

		string AppPath { get; }
	}
}
