﻿using System;

// ReSharper disable once CheckNamespace
namespace Common
{
	/// <summary>
	/// Хост приложения.
	/// Знает какие сервисы нужно регистрировать для данного приложения
	/// и выбирает каким <see cref="IAppBuilder"/> собирать воедино данные сервисы.
	/// </summary>
	public interface IAppHost
	{
		event Action AppClosing;

		bool Closed { get; }

		void RunApplication(IServiceLocator serviceLocator);
		void RegisterServices(IServiceRegistrator serviceRegistrator);
		void DeregisterServices(IServiceDeregistrator serviceDeregistrator);

		void TerminateApplication(CloseReason closeReason);

		void LogException(Exception exception);
		void LogInfo(string message);
	}
}
