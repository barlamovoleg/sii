﻿// ReSharper disable once CheckNamespace
namespace Common
{
	public enum VersionType
	{
		PreAlpha,
		Alpha,
		Beta,
		Release
	}
}
