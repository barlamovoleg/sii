﻿using System;

// ReSharper disable once CheckNamespace
namespace Common
{
	/// <summary>
	/// Помошник в закрытии приложения.
	/// Управляет закрытием <see cref="IAppHost"/> и генерирует событие <see cref="Shutdown"/>.
	/// Используется из класса наивысшего уровня в иерархии приложения.
	/// </summary>
	public class AppTerminator
	{
		public event Action Shutdown;

		private readonly IAppHost _appHost;
		private readonly IServiceDeregistrator _serviceDeregistrator;

		public AppTerminator(IAppHost appHost, IServiceDeregistrator serviceDeregistrator)
		{
			_appHost = appHost;
			_serviceDeregistrator = serviceDeregistrator;
			_appHost.AppClosing += AppHostOnAppClosing;

			_appHost.LogInfo(CommonStrings.TerminateAppMessages.TerminatorCreated);
		}

		public void AppOnSessionEnding()
		{
			if (!_appHost.Closed)
			{
				_appHost.LogInfo(CommonStrings.TerminateAppMessages.TryingCloseAppSessionEnd);
				_appHost.TerminateApplication(CloseReason.System);
				_appHost.LogInfo(CommonStrings.ServicesMessages.TryingDeregisteringServices);
				_appHost.DeregisterServices(_serviceDeregistrator);
			}
		}

		public void AppOnExit()
		{
			if (!_appHost.Closed)
			{
				_appHost.LogInfo(CommonStrings.TerminateAppMessages.TryingCloseAppEnd);
				_appHost.TerminateApplication(CloseReason.Unknown);
				_appHost.LogInfo(CommonStrings.ServicesMessages.TryingDeregisteringServices);
				_appHost.DeregisterServices(_serviceDeregistrator);
			}
		}

		private void AppHostOnAppClosing()
		{
			_appHost.LogInfo(CommonStrings.TerminateAppMessages.HostRequestClose);
			_appHost.LogInfo(CommonStrings.ServicesMessages.TryingDeregisteringServices);
			_appHost.DeregisterServices(_serviceDeregistrator);
			RaiseShutdown();
		}

		private void RaiseShutdown()
		{
			var handler = Shutdown;
			if (handler != null)
				handler();
		}

	}
}
