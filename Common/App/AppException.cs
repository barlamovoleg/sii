﻿using System;
using System.Runtime.Serialization;

// ReSharper disable once CheckNamespace
namespace Common
{
	public class AppException : Exception
	{
		/// <summary>
		/// Флаг указывающий на то что ошибка была критической,
		/// и приложение будет закрыто.
		/// </summary>
		public bool IsFatalException { get; set; }

		private const string ExceptionKey = "Exception";
		private readonly string _key;

		public AppException(string message, string key)
			: base(message)
		{
			_key = key;
		}


		public AppException(string message, Exception innerException, string key)
			: base(message, innerException)
		{
			_key = key;
		}


		protected AppException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{ }

		public virtual string PreMessage
		{
			get { return string.Empty; }
		}

		public sealed override string Message
		{
			get { return string.Format(" {0} [{1}] : {2} : {3}", GetExceptionTypeInfo(), PreMessage, base.Message, _key); }
		}

		private string GetExceptionTypeInfo()
		{
			var type = GetType().Name;
			// ReSharper disable once StringIndexOfIsCultureSpecific.1
			var index = type.IndexOf(ExceptionKey);
			if (index < 0)
				return type;

			var keyLenght = ExceptionKey.Length;
			return type.Remove(index, keyLenght);
		}
	}
}
