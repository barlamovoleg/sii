﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Common
{
	/// <summary>
	/// Базовый <see cref="IServiceLocator"/>.
	/// Он имеет стандартный функционал без извращений.
	/// </summary>
	public class CommonServiceController : IServiceLocator, IServiceRegistrator, IServiceDeregistrator
	{
		private readonly Dictionary<Guid, IService> _registeredServices;

		public CommonServiceController()
		{
			_registeredServices = new Dictionary<Guid, IService>();
		}

		public T GetService<T>() where T : IService
		{
			var serviceType = typeof(T);
			var services = _registeredServices.Values;
			foreach (var service in services)
			{
				var type = service.GetType();
				if (serviceType.IsAssignableFrom(type))
					return (T)service;
			}
			throw new AppException(serviceType.Name + "service type not found",
				"{BE822FFE-4CF6-4D48-B201-1BC854156365}");
		}

		public T TryGetServiceOrNull<T>() where T : IService
		{
			var serviceType = typeof(T);
			var services = _registeredServices.Values;
			foreach (var service in services)
			{
				var type = service.GetType();
				if (serviceType.IsAssignableFrom(type))
					return (T)service;
			}
			return default(T);
		}

		public IService GetService(Type serviceType)
		{
			var services = _registeredServices.Values;
			foreach (var service in services)
			{
				var type = service.GetType();
				if (serviceType.IsAssignableFrom(type))
					return service;
			}
			throw new AppException(serviceType.Name + "service type not found",
				"{11C6D603-1801-446E-9752-B5FD26EA8F41}");
		}

		public IService TryGetServiceOrNull(Type serviceType)
		{
			var services = _registeredServices.Values;
			// ReSharper disable once LoopCanBeConvertedToQuery
			foreach (var service in services)
			{
				var type = service.GetType();
				if (serviceType.IsAssignableFrom(type))
					return service;
			}
			return null;
		}

		public bool ServiceExist<T>() where T : IService
		{
			var serviceType = typeof(T);
			var services = _registeredServices.Values;
			return services.Select(service => service.GetType()).Any(serviceType.IsAssignableFrom);
		}

		public void RegisterService<T>(T service) where T : IService
		{
			var type = typeof(T);
			if (_registeredServices.ContainsKey(service.Key))
				throw new AppException("service " + type.Name + " already registered",
				"{869BE60B-A715-49F4-A16D-8306646E60C2}");

			_registeredServices.Add(service.Key, service);
		}

		public bool TryRegisterService<T>(T service) where T : IService
		{
			if (_registeredServices.ContainsKey(service.Key))
				return false;

			_registeredServices.Add(service.Key, service);
			return true;
		}

		public void DeregisterService<T>(bool dispose = false) where T : IService
		{
			IService serviceForDelete = null;
			var serviceType = typeof(T);
			var services = _registeredServices.Values;
			// ReSharper disable once LoopCanBeConvertedToQuery
			foreach (var service in services)
			{
				var type = service.GetType();
				if (serviceType.IsAssignableFrom(type))
				{
					serviceForDelete = service;
					break;
				}
			}
			if (serviceForDelete == null)
				throw new AppException(serviceType.Name + "service type not found for deregistering ",
				"{F586BCD9-9BC7-45E3-9BB4-98BA59B74030}");

			_registeredServices.Remove(serviceForDelete.Key);
			if (dispose)
				serviceForDelete.Dispose();
		}

		public void DeregisterService<T>(T service, bool dispose = false) where T : IService
		{
			IService serviceForDelete = null;
			var services = _registeredServices.Values;
			// ReSharper disable once LoopCanBeConvertedToQuery
			foreach (var s in services)
			{
				if (Equals(s.Key, service.Key))
				{
					serviceForDelete = s;
					break;
				}
			}
			if (serviceForDelete == null)
				throw new AppException(service.GetType().Name + "service type not found for deregistering ",
				"{893B3A19-F1E3-4498-A7F6-CADFAD5C55E2}");

			_registeredServices.Remove(serviceForDelete.Key);
			if (dispose)
				serviceForDelete.Dispose();
		}

		public bool TryDeregisterService<T>(bool dispose = false) where T : IService
		{
			IService serviceForDelete = null;
			var serviceType = typeof(T);
			var services = _registeredServices.Values;
			// ReSharper disable once LoopCanBeConvertedToQuery
			foreach (var service in services)
			{
				var type = service.GetType();
				if (serviceType.IsAssignableFrom(type))
				{
					serviceForDelete = service;
					break;
				}
			}
			if (serviceForDelete == null)
				return false;

			_registeredServices.Remove(serviceForDelete.Key);
			if (dispose)
				serviceForDelete.Dispose();

			return true;
		}

		public bool TryDeregisterService<T>(T service, bool dispose = false) where T : IService
		{
			IService serviceForDelete = null;
			var services = _registeredServices.Values;
			// ReSharper disable once LoopCanBeConvertedToQuery
			foreach (var s in services)
			{
				if (Equals(s.Key, service.Key))
				{
					serviceForDelete = s;
					break;
				}
			}
			if (serviceForDelete == null)
				return false;

			_registeredServices.Remove(serviceForDelete.Key);
			if (dispose)
				serviceForDelete.Dispose();

			return true;
		}
	}
}
