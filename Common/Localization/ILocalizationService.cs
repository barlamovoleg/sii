﻿namespace Common.Localization
{
	public interface ILocalizationService : IService
	{
		ILocalization CreateLocalization(ILocalizationParams localizationParams);
	}
}
