﻿using System.Globalization;

namespace Common.Localization
{
	public interface ILocalization
	{
		string Name { get; }
		Language Language { get; }
		string Translate(string key);
		CultureInfo CultureInfo { get; }
	}
}
