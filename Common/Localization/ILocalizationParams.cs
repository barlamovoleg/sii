﻿namespace Common.Localization
{
	public interface ILocalizationParams
	{
		string LocalizationName { get; }
		Language Language { get; }

		string LocalizationPath { get; }
		string LocalizationKey { get; }

		string GetUntranslatedString(string key);
	}
}
