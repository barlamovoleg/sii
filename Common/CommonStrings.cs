﻿namespace Common
{
	/// <summary>
	/// Строки относящиейся к разным частям ЛЮБОГО приложения.
	/// </summary>
	public static class CommonStrings
	{
		/// <summary>
		/// Сообщения исключений, общие ошибки для логирования.
		/// Вспомогательные строки для вывода и логирования исключений и ошибок общего характера.
		/// </summary>
		public static class ExceptionsAndErrors
		{
			/// <summary>
			/// Сообщения исключений связанных с загрузкой\выгрузкой сервисов.
			/// </summary>
			public static class ServicesExceptionMessages
			{
				/// <summary>
				/// Attempt to reload services
				/// </summary>
				public static string ReloadServices = "Attempt to reload services";
				/// <summary>
				/// Error while registering app services
				/// </summary>
				public static string RegisteringServices = "Error while registering app services";
				/// <summary>
				/// Error while deregistering app services
				/// </summary>
				public static string DeregisteringServices = "Error while deregistering app services";
				/// <summary>
				/// "Runable service has not been loaded"
				/// </summary>
				public static string RunnableServiceHasNotBeenLoaded = "Runable service has not been loaded";
			}

			/// <summary>
			/// Сообщения исключений связанных с построением и инициализацией приложения.
			/// </summary>
			public static class BuildingAppExceptionMessages
			{
				/// <summary>
				/// Error while pre-initializing app
				/// </summary>
				public static string PreInitializingException = "Error while pre-initializing app";
				/// <summary>
				/// Error while building or running app
				/// </summary>
				public static string BuildingOrRunning = "Error while building or running app";
				/// <summary>
				/// Trying to run the app before build it
				/// </summary>
				public static string RunAppBeforeBuild = "Trying to run the app before build it";
			}

			/// <summary>
			/// Сообщения ошибок во время загрузки\выгрузки сервисов.
			/// </summary>
			public static class ServicesErrors
			{
				/// <summary>
				/// "Сервис {0} не удалось получить"
				/// </summary>
				public static string ServiceNotLoaded = "Сервис {0} не удалось получить";
				/// <summary>
				/// "Ошибка при попытке заполение требований сервиса {0}: {1}"
				/// </summary>
				public static string ErrorInFillReqService = "Ошибка при попытке заполение требований сервиса {0}: {1}";
				/// <summary>
				/// Error while creating system log
				/// </summary>
				public static string CreatingSystemLog = "Error while creating system log";
			}

			/// <summary>
			/// Критическая ошибка, приложение будет закрыто
			/// </summary>
			public static string CriticalError = "Критическая ошибка, приложение будет закрыто";
			/// <summary>
			/// Fatal Exception
			/// </summary>
			public static string FatalErrorTitle = "Fatal Exception";
			/// <summary>
			/// System Message: 
			/// </summary>
			public static string SystemMessage = "System Message: ";
			/// <summary>
			///The Application will be closed.
			/// </summary>
			public static string ApplicationWillBeClosed = "The Application will be closed.";
		}

		/// <summary>
		/// Сообщения связанные с завершением работы приложения.
		/// </summary>
		public static class TerminateAppMessages
		{
			/// <summary>
			/// Терминатор приложения создан.
			/// </summary>
			public static string TerminatorCreated = "Терминатор приложения создан.";
			/// <summary>
			/// Попытка закрытия хоста приложения по причине закрытия сессии windows.
			/// </summary>
			public static string TryingCloseAppSessionEnd = "Попытка закрытия хоста приложения по причине закрытия сессии windows.";
			/// <summary>
			/// Попытка закрытия хоста приложения по причине закрытия клиента приложения.
			/// </summary>
			public static string TryingCloseAppEnd = "Попытка закрытия хоста приложения по причине закрытия клиента приложения.";
			/// <summary>
			/// Хост приложения потребовал закрытия.
			/// </summary>
			public static string HostRequestClose = "Хост приложения потребовал закрытия.";
		}

		/// <summary>
		/// Сообщения связанные с загрузкой\выгрузкой сервисов
		/// </summary>
		public static class ServicesMessages
		{
			/// <summary>
			/// Попытка дерегистрации сервисов.
			/// </summary>
			public static string TryingDeregisteringServices = "Попытка дерегистрации сервисов.";
			/// <summary>
			/// Сервисы успешно загружены
			/// </summary>
			public static string ServicesLoaded = "Сервисы успешно загружены";
			/// <summary>
			/// Сервисы успешно выгружены
			/// </summary>
			public static string ServicesUnloaded = "Сервисы успешно выгружены";
			/// <summary>
			/// Попытка регистрации сервисов
			/// </summary>
			public static string TryingRegisterServices = "Попытка регистрации сервисов";
			/// <summary>
			/// Попытка получения сервиса {0}
			/// </summary>
			public static string TryLoadingService = "Попытка получения сервиса {0}";
			/// <summary>
			/// Сервис {0} успешно получен
			/// </summary>
			public static string ServiceLoaded = "Сервис {0} успешно получен";
			/// <summary>
			/// Попытка получения требований сервиса {0}.
			/// </summary>
			public static string TryLoadReqService = "Попытка получения требований сервиса {0}.";
		}

		/// <summary>
		/// Сообщения связанные с построением или запуском приложения
		/// </summary>
		public static class BuildingOrRunningAppMessages
		{
			/// <summary>
			/// Попытка запуска хоста приложения
			/// </summary>
			public static string TryingHostRun = "Попытка запуска хоста приложения";
			/// <summary>
			/// Приложение успешно запущено.
			/// </summary>
			public static string AppRunned = "Приложение успешно запущено.";
			/// <summary>
			/// Попытка сборки приложения.
			/// </summary>
			public static string TryBuildingApp = "Попытка сборки приложения.";
			/// <summary>
			/// Попытка запуска приложения.
			/// </summary>
			public static string TryAppRun = "Попытка запуска приложения.";
			/// <summary>
			/// Приложение успешно собрано.
			/// </summary>
			public static string AppBuilded = "Приложение успешно собрано.";
		}

		/// <summary>
		/// Имя системного логера: SystemLog
		/// </summary>
		public static string SystemLogName = "SystemLog";
		/// <summary>
		/// ERROR: 
		/// </summary>
		public static string Error = "ERROR: ";
		/// <summary>
		/// EXCEPTION: 
		/// </summary>
		public static string Exception = "EXCEPTION: ";
		/// <summary>
		/// Имя логера заглушки
		/// </summary>
		public static string DummyLoggerName = "#DummyLogger";

	}
}
