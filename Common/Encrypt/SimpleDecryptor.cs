﻿using System;
using Common.Converters;

namespace Common.Encrypt
{
	public class SimpleDecryptor
	{
		private readonly byte[] _key;
		private readonly byte[] _vector;
		private readonly int _keyLenght;
		private readonly int _vectorLenght;

		private readonly IConverter<string, byte[]> _converterToByte;
		private readonly IConverter<byte[], string> _converterToString;

		public SimpleDecryptor(byte[] key, byte[] vector, int keyLenght, int vectorLenght)
		{
			if (key == null)
				throw new ArgumentNullException("key");
			if (vector == null)
				throw new ArgumentNullException("vector");
			if (keyLenght < 1)
				throw new ArgumentException("keyLenght");
			if (vectorLenght < 1)
				throw new ArgumentException("vectorLenght");

			_key = key;
			_vector = vector;
			_keyLenght = keyLenght;
			_vectorLenght = vectorLenght;

			_converterToByte = new StringByteConverter();
			_converterToString = new StringByteConverter();
		}

		public SimpleDecryptor(string key, string vector)
		{
			if (string.IsNullOrEmpty(key))
				throw new ArgumentException("key");
			if (string.IsNullOrEmpty(vector))
				throw new ArgumentException("vector");

			_converterToByte = new StringByteConverter();
			_converterToString = new StringByteConverter();

			_key = _converterToByte.Convert(key);
			_vector = _converterToByte.Convert(vector);
			_keyLenght = key.Length;
			_vectorLenght = vector.Length;
		}

		public string DecodeString(string text)
		{
			byte[] bytes = _converterToByte.Convert(text);

			for (int i = 0; i < bytes.Length; i++)
				bytes[i] = (byte)((bytes[i] - _key[i % _keyLenght] * _vector[i % _vectorLenght]) % (byte.MaxValue + 1));

			return _converterToString.Convert(bytes);
		}

		public byte[] DecodeBytes(byte[] bytes)
		{
			byte[] resbytes = bytes;

			for (int i = 0; i < bytes.Length; i++)
				bytes[i] = (byte)((bytes[i] - _key[i % _keyLenght] * _vector[i % _vectorLenght]) % (byte.MaxValue + 1));

			return resbytes;
		}
	}
}
