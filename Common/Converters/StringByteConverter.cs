﻿using System;

namespace Common.Converters
{
	public class StringByteConverter : IConverter<byte[],string>, IConverter<string,byte[]>
	{
		public string Convert(byte[] value)
		{
			var chars = new char[value.Length / sizeof(char)];
			Buffer.BlockCopy(value, 0, chars, 0, value.Length);
			return new string(chars);
		}

		public byte[] Convert(string value)
		{
			var bytes = new byte[value.Length * sizeof(char)];
			Buffer.BlockCopy(value.ToCharArray(), 0, bytes, 0, bytes.Length);
			return bytes;
		}
	}
}
