﻿using System;

namespace Common.Algorithms
{
	public static class PerlinNoise2D
	{
		static float Noise2D(float x, float y)
		{
			int n = (int)x + (int)y * 57;
			n = (n << 13) ^ n;
			return (1.0f - ((n * (n * n * 15731 + 789221) + 1376312589) & 0x7fffffff) /
					1073741824.0f);
		}

		static float SmoothedNoise2D(float x, float y)
		{
			float corners = (Noise2D(x - 1, y - 1) + Noise2D(x + 1, y - 1) +
				 Noise2D(x - 1, y + 1) + Noise2D(x + 1, y + 1)) / 16;
			float sides = (Noise2D(x - 1, y) + Noise2D(x + 1, y) +
				 Noise2D(x, y - 1) + Noise2D(x, y + 1)) / 8;
			float center = Noise2D(x, y) / 4;
			return corners + sides + center;
		}

		static float CosInterpolate(float v1, float v2, float a)
		{
			var angle = (float)(a * Math.PI);
			float prc = (float)(1.0f - Math.Cos(angle)) * 0.5f;
			return v1 * (1.0f - prc) + v2 * prc;
		}

		private static float CompileNoise(float x, float y)
		{
			float intX = (int)(x);
			float fractionalX = x - intX;
			float intY = (int)(y);
			float fractionalY = y - intY;
			float v1 = SmoothedNoise2D(intX, intY);
			float v2 = SmoothedNoise2D(intX + 1, intY);
			float v3 = SmoothedNoise2D(intX, intY + 1);
			float v4 = SmoothedNoise2D(intX + 1, intY + 1);
			float i1 = CosInterpolate(v1, v2, fractionalX);
			float i2 = CosInterpolate(v3, v4, fractionalX);
			return CosInterpolate(i1, i2, fractionalY);
		}

		private static float PerlinNoise_2D(float x, float y, float factor, int octaveCount, float frequency, float amplitude, float persistence = 0.65f)
		{
			float total = 0;

			x += (factor);
			y += (factor);

			for (int i = 0; i < octaveCount; i++)
			{
				total += CompileNoise(x * frequency, y * frequency) * amplitude;
				amplitude *= persistence;
				frequency *= 2;
			}

			var result = total + 0.5f;
			if (result < 0)
				result = 0;
			return result;
		}

		public static float[,] GetArray(float seed, int size, float minValue, float maxValue, int octavesCount, float frequency, float amplitude, float persistence = 0.65f)
		{
			var result = new float[size, size];

			for (int i = 0; i < size; i++)
				for (int j = 0; j < size; j++)
				{
					var value = PerlinNoise_2D(i, j, seed, octavesCount, frequency, amplitude, persistence);
					var d = maxValue - minValue;
					result[i, j] = value * d - Math.Abs(minValue);
				}

			return result;
		}
	}
}
