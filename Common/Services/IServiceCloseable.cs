﻿using System;

// ReSharper disable once CheckNamespace
namespace Common
{
	/// <summary>
	/// Сервис имеющий логику закрытия.
	/// </summary>
	public interface IServiceCloseable : IService
	{
		/// <summary>
		/// Сервис закрылся.
		/// </summary>
		event Action<CloseReason> Closed;
		/// <summary>
		/// Сервис желает закрыться.
		/// </summary>
		event Action<AppClosingEventArgs> Closing;

		/// <summary>
		/// Внешняя подсистема запрашивает закрытия сервиса по причине <param name="closingEventArgs"/>
		/// </summary>
		void OnCloseRequest(AppClosingEventArgs closingEventArgs);
		/// <summary>
		/// Требование закрытия сервиса.
		/// </summary>
		void Close(CloseReason closeReason);
	}
}
