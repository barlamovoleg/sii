﻿// ReSharper disable once CheckNamespace
namespace Common
{
	public interface IServiceRunable : IService
	{
		void Run();
	}
}
