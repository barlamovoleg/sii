﻿// ReSharper disable once CheckNamespace
namespace Common
{
	public interface IGraphicsHostService<out T> : IService where T : class
	{
		T GraphicHost { get; }
		void Display();
	}
}
