﻿// ReSharper disable once CheckNamespace
namespace Common
{
	public interface IServiceDeregistrator
	{
		void DeregisterService<T>(bool dispose = false) where T : IService;
		void DeregisterService<T>(T service, bool dispose = false) where T : IService;
		bool TryDeregisterService<T>(bool dispose = false) where T : IService;
		bool TryDeregisterService<T>(T service, bool dispose = false) where T : IService;
	}
}
