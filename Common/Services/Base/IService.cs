﻿using System;

// ReSharper disable once CheckNamespace
namespace Common
{
	public interface IService : IDisposable
	{
		Guid Key { get; }
	}
}
