﻿// ReSharper disable once CheckNamespace
namespace Common
{
	public interface IServiceRegistrator
	{
		void RegisterService<T>(T service) where T : IService;
		bool TryRegisterService<T>(T service) where T : IService;
	}
}
