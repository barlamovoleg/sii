﻿using System;

// ReSharper disable once CheckNamespace
namespace Common
{
	public interface IServiceLocator
	{
		T GetService<T>() where T : IService;
		T TryGetServiceOrNull<T>() where T : IService;
		IService GetService(Type serviceType);
		IService TryGetServiceOrNull(Type serviceType);
		bool ServiceExist<T>() where T : IService;
	}
}
