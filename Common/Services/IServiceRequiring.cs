﻿using System;
using System.Collections.Generic;

// ReSharper disable once CheckNamespace
namespace Common
{
	/// <summary>
	/// Сервис которые требует для своей работы другие сервисы.
	/// <see cref="IAppBuilder"/> будет обязан удовлетворить все требования.
	/// </summary>
	public interface IServiceRequiring : IService
	{
		/// <summary>
		/// Требующиеся типы сервисов.
		/// </summary>
		Type[] RequiredServices { get; }
		/// <summary>
		/// Passes the required services.
		/// </summary>
		void PassRequiredServices(IService[] requirements);
	}
}
