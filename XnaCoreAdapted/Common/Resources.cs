﻿using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

// ReSharper disable ConvertToConstant.Local
namespace XnaCoreAdapted.Common
{
	[CompilerGenerated]
	[GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "2.0.0.0")]
	[DebuggerNonUserCode]
	internal sealed class Resources
	{
		private static ResourceManager _resourceMan;
		private static CultureInfo _resourceCulture;

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		internal static ResourceManager ResourceManager
		{
			get
			{
				if (ReferenceEquals(_resourceMan, null))
					_resourceMan = new ResourceManager("Microsoft.Xna.Framework.Resources", typeof(Resources).Assembly);
				return _resourceMan;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		internal static CultureInfo Culture
		{
			get
			{
				return _resourceCulture;
			}
			set
			{
				_resourceCulture = value;
			}
		}

		internal static string BackBufferDimMustBePositive
		{
			get
			{
				var s = "BackBufferDimMustBePositive";
				return ResourceManager.GetString(s, _resourceCulture);
			}
		}

		internal static string CannotAddSameComponentMultipleTimes
		{
			get
			{
				var s = "CannotAddSameComponentMultipleTimes";
				return ResourceManager.GetString(s, _resourceCulture);
			}
		}

		internal static string CannotSetItemsIntoGameComponentCollection
		{
			get
			{
				var s = "CannotSetItemsIntoGameComponentCollection";
				return ResourceManager.GetString(s, _resourceCulture);
			}
		}

		internal static string DefaultTitleName
		{
			get
			{
				var s = "DefaultTitleName";
				return ResourceManager.GetString(s, _resourceCulture);
			}
		}

		internal static string Direct3DCreateError
		{
			get
			{
				var s = "Direct3DCreateError";
				return ResourceManager.GetString(s, _resourceCulture);
			}
		}

		internal static string Direct3DInvalidCreateParameters
		{
			get
			{
				var s = "Direct3DInvalidCreateParameters";
				return ResourceManager.GetString(s, _resourceCulture);
			}
		}

		internal static string GameCannotBeNull
		{
			get
			{
				var s = "GameCannotBeNull";
				return ResourceManager.GetString(s, _resourceCulture);
			}
		}

		internal static string GameNotDerivedFromValidGameType
		{
			get
			{
				var s = "GameNotDerivedFromValidGameType";
				return ResourceManager.GetString(s, _resourceCulture);
			}
		}

		internal static string CannotCreateGameType
		{
			get
			{
				var s = "CannotCreateGameType";
				return ResourceManager.GetString(s, _resourceCulture);
			}
		}

		internal static string RunNotSupported
		{
			get
			{
				var s = "RunNotSupported";
				return ResourceManager.GetString(s, _resourceCulture);
			}
		}

		internal static string GraphicsComponentNotAttachedToGame
		{
			get
			{
				var s = "GraphicsComponentNotAttachedToGame";
				return ResourceManager.GetString(s, _resourceCulture);
			}
		}

		internal static string GraphicsDeviceManagerAlreadyPresent
		{
			get
			{
				var s = "GraphicsDeviceManagerAlreadyPresent";
				return ResourceManager.GetString(s, _resourceCulture);
			}
		}

		internal static string InactiveSleepTimeCannotBeZero
		{
			get
			{
				var s = "InactiveSleepTimeCannotBeZero";
				return ResourceManager.GetString(s, _resourceCulture);
			}
		}

		internal static string InvalidScreenAdapter
		{
			get
			{
				var s = "InvalidScreenAdapter";
				return ResourceManager.GetString(s, _resourceCulture);
			}
		}

		internal static string InvalidScreenDeviceName
		{
			get
			{
				var s = "InvalidScreenDeviceName";
				return ResourceManager.GetString(s, _resourceCulture);
			}
		}

		internal static string MissingGraphicsDeviceService
		{
			get
			{
				var s = "MissingGraphicsDeviceService";
				return ResourceManager.GetString(s, _resourceCulture);
			}
		}

		internal static string MustCallBeginDeviceChange
		{
			get
			{
				var s = "MustCallBeginDeviceChange";
				return ResourceManager.GetString(s, _resourceCulture);
			}
		}

		internal static string NoAudioHardware
		{
			get
			{
				var s = "NoAudioHardware";
				return ResourceManager.GetString(s, _resourceCulture);
			}
		}

		internal static string NoCompatibleDevices
		{
			get
			{
				var s = "NoCompatibleDevices";
				return ResourceManager.GetString(s, _resourceCulture);
			}
		}

		internal static string NoCompatibleDevicesAfterRanking
		{
			get
			{
				var s = "NoCompatibleDevicesAfterRanking";
				return ResourceManager.GetString(s, _resourceCulture);
			}
		}

		internal static string NoGraphicsDeviceService
		{
			get
			{
				var s = "NoGraphicsDeviceService";
				return ResourceManager.GetString(s, _resourceCulture);
			}
		}

		internal static string NoHighResolutionTimer
		{
			get
			{
				var s = "NoHighResolutionTimer";
				return ResourceManager.GetString(s, _resourceCulture);
			}
		}

		internal static string NoMultipleRuns
		{
			get
			{
				var s = "NoMultipleRuns";
				return ResourceManager.GetString(s, _resourceCulture);
			}
		}

		internal static string NoNullUseDefaultAdapter
		{
			get
			{
				var s = "NoNullUseDefaultAdapter";
				return ResourceManager.GetString(s, _resourceCulture);
			}
		}

		internal static string NoSuitableGraphicsDevice
		{
			get
			{
				var s = "NoSuitableGraphicsDevice";
				return ResourceManager.GetString(s, _resourceCulture);
			}
		}

		internal static string NullOrEmptyScreenDeviceName
		{
			get
			{
				var s = "NullOrEmptyScreenDeviceName";
				return ResourceManager.GetString(s, _resourceCulture);
			}
		}

		internal static string PreviousDrawThrew
		{
			get
			{
				var s = "PreviousDrawThrew";
				return ResourceManager.GetString(s, _resourceCulture);
			}
		}

		internal static string PropertyCannotBeCalledBeforeInitialize
		{
			get
			{
				var s = "PropertyCannotBeCalledBeforeInitialize";
				return ResourceManager.GetString(s, _resourceCulture);
			}
		}

		internal static string ServiceAlreadyPresent
		{
			get
			{
				var s = "ServiceAlreadyPresent";
				return ResourceManager.GetString(s, _resourceCulture);
			}
		}

		internal static string ServiceMustBeAssignable
		{
			get
			{
				var s = "ServiceMustBeAssignable";
				return ResourceManager.GetString(s, _resourceCulture);
			}
		}

		internal static string ServiceProviderCannotBeNull
		{
			get
			{
				var s = "ServiceProviderCannotBeNull";
				return ResourceManager.GetString(s, _resourceCulture);
			}
		}

		internal static string ServiceTypeCannotBeNull
		{
			get
			{
				var s = "ServiceTypeCannotBeNull";
				return ResourceManager.GetString(s, _resourceCulture);
			}
		}

		internal static string TargetElaspedCannotBeZero
		{
			get
			{
				var s = "TargetElaspedCannotBeZero";
				return ResourceManager.GetString(s, _resourceCulture);
			}
		}

		internal static string TitleCannotBeNull
		{
			get
			{
				var s = "TitleCannotBeNull";
				return ResourceManager.GetString(s, _resourceCulture);
			}
		}

		internal static string ValidateBackBufferDimsFullScreen
		{
			get
			{
				var s = "ValidateBackBufferDimsFullScreen";
				return ResourceManager.GetString(s, _resourceCulture);
			}
		}

		internal static string ValidateBackBufferDimsModeFullScreen
		{
			get
			{
				var s = "ValidateBackBufferDimsModeFullScreen";
				return ResourceManager.GetString(s, _resourceCulture);
			}
		}
	}
}
