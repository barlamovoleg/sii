﻿using Microsoft.Xna.Framework;

namespace XnaCoreAdapted.Common
{
	internal static class Helpers
	{
		internal static DisplayOrientation ChooseOrientation(DisplayOrientation orientation, int width, int height, bool allowLandscapeLeftAndRight)
		{
			if ((orientation & (DisplayOrientation.LandscapeLeft | DisplayOrientation.LandscapeRight | DisplayOrientation.Portrait)) !=
				DisplayOrientation.Default)
				return orientation;
			if (width <= height)
				return DisplayOrientation.Portrait;
			return allowLandscapeLeftAndRight ? DisplayOrientation.LandscapeLeft | DisplayOrientation.LandscapeRight : DisplayOrientation.LandscapeLeft;
		}
	}
}
