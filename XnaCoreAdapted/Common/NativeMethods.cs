﻿using System;
using System.Runtime.InteropServices;
using System.Security;

// ReSharper disable InconsistentNaming
namespace XnaCoreAdapted.Common
{
	internal static class NativeMethods
	{
		[SuppressUnmanagedCodeSecurity]
		[DllImport("kernel32")]
		[return: MarshalAs(UnmanagedType.Bool)]
		internal static extern bool QueryPerformanceFrequency(ref long performanceFrequency);

		[SuppressUnmanagedCodeSecurity]
		[DllImport("kernel32")]
		[return: MarshalAs(UnmanagedType.Bool)]
		internal static extern bool QueryPerformanceCounter(ref long performanceCount);

		[SuppressUnmanagedCodeSecurity]
		[DllImport("user32.dll", CharSet = CharSet.Auto)]
		[return: MarshalAs(UnmanagedType.Bool)]
		internal static extern bool PeekMessage(out Message msg, IntPtr hWnd, uint messageFilterMin, uint messageFilterMax, uint flags);

		[SuppressUnmanagedCodeSecurity]
		[DllImport("user32.dll")]
		[return: MarshalAs(UnmanagedType.Bool)]
		internal static extern bool GetWindowRect(IntPtr hWnd, out RECT rect);

		[SuppressUnmanagedCodeSecurity]
		[DllImport("user32.dll")]
		[return: MarshalAs(UnmanagedType.Bool)]
		internal static extern bool GetClientRect(IntPtr hWnd, out RECT rect);

		[SuppressUnmanagedCodeSecurity]
		[DllImport("user32.dll")]
		[return: MarshalAs(UnmanagedType.Bool)]
		internal static extern bool ClientToScreen(IntPtr hWnd, out POINT point);

		[SuppressUnmanagedCodeSecurity]
		[DllImport("user32.dll")]
		internal static extern IntPtr MonitorFromWindow(IntPtr hWnd, uint flags);

		[DllImport("kernel32.dll")]
		internal static extern IntPtr GetCurrentThread();

		[DllImport("kernel32.dll")]
		internal static extern IntPtr GetCurrentProcess();

		[DllImport("kernel32.dll")]
		internal static extern UIntPtr SetThreadAffinityMask(IntPtr hThread, UIntPtr dwThreadAffinityMask);

		[DllImport("kernel32.dll", SetLastError = true)]
		internal static extern bool GetProcessAffinityMask(IntPtr hProcess, out UIntPtr lpProcessAffinityMask, out UIntPtr lpSystemAffinityMask);

		internal enum WindowMessage : uint
		{
			Destroy = 2U,
			Size = 5U,
			Paint = 15U,
			Close = 16U,
			Quit = 18U,
			ActivateApplication = 28U,
			SetCursor = 32U,
			GetMinMax = 36U,
			NonClientHitTest = 132U,
			KeyDown = 256U,
			KeyUp = 257U,
			Character = 258U,
			SystemKeyDown = 260U,
			SystemKeyUp = 261U,
			SystemCharacter = 262U,
			SystemCommand = 274U,
			MouseMove = 512U,
			LeftButtonDown = 513U,
			MouseFirst = 513U,
			LeftButtonUp = 514U,
			LeftButtonDoubleClick = 515U,
			RightButtonDown = 516U,
			RightButtonUp = 517U,
			RightButtonDoubleClick = 518U,
			MiddleButtonDown = 519U,
			MiddleButtonUp = 520U,
			MiddleButtonDoubleClick = 521U,
			MouseWheel = 522U,
			XButtonDown = 523U,
			XButtonUp = 524U,
			MouseLast = 525U,
			XButtonDoubleClick = 525U,
			EnterMenuLoop = 529U,
			ExitMenuLoop = 530U,
			PowerBroadcast = 536U,
			EnterSizeMove = 561U,
			ExitSizeMove = 562U,
		}

		public enum MouseButtons
		{
			Left = 1,
			Right = 2,
			Middle = 16,
			Side1 = 32,
			Side2 = 64,
		}

		public struct Message
		{
			public IntPtr hWnd;
			public WindowMessage msg;
			public IntPtr wParam;
			public IntPtr lParam;
			public uint time;
			public System.Drawing.Point p;
		}

		public struct MinMaxInformation
		{
			public System.Drawing.Point reserved;
			public System.Drawing.Point MaxSize;
			public System.Drawing.Point MaxPosition;
			public System.Drawing.Point MinTrackSize;
			public System.Drawing.Point MaxTrackSize;
		}

		public struct MonitorInformation
		{
			public uint Size;
			public System.Drawing.Rectangle MonitorRectangle;
			public System.Drawing.Rectangle WorkRectangle;
			public uint Flags;
		}

		public struct RECT
		{
			public int Left;
			public int Top;
			public int Right;
			public int Bottom;
		}

		public struct POINT
		{
			public int X;
			public int Y;
		}
	}
}
