﻿using System;
using System.Collections.Generic;
using System.Threading;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using XnaCoreAdapted.Common;
using XnaCoreAdapted.Comparers;
using XnaCoreAdapted.Core;
using XnaCoreAdapted.Integration;

// ReSharper disable CSharpWarnings::CS1584
namespace XnaCoreAdapted
{
	public class Game : IDisposable
	{
		/// <summary>
		///     Raised when the game gains focus.
		/// </summary>
		/// <param name="" />
		public event EventHandler<EventArgs> Activated;

		/// <summary>
		///     Raised when the game loses focus.
		/// </summary>
		/// <param name="" />
		public event EventHandler<EventArgs> Deactivated;

		/// <summary>
		///     Raised when the game is exiting.
		/// </summary>
		/// <param name="" />
		public event EventHandler<EventArgs> Exiting;

		/// <summary>
		///     Raised when the game is being disposed.
		/// </summary>
		/// <param name="" />
		public event EventHandler<EventArgs> Disposed;

		/// <summary>
		///     Gets the start up parameters in LaunchParameters.
		/// </summary>
		public LaunchParameters LaunchParameters
		{
			get { return _launchParameters; }
		}

		/// <summary>
		///     Gets the collection of GameComponents owned by the game.
		/// </summary>
		public GameComponentCollection Components
		{
			get { return _gameComponents; }
		}

		/// <summary>
		///     Gets the GameServiceContainer holding all the service providers attached to the Game.
		/// </summary>
		public GameServiceContainer Services
		{
			get { return _gameServices; }
		}

		/// <summary>
		///     Gets or sets the time to sleep when the game is inactive.
		/// </summary>
		public TimeSpan InactiveSleepTime
		{
			get { return _inactiveSleepTime; }
			set
			{
				if (value < TimeSpan.Zero)
					throw new ArgumentOutOfRangeException("value", Resources.InactiveSleepTimeCannotBeZero);
				_inactiveSleepTime = value;
			}
		}

		/// <summary>
		///     Gets or sets a value indicating whether the mouse cursor should be visible.
		/// </summary>
		public bool IsMouseVisible
		{
			get { return _isMouseVisible; }
			set
			{
				_isMouseVisible = value;
				if (Window == null)
					return;
				Window.IsMouseVisible = value;
			}
		}

		/// <summary>
		///     Gets or sets the target time between calls to Update when IsFixedTimeStep is true. Reference page contains links to
		///     related code samples.
		/// </summary>
		public TimeSpan TargetElapsedTime
		{
			get { return _targetElapsedTime; }
			set
			{
				if (value <= TimeSpan.Zero)
					throw new ArgumentOutOfRangeException("value", Resources.TargetElaspedCannotBeZero);
				_targetElapsedTime = value;
			}
		}

		/// <summary>
		///     Gets or sets a value indicating whether to use fixed time steps.
		/// </summary>
		public bool IsFixedTimeStep
		{
			get { return _isFixedTimeStep; }
			set { _isFixedTimeStep = value; }
		}

		/// <summary>
		///     Gets the underlying operating system window.
		/// </summary>
		public GameWindow Window
		{
			get
			{
				if (_host != null)
					return _host.Window;
				return null;
			}
		}

		/// <summary>
		///     Indicates whether the game is currently the active application.
		/// </summary>
		public bool IsActive
		{
			get
			{
				bool flag = false;
				if (GamerServicesDispatcher.IsInitialized)
					flag = Guide.IsVisible;
				if (_isActive)
					return !flag;
				return false;
			}
		}

		/// <summary>
		///     Gets the current GraphicsDevice.
		/// </summary>
		public GraphicsDevice GraphicsDevice
		{
			get
			{
				IGraphicsDeviceService graphicsDeviceService = _graphicsDeviceService;
				if (graphicsDeviceService == null)
				{
					graphicsDeviceService = Services.GetService(typeof(IGraphicsDeviceService)) as IGraphicsDeviceService;
					if (graphicsDeviceService == null)
						throw new InvalidOperationException(Resources.NoGraphicsDeviceService);
				}
				return graphicsDeviceService.GraphicsDevice;
			}
		}

		/// <summary>
		///     Gets or sets the current ContentManager.
		/// </summary>
		public ContentManager Content
		{
			get { return _content; }
			set
			{
				if (value == null)
					throw new ArgumentNullException();
				_content = value;
			}
		}

		/// <summary>
		///     Immediately releases the unmanaged resources used by this object.
		/// </summary>
		public void Dispose()
		{
			if (Window != null)
			{
				Window.Close();
				Window.Dispose();
			}
			if (_host != null)
				_host.Dispose();

			_xnaGameContainer.Dispose();

			Dispose(true);
			GC.SuppressFinalize(this);
		}

		internal bool IsActiveIgnoringGuide
		{
			get { return _isActive; }
		}

		internal IXnaGameContainer XnaGameContainer { get { return _xnaGameContainer; } }
		private readonly IXnaGameContainer _xnaGameContainer;

		private bool ShouldExit
		{
			get { return _exitRequested; }
		}

		private readonly GameClock _clock;
		private readonly List<IDrawable> _currentlyDrawingComponents = new List<IDrawable>();
		private readonly List<IUpdateable> _currentlyUpdatingComponents = new List<IUpdateable>();
		private readonly List<IDrawable> _drawableComponents = new List<IDrawable>();
		private readonly GameComponentCollection _gameComponents;
		private readonly GameServiceContainer _gameServices = new GameServiceContainer();
		private readonly GameTime _gameTime = new GameTime();
		private readonly TimeSpan _maximumElapsedTime = TimeSpan.FromMilliseconds(500.0);
		private readonly LaunchParameters _launchParameters;
		private readonly List<IGameComponent> _notYetInitialized = new List<IGameComponent>();
		private readonly List<IUpdateable> _updateableComponents = new List<IUpdateable>();
		private bool _isFixedTimeStep = true;
		private TimeSpan _accumulatedElapsedGameTime;
		private ContentManager _content;
		private bool _doneFirstDraw;
		private bool _doneFirstUpdate;
		private bool _drawRunningSlowly;
		private bool _endRunRequired;
		private bool _exitRequested;
		private bool _forceElapsedTimeToZero;
		private IGraphicsDeviceManager _graphicsDeviceManager;
		private IGraphicsDeviceService _graphicsDeviceService;
		private GameHost _host;
		private bool _inRun;
		private TimeSpan _inactiveSleepTime;
		private bool _isActive;
		private bool _isMouseVisible;
		private TimeSpan _lastFrameElapsedGameTime;
		private bool _suppressDraw;
		private TimeSpan _targetElapsedTime;
		private TimeSpan _totalGameTime;
		private int _updatesSinceRunningSlowly1 = int.MaxValue;
		private int _updatesSinceRunningSlowly2 = int.MaxValue;

		/// <summary>
		///     Initializes a new instance of this class, which provides basic graphics device initialization, game logic,
		///     rendering code, and a game loop.  Reference page contains code sample.
		/// </summary>
		public Game(IXnaGameContainer xnaGameContainer)
		{
			_xnaGameContainer = xnaGameContainer;
			FrameworkDispatcher.Update();
			EnsureHost();
			_launchParameters = new LaunchParameters();
			_gameComponents = new GameComponentCollection();
			_gameComponents.ComponentAdded += GameComponentAdded;
			_gameComponents.ComponentRemoved += GameComponentRemoved;
			_content = new ContentManager(_gameServices);
			_host.Window.Paint += Paint;
			_clock = new GameClock();
			_totalGameTime = TimeSpan.Zero;
			_accumulatedElapsedGameTime = TimeSpan.Zero;
			_lastFrameElapsedGameTime = TimeSpan.Zero;
			_targetElapsedTime = TimeSpan.FromTicks(166667L);
			_inactiveSleepTime = TimeSpan.FromMilliseconds(20.0);
		}

		/// <summary>
		///     Allows a Game to attempt to free resources and perform other cleanup operations before garbage collection reclaims
		///     the Game.
		/// </summary>
		~Game()
		{
			Dispose(false);
		}

		/// <summary>
		///     Call this method to initialize the game, begin running the game loop, and start processing events for the game.
		/// </summary>
		public void Run()
		{
			RunGame(true);
		}

		/// <summary>
		///     Resets the elapsed time counter.
		/// </summary>
		public void ResetElapsedTime()
		{
			_forceElapsedTimeToZero = true;
			_drawRunningSlowly = false;
			_updatesSinceRunningSlowly1 = int.MaxValue;
			_updatesSinceRunningSlowly2 = int.MaxValue;
		}

		/// <summary>
		///     Run the game through what would happen in a single tick of the game clock; this method is designed for debugging
		///     only.
		/// </summary>
		public void RunOneFrame()
		{
			if (_host == null)
				return;
			_host.RunOneFrame();
		}

		/// <summary>
		///     Updates the game's clock and calls Update and Draw.
		/// </summary>
		public void Tick()
		{
			if (ShouldExit)
				return;
			if (!_isActive)
				Thread.Sleep((int)_inactiveSleepTime.TotalMilliseconds);
			_clock.Step();
			bool flag = true;
			TimeSpan timeSpan1 = _clock.ElapsedAdjustedTime;
			if (timeSpan1 < TimeSpan.Zero)
				timeSpan1 = TimeSpan.Zero;
			if (_forceElapsedTimeToZero)
			{
				timeSpan1 = TimeSpan.Zero;
				_forceElapsedTimeToZero = false;
			}
			if (timeSpan1 > _maximumElapsedTime)
				timeSpan1 = _maximumElapsedTime;
			if (_isFixedTimeStep)
			{
				if (Math.Abs(timeSpan1.Ticks - _targetElapsedTime.Ticks) < _targetElapsedTime.Ticks >> 6)
					timeSpan1 = _targetElapsedTime;
				_accumulatedElapsedGameTime += timeSpan1;
				long num = _accumulatedElapsedGameTime.Ticks / _targetElapsedTime.Ticks;
				_accumulatedElapsedGameTime = TimeSpan.FromTicks(_accumulatedElapsedGameTime.Ticks % _targetElapsedTime.Ticks);
				_lastFrameElapsedGameTime = TimeSpan.Zero;
				if (num == 0L)
					return;
				TimeSpan timeSpan2 = _targetElapsedTime;
				if (num > 1L)
				{
					_updatesSinceRunningSlowly2 = _updatesSinceRunningSlowly1;
					_updatesSinceRunningSlowly1 = 0;
				}
				else
				{
					if (_updatesSinceRunningSlowly1 < int.MaxValue)
						++_updatesSinceRunningSlowly1;
					if (_updatesSinceRunningSlowly2 < int.MaxValue)
						++_updatesSinceRunningSlowly2;
				}
				_drawRunningSlowly = _updatesSinceRunningSlowly2 < 20;
				while (num > 0L && !ShouldExit)
				{
					--num;
					try
					{
						_gameTime.ElapsedGameTime = timeSpan2;
						_gameTime.TotalGameTime = _totalGameTime;
						_gameTime.IsRunningSlowly = _drawRunningSlowly;
						Update(_gameTime);
						flag = flag & _suppressDraw;
						_suppressDraw = false;
					}
					finally
					{
						_lastFrameElapsedGameTime += timeSpan2;
						_totalGameTime += timeSpan2;
					}
				}
			}
			else
			{
				TimeSpan timeSpan2 = timeSpan1;
				_drawRunningSlowly = false;
				_updatesSinceRunningSlowly1 = int.MaxValue;
				_updatesSinceRunningSlowly2 = int.MaxValue;
				if (!ShouldExit)
				{
					try
					{
						_gameTime.ElapsedGameTime = _lastFrameElapsedGameTime = timeSpan2;
						_gameTime.TotalGameTime = _totalGameTime;
						_gameTime.IsRunningSlowly = false;
						Update(_gameTime);
						// ReSharper disable once ConditionIsAlwaysTrueOrFalse
						flag = flag & _suppressDraw;
						_suppressDraw = false;
					}
					finally
					{
						_totalGameTime += timeSpan2;
					}
				}
			}
			if (flag)
				return;
			DrawFrame();
		}

		/// <summary>
		///     Prevents calls to Draw until the next Update.
		/// </summary>
		public void SuppressDraw()
		{
			_suppressDraw = true;
		}

		/// <summary>
		///     Exits the game.
		/// </summary>
		public void Exit()
		{
			_exitRequested = true;
			_host.Exit();
			if (!_inRun || !_endRunRequired)
				return;
			EndRun();
			_inRun = false;
		}

		internal void StartGameLoop()
		{
			RunGame(false);
		}

		/// <summary>
		///     Called after all components are initialized but before the first update in the game loop.
		/// </summary>
		protected virtual void BeginRun()
		{
		}

		/// <summary>
		///     Called after the game loop has stopped running before exiting.
		/// </summary>
		protected virtual void EndRun()
		{
		}

		/// <summary>
		///     Reference page contains links to related conceptual articles.
		/// </summary>
		/// <param name="gameTime">Time passed since the last call to Update.</param>
		protected virtual void Update(GameTime gameTime)
		{
			// ReSharper disable once ForCanBeConvertedToForeach
			for (int index = 0; index < _updateableComponents.Count; ++index)
				_currentlyUpdatingComponents.Add(_updateableComponents[index]);
			// ReSharper disable once ForCanBeConvertedToForeach
			for (int index = 0; index < _currentlyUpdatingComponents.Count; ++index)
			{
				IUpdateable updateable = _currentlyUpdatingComponents[index];
				if (updateable.Enabled)
					updateable.Update(gameTime);
			}
			_currentlyUpdatingComponents.Clear();
			FrameworkDispatcher.Update();
			_doneFirstUpdate = true;
		}

		/// <summary>
		///     Starts the drawing of a frame. This method is followed by calls to Draw and EndDraw.
		/// </summary>
		protected virtual bool BeginDraw()
		{
			return _graphicsDeviceManager == null || _graphicsDeviceManager.BeginDraw();
		}

		/// <summary>
		///     Reference page contains code sample.
		/// </summary>
		/// <param name="gameTime">Time passed since the last call to Draw.</param>
		protected virtual void Draw(GameTime gameTime)
		{
			// ReSharper disable once ForCanBeConvertedToForeach
			for (int index = 0; index < _drawableComponents.Count; ++index)
				_currentlyDrawingComponents.Add(_drawableComponents[index]);
			// ReSharper disable once ForCanBeConvertedToForeach
			for (int index = 0; index < _currentlyDrawingComponents.Count; ++index)
			{
				IDrawable drawable = _currentlyDrawingComponents[index];
				if (drawable.Visible)
					drawable.Draw(gameTime);
			}
			_currentlyDrawingComponents.Clear();
		}

		/// <summary>
		///     Ends the drawing of a frame. This method is preceeded by calls to Draw and BeginDraw.
		/// </summary>
		protected virtual void EndDraw()
		{
			if (_graphicsDeviceManager == null)
				return;
			_graphicsDeviceManager.EndDraw();
		}

		/// <summary>
		///     Called after the Game and GraphicsDevice are created, but before LoadContent.  Reference page contains code sample.
		/// </summary>
		protected virtual void Initialize()
		{
			HookDeviceEvents();
			while (_notYetInitialized.Count != 0)
			{
				_notYetInitialized[0].Initialize();
				_notYetInitialized.RemoveAt(0);
			}
			if (_graphicsDeviceService == null || _graphicsDeviceService.GraphicsDevice == null)
				return;
			LoadContent();
		}

		/// <summary>
		///     Raises the Activated event. Override this method to add code to handle when the game gains focus.
		/// </summary>
		/// <param name="sender">The Game.</param>
		/// <param name="args">Arguments for the Activated event.</param>
		protected virtual void OnActivated(object sender, EventArgs args)
		{
			if (Activated == null)
				return;

			Activated(this, args);
		}

		/// <summary>
		///     Raises the Deactivated event. Override this method to add code to handle when the game loses focus.
		/// </summary>
		/// <param name="sender">The Game.</param>
		/// <param name="args">Arguments for the Deactivated event.</param>
		protected virtual void OnDeactivated(object sender, EventArgs args)
		{
			if (Deactivated == null)
				return;

			Deactivated(this, args);
		}

		/// <summary>
		///     Raises an Exiting event. Override this method to add code to handle when the game is exiting.
		/// </summary>
		/// <param name="sender">The Game.</param>
		/// <param name="args">Arguments for the Exiting event.</param>
		protected virtual void OnExiting(object sender, EventArgs args)
		{
			if (Exiting == null)
				return;
			Exiting(null, args);
		}

		/// <summary />
		protected virtual void LoadContent()
		{
		}

		/// <summary>
		///     Called when graphics resources need to be unloaded. Override this method to unload any game-specific graphics
		///     resources.
		/// </summary>
		protected virtual void UnloadContent()
		{
		}

		/// <summary>
		///     Releases all resources used by the Game class.
		/// </summary>
		/// <param name="disposing">
		///     true to release both managed and unmanaged resources; false to release only unmanaged
		///     resources.
		/// </param>
		protected virtual void Dispose(bool disposing)
		{
			if (!disposing)
				return;
			lock (this)
			{
				// ReSharper disable InconsistentNaming
				var local_0 = new IGameComponent[_gameComponents.Count];
				_gameComponents.CopyTo(local_0, 0);
				// ReSharper disable once ForCanBeConvertedToForeach
				for (int local_1 = 0; local_1 < local_0.Length; ++local_1)
				{
					var local_2 = local_0[local_1] as IDisposable;
					if (local_2 != null)
						local_2.Dispose();
				}
				var local_3 = _graphicsDeviceManager as IDisposable;
				if (local_3 != null)
					local_3.Dispose();
				UnhookDeviceEvents();
				if (Disposed == null)
					return;
				Disposed(this, EventArgs.Empty);
			}
		}

		/// <summary>
		///     This is used to display an error message if there is no suitable graphics device or sound card.
		/// </summary>
		/// <param name="exception">The exception to display.</param>
		protected virtual bool ShowMissingRequirementMessage(Exception exception)
		{
			if (_host != null)
				return _host.ShowMissingRequirementMessage(exception);
			return false;
		}

		private void Paint(object sender, EventArgs e)
		{
			if (!_doneFirstDraw)
				return;
			DrawFrame();
		}

		private void RunGame(bool useBlockingRun)
		{
			try
			{
				_graphicsDeviceManager = Services.GetService(typeof(IGraphicsDeviceManager)) as IGraphicsDeviceManager;
				if (_graphicsDeviceManager != null)
					_graphicsDeviceManager.CreateDevice();
				Initialize();
				_inRun = true;
				BeginRun();
				_gameTime.ElapsedGameTime = TimeSpan.Zero;
				_gameTime.TotalGameTime = _totalGameTime;
				_gameTime.IsRunningSlowly = false;
				Update(_gameTime);
				_doneFirstUpdate = true;
				if (useBlockingRun)
				{
					if (_host != null)
						_host.Run();
					EndRun();
				}
				else
				{
					if (_host != null)
						_host.StartGameLoop();
					_endRunRequired = true;
				}
			}
			catch (NoSuitableGraphicsDeviceException ex)
			{
				if (ShowMissingRequirementMessage(ex))
					return;
				throw;
			}
			catch (NoAudioHardwareException ex)
			{
				if (ShowMissingRequirementMessage(ex))
					return;
				throw;
			}
			finally
			{
				if (!_endRunRequired)
					_inRun = false;
			}
		}

		private void DrawFrame()
		{
			try
			{
				if (ShouldExit || !_doneFirstUpdate || (Window.IsMinimized || !BeginDraw()))
					return;
				_gameTime.TotalGameTime = _totalGameTime;
				_gameTime.ElapsedGameTime = _lastFrameElapsedGameTime;
				_gameTime.IsRunningSlowly = _drawRunningSlowly;
				Draw(_gameTime);
				EndDraw();
				_doneFirstDraw = true;
			}
			finally
			{
				_lastFrameElapsedGameTime = TimeSpan.Zero;
			}
		}

		private void GameComponentRemoved(object sender, GameComponentCollectionEventArgs e)
		{
			if (!_inRun)
				_notYetInitialized.Remove(e.GameComponent);
			var updateable = e.GameComponent as IUpdateable;
			if (updateable != null)
			{
				_updateableComponents.Remove(updateable);
				updateable.UpdateOrderChanged -= UpdateableUpdateOrderChanged;
			}
			var drawable = e.GameComponent as IDrawable;
			if (drawable == null)
				return;
			_drawableComponents.Remove(drawable);
			drawable.DrawOrderChanged -= DrawableDrawOrderChanged;
		}

		private void GameComponentAdded(object sender, GameComponentCollectionEventArgs e)
		{
			if (_inRun)
				e.GameComponent.Initialize();
			else
				_notYetInitialized.Add(e.GameComponent);
			var updateable = e.GameComponent as IUpdateable;
			if (updateable != null)
			{
				int num = _updateableComponents.BinarySearch(updateable, UpdateOrderComparer.Default);
				if (num < 0)
				{
					int index = ~num;
					while (index < _updateableComponents.Count && _updateableComponents[index].UpdateOrder == updateable.UpdateOrder)
						++index;
					_updateableComponents.Insert(index, updateable);
					updateable.UpdateOrderChanged += UpdateableUpdateOrderChanged;
				}
			}
			var drawable = e.GameComponent as IDrawable;
			if (drawable == null)
				return;
			int num1 = _drawableComponents.BinarySearch(drawable, DrawOrderComparer.Default);
			if (num1 >= 0)
				return;
			int index1 = ~num1;
			while (index1 < _drawableComponents.Count && _drawableComponents[index1].DrawOrder == drawable.DrawOrder)
				++index1;
			_drawableComponents.Insert(index1, drawable);
			drawable.DrawOrderChanged += DrawableDrawOrderChanged;
		}

		private void DrawableDrawOrderChanged(object sender, EventArgs e)
		{
			var drawable = sender as IDrawable;
			_drawableComponents.Remove(drawable);
			// ReSharper disable once AssignNullToNotNullAttribute
			int num = _drawableComponents.BinarySearch(drawable, DrawOrderComparer.Default);
			if (num >= 0)
				return;
			int index = ~num;
			// ReSharper disable once PossibleNullReferenceException
			while (index < _drawableComponents.Count && _drawableComponents[index].DrawOrder == drawable.DrawOrder)
				++index;
			_drawableComponents.Insert(index, drawable);
		}

		private void UpdateableUpdateOrderChanged(object sender, EventArgs e)
		{
			var updateable = sender as IUpdateable;
			_updateableComponents.Remove(updateable);
			// ReSharper disable once AssignNullToNotNullAttribute
			int num = _updateableComponents.BinarySearch(updateable, UpdateOrderComparer.Default);
			if (num >= 0)
				return;
			int index = ~num;
			// ReSharper disable once PossibleNullReferenceException
			while (index < _updateableComponents.Count && _updateableComponents[index].UpdateOrder == updateable.UpdateOrder)
				++index;
			_updateableComponents.Insert(index, updateable);
		}

		private void EnsureHost()
		{
			if (_host != null)
				return;
			_host = new WindowsGameHost(this);
			_host.Activated += HostActivated;
			_host.Deactivated += HostDeactivated;
			_host.Suspend += HostSuspend;
			_host.Resume += HostResume;
			_host.Idle += HostIdle;
			_host.Exiting += HostExiting;
		}

		private void HostSuspend(object sender, EventArgs e)
		{
			_clock.Suspend();
		}

		private void HostResume(object sender, EventArgs e)
		{
			_clock.Resume();
		}

		private void HostExiting(object sender, EventArgs e)
		{
			OnExiting(this, EventArgs.Empty);
		}

		private void HostIdle(object sender, EventArgs e)
		{
			Tick();
		}

		private void HostDeactivated(object sender, EventArgs e)
		{
			if (!_isActive)
				return;
			_isActive = false;
			OnDeactivated(this, EventArgs.Empty);
		}

		private void HostActivated(object sender, EventArgs e)
		{
			if (_isActive)
				return;
			_isActive = true;
			OnActivated(this, EventArgs.Empty);
		}

		private void HookDeviceEvents()
		{
			_graphicsDeviceService = Services.GetService(typeof(IGraphicsDeviceService)) as IGraphicsDeviceService;
			if (_graphicsDeviceService == null)
				return;
			_graphicsDeviceService.DeviceCreated += DeviceCreated;
			_graphicsDeviceService.DeviceResetting += DeviceResetting;
			_graphicsDeviceService.DeviceReset += DeviceReset;
			_graphicsDeviceService.DeviceDisposing += DeviceDisposing;
		}

		private void UnhookDeviceEvents()
		{
			if (_graphicsDeviceService == null)
				return;
			_graphicsDeviceService.DeviceCreated -= DeviceCreated;
			_graphicsDeviceService.DeviceResetting -= DeviceResetting;
			_graphicsDeviceService.DeviceReset -= DeviceReset;
			_graphicsDeviceService.DeviceDisposing -= DeviceDisposing;
		}

		private void DeviceResetting(object sender, EventArgs e)
		{
		}

		private void DeviceReset(object sender, EventArgs e)
		{
		}

		private void DeviceCreated(object sender, EventArgs e)
		{
			LoadContent();
		}

		private void DeviceDisposing(object sender, EventArgs e)
		{
			_content.Unload();
			UnloadContent();
		}
	}
}