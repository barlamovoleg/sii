﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Rectangle = Microsoft.Xna.Framework.Rectangle;

namespace XnaCoreAdapted.Integration
{
	public interface IXnaGameContainer : IDisposable
	{
		event EventHandler ApplicationIdle;

		event EventHandler Suspend;
		event EventHandler Resume;
		event EventHandler ApplicationActivated;
		event EventHandler ApplicationDeactivated;
		event EventHandler UserResized;
		event EventHandler ScreenChanged;
		event CancelEventHandler Closing;
		event PaintEventHandler Paint;

		bool AllowUserResizing { get; set; }

		IntPtr Handle { get; }
		Rectangle ClientBounds { get; }
		Screen DeviceScreen { get; }
		bool IsMinimized { get; }

		string Text { set; }
		bool IsMouseVisible {set; }
		Icon WindowIcon { set; }

		void Run();
		void Close();
		void BeginScreenDeviceChange(bool willBeFullScreen);
		void EndScreenDeviceChange(string screenDeviceName, int clientWidth, int clientHeight);
	}
}
