﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Microsoft.Xna.Framework.GamerServices;
using XnaCoreAdapted.Common;
using XnaCoreAdapted.Core;

// ReSharper disable DoNotCallOverridableMethodsInConstructor
namespace XnaCoreAdapted.Integration
{
	public abstract class FormXnaContainerAdapter : Control, IXnaGameContainer
	{
		public event EventHandler Suspend;
		public event EventHandler Resume;
		public event EventHandler ScreenChanged;
		public event EventHandler UserResized;
		public event EventHandler ApplicationActivated;
		public event EventHandler ApplicationDeactivated;

		protected abstract event EventHandler ApplicationIdle;
		protected abstract event CancelEventHandler Closing;
		protected abstract event EventHandler ResizeBegin;
		protected abstract event EventHandler ResizeEnd;
		protected abstract event EventHandler Activated;
		protected abstract event EventHandler Deactivated;

		protected abstract Icon WindowIcon { set; }
		protected abstract FormWindowState WindowState { get; set; }
		protected abstract bool MaximizeBox { get; set; }
		protected abstract FormBorderStyle FormBorderStyle { get; set; }
		protected abstract bool TopMost { get; set; }
		protected abstract Rectangle RestoreBounds { get; }

		event EventHandler IXnaGameContainer.ApplicationIdle
		{
			add { ApplicationIdle += value; }
			remove { ApplicationIdle -= value; }
		}

		event CancelEventHandler IXnaGameContainer.Closing
		{
			add { Closing += value; }
			remove { Closing -= value; }
		}

		Icon IXnaGameContainer.WindowIcon
		{
			set { WindowIcon = value; }
		}

		bool IXnaGameContainer.AllowUserResizing
		{
			get { return _allowUserResizing; }
			set
			{
				if (_allowUserResizing == value)
					return;
				_allowUserResizing = value;
				UpdateBorderStyle();
			}
		}

		bool IXnaGameContainer.IsMouseVisible
		{
			set
			{
				if (_isMouseVisible == value)
					return;
				_isMouseVisible = value;
				if (_isMouseVisible)
				{
					if (!_hidMouse)
						return;
					Cursor.Show();
					_hidMouse = false;
				}
				else
				{
					if (_hidMouse)
						return;
					Cursor.Hide();
					_hidMouse = true;
				}
			}
		}

		Screen IXnaGameContainer.DeviceScreen
		{
			get { return _screen; }
		}

		Microsoft.Xna.Framework.Rectangle IXnaGameContainer.ClientBounds
		{
			get
			{
				Point point = PointToScreen(Point.Empty);
				return new Microsoft.Xna.Framework.Rectangle(point.X, point.Y, ClientSize.Width, ClientSize.Height);
			}
		}

		bool IXnaGameContainer.IsMinimized
		{
			get
			{
				if (ClientSize.Width != 0)
					return ClientSize.Height == 0;
				return true;
			}
		}

		private bool _allowUserResizing;
		private bool _centerScreen;
		private bool _deviceChangeChangedVisible;
		private bool? _deviceChangeWillBeFullScreen;
		private bool _firstPaint = true;
		private bool _freezeOurEvents;
		private bool _hidMouse;
		private bool _isFullScreenMaximized;
		private bool _isMouseVisible;
		private Size _oldClientSize;
		private bool _oldVisible;
		private FormWindowState _resizeWindowState;
		private Rectangle _savedBounds;
		private FormBorderStyle _savedFormBorderStyle;
		private Rectangle _savedRestoreBounds;
		private FormWindowState _savedWindowState;
		private Screen _screen;
		private Size _startResizeSize = Size.Empty;
		private bool _userResizing;

		protected FormXnaContainerAdapter()
		{
			SuspendLayout();
			//this.AutoScaleDimensions = new SizeF(6f, 13f);
			//this.AutoScaleMode = AutoScaleMode.Font;
			CausesValidation = false;
			ClientSize = new Size(292, 266);
			Name = "GameForm";
			Text = "GameForm";
			ResizeBegin += Form_ResizeBegin;
			ClientSizeChanged += Form_ClientSizeChanged;
			Resize += Form_Resize;
			LocationChanged += Form_LocationChanged;
			ResizeEnd += Form_ResizeEnd;
			MouseEnter += Form_MouseEnter;
			MouseLeave += Form_MouseLeave;
			Activated += OnActivated;
			Deactivated += OnDeactivated;
			ResumeLayout(false);
		}

		public abstract void Close();
		protected abstract void Run();

		protected void Initialize()
		{
			try
			{
				_freezeOurEvents = true;
				_resizeWindowState = WindowState;
				_screen = WindowsGameWindow.ScreenFromHandle(Handle);
				SetStyle(ControlStyles.Opaque | ControlStyles.AllPaintingInWmPaint, false);
				ClientSize = new Size(GameWindow.DefaultClientWidth, GameWindow.DefaultClientHeight);
				UpdateBorderStyle();
			}
			finally
			{
				_freezeOurEvents = false;
			}
		}

		protected override void Dispose(bool disposing)
		{
			Suspend = null;
			Resume = null;
			ScreenChanged = null;
			UserResized = null;
			ApplicationActivated = null;
			ApplicationDeactivated = null;

			base.Dispose(disposing);
		}

		protected override void OnPaintBackground(PaintEventArgs e)
		{
			if (!_firstPaint)
				return;
			base.OnPaintBackground(e);
			_firstPaint = false;
		}

		protected override bool ProcessDialogKey(Keys keyData)
		{
			Keys keys = keyData & Keys.KeyCode;
			if ((keyData & Keys.Alt) == Keys.Alt && (keys == Keys.F4 || keys == Keys.None))
				return base.ProcessDialogKey(keyData);
			if (GamerServicesDispatcher.IsInitialized && (keys == Keys.Home || Guide.IsVisible))
				return base.ProcessDialogKey(keyData);
			return true;
		}

		void IXnaGameContainer.BeginScreenDeviceChange(bool willBeFullScreen)
		{
			_oldClientSize = ClientSize;
			if (willBeFullScreen && !_isFullScreenMaximized)
			{
				_savedFormBorderStyle = FormBorderStyle;
				_savedWindowState = WindowState;
				_savedBounds = Bounds;
				if (WindowState == FormWindowState.Maximized)
					_savedRestoreBounds = RestoreBounds;
			}
			if (willBeFullScreen != _isFullScreenMaximized)
			{
				_deviceChangeChangedVisible = true;
				_oldVisible = Visible;
				Visible = false;
			}
			else
				_deviceChangeChangedVisible = false;
			if (!willBeFullScreen && _isFullScreenMaximized)
			{
				TopMost = false;
				FormBorderStyle = _savedFormBorderStyle;
				if (_savedWindowState == FormWindowState.Maximized)
					SetBoundsCore(_screen.Bounds.X, _screen.Bounds.Y, _savedRestoreBounds.Width, _savedRestoreBounds.Height, BoundsSpecified.Size);
				else
					SetBoundsCore(_screen.Bounds.X, _screen.Bounds.Y, _savedBounds.Width, _savedBounds.Height, BoundsSpecified.Size);
			}
			if (willBeFullScreen != _isFullScreenMaximized)
				SendToBack();
			_deviceChangeWillBeFullScreen = willBeFullScreen;
		}

		void IXnaGameContainer.Run()
		{
			Run();
		}

		void IXnaGameContainer.EndScreenDeviceChange(string screenDeviceName, int clientWidth, int clientHeight)
		{
			if (!_deviceChangeWillBeFullScreen.HasValue)
				throw new InvalidOperationException(Resources.MustCallBeginDeviceChange);
			bool flag = false;
			if (_deviceChangeWillBeFullScreen.Value)
			{
				Screen screen = WindowsGameWindow.ScreenFromDeviceName(screenDeviceName);
				Rectangle bounds = Screen.GetBounds(new Point(screen.Bounds.X, screen.Bounds.Y));
				if (!_isFullScreenMaximized)
				{
					flag = true;
					TopMost = true;
					FormBorderStyle = FormBorderStyle.None;
					WindowState = FormWindowState.Normal;
					BringToFront();
				}
				Location = new Point(bounds.X, bounds.Y);
				ClientSize = new Size(bounds.Width, bounds.Height);
				_isFullScreenMaximized = true;
			}
			else
			{
				if (_isFullScreenMaximized)
				{
					flag = true;
					BringToFront();
				}
				ResizeWindow(screenDeviceName, clientWidth, clientHeight, _centerScreen);
			}
			if (_deviceChangeChangedVisible)
				Visible = _oldVisible;
			if (flag && _oldClientSize != ClientSize)
				OnUserResized(true);
			_deviceChangeWillBeFullScreen = new bool?();
		}

		private void OnDeactivated(object sender, EventArgs eventArgs)
		{
			OnActivateApp(false);
		}

		private void OnActivated(object sender, EventArgs eventArgs)
		{
			OnActivateApp(true);
		}

		private void Form_ResizeBegin(object sender, EventArgs e)
		{
			_startResizeSize = ClientSize;
			_userResizing = true;
			OnSuspend();
		}

		private void Form_Resize(object sender, EventArgs e)
		{
			if (_resizeWindowState != WindowState)
			{
				_resizeWindowState = WindowState;
				_firstPaint = true;
				OnUserResized(false);
				Invalidate();
			}
			if (!_userResizing || !(ClientSize != _startResizeSize))
				return;
			Invalidate();
		}

		private void Form_ResizeEnd(object sender, EventArgs e)
		{
			_userResizing = false;
			if (ClientSize != _startResizeSize)
			{
				_centerScreen = false;
				OnUserResized(false);
			}
			_firstPaint = true;
			OnResume();
		}

		private void Form_ClientSizeChanged(object sender, EventArgs e)
		{
			UpdateScreen();
		}

		private void Form_LocationChanged(object sender, EventArgs e)
		{
			if (_userResizing)
				_centerScreen = false;
			UpdateScreen();
		}

		private void Form_MouseEnter(object sender, EventArgs e)
		{
			if (_isMouseVisible || _hidMouse)
				return;
			Cursor.Hide();
			_hidMouse = true;
		}

		private void Form_MouseLeave(object sender, EventArgs e)
		{
			if (!_hidMouse)
				return;
			Cursor.Show();
			_hidMouse = false;
		}

		private void ResizeWindow(string screenDeviceName, int clientWidth, int clientHeight, bool center)
		{
			Screen screen = WindowsGameWindow.ScreenFromDeviceName(screenDeviceName);
			Rectangle bounds = Screen.GetBounds(new Point(screen.Bounds.X, screen.Bounds.Y));
			int x1;
			int y1;
			if (screenDeviceName != WindowsGameWindow.DeviceNameFromScreen(screen))
			{
				x1 = bounds.X;
				y1 = bounds.Y;
			}
			else
			{
				x1 = _screen.Bounds.X;
				y1 = _screen.Bounds.Y;
			}
			if (_isFullScreenMaximized)
			{
				Size size = SizeFromClientSize(new Size(clientWidth, clientHeight));
				if (_savedWindowState == FormWindowState.Maximized)
					SetBoundsCore(_savedRestoreBounds.X - _screen.Bounds.X + x1, _savedRestoreBounds.Y - _screen.Bounds.Y + y1, _savedRestoreBounds.Width,
						_savedRestoreBounds.Height, BoundsSpecified.All);
				else if (center)
					SetBoundsCore(x1 + bounds.Width / 2 - size.Width / 2, y1 + bounds.Height / 2 - size.Height / 2, size.Width, size.Height, BoundsSpecified.All);
				else
					SetBoundsCore(_savedBounds.X - _screen.Bounds.X + x1, _savedBounds.Y - _screen.Bounds.Y + y1, size.Width, size.Height, BoundsSpecified.All);
				WindowState = _savedWindowState;
				_isFullScreenMaximized = false;
			}
			else
			{
				if (WindowState != FormWindowState.Normal)
					return;
				int x2;
				int y2;
				if (center)
				{
					Size size = SizeFromClientSize(new Size(clientWidth, clientHeight));
					x2 = x1 + bounds.Width / 2 - size.Width / 2;
					y2 = y1 + bounds.Height / 2 - size.Height / 2;
				}
				else
				{
					x2 = x1 + Bounds.X - _screen.Bounds.X;
					y2 = y1 + Bounds.Y - _screen.Bounds.Y;
				}
				if (x2 != Location.X || y2 != Location.Y)
					Location = new Point(x2, y2);
				if (ClientSize.Width == clientWidth && ClientSize.Height == clientHeight)
					return;
				ClientSize = new Size(clientWidth, clientHeight);
			}
		}

		private void UpdateBorderStyle()
		{
			if (!_allowUserResizing)
			{
				MaximizeBox = false;
				if (_isFullScreenMaximized)
					return;
				FormBorderStyle = FormBorderStyle.FixedSingle;
			}
			else
			{
				MaximizeBox = true;
				if (_isFullScreenMaximized)
					return;
				FormBorderStyle = FormBorderStyle.Sizable;
			}
		}

		private void UpdateScreen()
		{
			if (_freezeOurEvents)
				return;
			Screen screen = Screen.FromHandle(Handle);
			if (_screen != null && _screen.Equals(screen))
				return;
			_screen = screen;
			if (_screen == null)
				return;
			OnScreenChanged();
		}

		private void OnSuspend()
		{
			if (Suspend == null)
				return;
			Suspend(this, EventArgs.Empty);
		}

		private void OnResume()
		{
			if (Resume == null)
				return;
			Resume(this, EventArgs.Empty);
		}

		private void OnScreenChanged()
		{
			if (ScreenChanged == null)
				return;
			ScreenChanged(this, EventArgs.Empty);
		}

		private void OnUserResized(bool forceEvent)
		{
			if (_freezeOurEvents && !forceEvent || UserResized == null)
				return;
			UserResized(this, EventArgs.Empty);
		}

		private void OnActivateApp(bool active)
		{
			if (active)
			{
				_firstPaint = true;
				_freezeOurEvents = false;
				if (_isFullScreenMaximized)
					TopMost = true;
				if (ApplicationActivated == null)
					return;
				ApplicationActivated(this, EventArgs.Empty);
			}
			else
			{
				if (ApplicationDeactivated != null)
					ApplicationDeactivated(this, EventArgs.Empty);
				_freezeOurEvents = true;
			}
		}
	}
}