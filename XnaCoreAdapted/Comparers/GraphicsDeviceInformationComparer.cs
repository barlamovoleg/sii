﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using GraphicsDeviceManager = XnaCoreAdapted.Core.GraphicsDeviceManager;

namespace XnaCoreAdapted.Comparers
{
	internal sealed class GraphicsDeviceInformationComparer : IComparer<GraphicsDeviceInformation>
	{
		private readonly GraphicsDeviceManager _graphics;

		internal GraphicsDeviceInformationComparer(GraphicsDeviceManager graphicsComponent)
		{
			_graphics = graphicsComponent;
		}

		public int Compare(GraphicsDeviceInformation d1, GraphicsDeviceInformation d2)
		{
			if (d1.GraphicsProfile != d2.GraphicsProfile)
			{
				return d1.GraphicsProfile <= d2.GraphicsProfile ? 1 : -1;
			}
			PresentationParameters presentationParameters1 = d1.PresentationParameters;
			PresentationParameters presentationParameters2 = d2.PresentationParameters;
			if (presentationParameters1.IsFullScreen != presentationParameters2.IsFullScreen)
			{
				return _graphics.IsFullScreen != presentationParameters1.IsFullScreen ? 1 : -1;
			}
			int num1 = RankFormat(presentationParameters1.BackBufferFormat);
			int num2 = RankFormat(presentationParameters2.BackBufferFormat);
			if (num1 != num2)
				return num1 >= num2 ? 1 : -1;
			if (presentationParameters1.MultiSampleCount != presentationParameters2.MultiSampleCount)
			{
				return presentationParameters1.MultiSampleCount <= presentationParameters2.MultiSampleCount ? 1 : -1;
			}
			float num3 = _graphics.PreferredBackBufferWidth == 0 || _graphics.PreferredBackBufferHeight == 0
				? GraphicsDeviceManager.DefaultBackBufferWidth/
				(float) GraphicsDeviceManager.DefaultBackBufferHeight
				: _graphics.PreferredBackBufferWidth/(float) _graphics.PreferredBackBufferHeight;
			float num4 = presentationParameters1.BackBufferWidth/(float) presentationParameters1.BackBufferHeight;
			float num5 = presentationParameters2.BackBufferWidth/(float) presentationParameters2.BackBufferHeight;
			float num6 = Math.Abs(num4 - num3);
			float num7 = Math.Abs(num5 - num3);
			if (Math.Abs(num6 - num7) > 0.200000002980232)
			{
				return (double) num6 >= (double) num7 ? 1 : -1;
			}
			int num8;
			int num9;
			if (_graphics.IsFullScreen)
			{
				if (_graphics.PreferredBackBufferWidth == 0 || _graphics.PreferredBackBufferHeight == 0)
				{
					GraphicsAdapter adapter1 = d1.Adapter;
					num8 = adapter1.CurrentDisplayMode.Width*adapter1.CurrentDisplayMode.Height;
					GraphicsAdapter adapter2 = d2.Adapter;
					num9 = adapter2.CurrentDisplayMode.Width*adapter2.CurrentDisplayMode.Height;
				}
				else
					num8 = num9 = _graphics.PreferredBackBufferWidth*_graphics.PreferredBackBufferHeight;
			}
			else
				num8 = _graphics.PreferredBackBufferWidth == 0 || _graphics.PreferredBackBufferHeight == 0
					? (num9 =
						GraphicsDeviceManager.DefaultBackBufferWidth*GraphicsDeviceManager.DefaultBackBufferHeight)
					: (num9 = _graphics.PreferredBackBufferWidth*_graphics.PreferredBackBufferHeight);
			int num10 = Math.Abs(presentationParameters1.BackBufferWidth*presentationParameters1.BackBufferHeight - num8);
			int num11 = Math.Abs(presentationParameters2.BackBufferWidth*presentationParameters2.BackBufferHeight - num9);
			if (num10 != num11)
			{
				return num10 >= num11 ? 1 : -1;
			}
			if (d1.Adapter != d2.Adapter)
			{
				if (d1.Adapter.IsDefaultAdapter)
					return -1;
				if (d2.Adapter.IsDefaultAdapter)
					return 1;
			}
			return 0;
		}

		private int RankFormat(SurfaceFormat format)
		{
			if (format == _graphics.PreferredBackBufferFormat)
				return 0;
			return SurfaceFormatBitDepth(format) ==
					SurfaceFormatBitDepth(_graphics.PreferredBackBufferFormat)
				? 1
				: int.MaxValue;
		}

		private static int SurfaceFormatBitDepth(SurfaceFormat format)
		{
			switch (format)
			{
				case SurfaceFormat.Color:
				case SurfaceFormat.Rgba1010102:
					return 32;
				case SurfaceFormat.Bgr565:
				case SurfaceFormat.Bgra5551:
				case SurfaceFormat.Bgra4444:
					return 16;
				default:
					return 0;
			}
		}
	}
}