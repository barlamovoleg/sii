﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace XnaCoreAdapted.Comparers
{
	internal sealed class DrawOrderComparer : IComparer<IDrawable>
	{
		public static readonly DrawOrderComparer Default = new DrawOrderComparer();

		static DrawOrderComparer()
		{
		}

		public int Compare(IDrawable x, IDrawable y)
		{
			if (x == null && y == null)
				return 0;
			if (x == null)
				return 1;
			if (y == null)
				return -1;
			if (x.Equals(y))
				return 0;
			return x.DrawOrder < y.DrawOrder ? -1 : 1;
		}
	}
}
