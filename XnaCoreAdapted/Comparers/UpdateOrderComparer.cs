﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace XnaCoreAdapted.Comparers
{
	internal sealed class UpdateOrderComparer : IComparer<IUpdateable>
	{
		public static readonly UpdateOrderComparer Default = new UpdateOrderComparer();

		static UpdateOrderComparer()
		{
		}

		public int Compare(IUpdateable x, IUpdateable y)
		{
			if (x == null && y == null)
				return 0;
			if (x == null)
				return 1;
			if (y == null)
				return -1;
			if (x.Equals(y))
				return 0;
			return x.UpdateOrder < y.UpdateOrder ? -1 : 1;
		}
	}
}
