﻿using System;

// ReSharper disable once CheckNamespace
namespace XnaCoreAdapted
{
	public class GameTime : Microsoft.Xna.Framework.GameTime
	{
		private TimeSpan _elapsedGameTime;
		private TimeSpan _totalGameTime;

		public GameTime()
		{
		}

		public GameTime(TimeSpan totalGameTime, TimeSpan elapsedGameTime, bool isRunningSlowly)
		{
			_totalGameTime = totalGameTime;
			_elapsedGameTime = elapsedGameTime;
			IsRunningSlowly = isRunningSlowly;
		}

		public GameTime(TimeSpan totalGameTime, TimeSpan elapsedGameTime)
			: this(totalGameTime, elapsedGameTime, false)
		{
		}

		public new bool IsRunningSlowly { get; internal set; }

		public new TimeSpan TotalGameTime
		{
			get { return _totalGameTime; }
			internal set { _totalGameTime = value; }
		}

		public new TimeSpan ElapsedGameTime
		{
			get { return _elapsedGameTime; }
			internal set { _elapsedGameTime = value; }
		}

	}
}