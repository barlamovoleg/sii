﻿using System;

namespace XnaCoreAdapted.Core
{
	internal abstract class GameHost : IDisposable
	{
		internal event EventHandler<EventArgs> Suspend;

		internal event EventHandler<EventArgs> Resume;

		internal event EventHandler<EventArgs> Activated;

		internal event EventHandler<EventArgs> Deactivated;

		internal event EventHandler<EventArgs> Idle;

		internal event EventHandler<EventArgs> Exiting;

		internal abstract GameWindow Window { get; }

		public void Dispose()
		{
			Suspend = null;
			Resume = null;
			Activated = null;
			Deactivated = null;
			Idle = null;
			Exiting = null;
		}

		internal abstract void Run();

		internal abstract void RunOneFrame();

		internal abstract void StartGameLoop();

		internal abstract void Exit();

		internal virtual bool ShowMissingRequirementMessage(Exception exception)
		{
			return false;
		}

		protected void OnSuspend()
		{
			if (Suspend == null)
				return;
			Suspend(this, EventArgs.Empty);
		}

		protected void OnResume()
		{
			if (Resume == null)
				return;
			Resume(this, EventArgs.Empty);
		}

		protected void OnActivated()
		{
			if (Activated == null)
				return;
			Activated(this, EventArgs.Empty);
		}

		protected void OnDeactivated()
		{
			if (Deactivated == null)
				return;
			Deactivated(this, EventArgs.Empty);
		}

		protected void OnIdle()
		{
			if (Idle == null)
				return;
			Idle(this, EventArgs.Empty);
		}

		protected void OnExiting()
		{
			if (Exiting == null)
				return;
			Exiting(this, EventArgs.Empty);
		}
	}
}