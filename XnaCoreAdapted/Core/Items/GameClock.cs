﻿using System;
using System.Diagnostics;

// ReSharper disable once CheckNamespace
namespace XnaCoreAdapted.Core
{
	internal sealed class GameClock
	{
		private long _baseRealTime;
		private TimeSpan _currentTimeBase;
		private TimeSpan _currentTimeOffset;
		private TimeSpan _elapsedAdjustedTime;
		private TimeSpan _elapsedTime;
		private long _lastRealTime;
		private bool _lastRealTimeValid;
		private int _suspendCount;
		private long _suspendStartTime;
		private long _timeLostToSuspension;

		public GameClock()
		{
			Reset();
		}

		internal TimeSpan CurrentTime
		{
			get { return _currentTimeBase + _currentTimeOffset; }
		}

		internal TimeSpan ElapsedTime
		{
			get { return _elapsedTime; }
		}

		internal TimeSpan ElapsedAdjustedTime
		{
			get { return _elapsedAdjustedTime; }
		}

		internal static long Counter
		{
			get { return Stopwatch.GetTimestamp(); }
		}

		internal static long Frequency
		{
			get { return Stopwatch.Frequency; }
		}

		internal void Reset()
		{
			_currentTimeBase = TimeSpan.Zero;
			_currentTimeOffset = TimeSpan.Zero;
			_baseRealTime = Counter;
			_lastRealTimeValid = false;
		}

		internal void Step()
		{
			long counter = Counter;
			if (!_lastRealTimeValid)
			{
				_lastRealTime = counter;
				_lastRealTimeValid = true;
			}
			try
			{
				_currentTimeOffset = CounterToTimeSpan(counter - _baseRealTime);
			}
			catch (OverflowException)
			{
				_currentTimeBase += _currentTimeOffset;
				_baseRealTime = _lastRealTime;
				try
				{
					_currentTimeOffset = CounterToTimeSpan(counter - _baseRealTime);
				}
				catch (OverflowException)
				{
					_baseRealTime = counter;
					_currentTimeOffset = TimeSpan.Zero;
				}
			}
			try
			{
				_elapsedTime = CounterToTimeSpan(counter - _lastRealTime);
			}
			catch (OverflowException)
			{
				_elapsedTime = TimeSpan.Zero;
			}
			try
			{
				long num = _lastRealTime + _timeLostToSuspension;
				_elapsedAdjustedTime = CounterToTimeSpan(counter - num);
				_timeLostToSuspension = 0L;
			}
			catch (OverflowException)
			{
				_elapsedAdjustedTime = TimeSpan.Zero;
			}
			_lastRealTime = counter;
		}

		internal void Suspend()
		{
			//++_suspendCount;
			//if (_suspendCount != 1)
			//    return;
			//_suspendStartTime = Counter;
		}

		internal void Resume()
		{
			//--_suspendCount;
			//if (_suspendCount > 0)
			//    return;
			//_timeLostToSuspension += Counter - _suspendStartTime;
			//_suspendStartTime = 0L;
		}

		private static TimeSpan CounterToTimeSpan(long delta)
		{
			const long num = 10000000L;
			return TimeSpan.FromTicks(checked(delta*num)/Frequency);
		}
	}
}