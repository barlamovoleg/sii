﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Threading;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using XnaCoreAdapted.Common;
using XnaCoreAdapted.Comparers;

// ReSharper disable CSharpWarnings::CS1584
// ReSharper disable DelegateSubtraction
// ReSharper disable once CheckNamespace
namespace XnaCoreAdapted.Core
{
	public class GraphicsDeviceManager : IGraphicsDeviceService, IDisposable, IGraphicsDeviceManager
	{
		/// <summary>
		///     Specifies the default minimum back-buffer width.
		/// </summary>
		public static readonly int DefaultBackBufferWidth = 800;

		/// <summary>
		///     Specifies the default minimum back-buffer height.
		/// </summary>
		public static readonly int DefaultBackBufferHeight = 480;

		private static readonly TimeSpan DeviceLostSleepTime = TimeSpan.FromMilliseconds(50.0);
		private readonly Game _game;
		private bool _allowMultiSampling;
		private SurfaceFormat _backBufferFormat;
		private int _backBufferHeight = Microsoft.Xna.Framework.GraphicsDeviceManager.DefaultBackBufferHeight;
		private int _backBufferWidth = Microsoft.Xna.Framework.GraphicsDeviceManager.DefaultBackBufferWidth;
		private bool _beginDrawOk;
		private DisplayOrientation _currentWindowOrientation;
		private DepthFormat _depthStencilFormat = DepthFormat.Depth24;
		private GraphicsDevice _device;
		private EventHandler<EventArgs> _deviceCreated;
		private EventHandler<EventArgs> _deviceDisposing;
		private EventHandler<EventArgs> _deviceReset;
		private EventHandler<EventArgs> _deviceResetting;
		private GraphicsProfile _graphicsProfile;
		private bool _inDeviceTransition;
		private bool _isDeviceDirty;
		private bool _isFullScreen;
		private bool _isReallyFullScreen;
		private int _resizedBackBufferHeight;
		private int _resizedBackBufferWidth;
		private DisplayOrientation _supportedOrientations;
		private bool _synchronizeWithVerticalRetrace = true;
		private bool _useResizedBackBuffer;

		static GraphicsDeviceManager()
		{
		}

		/// <summary>
		///     Creates a new GraphicsDeviceManager and registers it to handle the configuration and management of the graphics
		///     device for the specified Game.
		/// </summary>
		/// <param name="game">Game the GraphicsDeviceManager should be associated with.</param>
		public GraphicsDeviceManager(Game game)
		{
			if (game == null)
				throw new ArgumentNullException("game", Resources.GameCannotBeNull);
			_game = game;
			if (game.Services.GetService(typeof (IGraphicsDeviceManager)) != null)
				throw new ArgumentException(Resources.GraphicsDeviceManagerAlreadyPresent);
			game.Services.AddService(typeof (IGraphicsDeviceManager), this);
			game.Services.AddService(typeof (IGraphicsDeviceService), this);
			game.Window.ClientSizeChanged += GameWindowClientSizeChanged;
			game.Window.ScreenDeviceNameChanged += GameWindowScreenDeviceNameChanged;
			game.Window.OrientationChanged += GameWindowOrientationChanged;
			_graphicsProfile = ReadDefaultGraphicsProfile();
		}

		/// <summary>
		///     Gets the graphics profile, which determines the graphics feature set.
		/// </summary>
		public GraphicsProfile GraphicsProfile
		{
			get { return _graphicsProfile; }
			set
			{
				_graphicsProfile = value;
				_isDeviceDirty = true;
			}
		}

		/// <summary>
		///     Gets or sets the format of the depth stencil.
		/// </summary>
		public DepthFormat PreferredDepthStencilFormat
		{
			get { return _depthStencilFormat; }
			set
			{
				_depthStencilFormat = value;
				_isDeviceDirty = true;
			}
		}

		/// <summary>
		///     Gets or sets the format of the back buffer.
		/// </summary>
		public SurfaceFormat PreferredBackBufferFormat
		{
			get { return _backBufferFormat; }
			set
			{
				_backBufferFormat = value;
				_isDeviceDirty = true;
			}
		}

		/// <summary>
		///     Gets or sets the preferred back-buffer width.
		/// </summary>
		public int PreferredBackBufferWidth
		{
			get { return _backBufferWidth; }
			set
			{
				if (value <= 0)
					throw new ArgumentOutOfRangeException("value", Resources.BackBufferDimMustBePositive);
				_backBufferWidth = value;
				_useResizedBackBuffer = false;
				_isDeviceDirty = true;
			}
		}

		/// <summary>
		///     Gets or sets the preferred back-buffer height.
		/// </summary>
		public int PreferredBackBufferHeight
		{
			get { return _backBufferHeight; }
			set
			{
				if (value <= 0)
					throw new ArgumentOutOfRangeException("value", Resources.BackBufferDimMustBePositive);
				_backBufferHeight = value;
				_useResizedBackBuffer = false;
				_isDeviceDirty = true;
			}
		}

		/// <summary>
		///     Gets or sets a value that indicates whether the device should start in full-screen mode.
		/// </summary>
		public bool IsFullScreen
		{
			get { return _isFullScreen; }
			set
			{
				_isFullScreen = value;
				_isDeviceDirty = true;
			}
		}

		/// <summary>
		///     Gets or sets a value that indicates whether to sync to the vertical trace (vsync) when presenting the back buffer.
		/// </summary>
		public bool SynchronizeWithVerticalRetrace
		{
			get { return _synchronizeWithVerticalRetrace; }
			set
			{
				_synchronizeWithVerticalRetrace = value;
				_isDeviceDirty = true;
			}
		}

		/// <summary />
		public bool PreferMultiSampling
		{
			get { return _allowMultiSampling; }
			set
			{
				_allowMultiSampling = value;
				_isDeviceDirty = true;
			}
		}

		/// <summary>
		///     Gets or sets the display orientations that are available if automatic rotation and scaling is enabled.
		/// </summary>
		public DisplayOrientation SupportedOrientations
		{
			get { return _supportedOrientations; }
			set
			{
				_supportedOrientations = value;
				_isDeviceDirty = true;
			}
		}

		void IDisposable.Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		void IGraphicsDeviceManager.CreateDevice()
		{
			ChangeDevice(true);
		}

		bool IGraphicsDeviceManager.BeginDraw()
		{
			if (!EnsureDevice())
				return false;
			_beginDrawOk = true;
			return true;
		}

		void IGraphicsDeviceManager.EndDraw()
		{
			if (!_beginDrawOk)
				return;
			if (_device == null)
				return;
			try
			{
				_device.Present();
			}
			catch (DeviceLostException)
			{
			}
			catch (DeviceNotResetException)
			{
			}
		}

		/// <summary>
		///     Gets the GraphicsDevice associated with the GraphicsDeviceManager.
		/// </summary>
		public GraphicsDevice GraphicsDevice
		{
			get { return _device; }
		}

		/// <summary>
		///     Raised when a new graphics device is created.
		/// </summary>
		/// <param name="" />
		public event EventHandler<EventArgs> DeviceCreated
		{
			add { _deviceCreated += value; }
			remove { _deviceCreated -= value; }
		}

		/// <summary>
		///     Raised when the GraphicsDeviceManager is about to be reset.
		/// </summary>
		/// <param name="" />
		public event EventHandler<EventArgs> DeviceResetting
		{
			add { _deviceResetting += value; }
			remove { _deviceResetting -= value; }
		}

		/// <summary>
		///     Raised when the GraphicsDeviceManager is reset.
		/// </summary>
		/// <param name="" />
		public event EventHandler<EventArgs> DeviceReset
		{
			add { _deviceReset += value; }
			remove { _deviceReset -= value; }
		}

		/// <summary>
		///     Raised when the GraphicsDeviceManager is being disposed.
		/// </summary>
		/// <param name="" />
		public event EventHandler<EventArgs> DeviceDisposing
		{
			add { _deviceDisposing += value; }
			remove { _deviceDisposing -= value; }
		}

		/// <summary>
		///     Raised when the GraphicsDeviceManager is changing the GraphicsDevice settings (during reset or recreation of the
		///     GraphicsDevice).
		/// </summary>
		/// <param name="" />
		public event EventHandler<PreparingDeviceSettingsEventArgs> PreparingDeviceSettings;

		/// <summary>
		///     Raised when the GraphicsDeviceManager is disposed.
		/// </summary>
		/// <param name="" />
		public event EventHandler<EventArgs> Disposed;

		/// <summary>
		///     Applies any changes to device-related properties, changing the graphics device as necessary.
		/// </summary>
		public void ApplyChanges()
		{
			if (_device != null && !_isDeviceDirty)
				return;
			ChangeDevice(false);
		}

		/// <summary>
		///     Toggles between full screen and windowed mode.
		/// </summary>
		public void ToggleFullScreen()
		{
			IsFullScreen = !IsFullScreen;
			ChangeDevice(false);
		}

		private void GameWindowScreenDeviceNameChanged(object sender, EventArgs e)
		{
			if (_inDeviceTransition)
				return;
			ChangeDevice(false);
		}

		private void GameWindowClientSizeChanged(object sender, EventArgs e)
		{
			if (_inDeviceTransition || _game.Window.ClientBounds.Height == 0 && _game.Window.ClientBounds.Width == 0)
				return;
			_resizedBackBufferWidth = _game.Window.ClientBounds.Width;
			_resizedBackBufferHeight = _game.Window.ClientBounds.Height;
			_useResizedBackBuffer = true;
			ChangeDevice(false);
		}

		private void GameWindowOrientationChanged(object sender, EventArgs e)
		{
			if (_inDeviceTransition || _game.Window.ClientBounds.Height == 0 && _game.Window.ClientBounds.Width == 0 ||
				_game.Window.CurrentOrientation == _currentWindowOrientation)
				return;
			ChangeDevice(false);
		}

		private bool EnsureDevice()
		{
			if (_device == null)
				return false;
			return EnsureDevicePlatform();
		}

		private void CreateDevice(GraphicsDeviceInformation newInfo)
		{
			if (_device != null)
			{
				_device.Dispose();
				_device = null;
			}
			OnPreparingDeviceSettings(this, new PreparingDeviceSettingsEventArgs(newInfo));
			MassagePresentParameters(newInfo.PresentationParameters);
			try
			{
				ValidateGraphicsDeviceInformation(newInfo);
				_device = new GraphicsDevice(newInfo.Adapter, newInfo.GraphicsProfile, newInfo.PresentationParameters);
				_device.DeviceResetting += HandleDeviceResetting;
				_device.DeviceReset += HandleDeviceReset;
				_device.DeviceLost += HandleDeviceLost;
				_device.Disposing += HandleDisposing;
			}
			catch (NoSuitableGraphicsDeviceException)
			{
				throw;
			}
			catch (ArgumentException ex)
			{
				throw new NoSuitableGraphicsDeviceException(Resources.Direct3DInvalidCreateParameters, ex);
			}
			catch (Exception ex)
			{
				throw new NoSuitableGraphicsDeviceException(Resources.Direct3DCreateError, ex);
			}
			ConfigureTouchInput(newInfo.PresentationParameters);
			OnDeviceCreated(this, EventArgs.Empty);
		}

		private void ChangeDevice(bool forceCreate)
		{
			if (_game == null)
				throw new InvalidOperationException(Resources.GraphicsComponentNotAttachedToGame);
			_inDeviceTransition = true;
			string screenDeviceName = _game.Window.ScreenDeviceName;
			int clientWidth = _game.Window.ClientBounds.Width;
			int clientHeight = _game.Window.ClientBounds.Height;
			bool flag1 = false;
			try
			{
				_game.Window.SetSupportedOrientations(Helpers.ChooseOrientation(_supportedOrientations, PreferredBackBufferWidth, PreferredBackBufferHeight, true));
				GraphicsDeviceInformation bestDevice = FindBestDevice(forceCreate);
				_game.Window.BeginScreenDeviceChange(bestDevice.PresentationParameters.IsFullScreen);
				flag1 = true;
				bool flag2 = true;
				if (!forceCreate && _device != null)
				{
					OnPreparingDeviceSettings(this, new PreparingDeviceSettingsEventArgs(bestDevice));
					if (CanResetDevice(bestDevice))
					{
						try
						{
							GraphicsDeviceInformation deviceInformation = bestDevice.Clone();
							MassagePresentParameters(bestDevice.PresentationParameters);
							ValidateGraphicsDeviceInformation(bestDevice);
							_device.Reset(deviceInformation.PresentationParameters, deviceInformation.Adapter);
							ConfigureTouchInput(deviceInformation.PresentationParameters);
							flag2 = false;
						}
// ReSharper disable once EmptyGeneralCatchClause
						catch
						{
						}
					}
				}
				if (flag2)
					CreateDevice(bestDevice);
// ReSharper disable once PossibleNullReferenceException
				PresentationParameters presentationParameters = _device.PresentationParameters;
				screenDeviceName = _device.Adapter.DeviceName;
				_isReallyFullScreen = presentationParameters.IsFullScreen;
				if (presentationParameters.BackBufferWidth != 0)
					clientWidth = presentationParameters.BackBufferWidth;
				if (presentationParameters.BackBufferHeight != 0)
					clientHeight = presentationParameters.BackBufferHeight;
				_isDeviceDirty = false;
			}
			finally
			{
				if (flag1)
					_game.Window.EndScreenDeviceChange(screenDeviceName, clientWidth, clientHeight);
				_currentWindowOrientation = _game.Window.CurrentOrientation;
				_inDeviceTransition = false;
			}
		}

		private void MassagePresentParameters(PresentationParameters pp)
		{
			bool flag1 = pp.BackBufferWidth == 0;
			bool flag2 = pp.BackBufferHeight == 0;
			if (pp.IsFullScreen)
				return;
			IntPtr hWnd = pp.DeviceWindowHandle;
			if (hWnd == IntPtr.Zero)
			{
				if (_game == null)
					throw new InvalidOperationException(Resources.GraphicsComponentNotAttachedToGame);
				hWnd = _game.Window.Handle;
			}
			NativeMethods.RECT rect;
			NativeMethods.GetClientRect(hWnd, out rect);
			if (flag1 && rect.Right == 0)
				pp.BackBufferWidth = 1;
			if (!flag2 || rect.Bottom != 0)
				return;
			pp.BackBufferHeight = 1;
		}

// ReSharper disable once UnusedParameter.Local
		private static void ConfigureTouchInput(PresentationParameters pp)
		{
			//TouchPanel.DisplayWidth = pp.BackBufferWidth;
			//TouchPanel.DisplayHeight = pp.BackBufferHeight;
			//TouchPanel.DisplayOrientation = pp.DisplayOrientation;
		}

		/// <summary>
		///     Finds the best device configuration that is compatible with the current device preferences.
		/// </summary>
		/// <param name="anySuitableDevice">
		///     true if the FindBestDevice can select devices from any available adapter; false if only
		///     the current adapter should be considered.
		/// </param>
		protected virtual GraphicsDeviceInformation FindBestDevice(bool anySuitableDevice)
		{
			return FindBestPlatformDevice(anySuitableDevice);
		}

		/// <summary>
		///     Determines whether the given GraphicsDeviceInformation is compatible with the existing graphics device.
		/// </summary>
		/// <param name="newDeviceInfo">Information describing the desired device configuration.</param>
		protected virtual bool CanResetDevice(GraphicsDeviceInformation newDeviceInfo)
		{
			return _device.GraphicsProfile == newDeviceInfo.GraphicsProfile;
		}

		/// <summary>
		///     Ranks the given list of devices that satisfy the given preferences.
		/// </summary>
		/// <param name="foundDevices">The list of devices to rank.</param>
		protected virtual void RankDevices(List<GraphicsDeviceInformation> foundDevices)
		{
			RankDevicesPlatform(foundDevices);
		}

		private void HandleDisposing(object sender, EventArgs e)
		{
			OnDeviceDisposing(this, EventArgs.Empty);
		}

		private void HandleDeviceLost(object sender, EventArgs e)
		{
		}

		private void HandleDeviceReset(object sender, EventArgs e)
		{
			OnDeviceReset(this, EventArgs.Empty);
		}

		private void HandleDeviceResetting(object sender, EventArgs e)
		{
			OnDeviceResetting(this, EventArgs.Empty);
		}

		/// <summary>
		///     Called when a device is created. Raises the DeviceCreated event.
		/// </summary>
		/// <param name="sender">The GraphicsDeviceManager.</param>
		/// <param name="args">Arguments for the DeviceCreated event.</param>
		protected virtual void OnDeviceCreated(object sender, EventArgs args)
		{
			if (_deviceCreated == null)
				return;
			_deviceCreated(sender, args);
		}

		/// <summary>
		///     Called when a device is being disposed. Raises the DeviceDisposing event.
		/// </summary>
		/// <param name="sender">The GraphicsDeviceManager.</param>
		/// <param name="args">Arguments for the DeviceDisposing event.</param>
		protected virtual void OnDeviceDisposing(object sender, EventArgs args)
		{
			if (_deviceDisposing == null)
				return;
			_deviceDisposing(sender, args);
		}

		/// <summary>
		///     Called when the device has been reset. Raises the DeviceReset event.
		/// </summary>
		/// <param name="sender">The GraphicsDeviceManager.</param>
		/// <param name="args">Arguments for the DeviceReset event.</param>
		protected virtual void OnDeviceReset(object sender, EventArgs args)
		{
			if (_deviceReset == null)
				return;
			_deviceReset(sender, args);
		}

		/// <summary>
		///     Called when the device is about to be reset. Raises the DeviceResetting event.
		/// </summary>
		/// <param name="sender">The GraphicsDeviceManager.</param>
		/// <param name="args">Arguments for the DeviceResetting event.</param>
		protected virtual void OnDeviceResetting(object sender, EventArgs args)
		{
			if (_deviceResetting == null)
				return;
			_deviceResetting(sender, args);
		}

		/// <summary>
		///     Releases the unmanaged resources used by the GraphicsDeviceManager and optionally releases the managed resources.
		/// </summary>
		/// <param name="disposing">true to release both automatic and manual resources; false to release only manual resources.</param>
		protected virtual void Dispose(bool disposing)
		{
			if (!disposing)
				return;
			if (_game != null)
			{
				if (_game.Services.GetService(typeof (IGraphicsDeviceService)) == this)
					_game.Services.RemoveService(typeof (IGraphicsDeviceService));
				_game.Window.ClientSizeChanged -= GameWindowClientSizeChanged;
				_game.Window.ScreenDeviceNameChanged -= GameWindowScreenDeviceNameChanged;
				_game.Window.OrientationChanged -= GameWindowOrientationChanged;
			}
			if (_device != null)
			{
				_device.Dispose();
				_device = null;
			}
			if (Disposed == null)
				return;
			Disposed(this, EventArgs.Empty);
		}

		/// <summary>
		///     Called when the GraphicsDeviceManager is changing the GraphicsDevice settings (during reset or recreation of the
		///     GraphicsDevice). Raises the PreparingDeviceSettings event.
		/// </summary>
		/// <param name="sender">The GraphicsDeviceManager.</param>
		/// <param name="args">The graphics device information to modify.</param>
		protected virtual void OnPreparingDeviceSettings(object sender, PreparingDeviceSettingsEventArgs args)
		{
			if (PreparingDeviceSettings == null)
				return;
			PreparingDeviceSettings(sender, args);
		}

		private GraphicsProfile ReadDefaultGraphicsProfile()
		{
			Stream manifestResourceStream = _game.GetType().Assembly.GetManifestResourceStream("Microsoft.Xna.Framework.RuntimeProfile");
			if (manifestResourceStream != null)
			{
				using (var streamReader = new StreamReader(manifestResourceStream))
				{
					string str = streamReader.ReadLine();
					if (str != null)
						return str.EndsWith("Reach") || !str.EndsWith("HiDef") ? GraphicsProfile.Reach : GraphicsProfile.HiDef;
				}
			}
			return GraphicsProfile.Reach;
		}

		private void RankDevicesPlatform(List<GraphicsDeviceInformation> foundDevices)
		{
			foundDevices.Sort(new GraphicsDeviceInformationComparer(this));
		}

		private GraphicsDeviceInformation FindBestPlatformDevice(bool anySuitableDevice)
		{
			var foundDevices = new List<GraphicsDeviceInformation>();
			AddDevices(anySuitableDevice, foundDevices);
			if (foundDevices.Count == 0 && PreferMultiSampling)
			{
				PreferMultiSampling = false;
				AddDevices(anySuitableDevice, foundDevices);
			}
			if (foundDevices.Count == 0)
			{
				throw new NoSuitableGraphicsDeviceException(string.Format(CultureInfo.CurrentCulture, Resources.NoCompatibleDevices, new object[]
				{
					_graphicsProfile
				}), null);
			}
			RankDevices(foundDevices);
			if (foundDevices.Count == 0)
				throw new NoSuitableGraphicsDeviceException(Resources.NoCompatibleDevicesAfterRanking, null);
			return foundDevices[0];
		}

		private void AddDevices(bool anySuitableDevice, List<GraphicsDeviceInformation> foundDevices)
		{
			IntPtr handle = _game.Window.Handle;
			foreach (GraphicsAdapter adapter in GraphicsAdapter.Adapters)
			{
				if (!anySuitableDevice)
				{
					if (!IsWindowOnAdapter(handle, adapter))
						continue;
				}
				try
				{
					if (adapter.IsProfileSupported(_graphicsProfile))
					{
						var baseDeviceInfo = new GraphicsDeviceInformation
						{
							Adapter = adapter,
							GraphicsProfile = _graphicsProfile,
							PresentationParameters =
							{
								DeviceWindowHandle = handle,
								MultiSampleCount = 0,
								IsFullScreen = IsFullScreen,
								PresentationInterval = SynchronizeWithVerticalRetrace ? PresentInterval.One : PresentInterval.Immediate
							}
						};
						AddDevices(adapter, adapter.CurrentDisplayMode, baseDeviceInfo, foundDevices);
						if (_isFullScreen)
						{
							foreach (DisplayMode mode in adapter.SupportedDisplayModes)
							{
								if (mode.Width >= 640 && mode.Height >= 480)
									AddDevices(adapter, mode, baseDeviceInfo, foundDevices);
							}
						}
					}
				}
				catch (NotSupportedException)
				{
				}
			}
		}

		private void AddDevices(GraphicsAdapter adapter, DisplayMode mode, GraphicsDeviceInformation baseDeviceInfo,
			List<GraphicsDeviceInformation> foundDevices)
		{
			GraphicsDeviceInformation deviceInformation = baseDeviceInfo.Clone();
			if (IsFullScreen)
			{
				deviceInformation.PresentationParameters.BackBufferWidth = mode.Width;
				deviceInformation.PresentationParameters.BackBufferHeight = mode.Height;
			}
			else if (_useResizedBackBuffer)
			{
				deviceInformation.PresentationParameters.BackBufferWidth = _resizedBackBufferWidth;
				deviceInformation.PresentationParameters.BackBufferHeight = _resizedBackBufferHeight;
			}
			else
			{
				deviceInformation.PresentationParameters.BackBufferWidth = PreferredBackBufferWidth;
				deviceInformation.PresentationParameters.BackBufferHeight = PreferredBackBufferHeight;
			}
			SurfaceFormat selectedFormat;
			DepthFormat selectedDepthFormat;
			int selectedMultiSampleCount;
			adapter.QueryBackBufferFormat(deviceInformation.GraphicsProfile, mode.Format, PreferredDepthStencilFormat, PreferMultiSampling ? 16 : 0,
				out selectedFormat, out selectedDepthFormat, out selectedMultiSampleCount);
			deviceInformation.PresentationParameters.BackBufferFormat = selectedFormat;
			deviceInformation.PresentationParameters.DepthStencilFormat = selectedDepthFormat;
			deviceInformation.PresentationParameters.MultiSampleCount = selectedMultiSampleCount;
			if (foundDevices.Contains(deviceInformation))
				return;
			foundDevices.Add(deviceInformation);
		}

		private bool IsWindowOnAdapter(IntPtr windowHandle, GraphicsAdapter adapter)
		{
// ReSharper disable once PossibleUnintendedReferenceComparison
			return WindowsGameWindow.ScreenFromAdapter(adapter) == WindowsGameWindow.ScreenFromHandle(windowHandle);
		}

		private bool EnsureDevicePlatform()
		{
			if (_isReallyFullScreen && !_game.IsActiveIgnoringGuide)
				return false;
			switch (_device.GraphicsDeviceStatus)
			{
				case GraphicsDeviceStatus.Lost:
					Thread.Sleep((int) DeviceLostSleepTime.TotalMilliseconds);
					return false;
				case GraphicsDeviceStatus.NotReset:
					Thread.Sleep((int) DeviceLostSleepTime.TotalMilliseconds);
					try
					{
						ChangeDevice(false);
						break;
					}
					catch (DeviceLostException)
					{
						return false;
					}
					catch
					{
						ChangeDevice(true);
						break;
					}
			}
			return true;
		}

		private void ValidateGraphicsDeviceInformation(GraphicsDeviceInformation devInfo)
		{
			GraphicsAdapter adapter = devInfo.Adapter;
			PresentationParameters presentationParameters = devInfo.PresentationParameters;
			if (!presentationParameters.IsFullScreen)
				return;
			if (presentationParameters.BackBufferWidth == 0 || presentationParameters.BackBufferHeight == 0)
				throw new ArgumentException(Resources.ValidateBackBufferDimsFullScreen);
			bool flag = true;
			DisplayMode currentDisplayMode = adapter.CurrentDisplayMode;
			if (currentDisplayMode.Format != presentationParameters.BackBufferFormat && currentDisplayMode.Width != presentationParameters.BackBufferWidth &&
				currentDisplayMode.Height != presentationParameters.BackBufferHeight)
			{
				flag = false;
// ReSharper disable once LoopCanBeConvertedToQuery
				foreach (DisplayMode displayMode in adapter.SupportedDisplayModes[presentationParameters.BackBufferFormat])
				{
					if (displayMode.Width == presentationParameters.BackBufferWidth && displayMode.Height == presentationParameters.BackBufferHeight)
					{
						flag = true;
						break;
					}
				}
			}
			if (!flag)
				throw new ArgumentException(Resources.ValidateBackBufferDimsModeFullScreen);
		}
	}
}