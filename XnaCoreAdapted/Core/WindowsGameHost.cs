﻿using System;
using System.Windows.Forms;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using XnaCoreAdapted.Common;
using XnaCoreAdapted.Integration;
using MessageBoxIcon = System.Windows.Forms.MessageBoxIcon;

namespace XnaCoreAdapted.Core
{
	internal class WindowsGameHost : GameHost
	{
		internal override GameWindow Window
		{
			get { return _gameWindow; }
		}

		private readonly WindowsGameWindow _gameWindow;
		private bool _doneRun;
		private bool _exitRequested;

		private readonly IXnaGameContainer _xnaGameContainer;

		public WindowsGameHost(Game game)
		{
			LockThreadToProcessor();
			_xnaGameContainer = game.XnaGameContainer;
			_gameWindow = new WindowsGameWindow(_xnaGameContainer);
			Mouse.WindowHandle = _gameWindow.Handle;
			//TouchPanel.WindowHandle = this.gameWindow.Handle;
			_gameWindow.IsMouseVisible = game.IsMouseVisible;
			_gameWindow.Activated += GameWindowActivated;
			_gameWindow.Deactivated += GameWindowDeactivated;
			_gameWindow.Suspend += GameWindowSuspend;
			_gameWindow.Resume += GameWindowResume;
		}

		internal override bool ShowMissingRequirementMessage(Exception exception)
		{
			string text;
			if (exception is NoSuitableGraphicsDeviceException)
			{
				text = Resources.NoSuitableGraphicsDevice + "\n\n" + exception.Message;
			}
			else
			{
				if (!(exception is NoAudioHardwareException))
					return base.ShowMissingRequirementMessage(exception);
				text = Resources.NoAudioHardware;
			}
			// ReSharper disable once UnusedVariable
			var num = (int)MessageBox.Show(text, _gameWindow.Title, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			return true;
		}

		internal override void Run()
		{
			if (_doneRun)
				throw new InvalidOperationException(Resources.NoMultipleRuns);
			try
			{
				_xnaGameContainer.ApplicationIdle += ApplicationIdle;
				_xnaGameContainer.Run();
			}
			catch(Exception)
			{
				_xnaGameContainer.ApplicationIdle -= ApplicationIdle;
				_doneRun = true;
				OnExiting();
			}
		}

		internal override void RunOneFrame()
		{
			_gameWindow.Tick();
			OnIdle();
			if (!GamerServicesDispatcher.IsInitialized)
				return;
			_gameWindow.IsGuideVisible = Guide.IsVisible;
		}

		internal override void StartGameLoop()
		{
		}

		internal override void Exit()
		{
			_exitRequested = true;
		}

		private void LockThreadToProcessor()
		{
			UIntPtr lpProcessAffinityMask;
			UIntPtr lpSystemAffinityMask;
			if (!NativeMethods.GetProcessAffinityMask(NativeMethods.GetCurrentProcess(), out lpProcessAffinityMask, out lpSystemAffinityMask) || !(lpProcessAffinityMask != UIntPtr.Zero))
				return;
			// ReSharper disable once UnusedVariable
			IntPtr num =
				UIntPtrToIntPtr(NativeMethods.SetThreadAffinityMask(NativeMethods.GetCurrentThread(),
					(UIntPtr)(lpProcessAffinityMask.ToUInt64() & (ulong)(~(long)lpProcessAffinityMask.ToUInt64() + 1L))));
		}

		private static IntPtr UIntPtrToIntPtr(UIntPtr uIntPtr)
		{
			return unchecked((IntPtr) (long) (ulong) uIntPtr);
		}

		private void GameWindowSuspend(object sender, EventArgs e)
		{
			OnSuspend();
		}

		private void GameWindowResume(object sender, EventArgs e)
		{
			OnResume();
		}

		private void GameWindowDeactivated(object sender, EventArgs e)
		{
			OnDeactivated();
		}

		private void GameWindowActivated(object sender, EventArgs e)
		{
			OnActivated();
		}

		private void ApplicationIdle(object sender, EventArgs e)
		{
			//NativeMethods.Message msg;
			//while (!NativeMethods.PeekMessage(out msg, IntPtr.Zero, 0U, 0U, 0U))
			//{
				if (_exitRequested)
					_gameWindow.Close();
				else
					RunOneFrame();
			//}
		}
	}
}