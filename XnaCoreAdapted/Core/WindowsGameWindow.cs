﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Resources;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using XnaCoreAdapted.Common;
using XnaCoreAdapted.Integration;
using Rectangle = Microsoft.Xna.Framework.Rectangle;

// ReSharper disable EmptyGeneralCatchClause
// ReSharper disable AssignNullToNotNullAttribute
namespace XnaCoreAdapted.Core
{
	internal class WindowsGameWindow : GameWindow
	{
		internal event EventHandler<EventArgs> Suspend;
		internal event EventHandler<EventArgs> Resume;

		public override IntPtr Handle
		{
			get
			{
				if (_mainForm != null)
					return _mainForm.Handle;
				return IntPtr.Zero;
			}
		}

		public override bool AllowUserResizing
		{
			get
			{
				if (_mainForm != null)
					return _mainForm.AllowUserResizing;
				return false;
			}
			set
			{
				if (_mainForm == null)
					return;
				_mainForm.AllowUserResizing = value;
			}
		}

		public override Rectangle ClientBounds
		{
			get { return _mainForm.ClientBounds; }
		}

		public override DisplayOrientation CurrentOrientation
		{
			get { return DisplayOrientation.Default; }
		}

		public override string ScreenDeviceName
		{
			get
			{
				if (_mainForm == null || _mainForm.DeviceScreen == null)
					return string.Empty;
				return DeviceNameFromScreen(_mainForm.DeviceScreen);
			}
		}

		internal override bool IsMouseVisible
		{
			get { return _isMouseVisible; }
			set
			{
				_isMouseVisible = value;
				if (_mainForm == null)
					return;
				_mainForm.IsMouseVisible = _isMouseVisible || _isGuideVisible;
			}
		}

		internal bool IsGuideVisible
		{
			set
			{
				if (value == _isGuideVisible)
					return;
				_isGuideVisible = value;
				if (_mainForm == null)
					return;
				_mainForm.IsMouseVisible = _isMouseVisible || _isGuideVisible;
			}
		}

		internal override bool IsMinimized
		{
			get
			{
				if (_mainForm == null)
					return false;
				return _mainForm.IsMinimized;
			}
		}

		private bool _inDeviceTransition;
		private bool _isGuideVisible;
		private bool _isMouseVisible;
		private IXnaGameContainer _mainForm;
		private Exception _pendingException;

		public WindowsGameWindow(IXnaGameContainer xnaContainer)
		{
			if (xnaContainer == null)
				throw new ArgumentNullException("xnaContainer");

			_mainForm = xnaContainer;
			Icon defaultIcon = GetDefaultIcon();
			if (defaultIcon != null)
				_mainForm.WindowIcon = defaultIcon;
			Title = GetDefaultTitleName();
			_mainForm.Suspend += mainForm_Suspend;
			_mainForm.Resume += mainForm_Resume;
			_mainForm.ScreenChanged += mainForm_ScreenChanged;
			_mainForm.ApplicationActivated += mainForm_ApplicationActivated;
			_mainForm.ApplicationDeactivated += mainForm_ApplicationDeactivated;
			_mainForm.UserResized += mainForm_UserResized;
			_mainForm.Closing += mainForm_Closing;
			_mainForm.Paint += mainForm_Paint;
		}

		protected override void Dispose(bool disposing)
		{
			Suspend = null;
			Resume = null;
		}

		public override void BeginScreenDeviceChange(bool willBeFullScreen)
		{
			_mainForm.BeginScreenDeviceChange(willBeFullScreen);
			_inDeviceTransition = true;
		}

		public override void EndScreenDeviceChange(string screenDeviceName, int clientWidth, int clientHeight)
		{
			try
			{
				_mainForm.EndScreenDeviceChange(screenDeviceName, clientWidth, clientHeight);
			}
			finally
			{
				_inDeviceTransition = false;
			}
		}

		internal override void Close()
		{
			if (_mainForm == null)
				return;
			_mainForm.Close();
			_mainForm = null;
		}

		internal void Tick()
		{
			if (_pendingException == null)
				return;
			Exception exception = _pendingException;
			_pendingException = null;
			throw exception;
		}

		internal static Screen ScreenFromHandle(IntPtr windowHandle)
		{
			int num1 = 0;
			Screen screen1 = null;
			NativeMethods.RECT rect;
			NativeMethods.GetWindowRect(windowHandle, out rect);
			var rectangle1 = new System.Drawing.Rectangle(rect.Left, rect.Top, rect.Right - rect.Left, rect.Bottom - rect.Top);
			foreach (Screen screen2 in Screen.AllScreens)
			{
				System.Drawing.Rectangle rectangle2 = rectangle1;
				rectangle2.Intersect(screen2.Bounds);
				int num2 = rectangle2.Width * rectangle2.Height;
				if (num2 > num1)
				{
					num1 = num2;
					screen1 = screen2;
				}
			}
			return screen1 ?? (Screen.AllScreens[0]);
		}

		internal static string DeviceNameFromScreen(Screen screen)
		{
			string str = screen.DeviceName;
			int length = screen.DeviceName.IndexOf(char.MinValue);
			if (length != -1)
				str = screen.DeviceName.Substring(0, length);
			return str;
		}

		internal static Screen ScreenFromDeviceName(string screenDeviceName)
		{
			if (string.IsNullOrEmpty(screenDeviceName))
				throw new ArgumentException(Resources.NullOrEmptyScreenDeviceName);
			foreach (Screen screen in Screen.AllScreens)
			{
				if (DeviceNameFromScreen(screen) == screenDeviceName)
					return screen;
			}
			throw new ArgumentException(Resources.InvalidScreenDeviceName, "screenDeviceName");
		}

		internal static Screen ScreenFromAdapter(GraphicsAdapter adapter)
		{
			foreach (Screen screen in Screen.AllScreens)
			{
				if (DeviceNameFromScreen(screen) == adapter.DeviceName)
					return screen;
			}
			throw new ArgumentException(Resources.InvalidScreenAdapter, "adapter");
		}

		protected override void SetTitle(string title)
		{
			if (_mainForm == null)
				return;
			_mainForm.Text = title;
		}

		protected internal override void SetSupportedOrientations(DisplayOrientation orientations)
		{
		}

		protected void OnSuspend()
		{
			if (Suspend == null)
				return;
			Suspend(this, EventArgs.Empty);
		}

		protected void OnResume()
		{
			if (Resume == null)
				return;
			Resume(this, EventArgs.Empty);
		}

		private void mainForm_ApplicationActivated(object sender, EventArgs e)
		{
			OnActivated();
		}

		private void mainForm_ApplicationDeactivated(object sender, EventArgs e)
		{
			OnDeactivated();
		}

		private void mainForm_ScreenChanged(object sender, EventArgs e)
		{
			OnScreenDeviceNameChanged();
		}

		private void mainForm_UserResized(object sender, EventArgs e)
		{
			OnClientSizeChanged();
		}

		private void mainForm_Paint(object sender, PaintEventArgs e)
		{
			if (_inDeviceTransition)
				return;
			try
			{
				OnPaint();
			}
			catch (Exception ex)
			{
				_pendingException = new InvalidOperationException(Resources.PreviousDrawThrew, ex);
			}
		}

		private void mainForm_Closing(object sender, CancelEventArgs e)
		{
			OnDeactivated();
		}

		private void mainForm_Resume(object sender, EventArgs e)
		{
			OnResume();
		}

		private void mainForm_Suspend(object sender, EventArgs e)
		{
			OnSuspend();
		}

		private static string GetAssemblyTitle(Assembly assembly)
		{
			if (assembly == null)
				return null;
			var assemblyTitleAttributeArray = (AssemblyTitleAttribute[])assembly.GetCustomAttributes(typeof(AssemblyTitleAttribute), true);
			// ReSharper disable once ConditionIsAlwaysTrueOrFalse
			if (assemblyTitleAttributeArray != null && assemblyTitleAttributeArray.Length > 0)
				return assemblyTitleAttributeArray[0].Title;
			return null;
		}

		private static string GetDefaultTitleName()
		{
			string assemblyTitle = GetAssemblyTitle(Assembly.GetEntryAssembly());
			if (!string.IsNullOrEmpty(assemblyTitle))
				return assemblyTitle;
			try
			{
				return Path.GetFileNameWithoutExtension(new Uri(Application.ExecutablePath).LocalPath);
			}
			catch (ArgumentNullException)
			{
			}
			catch (UriFormatException)
			{
			}
			return Resources.DefaultTitleName;
		}

		private static Icon FindFirstIcon(Assembly assembly)
		{
			if (assembly == null)
				return null;
			foreach (string name in assembly.GetManifestResourceNames())
			{
				try
				{
					return new Icon(assembly.GetManifestResourceStream(name));
				}
				catch
				{
					try
					{
						IDictionaryEnumerator enumerator = new ResourceReader(assembly.GetManifestResourceStream(name)).GetEnumerator();
						while (enumerator.MoveNext())
						{
							var icon = enumerator.Value as Icon;
							if (icon != null)
								return icon;
						}
					}
					catch
					{
					}
				}
			}
			return null;
		}

		private static Icon GetDefaultIcon()
		{
			Assembly entryAssembly = Assembly.GetEntryAssembly();
			if (entryAssembly != null)
			{
				try
				{
					Icon associatedIcon = Icon.ExtractAssociatedIcon(entryAssembly.Location);
					if (associatedIcon != null)
						return associatedIcon;
				}
				catch
				{
				}
			}
			return FindFirstIcon(entryAssembly) ?? new Icon(typeof(Microsoft.Xna.Framework.Game), "Game.ico");
		}
	}
}