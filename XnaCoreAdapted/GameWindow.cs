﻿using System;
using Microsoft.Xna.Framework;
using XnaCoreAdapted.Common;

// ReSharper disable CSharpWarnings::CS1584
namespace XnaCoreAdapted
{
	public abstract class GameWindow : IDisposable
	{
		internal event EventHandler<EventArgs> Activated;
		internal event EventHandler<EventArgs> Deactivated;
		internal event EventHandler<EventArgs> Paint;

		/// <summary>
		///     Gets and sets the title of the system window.
		/// </summary>
		public string Title
		{
			get { return _title; }
			set
			{
				if (value == null)
					throw new ArgumentNullException("value", Resources.TitleCannotBeNull);
				if (_title == value)
					return;
				_title = value;
				SetTitle(_title);
			}
		}

		internal static readonly int DefaultClientWidth = 800;
		internal static readonly int DefaultClientHeight = 600;

		internal abstract bool IsMouseVisible { get; set; }
		internal abstract bool IsMinimized { get; }

		private string _title;

		static GameWindow()
		{
		}

		internal GameWindow()
		{
			_title = string.Empty;
		}

		/// <summary>
		///     Gets the handle to the system window.
		/// </summary>
		public abstract IntPtr Handle { get; }

		/// <summary>
		///     Specifies whether to allow the user to resize the game window.
		/// </summary>
		public abstract bool AllowUserResizing { get; set; }

		/// <summary>
		///     The screen dimensions of the game window's client rectangle.
		/// </summary>
		public abstract Rectangle ClientBounds { get; }

		/// <summary>
		///     Gets the device name of the screen the window is currently in.
		/// </summary>
		public abstract string ScreenDeviceName { get; }

		/// <summary>
		///     Gets the current display orientation, which reflects the physical orientation of the phone in the user's hand.
		/// </summary>
		public abstract DisplayOrientation CurrentOrientation { get; }

		/// <summary>
		///     Raised when the GameWindow moves to a different display.
		/// </summary>
		/// <param name="" />
		public event EventHandler<EventArgs> ScreenDeviceNameChanged;

		/// <summary>
		///     Raised when the size of the GameWindow changes.
		/// </summary>
		/// <param name="" />
		public event EventHandler<EventArgs> ClientSizeChanged;

		/// <summary>
		///     Describes the event raised when the display orientation of the GameWindow changes. When this event occurs, the XNA
		///     Framework automatically adjusts the game orientation based on the value specified by the developer with
		///     SupportedOrientations.
		/// </summary>
		/// <param name="" />
		public event EventHandler<EventArgs> OrientationChanged;

		public void Dispose()
		{
			Activated = null;
			Deactivated = null;
			Paint = null;
			ScreenDeviceNameChanged = null;
			ClientSizeChanged = null;
			OrientationChanged = null;

			Dispose(true);
		}

		/// <summary>
		///     Starts a device transition (windowed to full screen or vice versa).
		/// </summary>
		/// <param name="willBeFullScreen">Specifies whether the device will be in full-screen mode upon completion of the change.</param>
		public abstract void BeginScreenDeviceChange(bool willBeFullScreen);

		/// <summary>
		///     Completes a device transition.
		/// </summary>
		/// <param name="screenDeviceName">
		///     The desktop screen to move the window to. This should be the screen device name of the
		///     graphics device that has transitioned to full screen.
		/// </param>
		/// <param name="clientWidth">The new width of the game's client window.</param>
		/// <param name="clientHeight">The new height of the game's client window.</param>
		public abstract void EndScreenDeviceChange(string screenDeviceName, int clientWidth, int clientHeight);

		/// <summary>
		///     Completes a device transition.
		/// </summary>
		/// <param name="screenDeviceName">
		///     The desktop screen to move the window to. This should be the screen device name of the
		///     graphics device that has transitioned to full screen.
		/// </param>
		public void EndScreenDeviceChange(string screenDeviceName)
		{
			EndScreenDeviceChange(screenDeviceName, ClientBounds.Width, ClientBounds.Height);
		}

		internal abstract void Close();

		protected abstract void Dispose(bool disposing);

		/// <summary>
		///     Sets the title of the GameWindow.
		/// </summary>
		/// <param name="title">The new title of the GameWindow.</param>
		protected abstract void SetTitle(string title);

		/// <summary>
		///     Sets the supported display orientations.
		/// </summary>
		/// <param name="orientations">A set of supported display orientations.</param>
		protected internal abstract void SetSupportedOrientations(DisplayOrientation orientations);

		/// <summary>
		///     Called when the GameWindow gets focus.
		/// </summary>
		protected void OnActivated()
		{
			if (Activated == null)
				return;
			Activated(this, EventArgs.Empty);
		}

		/// <summary>
		///     Called when the GameWindow loses focus.
		/// </summary>
		protected void OnDeactivated()
		{
			if (Deactivated == null)
				return;
			Deactivated(this, EventArgs.Empty);
		}

		/// <summary>
		///     Called when the GameWindow needs to be painted.
		/// </summary>
		protected void OnPaint()
		{
			if (Paint == null)
				return;
			Paint(this, EventArgs.Empty);
		}

		/// <summary>
		///     Called when the GameWindow is moved to a different screen. Raises the ScreenDeviceNameChanged event.
		/// </summary>
		protected void OnScreenDeviceNameChanged()
		{
			if (ScreenDeviceNameChanged == null)
				return;
			ScreenDeviceNameChanged(this, EventArgs.Empty);
		}

		/// <summary>
		///     Called when the size of the client window changes. Raises the ClientSizeChanged event.
		/// </summary>
		protected void OnClientSizeChanged()
		{
			if (ClientSizeChanged == null)
				return;
			ClientSizeChanged(this, EventArgs.Empty);
		}

		/// <summary>
		///     Called when the GameWindow display orientation changes.
		/// </summary>
		protected void OnOrientationChanged()
		{
			if (OrientationChanged == null)
				return;
			OrientationChanged(this, EventArgs.Empty);
		}
	}
}