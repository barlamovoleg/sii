﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Threading;
using Common;
using Common.Logger;
using Logger;

namespace Localization
{
	internal partial class App
	{
		internal static IAppInfo AppInfo { get; private set; }
		internal static ILogger Logger { get; private set; }
		internal static Config CurrentConfig { get; private set; }

		private static ConfigController _configController;

		private LocalizationMainWindow _mainWindow;

		private void App_OnStartup(object sender, StartupEventArgs e)
		{
			AppInfo = new LocalizationAppInfo();
			var logManager = new LogManager(AppInfo, LocalizationResources.LoggerName);
			Logger = logManager.GetLogger(LocalizationResources.LoggerName);
			Logger.LogInfo(LocalizationResources.TryingToRunApp);

			_configController = new ConfigController(AppInfo, Logger);
			CurrentConfig = _configController.LoadConfig();

			_mainWindow = new LocalizationMainWindow();
			MainWindow = _mainWindow;
			_mainWindow.Show();

			_mainWindow.Closing += MainWindowOnClosing;
			_mainWindow.Closed += MainWindowOnClosed;
			Logger.LogInfo(LocalizationResources.RunAppSuccess);
		}

		private void MainWindowOnClosed(object sender, EventArgs eventArgs)
		{
			Logger.LogInfo(LocalizationResources.ShutdownApp);
			Logger.LogInfo(_configController.TrySaveConfig(CurrentConfig)
				? LocalizationResources.ConfigSaved
				: LocalizationResources.ConfigNotSaved);
			Shutdown();
		}

		private void MainWindowOnClosing(object sender, CancelEventArgs cancelEventArgs)
		{
			cancelEventArgs.Cancel = !_mainWindow.CanClose();
		}

		private void App_OnDispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
		{
			e.Handled = true;

			if (Logger != null)
				Logger.LogException("Unhandled exception: ", e.Exception);

			if (SystemConfiguration.IsDebug)
				Console.Beep(500, 500);
		}
	}
}
