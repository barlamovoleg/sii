﻿using System.ComponentModel;
using System.Windows;
using System.Xml;
using Common.Logger;
using Microsoft.Win32;

namespace Localization
{
	internal partial class LocalizationMainWindow : INotifyPropertyChanged
	{
		public const string LOCALIZATION_FILE_RESOLUTION = "lczf";

		public event PropertyChangedEventHandler PropertyChanged;
		private void RaisePropertyChanged(string propertyName)
		{
			var handler = PropertyChanged;
			if (handler != null)
				handler(this, new PropertyChangedEventArgs(propertyName));
		}

		private bool _isDocumentOpend;
		public bool IsDocumentOpend
		{
			get { return _isDocumentOpend; }
			private set
			{
				if (value != _isDocumentOpend)
				{
					_isDocumentOpend = value;
					RaisePropertyChanged("IsDocumentOpend");
				}
			}
		}

		private ILogger Logger { get { return App.Logger; } }

		public LocalizationMainWindow()
		{
			InitializeComponent();

			if (!string.IsNullOrWhiteSpace(App.CurrentConfig.LastOpendFilePath))
				OpenDocument(App.CurrentConfig.LastOpendFilePath);
		}

		public bool CanClose()
		{
			if (!IsDocumentOpend)
				return true;

			return TryCloseDocument(false);
		}

		public void NewDocument()
		{
			Logger.LogInfo("Trying to create new document");
			if (IsDocumentOpend)
			{
				Logger.LogInfo("Closing opened document");
				bool canClose = TryCloseDocument(true);
				if (!canClose)
				{
					Logger.LogInfo("Opened document has not been closed");
					return;
				}
			}

			IsDocumentOpend = true;
			Editor.NewDocument();
			Logger.LogInfo("New document has been created");
		}

		public void OpenDocument()
		{
			Logger.LogInfo("Open document dialog showed");
			var lastPath = App.CurrentConfig.LastOpendFilePath;
			var dialog = new OpenFileDialog
			{
				AddExtension = true,
				CheckFileExists = true,
				CheckPathExists = true,
				DefaultExt = LOCALIZATION_FILE_RESOLUTION,
				InitialDirectory = string.IsNullOrWhiteSpace(lastPath) ? App.AppInfo.AppPath : lastPath,
				Filter = string.Format("(*.{0})|*.{0}", LOCALIZATION_FILE_RESOLUTION),
				Title = "Open localization file"
			};

			var dialogResult = dialog.ShowDialog();
			if (dialogResult == null || dialogResult == false)
			{
				Logger.LogInfo("Opening document canceled");
				return;
			}

			OpenDocument(dialog.FileName);
		}

		public void OpenDocument(string documentPath)
		{
			Logger.LogInfo("Trying to open the document");
			if (IsDocumentOpend)
			{
				Logger.LogInfo("Closing opened document");
				bool canClose = TryCloseDocument(true);
				if (!canClose)
				{
					Logger.LogInfo("Opened document has not been closed");
					return;
				}
			}

			var document = GetDocumentFromPath(documentPath);
			if (document == null)
			{
				Logger.LogInfo("The document has not been converted to xml");
				return;
			}

			App.CurrentConfig.LastOpendFilePath = documentPath;
			IsDocumentOpend = true;
			Editor.LoadFromDocument(document);
			Logger.LogInfo("Document has been opened");
		}

		public bool TryCloseDocument(bool removeFromConfig)
		{
			var mbResult = MessageBox.Show(this, LocalizationResources.SaveDocumentDialog,
				LocalizationResources.SaveDocumentDialogTitle, MessageBoxButton.YesNoCancel, MessageBoxImage.Question);

			if (mbResult == MessageBoxResult.Cancel)
				return false;

			if (mbResult == MessageBoxResult.Yes)
			{
				Logger.LogInfo("Saving opened document before close");
				if (!SaveOpendDocument(false))
					return false;
			}

			App.CurrentConfig.OutputPath = string.Empty;
			Editor.CloseDocument();
			IsDocumentOpend = false;
			Logger.LogInfo("Opened document closed");
			return true;
		}

		public bool SaveOpendDocument(bool saveAs)
		{
			var document = Editor.GetDocument();

			var lastPath = App.CurrentConfig.LastOpendFilePath;
			if (string.IsNullOrWhiteSpace(lastPath) || saveAs)
			{
				Logger.LogInfo("Document will be saved as Save as...");
				var dialog = new SaveFileDialog
				{
					OverwritePrompt = true,
					AddExtension = true,
					DefaultExt = LOCALIZATION_FILE_RESOLUTION,
					FileName = "New Localization File",
					InitialDirectory = string.IsNullOrWhiteSpace(lastPath) ? App.AppInfo.AppPath : lastPath,
					Filter = string.Format("(*.{0})|*.{0}", LOCALIZATION_FILE_RESOLUTION),
					Title = "Save localization file"
				};

				var dialogResult = dialog.ShowDialog();
				if (dialogResult == null || dialogResult == false)
				{
					Logger.LogInfo("Saving document canceled");
					return false;
				}

				var path = dialog.FileName;
				App.CurrentConfig.LastOpendFilePath = path;
				SaveDocumentToPath(document, path);
				Logger.LogInfo("Document saved");
				return true;
			}

			Logger.LogInfo("Document will be saved as Save");
			SaveDocumentToPath(document, lastPath);
			Logger.LogInfo("Document saved");
			return true;
		}

		private XmlDocument GetDocumentFromPath(string documentPath)
		{
			var doc = new XmlDocument();
			doc.Load(documentPath);
			return doc;
		}

		private void SaveDocumentToPath(XmlDocument document, string pathForSave)
		{
			document.Save(pathForSave);
		}

		private void Exit_OnClick(object sender, RoutedEventArgs e)
		{
			Close();
		}

		private void NewFile_OnClick(object sender, RoutedEventArgs e)
		{
			NewDocument();
		}

		private void OpenFile_OnClick(object sender, RoutedEventArgs e)
		{
			OpenDocument();
		}

		private void SaveFile_OnClick(object sender, RoutedEventArgs e)
		{
			SaveOpendDocument(false);
		}

		private void SaveFileAs_OnClick(object sender, RoutedEventArgs e)
		{
			SaveOpendDocument(true);
		}
	}
}
