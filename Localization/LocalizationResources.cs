﻿namespace Localization
{
	internal static class LocalizationResources
	{
		internal static string AppName
		{
			get { return "Localization"; }
		}
		internal static string LoggerName
		{
			get { return "CommonLogger"; }
		}
		internal static string TryingToRunApp
		{
			get { return "Попытка запуска приложения"; }
		}
		internal static string RunAppSuccess
		{
			get { return "Приложение успешно запущенно"; }
		}
		internal static string ShutdownApp
		{
			get { return "Приложение закрывается"; }
		}
		internal static string OldConfigNotFouned
		{
			get { return "Не удалось найти файл конфигурации"; }
		}
		internal static string OldConfigNotLoaded
		{
			get { return "Ошибка во время загрузки файла конфигурации"; }
		}
		internal static string ConfigSaved
		{
			get { return "Файл конфигурации сохранен"; }
		}
		internal static string ConfigNotSaved
		{
			get { return "Файл конфигурации не удалось сохранить"; }
		}
		internal static string ConfigSaveError
		{
			get { return "Ошибка во время загрузки файла конфигурации"; }
		}
		internal static string SaveDocumentDialog
		{
			get { return "Save the document before closing?"; }
		}
		internal static string SaveDocumentDialogTitle
		{
			get { return "Closing document"; }
		}
	}
}
