﻿using System;
using System.IO;
using Common;
using Common.Logger;

// ReSharper disable once CheckNamespace
namespace Localization
{
	internal class ConfigController
	{
		private const string ConfigName = "localizationConfig";

		public Config LastLoadedConfig { get; private set; }

		private readonly ILogger _logger;
		private readonly IAppInfo _appInfo;

		public ConfigController(IAppInfo appInfo, ILogger logger)
		{
			_logger = logger;
			_appInfo = appInfo;
		}

		public Config LoadConfig()
		{
			return TryLoadSavedConfig() ?? new Config();
		}

		public bool TrySaveConfig(Config config)
		{
			if (config == null)
				throw new ArgumentNullException("config");

			var configPath = Path.Combine(_appInfo.AppPath, ConfigName);
			try
			{
				FileHelper.SerializeToFile(config, configPath);
			}
			catch (Exception e)
			{
				_logger.LogException(LocalizationResources.ConfigSaveError, e);
				return false;
			}
			return true;
		}

		private Config TryLoadSavedConfig()
		{
			var configPath = Path.Combine(_appInfo.AppPath, ConfigName);
			if (!File.Exists(configPath))
			{
				_logger.LogInfo(LocalizationResources.OldConfigNotFouned);
				return null;
			}

			try
			{
				LastLoadedConfig = FileHelper.DeserializeFromFile<Config>(configPath);
				return LastLoadedConfig;
			}
			catch (Exception e)
			{
				_logger.LogException(LocalizationResources.OldConfigNotLoaded, e);
				return null;
			}

		}
	}
}
