﻿using System;

// ReSharper disable once CheckNamespace
namespace Localization
{
	[Serializable]
	internal class Config
	{
		public string LastOpendFilePath;
		public string OutputPath;
	}
}
