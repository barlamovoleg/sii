﻿using System;
using Common;

namespace Localization
{
	internal class LocalizationAppInfo : IAppInfo
	{
		public string AppName
		{
			get { return LocalizationResources.AppName; }
		}

		public string FullVersion
		{
			get { return string.Empty; }
		}

		public string Version
		{
			get { return string.Empty; }
		}

		public string AppFullName
		{
			get { return AppName; }
		}

		public VersionType VersionType
		{
			get { return VersionType.Release; }
		}

		public string AppPath
		{
			get { return Environment.CurrentDirectory; }
		}
	}
}
