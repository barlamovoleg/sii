﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Xml;

// ReSharper disable once CheckNamespace
namespace Localization
{
	internal partial class Editor : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;
		private void RaisePropertyChanged(string propertyName)
		{
			var handler = PropertyChanged;
			if (handler != null)
				handler(this, new PropertyChangedEventArgs(propertyName));
		}

		private TreeNode _copiedNode;
		public TreeNode CopiedNode
		{
			get { return _copiedNode; }
			set
			{
				// ReSharper disable once PossibleUnintendedReferenceComparison
				if (value != _copiedNode)
				{
					_copiedNode = value;
					RaisePropertyChanged("CopiedNode");
				}
			}
		}

		public Editor()
		{
			InitializeComponent();
		}

		public void LoadFromDocument(XmlDocument document)
		{
			
		}

		public void CloseDocument()
		{
			
		}

		public void NewDocument()
		{
			
		}

		public XmlDocument GetDocument()
		{
			return null;
		}

		private void AddNode_OnClick(object sender, RoutedEventArgs e)
		{
			var window = new ChangeNameWindow("NewNode");
			var result = window.ShowDialog();
			if (result == null || result == false)
				return;

			var name = window.SelectedName;
			var newNode = new TreeNode(Guid.NewGuid(), name);
			KeysTree.Items.Add(newNode);
		}

		private void AddSubNode_OnClick(object sender, RoutedEventArgs e)
		{
			var selected = KeysTree.SelectedItem as TreeNode;
			if (selected == null)
				throw new ArgumentException("Selected element in tree");

			var window = new ChangeNameWindow("NewNode");
			var result = window.ShowDialog();
			if (result == null || result == false)
				return;

			var name = window.SelectedName;
			var newNode = new TreeNode(Guid.NewGuid(), name);
			selected.Items.Add(newNode);
			selected.IsExpanded = true;
		}

		private void EditNode_OnClick(object sender, RoutedEventArgs e)
		{
			var selected = KeysTree.SelectedItem as TreeNode;
			if (selected == null)
				throw new ArgumentException("Selected element in tree");

			var window = new ChangeNameWindow(selected.Key);
			var result = window.ShowDialog();
			if (result == null || result == false)
				return;

			var name = window.SelectedName;
			selected.Key = name;
		}

		private void RemoveNode_OnClick(object sender, RoutedEventArgs e)
		{
			var selected = KeysTree.SelectedItem as TreeViewItem;
			if (selected == null)
				throw new ArgumentException("Selected element in tree");

			if (selected.Items.Count > 0)
			{
				MessageBox.Show(Application.Current.MainWindow,
					"You can not delete this node becouse it has one or many child elements.", "Deleting node", MessageBoxButton.OK,
					MessageBoxImage.Error);
				return;
			}

			var result = MessageBox.Show(Application.Current.MainWindow,
					"Are you sure you want to delete selected node?", "Deleting node", MessageBoxButton.YesNo,
					MessageBoxImage.Warning);

			if (result != MessageBoxResult.Yes)
				return;

			RemoveNode(selected);
		}

		private void RemoveNode(TreeViewItem node)
		{
			var parent = node.Parent as TreeViewItem;
			if (parent == null)
				KeysTree.Items.Remove(node);
			else
				parent.Items.Remove(node);
		}

		private void CopyNode_OnClick(object sender, RoutedEventArgs e)
		{
			throw new NotImplementedException();
		}

		private void PastNode_OnClick(object sender, RoutedEventArgs e)
		{
			var copy = CopiedNode;
			CopiedNode = null;

			var selected = KeysTree.SelectedItem as TreeViewItem;
			if (selected == null)
			{
				KeysTree.Items.Add(copy);
				return;
			}

			selected.Items.Add(copy);
			copy.IsExpanded = true;
		}

		private void CutNode_OnClick(object sender, RoutedEventArgs e)
		{
			var selected = KeysTree.SelectedItem as TreeNode;
			if (selected == null)
				throw new ArgumentException("Selected element in tree");

			CopiedNode = selected;
			RemoveNode(selected);
		}
	}
}
