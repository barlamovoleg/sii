﻿using System.Collections.Generic;
using System.Windows.Media;


// ReSharper disable once CheckNamespace
namespace Localization
{
	internal class EditableColumn
	{
		public Dictionary<string,string> Values { get; private set; }

		public EditableColumn()
		{
			Values = new Dictionary<string, string>();
		}
	}
}
