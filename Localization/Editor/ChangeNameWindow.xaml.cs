﻿using System.Windows;

// ReSharper disable once CheckNamespace
namespace Localization
{
	internal partial class ChangeNameWindow
	{
		public string SelectedName
		{
			get { return TextBox.Text; }
		}

		public ChangeNameWindow(string startText)
		{
			InitializeComponent();

			TextBox.Text = startText;
		}

		private void ButtonOk_OnClick(object sender, RoutedEventArgs e)
		{
			DialogResult = true;
			Close();
		}

		private void ButtonCancel_OnClick(object sender, RoutedEventArgs e)
		{
			DialogResult = false;
			Close();
		}
	}
}
