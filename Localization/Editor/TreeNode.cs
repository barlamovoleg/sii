﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Controls;

// ReSharper disable once CheckNamespace
namespace Localization
{
	internal class TreeNode : TreeViewItem, INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;
		private void RaisePropertyChanged(string propertyName)
		{
			var handler = PropertyChanged;
			if (handler != null)
				handler(this, new PropertyChangedEventArgs(propertyName));
		}

		public Guid Id { get; private set; }

		private string _key;
		public string Key
		{
			get { return _key; }
			set
			{
				if (value != _key)
				{
					_key = value;
					Header = value;
					RaisePropertyChanged("Key");
				}
			}
		}

		public List<EditableColumn> Columns { get; private set; }

		public TreeNode(Guid id, string key)
		{
			Id = id;
			Key = key;
			Columns = new List<EditableColumn>();
		}

		public override string ToString()
		{
			return Key;
		}
	}
}
