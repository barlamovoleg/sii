﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq.Expressions;

// ReSharper disable once CheckNamespace
namespace GUISDK
{
	public abstract class BaseModel : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;
		private void RaisePropertyChanged(string propertyName)
		{
			var handler = PropertyChanged;
			if (handler != null)
				handler(this, new PropertyChangedEventArgs(propertyName));
		}

		private readonly Dictionary<string, object> _notifyPropertyValues = new Dictionary<string, object>();

		protected T NotifyPropertyGet<T>(Expression<Func<T>> property)
		{
			var notifyPropertyName = GetPropertyNameByExpression(property);

			if (!_notifyPropertyValues.ContainsKey(notifyPropertyName))
				_notifyPropertyValues.Add(notifyPropertyName, default(T));

			return (T)_notifyPropertyValues[notifyPropertyName];
		}

		protected void NotifyPropertySet<T>(Expression<Func<T>> property, T newValue)
		{
			var notifyPropertyName = GetPropertyNameByExpression(property);

			if (!_notifyPropertyValues.ContainsKey(notifyPropertyName))
				_notifyPropertyValues.Add(notifyPropertyName, default(T));

			var lastValue = (T)_notifyPropertyValues[notifyPropertyName];
			if (!Equals(lastValue, newValue))
			{
				_notifyPropertyValues[notifyPropertyName] = newValue;
				RaisePropertyChanged(notifyPropertyName);
			}
		}

		private string GetPropertyNameByExpression<T>(Expression<Func<T>> property)
		{
			var me = property.Body as MemberExpression;
			if (me == null)
				throw new ArgumentException("You must pass a lambda of the form: '() => Class.Property' or '() => object.Property'");

			return me.Member.Name;
		}
	}
}
