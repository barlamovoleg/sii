﻿// ReSharper disable once CheckNamespace
namespace GUISDK
{
	public interface IModelPresenter<in T> where T : BaseModel
	{
		void SetModel(T dataModel);
	}
}
