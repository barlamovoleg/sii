﻿using System;
using System.Windows.Input;

// ReSharper disable once CheckNamespace
namespace GUISDK
{
	public class RelayCommand : ICommand
	{
		public event EventHandler CanExecuteChanged;

		private readonly Action<object> _action;

		public RelayCommand(Action<object> action)
		{
			_action = action;
		}

		public void Execute(object parameter)
		{
			_action(parameter);
		}

		public bool CanExecute(object parameter)
		{
			return true;
		}
	}
}
