﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;

namespace GUISDK.Components
{
	public partial class RadioButtonDouble : INotifyPropertyChanged
	{
		public event Action ValueChanged;

		public event PropertyChangedEventHandler PropertyChanged;
		private void RaisePropertyChanged(string propertyName)
		{
			var handler = PropertyChanged;
			if (handler != null)
				handler(this, new PropertyChangedEventArgs(propertyName));
		}

		private string _leftName = "Лево";
		public string LeftName
		{
			get { return _leftName; }
			set
			{
				_leftName = value;
				RaisePropertyChanged("LeftName");
			}
		}

		private string _rightName = "Право";
		public string RightName
		{
			get { return _rightName; }
			set
			{
				_rightName = value;
				RaisePropertyChanged("RightName");
			}
		}

		private bool _isRightChecked;
		public bool IsRightChecked
		{
			get { return _isRightChecked; }
			set
			{
				_isRightChecked = value;
				RaisePropertyChanged("IsRightChecked");
			}
		}

		public RadioButtonDouble()
		{
			InitializeComponent();
		}

		private void Left_OnPreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			IsRightChecked = false;
			RaiseValueChanged();
		}

		private void Right_OnPreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			IsRightChecked = true;
			RaiseValueChanged();
		}

		private void RaiseValueChanged()
		{
			var handler = ValueChanged;
			if (handler != null)
				handler();
		}
	}
}
