﻿using System;
using System.Windows;

// ReSharper disable once CheckNamespace
namespace GUISDK.Components
{
	public abstract class SidePanelButtonModel : BaseModel, IDisposable
	{
		public string Title
		{
			get { return NotifyPropertyGet(() => Title); }
			set { NotifyPropertySet(() => Title, value); }
		}

		public bool IsVisible
		{
			get { return NotifyPropertyGet(() => IsVisible); }
			set { NotifyPropertySet(() => IsVisible, value); }
		}

		public bool IsSelected
		{
			get { return NotifyPropertyGet(() => IsSelected); }
			internal set { NotifyPropertySet(() => IsSelected, value); }
		}

		protected SidePanelButtonModel()
		{
			IsVisible = true;
			Title = "EmptyButton";
		}

		public void Dispose()
		{
			DisposeInternal();
		}

		public override string ToString()
		{
			return Title;
		}

		protected internal abstract FrameworkElement GetContent();

		protected internal virtual void OnUnselected()
		{

		}

		protected virtual void DisposeInternal()
		{

		}
	}
}
