﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Animation;

// ReSharper disable once CheckNamespace
namespace GUISDK.Components
{
	public partial class SidePanel : INotifyPropertyChanged, IDisposable
	{
		public event PropertyChangedEventHandler PropertyChanged;
		private void RaisePropertyChanged(string propertyName)
		{
			var handler = PropertyChanged;
			if (handler != null)
				handler(this, new PropertyChangedEventArgs(propertyName));
		}

		public static readonly DependencyProperty TopButtonsProperty = DependencyProperty.Register(
			"TopButtons",
			typeof(ObservableCollection<SidePanelButtonModel>),
			typeof(SidePanel),
			new FrameworkPropertyMetadata(null));

		public ObservableCollection<SidePanelButtonModel> TopButtons
		{
			get { return (ObservableCollection<SidePanelButtonModel>)GetValue(TopButtonsProperty); }
			set { SetValue(TopButtonsProperty, value); }
		}

		public static readonly DependencyProperty BottomButtonsProperty = DependencyProperty.Register(
			"BottomButtons",
			typeof(ObservableCollection<SidePanelButtonModel>),
			typeof(SidePanel),
			new FrameworkPropertyMetadata(null));

		public ObservableCollection<SidePanelButtonModel> BottomButtons
		{
			get { return (ObservableCollection<SidePanelButtonModel>)GetValue(BottomButtonsProperty); }
			set { SetValue(BottomButtonsProperty, value); }
		}

		public static readonly DependencyProperty TopButtonsDataTemplateProperty = DependencyProperty.Register(
			"TopButtonsDataTemplate",
			typeof(DataTemplate),
			typeof(SidePanel),
			new FrameworkPropertyMetadata(null));

		public DataTemplate TopButtonsDataTemplate
		{
			get { return (DataTemplate)GetValue(TopButtonsDataTemplateProperty); }
			set { SetValue(TopButtonsDataTemplateProperty, value); }
		}

		public static readonly DependencyProperty BottomButtonsDataTemplateProperty = DependencyProperty.Register(
			"BottomButtonsDataTemplate",
			typeof(DataTemplate),
			typeof(SidePanel),
			new FrameworkPropertyMetadata(null));

		public DataTemplate BottomButtonsDataTemplate
		{
			get { return (DataTemplate)GetValue(BottomButtonsDataTemplateProperty); }
			set { SetValue(BottomButtonsDataTemplateProperty, value); }
		}

		private static readonly DependencyProperty ButtonsPanelWidthProperty = DependencyProperty.Register(
			"ButtonsPanelWidth",
			typeof(double),
			typeof(SidePanel),
			new FrameworkPropertyMetadata(0.0,
				FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
				ButtonsPanelWidthChanged));

		private double ButtonsPanelWidth
		{
			get { return (double)GetValue(ButtonsPanelWidthProperty); }
			set { SetValue(ButtonsPanelWidthProperty, value); }
		}

		private bool _isClosed;
		public bool IsClosed
		{
			get { return _isClosed; }
			set
			{
				if (value != _isClosed)
				{
					_isClosed = value;
					RaisePropertyChanged("IsClosed");
				}
			}
		}

		private FrameworkElement _defaultContent;
		public FrameworkElement DefaultContent
		{
			get { return _defaultContent; }
			set
			{
				if (value != _defaultContent)
				{
					_defaultContent = value;
					RaisePropertyChanged("DefaultContent");
				}
			}
		}

		private FrameworkElement _currentContent;
		public FrameworkElement CurrentContent
		{
			get { return _currentContent; }
			private set
			{
				if (value != _currentContent)
				{
					_currentContent = value;
					RaisePropertyChanged("CurrentContent");
				}
			}
		}

		public ICommand CloseCommand { get; private set; }
		public SidePanelButtonModel SelectedButton { get; private set; }

		private const double ShowDureation = 0.35;

		private readonly DoubleAnimation _animationClose;
		private readonly DoubleAnimation _animationShow;

		public SidePanel()
		{
			CloseCommand = new RelayCommand(OnCloseCommand);
			InitializeComponent();

			_animationClose = new DoubleAnimation();
			_animationClose.Completed += AnimationCloseOnCompleted;
			_animationShow = new DoubleAnimation();
			_animationShow.Completed += AnimationShowOnCompleted;

			Loaded += OnLoaded;
		}

		public void Dispose()
		{
			if (TopButtons != null)
			{
				foreach (var sidePanelButtonModel in TopButtons)
					sidePanelButtonModel.Dispose();
			}
			if (BottomButtons != null)
			{
				foreach (var sidePanelButtonModel in BottomButtons)
					sidePanelButtonModel.Dispose();
			}
		}

		public void SelectButton(SidePanelButtonModel buttonModel)
		{
			if (SelectedButton != null && SelectedButton == buttonModel)
			{
				UnselectButton();
				return;
			}

			var content = buttonModel.GetContent();
			if (content == null)
				return;

			if (SelectedButton != null)
				UnselectButton();

			ShowNewContent(content);
			buttonModel.IsSelected = true;
			SelectedButton = buttonModel;
		}

		public void UnselectButton()
		{
			HideCurrentContent();
			SelectedButton.IsSelected = false;
			SelectedButton.OnUnselected();
			SelectedButton = null;
		}

		private void AnimationShowOnCompleted(object sender, EventArgs eventArgs)
		{
			IsClosed = false;
		}

		private void AnimationCloseOnCompleted(object sender, EventArgs eventArgs)
		{
			IsClosed = true;
		}

		private void OnCloseCommand(object o)
		{
			BeginAnimation(ButtonsPanelWidthProperty, IsClosed ? _animationShow : _animationClose);
		}

		private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
		{
			Loaded -= OnLoaded;
			CurrentContent = DefaultContent;
			ButtonsPanelWidth = ButtonPanel.ActualWidth;

			_animationShow.From = 0;
			_animationShow.To = ButtonsPanelWidth;
			_animationShow.Duration = TimeSpan.FromSeconds(ShowDureation);
			_animationClose.From = ButtonsPanelWidth;
			_animationClose.To = 0;
			_animationClose.Duration = TimeSpan.FromSeconds(ShowDureation);
		}

		private void ShowNewContent(FrameworkElement newContent)
		{
			CurrentContent = newContent;
		}

		private void HideCurrentContent()
		{
			CurrentContent = DefaultContent;
		}

		private static void ButtonsPanelWidthChanged(DependencyObject dependencyObject,
			DependencyPropertyChangedEventArgs eventArgs)
		{
			var sender = dependencyObject as SidePanel;
			if (sender == null)
				return;

			sender.ButtonPanel.Width = (double)eventArgs.NewValue;
		}

		private void OnItemContainerClick(object sender, MouseButtonEventArgs e)
		{
			var container = sender as ContentPresenter;
			if (container == null)
				return;

			var model = container.DataContext as SidePanelButtonModel;
			if (model != null)
				SelectButton(model);
		}
	}
}
