﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Reflection;
using SIICore.Data;

namespace GUISDK.Components
{
	public partial class TreeViewControl
    {
        public Action NodeCreated;
        public Action<List<Node>> SubnodeCreated;
        public Action<List<Node>> NodeDeleted;
        public Action<Node> SelectedNodeChanged;
        public Action<SIICore.Data.Frame> FrameUpdated;

		public List<Node> nodes = new List<Node>();
        private readonly List<Node> selectedNodes = new List<Node>();

        public TreeViewControl()
        {
            InitializeComponent();

            AllowMultiSelection(treeView);
        }

        private Node GetNode(TreeViewItem treeViewItem)
        {
            return nodes.Find(item => item.TreeViewNode == treeViewItem);
        }

        public void Clear()
        {
            treeView.Items.Clear();
        }

        public void NewNode(Node node)
        {
            //nodes.Add(node);
            treeView.Items.Add(node.TreeViewNode);
        }

        public void NewSubnode(Node parent, Node child)
        {
            //nodes.Add(child);
            parent.TreeViewNode.Items.Add(child.TreeViewNode);
            parent.TreeViewNode.IsExpanded = true;
        }

        public void DeleteNode(Node node)
        {
            //nodes.Remove(node);

            if (node.TreeViewNode.Items.Count > 0)
            {
                for (; node.TreeViewNode.Items.Count > 0; )
                {
                    var item = node.TreeViewNode.Items[0] as TreeViewItem;
                    node.TreeViewNode.Items.Remove(item);
                    treeView.Items.Add(item);
                }
            }
            if (node.TreeViewNode.Parent is TreeView)//TODO касты.. is а потом as. Делай as в локал переменку и Проверяй ее на нул.
            {//TODO каст. если нул? Строгое приведение
                (node.TreeViewNode.Parent as TreeView).Items.Remove(node.TreeViewNode);
            }
            else
            {//TODO плохо. А если и не то и не то? Странная логика вообще.. DeleteNode и там может быть TreeView?! Я думал всегда тривьюайтем
                (node.TreeViewNode.Parent as TreeViewItem).Items.Remove(node.TreeViewNode);
            }
        }

        public bool NewLink(Node parent, Node child)
        {
            try
            {
                treeView.Items.Remove(child.TreeViewNode);
                parent.TreeViewNode.Items.Add(child.TreeViewNode);
                parent.TreeViewNode.ExpandSubtree();
                child.IsSelected = true;
                return true;
            }
            catch
            {
                treeView.Items.Add(child.TreeViewNode);
                return false;
            }
        }

        public void DeleteLink(Node parent, Node child)
        {
            parent.TreeViewNode.Items.Remove(child.TreeViewNode);
            treeView.Items.Add(child.TreeViewNode);
        }

        private void NewNode_Click(object sender, RoutedEventArgs e)
        {
            NodeCreated();
        }

        private void NewSubnode_Click(object sender, RoutedEventArgs e)
		{
            var tempSelectedNodes = new List<Node>();
            foreach (Node node in selectedNodes)
                tempSelectedNodes.Add(node);

            SubnodeCreated(tempSelectedNodes);
        }

        private void DeleteNode_Click(object sender, RoutedEventArgs e)
		{
            var tempSelectedNodes = new List<Node>();
            foreach (Node node in selectedNodes)
                tempSelectedNodes.Add(node);

            NodeDeleted(tempSelectedNodes);
        }

        private void treeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            treeView.SelectedItemChanged -= treeView_SelectedItemChanged;

            var selectedItem = treeView.SelectedItem as TreeViewItem;
            var selectedNode = GetNode(selectedItem);
            if (selectedNode == null)
            {
                treeView.SelectedItemChanged += treeView_SelectedItemChanged;
                return;
            }

            if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
            {
                var isSelectionChangeActive = IsSelectionChangeActiveProperty.GetValue(treeView, null);

                IsSelectionChangeActiveProperty.SetValue(treeView, true, null);
                selectedNodes.ForEach(item => item.IsSelected = true);

                IsSelectionChangeActiveProperty.SetValue(treeView, isSelectionChangeActive, null);
            }
            else
            {
                selectedNodes.ForEach(item => item.IsSelected = false);
                selectedNodes.Clear();
            }

            if (!selectedNodes.Contains(selectedNode))
            {
                selectedNode.IsSelected = true;
                selectedNodes.Add(selectedNode);
            }
            else
            {
                selectedNode.IsSelected = false;
                selectedNodes.Remove(selectedNode);
            }

            if (selectedNodes.Count == 1)
                SelectedNodeChanged(selectedNode);
            else
                SelectedNodeChanged(null);

            treeView.SelectedItemChanged += treeView_SelectedItemChanged;
        }

        private void treeView_PreviewMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            var originalSource = e.OriginalSource as DependencyObject;
            TreeViewItem treeViewItem = VisualUpwardSearch(originalSource);

            if (treeViewItem != null)
            {
                treeViewItem.Focus();
                e.Handled = true;
            }
        }

        static TreeViewItem VisualUpwardSearch(DependencyObject source)
		{
            while (source != null && !(source is TreeViewItem))
                source = VisualTreeHelper.GetParent(source);
		
            return source as TreeViewItem;
        }





        //--------------------------------------------------------------------
        private static readonly PropertyInfo IsSelectionChangeActiveProperty
            = typeof(TreeView).GetProperty
            (
              "IsSelectionChangeActive",
              BindingFlags.NonPublic | BindingFlags.Instance
            );

        private void AllowMultiSelection(TreeView treeView)
        {
            if (IsSelectionChangeActiveProperty == null) 
                return;

            treeView.SelectedItemChanged += treeView_SelectedItemChanged;
        }




        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                var selectedItem = treeView.SelectedItem as TreeViewItem;
                var selectedNode = GetNode(selectedItem);
                var txtFrameName = sender as TextBox;
                selectedNode.Name = txtFrameName.Text;
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            foreach (Node node in nodes)
            {
                FrameUpdated(node.FrameNode);
            }
        }
    }
}
