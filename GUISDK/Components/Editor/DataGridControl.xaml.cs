﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using SIICore.Data;
using SIICore.Models;
using Microsoft.Win32;

namespace GUISDK.Components
{
    public partial class DataGridControl
    {
        public Action<SIICore.Data.Frame, Slot> SlotCreated;
        public Action<SIICore.Data.Frame, Slot> SlotDeleted;
        public Action<SIICore.Data.Frame> FrameUpdated;

        public SIICore.Data.Frame ActiveFrame { get; private set; }

        public List<Node> nodes = new List<Node>();

        private DataGrid dgResults;

        public DataGridControl()
        {
            InitializeComponent();
        }

        private Node GetNode(SIICore.Data.Frame frame)
        {
            return nodes.Find(item => item.FrameNode == frame);
        }

        private SIICore.Data.Frame GetFrameSystem()
        {
            return nodes.Find(item => item.FrameNode.Name == "System").FrameNode;
        }

        private SIICore.Data.Frame GetFrameById(string id)
        {
            return nodes.Find(item => item.FrameNode.Id == id).FrameNode;
        }

        private void CreateDomains(SIICore.Data.Frame frame, List<string> domains)
        {
            foreach (SIICore.Data.Frame child in frame.children)
            {
                if ((child.children.Count > 0) || (child.Type == SIICore.Data.Frame.FrameType.System))
                    domains.Add(DomainHelper.getInnerType(child));
            }
            foreach (SIICore.Data.Frame child in frame.children)
            {
                if (child.children.Count > 0)
                    CreateDomains(child, domains);
            }
        }

        private void CreateDefaultValues(SIICore.Data.Frame frame, List<Object> defaultValues)
        {
            foreach (SIICore.Data.Frame child in frame.children)
            {
                defaultValues.Add(DomainHelper.getInnerType(child));
            }
            foreach (SIICore.Data.Frame child in frame.children)
            {
                CreateDefaultValues(child, defaultValues);
            }
        }

        public void LoadSlots(SIICore.Data.Frame frame)
        {
            btnAddResult.Visibility = btnUpdateResult.Visibility = btnDeleteResult.Visibility = Visibility.Hidden;

            dataGrid.ItemsSource = null;
            if (frame == null)
                return;

            ActiveFrame = frame;
            dataGrid.ItemsSource = ActiveFrame.slots;
            dataGrid.IsEnabled = ActiveFrame.Type != SIICore.Data.Frame.FrameType.System;
            txtName.IsEnabled = ActiveFrame.Type != SIICore.Data.Frame.FrameType.System;
            txtName.Text = ActiveFrame.Name;
            UpdateDomains();
        }

        private void UpdateDomain(Slot slot)
        {
            var domains = new List<string>();
            domains.Add(DomainHelper.getInnerType(""));
            domains.Add(DomainHelper.getInnerType(0));
            domains.Add(DomainHelper.getInnerType("file"));
            domains.Add(DomainHelper.getInnerType("result"));
            var frameSystem = GetFrameSystem();
            CreateDomains(frameSystem, domains);
            slot.DefaultValue.Types = domains;

            slot.DefaultValue.Data = slot.DefaultValue.Data; // =)

            LoadDatas(slot.DefaultValue.Type, slot);
        }

        private void UpdateDomains()
        {
            foreach (Slot slot in ActiveFrame.slots)
            {
                UpdateDomain(slot);
            }
        }

        private void LoadDatas(string type, Slot slot)
        {
            switch (type.Split('@')[0])
            {
                case "FrameType":
                    LoadDefaultValues(type, slot);
                    break;
                case "ResultType":
                    LoadResults(slot.DefaultValue.Data.ToString(), slot);
                    break;
            }
        }

        private void LoadDefaultValues(string type, Slot slot)
        {
            var domainType = DomainHelper.getDomainType(type);
            if (domainType == DomainTypes.FrameType)
            {
                var defaultValues = new List<Object>();
                var frame = GetFrameById(DomainHelper.getFrameId(type));
                defaultValues.Add(DomainHelper.getInnerType(frame));
                CreateDefaultValues(frame, defaultValues);
                slot.DefaultValue.Datas = defaultValues;
            }
        }

        private void LoadResults(string strResult, Slot slot)
        {
            var results = ResultHelper.getResults(strResult);
            foreach(Result result in results)
                LoadFrames(result);
            slot.DefaultValue.Datas = results;
            if (dgResults != null)
                dgResults.ItemsSource = slot.DefaultValue.Datas;
        }   

        private void LoadFrames(Result result)
        {
            var frames = new List<string>();
            foreach (Slot slot in ActiveFrame.slots)
            {
                string data = slot.DefaultValue.Data.ToString();
                if (data.StartsWith("FrameType"))
                    frames.Add(data);
            } 
            if (frames.Count > 0)
            {
                if (!frames.Contains(result.Frame))
                    result.Frame = frames[0];
                LoadFrameSlots(GetFrameById(DomainHelper.getFrameId(result.Frame)), result);
            }
            else
            {
                result.Frame = null;
                result.Slot = null;
                result.Slots = null;
            }
            result.Frames = frames;
        }

        private void LoadFrameSlots(SIICore.Data.Frame frame, Result result)
        {
            var slots = new List<string>();
            foreach (Slot slot in frame.slots)
            {
                if (slot.DefaultValue.Type == "NumberType@@")
                {
                    slots.Add(DomainHelper.getInnerType(slot));
                }
            }
            if (slots.Count > 0)
                if (!slots.Contains(result.Slot))
                    result.Slot = slots[0];
            result.Slots = slots;
        }

        private void UpdateFrames()
        {
            var slotResult = ActiveFrame.slots.Find(slot => slot.DefaultValue.Type.StartsWith("ResultType"));
            if (slotResult == null)
                return;

            foreach (Result result in slotResult.DefaultValue.Datas)
            {
                LoadFrames(result);
            }
        }

        private void NewSlot_Click(object sender, RoutedEventArgs e)
        {
            if (ActiveFrame == null)
                return;

            var slot = new Slot("Новый слот", new Value(DomainHelper.getInnerType(""), ""), Slot.Inheritance.Override);
            UpdateDomain(slot);
            SlotCreated(ActiveFrame, slot);
            dataGrid.Items.Refresh();

            UpdateFrames();
        }

        private void DeleteSlot_Click(object sender, RoutedEventArgs e)
        {
            var selectedSlot = dataGrid.SelectedItem as Slot;
            if (selectedSlot == null)
                return;

            SlotDeleted(ActiveFrame, selectedSlot);
            dataGrid.Items.Refresh();

            UpdateFrames();
        }

        private void UpdateFrame_Click(object sender, RoutedEventArgs e)
        {
            FrameUpdated(ActiveFrame);
        }

        private void cbType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedSlot = dataGrid.SelectedItem as Slot;
            var cbType = sender as ComboBox;
            var type = cbType.SelectedItem.ToString();

            LoadDatas(type, selectedSlot);
            switch (type.Split('@')[0])
            {
                case "NumberType":
                    selectedSlot.DefaultValue.Data = 0;
                    break;
                case "StringType":
                    selectedSlot.DefaultValue.Data = "";
                    break;
                case "FileType":
                    selectedSlot.DefaultValue.Data = DomainHelper.getInnerType("file");
                    break;
                case "ResultType":
                    selectedSlot.DefaultValue.Data = DomainHelper.getInnerType("result");
                    break;
                case "FrameType":
                    selectedSlot.DefaultValue.Data = selectedSlot.DefaultValue.Datas[0];
                    break;
            }
        }

        private void TextBox_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            var selectedSlot = dataGrid.SelectedItem as Slot;
            if ((!selectedSlot.DefaultValue.Type.StartsWith("NumberType")) && (!selectedSlot.DefaultValue.Type.StartsWith("ResultType")))
                return;
            int num;
            if (!int.TryParse(e.Text, out num))
                e.Handled = true;
        }

        private void txtName_TextChanged(object sender, TextChangedEventArgs e)
        {
            var node = GetNode(ActiveFrame);
            node.Name = txtName.Text;
        }

        private void btnOpenDialog_Click(object sender, RoutedEventArgs e)
        {
            var openDialog = new OpenFileDialog();
            if (openDialog.ShowDialog().Value)
            {
                var selectedSlot = dataGrid.SelectedItem as Slot;
                selectedSlot.DefaultValue.Data = DomainHelper.getInnerType("file") + openDialog.FileName;
            }
        }

        private void dataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedSlot = dataGrid.SelectedItem as Slot;
            if (selectedSlot == null)
                return;

            if (selectedSlot.DefaultValue.Data.ToString().StartsWith("ResultType"))
            {
                dataGrid.RowDetailsVisibilityMode = DataGridRowDetailsVisibilityMode.VisibleWhenSelected;
                btnAddResult.Visibility = btnUpdateResult.Visibility = btnDeleteResult.Visibility = Visibility.Visible;
            }
            else
            {
                dataGrid.RowDetailsVisibilityMode = DataGridRowDetailsVisibilityMode.Collapsed;
                btnAddResult.Visibility = btnUpdateResult.Visibility = btnDeleteResult.Visibility = Visibility.Hidden;
            }
        }

        private void cbFrame_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedSlot = dataGrid.SelectedItem as Slot;
            var cbFrame = sender as ComboBox;
            var frame = cbFrame.SelectedItem.ToString();
            var selectedResult = dgResults.SelectedItem as Result;
            if (selectedResult != null)
            {
                selectedResult.Frame = frame;
                LoadFrameSlots(GetFrameById(DomainHelper.getFrameId(frame)), selectedResult);
            }
        }

        private void cbSlot_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedSlot = dataGrid.SelectedItem as Slot;
            var cbSlot = sender as ComboBox;
            var slot = cbSlot.SelectedItem.ToString();
            var selectedResult = dgResults.SelectedItem as Result;
            if (selectedResult != null)
                selectedResult.Slot = slot;
        }

        private void cbOperation_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedSlot = dataGrid.SelectedItem as Slot;
            var cbOperation = sender as ComboBox;
            var operation = cbOperation.SelectedItem.ToString();
            var selectedResult = dgResults.SelectedItem as Result;
            if (selectedResult != null)
                selectedResult.Operation = operation;
        }

        private void txtValue_TextChanged(object sender, TextChangedEventArgs e)
        {
            var selectedSlot = dataGrid.SelectedItem as Slot;
            var txtValue = sender as TextBox;
            var value = txtValue.Text;
            var selectedResult = dgResults.SelectedItem as Result;
            if (selectedResult != null)
                selectedResult.Value = value;
        }

        private void DataGrid_Initialized(object sender, EventArgs e)
        {
            dgResults = sender as DataGrid;
        }

        private void cbData_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var cbData = sender as ComboBox;
            var selectedSlot = dataGrid.SelectedItem as Slot;
            selectedSlot.DefaultValue.Data = cbData.SelectedItem;

            UpdateFrames();
        }

        private void btnAddResult_Click(object sender, RoutedEventArgs e)
        {
            var selectedSlot = dataGrid.SelectedItem as Slot;
            if (selectedSlot == null)
                return;

            var result = new Result();
            LoadFrames(result);
            selectedSlot.DefaultValue.Datas.Add(result);
            dgResults.Items.Refresh();
        }

        private void btnDeleteResult_Click(object sender, RoutedEventArgs e)
        {
            var selectedSlot = dataGrid.SelectedItem as Slot;
            if (selectedSlot == null)
                return;
            var selectedResult = dgResults.SelectedItem as Result;
            if (selectedResult == null)
                return;

            selectedSlot.DefaultValue.Datas.Remove(selectedResult);
            dgResults.Items.Refresh();
        }

        private void btnUpdateResult_Click(object sender, RoutedEventArgs e)
        {
            var selectedSlot = dataGrid.SelectedItem as Slot;
            if (selectedSlot == null)
                return;

            selectedSlot.DefaultValue.Data = ResultHelper.getStrResult(selectedSlot.DefaultValue.Datas);
        }
    }
}
