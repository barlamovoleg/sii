﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using MindFusion.Diagramming.Wpf;
using SIICore.Data;

namespace GUISDK.Components
{
    public class Node
    {
        public System.Windows.Controls.TreeViewItem TreeViewNode { get; private set; }

        public ShapeNode DiagramNode { get; private set; }

        public SIICore.Data.Frame FrameNode { get; private set; }

        private bool _isSelected;
        public bool IsSelected 
        {
            get
            {
                return _isSelected;
            }
            set 
            {
                _isSelected = value;
                TreeViewNode.IsSelected = value;
                DiagramNode.Selected = value; 
            } 
        }

        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                TreeViewNode.Header = value;
                DiagramNode.Text = value;
                FrameNode.Name = value;
            }
        }

        public Node()
        {
            FrameNode = new SIICore.Data.Frame("Новый фрейм");
            TreeViewNode = new System.Windows.Controls.TreeViewItem();
            DiagramNode = new ShapeNode()
            {
                TextAlignment = TextAlignment.Center,
                Shape = Shapes.RoundRect
            };
            Name = FrameNode.Name;
        }

        public Node(SIICore.Data.Frame frame)
        {
            FrameNode = frame;
            TreeViewNode = new System.Windows.Controls.TreeViewItem();
            DiagramNode = new ShapeNode()
            {
                TextAlignment = TextAlignment.Center,
                Shape = Shapes.RoundRect
            };
            Name = FrameNode.Name;
        }
    }
}
