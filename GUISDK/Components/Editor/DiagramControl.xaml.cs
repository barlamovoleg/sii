﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using MindFusion.Diagramming.Wpf;
using SIICore.Data;

namespace GUISDK.Components
{
	public partial class DiagramControl
    {
        private const int NEW_NODE_POSITION_X = 5;
        private const int NEW_NODE_POSITION_Y = 33;

        public System.Action NodeCreated;
        public Action<List<Node>> SubnodeCreated;
        public Action<List<Node>> NodeDeleted;
        public Func<Node, Node, bool> LinkCreated;
        public Func<Node, Node, bool> LinkDeleted;
        public Action<Node> SelectedNodeChanged;
        public System.Action<Frame> FrameUpdated;

        private Point lastMouseDown = new Point(NEW_NODE_POSITION_X, NEW_NODE_POSITION_Y);

        public List<Node> nodes = new List<Node>();

        public DiagramControl()
        {
            InitializeComponent();
        }

        private Node GetNode(ShapeNode shapeNode)
        {
            return nodes.Find(item => item.DiagramNode == shapeNode);
        }

        public void Clear()
        {
            diagram.NodeDeleted -= diagram_NodeDeleted;
            diagram.LinkDeleted -= diagram_LinkDeleted;

            for (; diagram.Nodes.Count > 0; )
                diagram.Nodes.RemoveAt(0);
            for (; diagram.Links.Count > 0; )
                diagram.Links.RemoveAt(0);

            diagram.NodeDeleted += diagram_NodeDeleted;
            diagram.LinkDeleted += diagram_LinkDeleted;
        }

        public void NewNode(Node node)
        {
            //nodes.Add(node);
            if ((node.FrameNode.Position.X == 0) && (node.FrameNode.Position.Y == 0) && (node.FrameNode.Position.Width == 0) && (node.FrameNode.Position.Height == 0))
                node.FrameNode.Position = new Rect(lastMouseDown.X, lastMouseDown.Y, node.DiagramNode.Text.Length * 9, 23);
            node.DiagramNode.Bounds = node.FrameNode.Position;
            diagram.Items.Add(node.DiagramNode);
            FrameUpdated(node.FrameNode);
            
            lastMouseDown.X = NEW_NODE_POSITION_X;
            lastMouseDown.Y = NEW_NODE_POSITION_Y;
        }

        public void NewSubnode(Node parent, Node child)
		{
            //nodes.Add(child);
            double newNodeLeft = parent.DiagramNode.IncomingLinks.Count == 0 ? parent.DiagramNode.Bounds.Left : parent.DiagramNode.IncomingLinks[parent.DiagramNode.IncomingLinks.Count - 1].Origin.Bounds.Right + 20;
            double newNodeTop = parent.DiagramNode.IncomingLinks.Count == 0 ? parent.DiagramNode.Bounds.Bottom + 30 : parent.DiagramNode.IncomingLinks[parent.DiagramNode.IncomingLinks.Count - 1].Origin.Bounds.Top;
            child.FrameNode.Position = new Rect(newNodeLeft, newNodeTop, child.DiagramNode.Text.Length * 9, 23);
            child.DiagramNode.Bounds = child.FrameNode.Position;
            diagram.Items.Add(child.DiagramNode);
            FrameUpdated(child.FrameNode);

            NewLink(parent, child);
        }

        public void DeleteNode(Node node)
        {
            diagram.NodeDeleted -= diagram_NodeDeleted;
            diagram.LinkDeleted -= diagram_LinkDeleted;

            //nodes.Remove(node);
            diagram.Nodes.Remove(node.DiagramNode);

            diagram.NodeDeleted += diagram_NodeDeleted;
            diagram.LinkDeleted += diagram_LinkDeleted;
        }

        public bool NewLink(Node parent, Node child)
        {
            var newLink = new DiagramLink(diagram, child.DiagramNode, parent.DiagramNode);
            diagram.Links.Add(newLink);
            return true;
        }

        public void DeleteLink(Node parent, Node child)
        {
            diagram.LinkDeleted -= diagram_LinkDeleted;
            if (child.DiagramNode.OutgoingLinks.Count > 0)
                diagram.Links.Remove(child.DiagramNode.OutgoingLinks[0]);
            diagram.LinkDeleted += diagram_LinkDeleted;
        }

        private void NewNode_Click(object sender, RoutedEventArgs e)
        {
            NodeCreated();
        }

        private void NewSubnode_Click(object sender, RoutedEventArgs e)
        {
            var tempSelectedNodes = new List<Node>();
            foreach (ShapeNode node in diagram.Selection.Nodes)
            {
                var parent = GetNode(node);
                tempSelectedNodes.Add(parent);
            }

            SubnodeCreated(tempSelectedNodes);
        }

        private void DeleteNode_Click(object sender, RoutedEventArgs e)
        {
            diagram.NodeDeleted -= diagram_NodeDeleted;

            var tempSelectedNodes = new List<Node>();
            foreach (ShapeNode node in diagram.Selection.Nodes)
            {
                var parent = GetNode(node);
                tempSelectedNodes.Add(parent);
            }

            NodeDeleted(tempSelectedNodes);

            diagram.NodeDeleted += diagram_NodeDeleted;
        }

        private void diagram_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            lastMouseDown.X = e.GetPosition(diagram).X;
            lastMouseDown.Y = e.GetPosition(diagram).Y;

            var tempNode = diagram.Nodes.FirstOrDefault(node => (lastMouseDown.X >= node.Bounds.Left) && (lastMouseDown.X <= node.Bounds.Right) && (lastMouseDown.Y >= node.Bounds.Top) && (lastMouseDown.Y <= node.Bounds.Bottom)) as ShapeNode;
            if ((tempNode != null) && (tempNode.Selected == false))
            {
                tempNode.Selected = true;
            }
        }

        private void diagram_LinkCreated(object sender, LinkEventArgs e)
		{
            var linkOrigin = e.Link.Origin as ShapeNode;
            var linkDestination = e.Link.Destination as ShapeNode;
            Node origin = GetNode(linkOrigin);
            Node destination = GetNode(linkDestination);
            if ((linkDestination.OutgoingLinks.Count > 1) || (!LinkCreated(destination, origin)))
            {
                diagram.LinkDeleted -= diagram_LinkDeleted;
                diagram.Links.Remove(e.Link);
                diagram.LinkDeleted += diagram_LinkDeleted;
            }
        }

        private void diagram_LinkDeleted(object sender, LinkEventArgs e)
        {
            var linkOrigin = e.Link.Origin as ShapeNode;
            var linkDestination = e.Link.Destination as ShapeNode;
            Node origin = GetNode(linkOrigin);
            Node destination = GetNode(linkDestination);
            if (!LinkDeleted(destination, origin))
            {
                diagram.LinkCreated -= diagram_LinkCreated;
                diagram.Links.Add(new DiagramLink(diagram, origin.DiagramNode, destination.DiagramNode));
                diagram.LinkCreated += diagram_LinkCreated;
            }
        }

        private void diagram_SelectionChanged(object sender, EventArgs e)
        {
            diagram.SelectionChanged -= diagram_SelectionChanged;

            if ((diagram.Selection.Nodes.Count > 0) && (diagram.Selection.Links.Count > 0))
                for (; diagram.Selection.Links.Count > 0; )
                    diagram.Selection.Links[0].Selected = false;

            foreach (Node node in nodes)
                node.IsSelected = node.DiagramNode.Selected;

            if (diagram.Selection.Nodes.Count == 1)
            {
                var selectedNode = diagram.Selection.Nodes[0] as ShapeNode;
                SelectedNodeChanged(GetNode(selectedNode));
            }
            else
                SelectedNodeChanged(null);
            

            diagram.SelectionChanged += diagram_SelectionChanged;
        }

        private void diagram_NodeDeleted(object sender, NodeEventArgs e)
        {
            var tempSelectedNodes = new List<Node>();
            var deletedNode = e.Node as ShapeNode;
            tempSelectedNodes.Add(GetNode(deletedNode));

            NodeDeleted(tempSelectedNodes);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void diagram_NodeModified(object sender, NodeEventArgs e)
        {
            var node = GetNode(e.Node as ShapeNode);
            node.FrameNode.Position = e.Node.Bounds;
            FrameUpdated(node.FrameNode);
        }
    }
}
