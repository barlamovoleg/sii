﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Animation;

// ReSharper disable once CheckNamespace
namespace GUISDK.Components
{
	public partial class LPanel : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;
		private void RaisePropertyChanged(string propertyName)
		{
			var handler = PropertyChanged;
			if (handler != null)
				handler(this, new PropertyChangedEventArgs(propertyName));
		}

		private static readonly DependencyProperty LeftPanelContentProperty = DependencyProperty.Register(
			"LeftPanelContent",
			typeof(FrameworkElement),
			typeof(LPanel),
			new PropertyMetadata(null));

		public FrameworkElement LeftPanelContent
		{
			get { return (FrameworkElement)GetValue(LeftPanelContentProperty); }
			set { SetValue(LeftPanelContentProperty, value); }
		}

		private static readonly DependencyProperty BottomPanelContentProperty = DependencyProperty.Register(
			"BottomPanelContent",
			typeof(FrameworkElement),
			typeof(LPanel),
			new PropertyMetadata(null));

		public FrameworkElement BottomPanelContent
		{
			get { return (FrameworkElement)GetValue(BottomPanelContentProperty); }
			set { SetValue(BottomPanelContentProperty, value); }
		}

		private static readonly DependencyProperty CentralContentProperty = DependencyProperty.Register(
			"CentralContent",
			typeof(FrameworkElement),
			typeof(LPanel),
			new PropertyMetadata(null));

		public FrameworkElement CentralContent
		{
			get { return (FrameworkElement)GetValue(CentralContentProperty); }
			set { SetValue(CentralContentProperty, value); }
		}

		private static readonly DependencyProperty PanelsWidthProperty = DependencyProperty.Register(
			"PanelsWidth",
			typeof(double),
			typeof(LPanel),
			new FrameworkPropertyMetadata(0.0,
				FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
				ButtonsPanelWidthChanged));

		private double PanelsWidth
		{
			get { return (double)GetValue(PanelsWidthProperty); }
			set { SetValue(PanelsWidthProperty, value); }
		}

		private bool _isClosed;
		public bool IsClosed
		{
			get { return _isClosed; }
			set
			{
				if (value != _isClosed)
				{
					_isClosed = value;
					RaisePropertyChanged("IsClosed");
				}
			}
		}

		private double _panelsSize = 200;
		public double PanelsSize
		{
			get { return _panelsSize; }
			set
			{
				if (Math.Abs(value - _panelsSize) > float.Epsilon)
				{
					_panelsSize = value;
					RaisePropertyChanged("PanelsSize");
				}
			}
		}

		public ICommand CloseCommand { get; private set; }

		private const double ShowDureation = 0.35;

		private readonly DoubleAnimation _animationClose;
		private readonly DoubleAnimation _animationShow;


		public LPanel()
		{
			CloseCommand = new RelayCommand(OnCloseCommand);
			InitializeComponent();

			_animationClose = new DoubleAnimation();
			_animationClose.Completed += AnimationCloseOnCompleted;
			_animationShow = new DoubleAnimation();
			_animationShow.Completed += AnimationShowOnCompleted;

			Loaded += OnLoaded;
		}

		private void AnimationShowOnCompleted(object sender, EventArgs eventArgs)
		{
			IsClosed = false;
		}

		private void AnimationCloseOnCompleted(object sender, EventArgs eventArgs)
		{
			IsClosed = true;
		}

		private void OnCloseCommand(object o)
		{
			BeginAnimation(PanelsWidthProperty, IsClosed ? _animationShow : _animationClose);
		}

		private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
		{
			Loaded -= OnLoaded;
			PanelsWidth = PanelsSize;

			_animationShow.From = 0;
			_animationShow.To = PanelsSize;
			_animationShow.Duration = TimeSpan.FromSeconds(ShowDureation);
			_animationClose.From = PanelsSize;
			_animationClose.To = 0;
			_animationClose.Duration = TimeSpan.FromSeconds(ShowDureation);
		}

		private static void ButtonsPanelWidthChanged(DependencyObject dependencyObject,
			DependencyPropertyChangedEventArgs eventArgs)
		{
			var sender = dependencyObject as LPanel;
			if (sender == null)
				return;

			sender.RowBottom.Height = new GridLength((double)eventArgs.NewValue);
			sender.ColumnLeft.Width = new GridLength((double)eventArgs.NewValue);
		}
	}
}
