﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows;
using SIICore.Data;

namespace GUISDK.Selectors
{
    class DataEditingTemplateSelector : DataTemplateSelector
    {
        public DataTemplate StringDataTemplate { get; set; }
        public DataTemplate ComboBoxDataTemplate { get; set; }
        public DataTemplate FileDataTemplate { get; set; }
        public DataTemplate ResultDataTemplate { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            ContentPresenter presenter = container as ContentPresenter;
            DataGridCell cell = presenter.Parent as DataGridCell;
            var slot = cell.DataContext as Slot;
            if (slot.DefaultValue.Data != null)
            {
                var data = slot.DefaultValue.Data.ToString();
                string[] items = data.Split('@');
                var type = items[0];
                switch (type)
                {
                    case "FrameType":
                        return ComboBoxDataTemplate;
                    case "FileType":
                        return FileDataTemplate;
                    case "ResultType":
                        return ResultDataTemplate;
                    default:
                        return StringDataTemplate;
                }
            }
            return StringDataTemplate;
        }
    }
}
