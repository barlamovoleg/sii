﻿using System;
using System.Globalization;
using System.IO;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace GUISDK.Converters
{
	public class FilePathToImageSourceConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var filePath = (string) value;
			if (string.IsNullOrWhiteSpace(filePath))
				return null;
			if (!File.Exists(filePath))
				return null;
			try
			{
				BitmapImage image = new BitmapImage();
				image.BeginInit();
				image.CacheOption = BitmapCacheOption.OnLoad;
				image.UriSource = new Uri(filePath);
				image.EndInit();
				return image;
			}
			catch (Exception e)
			{
				return null;
			}
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
