﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using SIICore.Models;

namespace GUISDK.Converters
{
    public class DomainToStringConverter : IValueConverter
    {
        private string type;
        private string id;
        private string name;

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var type = (string)value;
            string[] items = type.Split('@');
            type = items[0];
            id = items[1];
            name = items[2];

            switch (type)
            {
                case "FrameType":
                    return name;
                case "StringType":
                    return "String";
                case "NumberType":
                    return "Number";
                case "FileType":
                    return "File";
                case "ResultType":
                    return "Result";
                default:
                    return "UnknownType";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return String.Format("{0}@{1}@{2}", type, id, name);
        }
    }
}
