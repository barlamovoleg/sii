﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using SIICore.Models;

namespace GUISDK.Converters
{
    public class DataToStringConverter : IValueConverter
    {
        private string type;
        private string id;
        private string name;

        private string result;

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null)
            {
                result = value.ToString();
                string[] items = result.Split('@');
                type = items[0];

                switch (type)
                {
                    case "FrameType":  
                        id = items[1];
                        name = items[2];
                        return name;
                    case "SlotType":
                        id = items[1];
                        name = items[2];
                        return name;
                    case "FileType":
                        name = items[2];
                        return name;
                    case "ResultType":
                        return "";
                }
            }
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            switch (type)
            {
                case "FrameType":
                case "SlotType":
                    return String.Format("{0}@{1}@{2}", type, id, name);
                case "FileType":
                    return String.Format("{0}@@{1}", type, value);
                case "ResultType":
                    return result;
                default:
                    return value;
            }
        }
    }
}
