﻿using System.Windows.Controls;
using Common.Visualizer;
using SIICore.Data;

// ReSharper disable once CheckNamespace
namespace SIICore
{
	public interface ICoreGameVisualizerService : IVisualizerService<IGameData, ContentControl>
	{
		bool IsGameDataLoaded { get; }

		ISiiGameVisualizer CreateVisualizer();

		void LoadGameData(IGameData gameData);
	}
}
