﻿using System.Collections.Generic;
using Common.Logger;

namespace SIICore
{
// ReSharper disable once InconsistentNaming
	public static class CoreLoggers
	{
		internal static ILogger SystemLog
		{
			get
			{
				if (_systemLog != null)
					return _systemLog;

				if (_loggerService == null)
					return _consoleSystemLogger;

				_systemLog = GetLogger(Common.CommonStrings.SystemLogName);
				return _systemLog;
			}
		}
		private static readonly ILogger _consoleSystemLogger = new ConsoleLogger("SystemConsoleLog");
		private static ILogger _systemLog;

		private static ILoggerService _loggerService;
		private static readonly List<ILogger> DummyLoggers = new List<ILogger>();

		public static ILogger GetLogger(string loggerName)
		{
			if (_loggerService == null)
			{
				var foundedLogger = DummyLoggers.Find(logger => logger.Name == loggerName);
				if (foundedLogger != null)
					return foundedLogger;

				var newLogger = new ConsoleLogger(loggerName);
				DummyLoggers.Add(newLogger);
				return newLogger;
			}

			return _loggerService.GetLogger(loggerName);
		}

		internal static void LoadLoggerService(ILoggerService loggerService)
		{
			_loggerService = loggerService;
		}
	}
}
