﻿// ReSharper disable once CheckNamespace
namespace SIICore
{
	internal static class CoreStrings
	{
		internal static class Exceptions
		{
			internal static string InheritorHostContainerOrGraphicHost = "Host container or Graphic host has not been set in inheritor";
			internal static string ServiceCloseableNotExist = "IServiceCloseable not set in inheritor of CoreAppHost";
		}

		internal static class System
		{
			internal static string LoggerExist = "Системный логер обнаружен.";
			internal static string LoggerNotExist = "Ни один логер не был найден, используется вывод по умолчанию.";
			internal static string LoggerServiceNotExist = "Не удалось получить сервис логирования в ядре приложения. Будет использован вывод по умолчанию.";
			internal static string VizualizerServiceNotExist = "Сервис визуализаторов не найден. Возвращен визуализатор - пустышка.";
			internal static string VizualizerServiceLoaded = "Сервис визуализатора загружен в ядро.";
			internal static string GameDataLoaded = "Данные игры загружены в сервис визуализатора.";
		}
	}
}
