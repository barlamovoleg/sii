﻿using System;
using System.Windows.Controls;
using Common.Visualizer;
using SIICore.Data;

// ReSharper disable once CheckNamespace
namespace SIICore
{
	public interface ISiiGameVisualizer : IVisualizer<IGameData, ContentControl>
	{
		event Action<IVizualizedObject> ObjectSelected;
		void ClearBrush();
		void SetDeleteBrush();
		void SetBrush(IVizualizedObject brush);
	}
}
