﻿using System;
using System.Windows.Controls;
using Common;
using SIICore.Data;

// ReSharper disable once CheckNamespace
namespace SIICore
{
	public class DumyVisualizer : ISiiGameVisualizer
	{
		public bool IsPlaying { get { return false; } }
		public bool IsAttached { get { return false; } }
		public bool IsDataSet { get { return false; } }
		public double CurrentSpeed { get { return 0; } }

		public void Dispose()
		{
		}
		public void SetData(IGameData data)
		{
		}
		public void RemoveData()
		{
		}
		public void Attach(IGraphicsHostService<ContentControl> host)
		{
		}
		public void Detach()
		{
		}
		public void Initialize()
		{
		}
		public void Play()
		{
		}
		public void Stop()
		{
		}
		public void OneStep()
		{
		}
		public void ChangePlaySpeed(double speed)
		{
		}

		public event Action<IVizualizedObject> ObjectSelected;

		public void ClearBrush()
		{
		}

		public void SetDeleteBrush()
		{
		}

		public void SetBrush(IVizualizedObject brush)
		{
		}
	}
}
