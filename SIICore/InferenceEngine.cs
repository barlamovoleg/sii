﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIICore.Data;
using SIICore.Models;

namespace SIICore
{
    public static class InferenceEngine
    {
	    private static IEnumerable<Frame> _frames;
        //может вернуть null
        public static Tuple<Frame, Frame> GetFrame(Frame frameInstance, List<Frame> frameNeighbours)
        {
            if (!ActiveRepositoryProvider.IsRepositoryReady)
                return null;

            var repository = ActiveRepositoryProvider.ActiveRepository;
			_frames = repository.GetAll();

			Frame action = _frames.FirstOrDefault(x => x.Name == Frame.SYSTEM_FRAME_ACTION);
            if (action == null)
                return null;

            var ret = new List<Frame>();
            foreach (var neighbour in frameNeighbours)
            {
                ret.Add(MakeInference(frameInstance, neighbour, action));
            }

            for (int i = 0; i < ret.Count; i++)
            {
                if (ret[i] != null)
                    return new Tuple<Frame, Frame>(ret[i], frameNeighbours[i]);
            }

            return null;
        }

        private static Frame MakeInference(Frame instance, Frame neighbour, Frame action)
        {
            foreach (var child in action.children)
            {
                var childRet = MakeInference(instance, neighbour, child);
                if (childRet != null) return childRet;
            }
            var subjectSlot = action.slots.FirstOrDefault(x => x.Name == Frame.SYSTEM_SLOT_SUBJECT);
            var objectSlot = action.slots.FirstOrDefault(x => x.Name == Frame.SYSTEM_SLOT_OBJECT);
            if (subjectSlot == null || objectSlot == null)
                return null;
            if (!Compare(instance, subjectSlot) || !Compare(neighbour, objectSlot))
                return null;
            return action;
        }

        private static bool Compare(Frame frame, Slot slot)
        {
            var slotFrameId = DomainHelper.getFrameId(slot.DefaultValue.Data.ToString());
            if (slotFrameId == null)
                return false;
            if (GetInstanceFrameId(frame) == slotFrameId)
                return true;
            if (frame.Parent == null)
                return false;
            return Compare(frame.Parent, slot);
        }

        private static string GetInstanceFrameId(Frame frame)
        {
			var objectFrame = _frames.FirstOrDefault(x => x.Name == Frame.SYSTEM_FRAME_OBJECT);
            if (objectFrame == null)
                return frame.Id;
            Frame pretendent = GetInstanceClass(frame, objectFrame);
            return pretendent != null ? pretendent.Id : frame.Id;
        }

        public static Frame GetInstanceClass(Frame instance, Frame frameObject)
        {
            foreach (var child in frameObject.children)
            {
                var childRet = GetInstanceClass(instance, child);
                if (childRet != null) return childRet;
            }
            if (instance.slots.Count < frameObject.slots.Count)
                return null;

            //если хотя бы один слот не привязался
            foreach (var slot in instance.slots)
            {
                //ищем по имени слот экземпляра среди слотов фрейма-объекта
                var objectSlot = frameObject.slots.FirstOrDefault(x => x.Name == slot.Name);
                if (objectSlot == null || objectSlot.DefaultValue.Type != slot.DefaultValue.Type ||
                    //гавно! отстоище! не четко!
                    !objectSlot.DefaultValue.Data.Equals(slot.DefaultValue.Data))
                    return null;

            }
            return frameObject;
        }
    }
}