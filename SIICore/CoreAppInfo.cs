﻿using System;
using Common;

namespace SIICore
{
	public abstract class CoreAppInfo : IAppInfo
	{
		public abstract string AppName { get; }

		public string AppFullName
		{
			get { return string.Format("{0} {1}", AppName, FullVersion); }
		}

		public string FullVersion
		{
			get
			{
				return string.Format("{0} {1}", Version, VersionTypeToString(VersionType));
			}
		}

		public string Version
		{
			get
			{
				return string.Format("{0}.{1}", VersionProvider.VERSION, VersionProvider.SUBVERSION);
			}
		}

		public VersionType VersionType
		{
			get { return VersionProvider.VERSION_TYPE; }
		}

		public string AppPath
		{
			get { return Environment.CurrentDirectory; }
		}

		private string VersionTypeToString(VersionType versionType)
		{
			if (versionType == VersionType.Release)
				return string.Empty;

			return versionType.ToString();
		}
	}
}
