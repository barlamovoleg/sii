﻿using System;
using System.Collections.Generic;
using Common;
using Common.Logger;

namespace SIICore
{
	public class CoreAppBuilder : IAppBuilder
	{
		public IAppInfo AppInfo { get; private set; }

		private IServiceRunable _runableService;
		private ILoggerService _loggerService;

		private bool _built;

		public CoreAppBuilder(IAppInfo appInfo)
		{
			AppInfo = appInfo;
		}

		public void Destroy()
		{
			if (_loggerService != null && _loggerService.HasErrors)
				_loggerService.OpenLog(new LogFilter(string.Empty));
		}

		public void Run()
		{
			if (!_built)
				throw new CoreException(CommonStrings.ExceptionsAndErrors.BuildingAppExceptionMessages.RunAppBeforeBuild,
					"{66733058-E45F-4272-B61A-74D1FB6A5CB6}") { IsFatalException = true };

			_runableService.Run();
		}

		public void Build(IServiceLocator serviceLocator)
		{
			_loggerService = TryLoadService<ILoggerService>(serviceLocator);
			CoreLoggers.LoadLoggerService(_loggerService);
			if (_loggerService == null)
				CoreLoggers.SystemLog.LogError(CoreStrings.System.LoggerServiceNotExist);

			var vizualizeService = TryLoadService<ICoreGameVisualizerService>(serviceLocator);
			VizualizersFabric.LoadVizualizerService(vizualizeService);

			_runableService = TryLoadService<IServiceRunable>(serviceLocator, true);
			if (_runableService == null)
				throw new CoreException(CommonStrings.ExceptionsAndErrors.ServicesExceptionMessages.RunnableServiceHasNotBeenLoaded,
					"{215CAF83-B645-472E-8FB2-C179AEA5F6E3}") { IsFatalException = true };

			_built = true;
		}

		private T TryLoadService<T>(IServiceLocator serviceLocator, bool withException = false) where T : IService
		{
			if (CoreLoggers.SystemLog != null)
				CoreLoggers.SystemLog.LogInfo(string.Format(CommonStrings.ServicesMessages.TryLoadingService, typeof(T).Name));
			var service = withException ? serviceLocator.GetService<T>() : serviceLocator.TryGetServiceOrNull<T>();

			if (CoreLoggers.SystemLog != null)
				CoreLoggers.SystemLog.LogInfo(string.Format(CommonStrings.ServicesMessages.TryLoadReqService, typeof(T).Name));
			if (service != null && LoadRequiring(serviceLocator, service))
			{
				if (CoreLoggers.SystemLog != null)
					CoreLoggers.SystemLog.LogInfo(string.Format(CommonStrings.ServicesMessages.ServiceLoaded, typeof(T).Name));
				return service;
			}

			if (CoreLoggers.SystemLog != null)
				CoreLoggers.SystemLog.LogInfo(string.Format(CommonStrings.ExceptionsAndErrors.ServicesErrors.ServiceNotLoaded, typeof(T).Name));
			return default(T);
		}

		private bool LoadRequiring(IServiceLocator serviceLocator, IService service)
		{
			var serviceRequiring = service as IServiceRequiring;
			if (serviceRequiring == null)
				return true;

			var servicesForLoad = new List<IService>();
			var requiring = serviceRequiring.RequiredServices;
			foreach (var reqService in requiring)
			{
				var serviceOrNull = serviceLocator.TryGetServiceOrNull(reqService);
				if (serviceOrNull == null)
					return false;

				servicesForLoad.Add(serviceOrNull);
			}

			try
			{
				serviceRequiring.PassRequiredServices(servicesForLoad.ToArray());
			}
			catch (Exception e)
			{
				CoreLoggers.SystemLog.LogError(string.Format(CommonStrings.ExceptionsAndErrors.ServicesErrors.ErrorInFillReqService, service.GetType().Name, e));
				return false;
			}
			return true;
		}
	}
}
