﻿using System;
using SIICore.Data;

namespace SIICore
{
	public class VizualizersFabric
	{
		public bool IsGameDataLoaded { get; private set; }

		public bool IsServiceExist
		{
			get { return _visualizerService != null; }
		}

		private static ICoreGameVisualizerService _visualizerService;

		public ISiiGameVisualizer CreateVizualizer()
		{
			if (_visualizerService == null)
			{
				CoreLoggers.SystemLog.LogError(CoreStrings.System.VizualizerServiceNotExist);
				return new DumyVisualizer();
			}

			return _visualizerService.CreateVisualizer();
		}

		internal static void LoadVizualizerService(ICoreGameVisualizerService visualizerService)
		{
			if (visualizerService == null)
				return;

			_visualizerService = visualizerService;
			CoreLoggers.SystemLog.LogInfo(CoreStrings.System.VizualizerServiceLoaded);
		}

		public void SetGameData(IGameData gameData)
		{
			if (gameData == null)
				throw new ArgumentNullException("gameData");

			_visualizerService.LoadGameData(gameData);
			IsGameDataLoaded = true;
			CoreLoggers.SystemLog.LogInfo(CoreStrings.System.GameDataLoaded);
		}
	}
}
