﻿using System;
using Common.Repository;
using SIICore.Data;

namespace SIICore
{
	public static class ActiveRepositoryProvider
	{
		public static bool IsRepositoryReady
		{
			get { return ActiveRepository != null; }
		}

		public static event Action ActiveRepositoryChanged;

		public static IRepositoryRelated<Frame, Slot> ActiveRepository { get; private set; }

		public static void SetNewRepository(IRepositoryRelated<Frame, Slot> repository)
		{
			ActiveRepository = repository;
			RaiseActiveRepositoryChanged();
		}

		private static void RaiseActiveRepositoryChanged()
		{
			var handler = ActiveRepositoryChanged;
			if (handler != null)
				handler();
		}
	}
}
