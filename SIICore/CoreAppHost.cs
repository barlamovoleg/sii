﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using Common;
using Common.Logger;

// ReSharper disable DoNotCallOverridableMethodsInConstructor
namespace SIICore
{
	// ReSharper disable once InconsistentNaming
	public abstract class CoreAppHost : IAppHost
	{
		public event Action AppClosing;

		public bool Closed { get; private set; }

		protected abstract IServiceCloseable MainService { get; }
		protected abstract IGraphicsHostService<Window> HostContainer { get; }

		private readonly IAppBuilder _appBuilder;
		private readonly Window _hostWindow;

		private List<IService> _services;
		private ILogger _systemLog;
		private bool _servicesRegistered;
		private bool _appRunned;
		private bool _loggerNotExist;
		private bool _firstLogerCheck;
		private bool _hostWindowClosing;

		protected CoreAppHost(IAppBuilder appBuilder)
		{
			if (MainService == null)
				throw new AppException(CoreStrings.Exceptions.ServiceCloseableNotExist,
					"{D7855F67-AF45-458A-A1A6-91EE83A48B6B}") { IsFatalException = true };

			if (HostContainer == null || HostContainer.GraphicHost == null)
				throw new AppException(CoreStrings.Exceptions.InheritorHostContainerOrGraphicHost,
					"{97D8C2FA-D674-462F-919D-1713A399C334}") { IsFatalException = true };

			_hostWindow = HostContainer.GraphicHost;
			Application.Current.MainWindow = _hostWindow;

			MainService.Closing += ServiceCloseableOnClosing;
			MainService.Closed += ServiceCloseableOnClosed;

			_hostWindow.Closing += HostWindowOnClosing;
			_hostWindow.Closed += HostWindowOnClosed;
			_appBuilder = appBuilder;
		}

		public void TerminateApplication(CloseReason closeReason)
		{
			if (_appRunned)
				_appBuilder.Destroy();

			_appRunned = false;
			if (_servicesRegistered)
				MainService.Close(closeReason);

			if (!Closed)
				_hostWindow.Close();

			Closed = true;
		}

		public void LogException(Exception exception)
		{
			Log(string.Format("Exception: {0}", exception.Message), exception);
		}

		public void LogInfo(string message)
		{
			Log(message);
		}

		public void RegisterServices(IServiceRegistrator serviceRegistrator)
		{
			if (_servicesRegistered)
				throw new AppException(CommonStrings.ExceptionsAndErrors.ServicesExceptionMessages.ReloadServices,
					"{C745438B-0F5D-441F-908A-65E75C3663E3}");

			_services = new List<IService> { MainService, HostContainer };

			try
			{
				serviceRegistrator.RegisterService(MainService);
				serviceRegistrator.RegisterService(HostContainer);

				var services = GetServices();
				foreach (var service in services)
				{
					serviceRegistrator.RegisterService(service);
					_services.Add(service);
				}
			}
			catch (Exception e)
			{
				throw new AppException(CommonStrings.ExceptionsAndErrors.ServicesExceptionMessages.RegisteringServices, e,
					"{E69ECC74-C939-4DAF-A0BC-87ACE28BDF4F}") { IsFatalException = true };
			}

			_servicesRegistered = true;
			LogInfo(CommonStrings.ServicesMessages.ServicesLoaded);
		}

		public void DeregisterServices(IServiceDeregistrator serviceDeregistrator)
		{
			if (!_servicesRegistered)
				return;

			try
			{
				foreach (var service in _services)
					serviceDeregistrator.DeregisterService(service, true);
			}
			catch (Exception e)
			{
				throw new AppException(CommonStrings.ExceptionsAndErrors.ServicesExceptionMessages.DeregisteringServices, e,
					"{E0E07C4C-0200-40F5-8E06-E77378E79F7F}") { IsFatalException = true };
			}
			_services.Clear();
			LogInfo(CommonStrings.ServicesMessages.ServicesUnloaded);
			_servicesRegistered = false;
		}

		public void RunApplication(IServiceLocator serviceLocator)
		{
			try
			{
				LogInfo(CommonStrings.BuildingOrRunningAppMessages.TryBuildingApp);
				_appBuilder.Build(serviceLocator);
				LogInfo(CommonStrings.BuildingOrRunningAppMessages.AppBuilded);
				LogInfo(CommonStrings.BuildingOrRunningAppMessages.TryAppRun);
				_appBuilder.Run();
			}
			catch (Exception e)
			{
				throw new AppException(CommonStrings.ExceptionsAndErrors.BuildingAppExceptionMessages.BuildingOrRunning, e,
					"{1BD8EDBE-9A2E-48B9-B938-AC20259446FD}") { IsFatalException = true };
			}

			_appRunned = true;
			LogInfo(CommonStrings.BuildingOrRunningAppMessages.AppRunned);
		}

		protected abstract IEnumerable<IService> GetServices();

		private void Log(string message, Exception exception = null)
		{
			if (_loggerNotExist || !_servicesRegistered)
			{
				Console.WriteLine(message);
				return;
			}

			if (_systemLog == null)
				TryGetSystemLog();

			if (_systemLog == null)
			{
				_loggerNotExist = true;
				Console.WriteLine(CoreStrings.System.LoggerNotExist);
				Console.WriteLine(message);
				return;
			}

			if (!_firstLogerCheck)
				_systemLog.LogInfo(CoreStrings.System.LoggerExist);

			_firstLogerCheck = true;

			if (exception != null)
				_systemLog.LogException(CommonStrings.ExceptionsAndErrors.SystemMessage, exception);
			else
				_systemLog.LogInfo(message);
		}

		private void ServiceCloseableOnClosed(CloseReason closeReason)
		{
			if (_appRunned)
				_appBuilder.Destroy();

			if (!_hostWindowClosing)
			{
				_hostWindow.Closing -= HostWindowOnClosing;
				_hostWindow.Close();
			}
		}

		private void ServiceCloseableOnClosing(AppClosingEventArgs appClosingEventArgs)
		{
			//TODO
		}

		private void HostWindowOnClosed(object sender, EventArgs eventArgs)
		{
			_appRunned = false;
			_hostWindow.Closing -= HostWindowOnClosing;
			_hostWindow.Closed -= HostWindowOnClosed;

			Closed = true;
			RaiseAppClosing();
		}

		private void HostWindowOnClosing(object sender, CancelEventArgs cancelEventArgs)
		{
			var closeEventArgs = new AppClosingEventArgs(CloseReason.User);
			MainService.OnCloseRequest(closeEventArgs);
			if (closeEventArgs.CancelClose)
			{
				cancelEventArgs.Cancel = true;
				return;
			}

			_hostWindowClosing = true;
			MainService.Close(closeEventArgs.CloseReason);
		}

		private void TryGetSystemLog()
		{
			ILoggerService loggerService = null;
			foreach (var service in _services)
			{
				loggerService = service as ILoggerService;
				if (loggerService != null)
					break;
			}

			if (loggerService == null)
				return;

			try
			{
				_systemLog = loggerService.GetLogger(CommonStrings.SystemLogName);
			}
			catch (Exception e)
			{
				_loggerNotExist = true;
				throw new AppException(CommonStrings.ExceptionsAndErrors.ServicesErrors.CreatingSystemLog, e,
					"{348C1098-4A02-4A93-BED9-61EBA4FF6BDA}");
			}
		}

		private void RaiseAppClosing()
		{
			var handler = AppClosing;
			if (handler != null)
				handler();
		}
	}
}
