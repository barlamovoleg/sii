﻿using Common;

namespace SIICore
{
	internal static class VersionProvider
	{
		internal const int VERSION = 1;
		internal const int SUBVERSION = 0;
		internal const VersionType VERSION_TYPE = VersionType.PreAlpha;
	}
}
