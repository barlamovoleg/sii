﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace SIICore.Data
{
    public class DefaultVizualizedObject : IVizualizedObject, INotifyPropertyChanged
	{
        public static ObservableCollection<IVizualizedObject> HISTORY = new ObservableCollection<IVizualizedObject>();

		public event Action IconUpdated;
		private const string IconSlotName = "Иконка";

		public int X { get { return GetX(); } }
		public int Y { get { return GetY(); } }
		public string IconPath { get; private set; }
		public Frame FrameData { get { return _frame; } }
		public string Name { get { return _frame.Name; } }

        private bool _isExpanded;
        public bool IsExpanded
        {
            get { return _isExpanded; }
            set
            {
                if (value != _isExpanded)
                {
                    _isExpanded = value;
                    NotifyPropertyChanged("IsExpanded");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }

		public void RefreshIcon()
		{
			var iconSlot = GetIconSlot();
			if (iconSlot != null)
			{
				string[] items = iconSlot.DefaultValue.Data.ToString().Split('@');
				if (items.Length > 2)
					IconPath = items[2];
			}

			if (IconUpdated != null)
				IconUpdated();

			if (!File.Exists(IconPath))
				IconPath = string.Empty;


		}

		private readonly Frame _frame;

		public DefaultVizualizedObject(int x, int y, Frame frame, bool flag = false)
		{
			_frame = frame.GetCopy();
            _frame.VizName = Guid.NewGuid().ToString();

            try
            {
                SetX(x);
                SetY(y);
            }
            catch
            {
            }

			var iconSlot = GetIconSlot();
			if (iconSlot != null)
			{
				string[] items = iconSlot.DefaultValue.Data.ToString().Split('@');
				if (items.Length > 2)
					IconPath = items[2];
			}

			if (!File.Exists(IconPath))
				IconPath = string.Empty;

            if (!flag && this.FrameData.IsObject())
                DefaultVizualizedObject.HISTORY.Add(this.Copy());
		}

        public DefaultVizualizedObject()
        {

        }

		private Slot GetIconSlot()
		{
			return _frame.slots.FirstOrDefault(slot => slot.Name == IconSlotName);
		}

		private int GetX()
		{
			var slot = GetXSlot();
			if (slot == null)
				return 0;

			return int.Parse(slot.DefaultValue.Data.ToString());
		}

		private int GetY()
		{
			var slot = GetYSlot();
			if (slot == null)
				return 0;

			return int.Parse(slot.DefaultValue.Data.ToString());
		}

		private void SetX(int x)
		{
			var slot = GetXSlot();
			if (slot == null)
				return;

			slot.DefaultValue.Data = x.ToString();
		}

		private void SetY(int y)
		{
			var slot = GetYSlot();
			if (slot == null)
				return;

			slot.DefaultValue.Data = y.ToString();
		}

		private Slot GetXSlot()
		{
			return _frame.slots.FirstOrDefault(slot => slot.Name == "Координата X");
		}

		private Slot GetYSlot()
		{
			return _frame.slots.FirstOrDefault(slot => slot.Name == "Координата Y");
		}
    }
}
