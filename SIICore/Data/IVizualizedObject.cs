﻿using System;
using System.Collections.Generic;
using System.Windows.Documents;

namespace SIICore.Data
{
    public interface IVizualizedObject
	{
		event Action IconUpdated;

        bool IsExpanded { get; set; }

		int X { get; }
		int Y { get; }
		string IconPath { get; }
		Frame FrameData { get; }
		string Name { get; }

		void RefreshIcon();
	}
}
