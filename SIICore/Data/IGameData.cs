﻿using System;
using System.Collections.Generic;

namespace SIICore.Data
{
	public interface IGameData
	{
		int Width { get; }
		int Height { get; }

		void GenerateData(Random seed);
		IList<IVizualizedObject> VizualizedObjects { get; }
		IList<IVizualizedObject> Actors { get; }
		IList<IVizualizedObject> Objects { get; }

		IList<IVizualizedObject> CreatableObjects { get; }

		IVizualizedObject AddObjectAs(IVizualizedObject copy, int x, int y);
	}
}
