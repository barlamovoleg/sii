﻿using System;

namespace SIICore.Data
{
	public static class GameDataFabric
	{
		public static IGameData GetGameData()
		{
			var repository = ActiveRepositoryProvider.ActiveRepository;
			var gameData = new GameData(repository);
			gameData.GenerateData(new Random());
			return gameData;
		}
	}
}
