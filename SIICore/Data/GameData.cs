﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Principal;
using Common.Repository;

namespace SIICore.Data
{
	internal class GameData : IGameData
	{
		internal const int MAP_SIZE = 20;

		private const string ObjectFrameName = "Объект(sys)";
		private const string CellFrameName = "Ячейка(sys)";
		private const string CreatureFrameName = "Актор(sys)";

		private enum FrameType
		{
			None,
			Cell,
			Object,
			Creature
		}

		public event Action Generated;
		public int Width { get; private set; }
		public int Height { get; private set; }

		public IList<IVizualizedObject> VizualizedObjects { get { return _vizualizedObjects; } }
		public IList<IVizualizedObject> Actors { get { return _actors; } }
		public IList<IVizualizedObject> Objects { get { return _objects; } }
		public IList<IVizualizedObject> CreatableObjects { get { return _creatableObjects; } }

		private IList<IVizualizedObject> _vizualizedObjects;
		private IList<IVizualizedObject> _objects;
		private IList<IVizualizedObject> _actors;
		private IList<IVizualizedObject> _creatableObjects;

		private readonly IRepositoryRelated<Frame, Slot> _frameRepository;

		public GameData(IRepositoryRelated<Frame, Slot> frameRepository)
		{
			_frameRepository = frameRepository;
		}

		public void GenerateData(Random seed)
		{
			_vizualizedObjects = new List<IVizualizedObject>();
			_actors = new List<IVizualizedObject>();
			_objects = new List<IVizualizedObject>();
			_creatableObjects = new List<IVizualizedObject>();
			Width = MAP_SIZE;
			Height = MAP_SIZE;

			var framesCells = new List<Frame>();
			var framesObjects = new List<Frame>();
			var framesCreatures = new List<Frame>();

			var frames = _frameRepository.GetAll();
			foreach (var frame in frames)
			{
				var frameType = GetFrameType(frame);
				switch (frameType)
				{
					case FrameType.None:
						continue;
					case FrameType.Cell: framesCells.Add(frame);
						break;
					case FrameType.Object: framesObjects.Add(frame);
						break;
					case FrameType.Creature: framesCreatures.Add(frame);
						break;
				}
			}

			var firstCellFrame = framesCells.FirstOrDefault();
			if (firstCellFrame != null)
			{
				for (int i = 0; i < Width; i++)
					for (int j = 0; j < Height; j++)
						_vizualizedObjects.Add(new DefaultVizualizedObject(i, j, firstCellFrame));
			}

			foreach (var cellType in framesCells)
			{
				if (CheckForCreatable(cellType))
					_creatableObjects.Add(new DefaultVizualizedObject(0, 0, cellType,true));
			}
			foreach (var objType in framesObjects)
				if (CheckForCreatable(objType))
                    _creatableObjects.Add(new DefaultVizualizedObject(0, 0, objType, true));
			foreach (var creature in framesCreatures)
				if (CheckForCreatable(creature))
                    _creatableObjects.Add(new DefaultVizualizedObject(0, 0, creature, true));

			RaiseGenerated();
		}

		private const string IconSlotName = "Иконка";
		private Slot GetIconSlot(Frame frame)
		{
			return frame.slots.FirstOrDefault(slot => slot.Name == IconSlotName);
		}

		private bool CheckForCreatable(Frame frame)
		{
			var iconSlot = GetIconSlot(frame);
			if (iconSlot != null)
			{
				string[] items = iconSlot.DefaultValue.Data.ToString().Split('@');
				if (items.Length > 2)
				{
					var iconPath = items[2];
					if (File.Exists(iconPath))
						return true;
				}
			}
			return false;
		}

		public IVizualizedObject AddObjectAs(IVizualizedObject copy, int x, int y)
		{
			var frame = copy.FrameData;
			var frameType = GetFrameType(frame);
			switch (frameType)
			{
				case FrameType.None:
					break;
				case FrameType.Cell:
					if (CheckForCreate(frameType, x, y))
					{
						var result = new DefaultVizualizedObject(x, y, frame);
						_vizualizedObjects.Add(result);
						return result;
					}
					break;
				case FrameType.Object:
					if (CheckForCreate(frameType, x, y))
					{
						var result = new DefaultVizualizedObject(x, y, frame);
						_objects.Add(result);
						_vizualizedObjects.Add(result);
						return result;
					}
					break;
				case FrameType.Creature:
					if (CheckForCreate(frameType, x, y))
					{
						var result = new DefaultVizualizedObject(x, y, frame);
						_actors.Add(result);
						_vizualizedObjects.Add(result);
						return result;
					}
					break;
			}
			return null;
		}

		private bool CheckForCreate(FrameType type, int x, int y)
		{
			foreach (var vizualizedObject in _vizualizedObjects)
			{
				if (vizualizedObject.X == x && vizualizedObject.Y == y)
				{
					var objType = GetFrameType(vizualizedObject.FrameData);
					if (objType == FrameType.Cell && type == FrameType.Cell)
						return false;
					if (objType != FrameType.Cell && type != FrameType.Cell)
						return false;
				}
			}
			return true;
		}

		private FrameType GetFrameType(Frame frame)
		{
			if (frame.Type == Frame.FrameType.System)
				return FrameType.None;

			var parent = frame.Parent;
			if (parent == null)
				return FrameType.None;

			if (parent.Type == Frame.FrameType.System)
			{
				var frameName = parent.Name;
				if (frameName == ObjectFrameName)
					return FrameType.Object;
				if (frameName == CellFrameName)
					return FrameType.Cell;
				if (frameName == CreatureFrameName)
					return FrameType.Creature;
				return FrameType.None;
			}
			return GetFrameType(parent);
		}

		private void RaiseGenerated()
		{
			var handler = Generated;
			if (handler != null)
				handler();
		}
	}
}
