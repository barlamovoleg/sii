﻿using System;
using System.Runtime.Serialization;
using Common;

// ReSharper disable once CheckNamespace
namespace SIICore
{
	public class CoreException : AppException
	{
		public CoreException(string message, string key)
			: base(message, key)
		{
		}

		public CoreException(string message, Exception innerException, string key)
			: base(message, innerException, key)
		{
		}

		protected CoreException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
	}
}
