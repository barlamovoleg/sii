﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIICore.Data;

namespace SIICore.Models
{
    public static class ResultHelper
    {
        public static List<Object> getResults(string strResults)
        {
            var results = new List<Object>();

            string[] items = strResults.Split('@');
            if (items.Length < 4)
                return results;

            for (int i = 1; i < items.Length; i += 6)
            {
                var result = new Result();
                result.Frame = "FrameType@" + items[i] + "@" + items[i + 1];
                result.Slot = "SlotType@" + items[i + 2] + "@" + items[i + 3];
                result.Operation = items[i + 4];
                result.Value = items[i + 5];
                results.Add(result);
            }

            return results;
        }

        public static string getStrResult(List<Object> results)
        {
            string strResult = "ResultType";
            if (results.Count == 0)
                return strResult + "@@";

            foreach (Result result in results)
            {
                strResult += "@" + result.Frame.Remove(0, result.Frame.IndexOf('@') + 1);
                strResult += "@" + result.Slot.Remove(0, result.Slot.IndexOf('@') + 1);
                strResult += "@" + result.Operation;
                strResult += "@" + result.Value;
            }
            return strResult;
        }
    }
}
