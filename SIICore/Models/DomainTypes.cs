﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIICore.Data;

namespace SIICore.Models
{
    public enum DomainTypes
    {
        StringType,
        NumberType,
        FrameType,
        FileType,
        ResultType,
        SlotType
    }

    public static class DomainHelper
    {
        public static string getInnerType(Frame frame)
        {
            return string.Format("{0}@{1}@{2}", DomainTypes.FrameType, frame.Id, frame.Name);
        }

        public static string getInnerType(Slot slot)
        {
            return string.Format("{0}@{1}@{2}", DomainTypes.SlotType, slot.Id, slot.Name);
        }

        public static string getInnerType(int number)
        {
            return string.Format("{0}@@", DomainTypes.NumberType);
        }

        public static string getInnerType(string str)
        {
            switch (str)
            {
                case "file":
                    return string.Format("{0}@@", DomainTypes.FileType);
                case "result":
                    return string.Format("{0}@@", DomainTypes.ResultType);
                default:
                    return string.Format("{0}@@", DomainTypes.StringType);
            }
        }

        public static DomainTypes getDomainType(string innerType)
        {
            DomainTypes ret = DomainTypes.StringType;
            Enum.TryParse<DomainTypes>(innerType.Split('@')[0], out ret);
            return ret;
        }

        public static string getFrameId(string innerType)
        {
            var domainType = getDomainType(innerType);
            if (domainType != DomainTypes.FrameType && domainType != DomainTypes.SlotType)
                return null;

            string[] items = innerType.Split('@');
            if (items.Length != 3)
                return null;

            return items[1];
        }
    }
}
