﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace SIICore.Data
{
    public class Result : INotifyPropertyChanged
    {
        private string _frame;
        public string Frame { get { return _frame; } set { _frame = value; OnPropertyChanged("Frame"); } }

        private string _slot;
        public string Slot { get { return _slot; } set { _slot = value; OnPropertyChanged("Slot"); } }

        private string _operation;
        public string Operation { get { return _operation; } set { _operation = value; OnPropertyChanged("Operation"); } }

        public string Value { get; set; }


        private List<string> _frames;
        public List<string> Frames { get { return _frames; } set { _frames = value; OnPropertyChanged("Frames"); } }

        private List<string> _slots;
        public List<string> Slots { get { return _slots; } set { _slots = value; OnPropertyChanged("Slots"); } }

        public List<string> Operations { get; set; }

        public Result()
        {
            Operation = "+";
            Operations = new List<string>();
            Operations.Add("+");
            Operations.Add("-");
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
    }
}
