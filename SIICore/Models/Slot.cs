﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


// ReSharper disable once CheckNamespace
namespace SIICore.Data
{
    public class Slot
    {
        public enum Inheritance
        {
            Override,
            Same,
            Unique
        }

        public String Id { get; set; }
        public String ArcId { get; set; }

        public String Name { get; set; }
        public Value DefaultValue { get; set; }
        //public Value Value { get; set; }

        public Inheritance InheritanceType { get; set; }

        public Slot(String name, Value defaultValue, Inheritance inheritance=Inheritance.Override)
        {
            Name = name;
            DefaultValue = defaultValue;
            InheritanceType = inheritance;
        }

        public Slot(String id, String name, String arcId)
        {
            Id = id;
            Name = name;
            ArcId = arcId;
        }

        public Slot(Slot other)
        {
            Name = other.Name;
            DefaultValue = new Value(other.DefaultValue.Type, other.DefaultValue.Data.ToString());
            //Value = other.Value;
            InheritanceType = other.InheritanceType;
        }
    }
}
