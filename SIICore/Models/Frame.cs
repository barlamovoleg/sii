﻿using System;
using System.Collections.Generic;
using System.Windows;
using Common.ObjectDeepCopy;


// ReSharper disable once CheckNamespace
namespace SIICore.Data
{
    public class Frame : IComparable
    {
		private const string ObjectFrameName = "Объект(sys)";
		private const string CellFrameName = "Ячейка(sys)";
		private const string CreatureFrameName = "Актор(sys)";

        public const string SYSTEM_FRAME_ACTION = "Действие(sys)";
        public const string SYSTEM_SLOT_SUBJECT = "Субъект";
        public const string SYSTEM_SLOT_OBJECT = "Объект";
        public const string SYSTEM_SLOT_RESULT = "Результат";

        public const string SYSTEM_FRAME_ACTOR = "Актор(sys)";
        public const string SYSTEM_SLOT_COORD_X = "Координата X";
        public const string SYSTEM_SLOT_COORD_Y = "Координата Y";

        public const string SYSTEM_FRAME_OBJECT = "Объект(sys)";


        public enum FrameType
        {
            Custom,
			System
        }

        public string VizName { get; set; }

        public String Id { get; set; }

        public String Name { get; set; }

        public List<Slot> slots { get; set; }

        public Frame Parent { get; set; }
        public List<Frame> children = new List<Frame>();

        //список id фреймов, которые требуются (в качестве слотов)
        public List<string> requipments = new List<string>();
        //public Frame Class { get; set; }
        //public List<Frame> instances = new List<Frame>();

        public FrameType Type { get; set; }

        public Rect Position { get; set; }

        //присоединенные процедуры
        public Frame(String name, FrameType type = FrameType.Custom)
        {
            Name = name;
            Type = type;
            slots = new List<Slot>();
        }

        public Frame(String id, String name, FrameType type = FrameType.Custom)
        {
            Id = id;
            Name = name;
            Type = type;
            slots = new List<Slot>();
        }

        public int CompareTo(object obj)
        {
            if (!(obj.GetType() == GetType()))
                return 0;

            return Name.CompareTo(((Frame)obj).Id);
        }

	    public Frame GetCopy()
	    {
		    return this.DeepCopy();
	    }

	    public bool IsObject()
	    {
		    return IsObject(this);
	    }

		private bool IsObject(Frame frame)
		{
			if (frame.Type == Frame.FrameType.System)
				return false;

			var parent = frame.Parent;
			if (parent == null)
				return false;

			if (parent.Type == Frame.FrameType.System)
			{
				var frameName = parent.Name;
				if (frameName == ObjectFrameName)
					return true;
				if (frameName == CellFrameName)
					return false;
				if (frameName == CreatureFrameName)
					return true;
				return false;
			}
			return IsObject(parent);
		}
    }
}
