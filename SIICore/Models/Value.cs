﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;


// ReSharper disable once CheckNamespace
namespace SIICore.Data
{
    public class Value : INotifyPropertyChanged
    {
        public string Type { get; set; }

        private Object _data;
        public Object Data
        {
            get
            {
                return _data;
            }

            set
            {
                //if (!(type == value.GetType()))
                //{
                //    throw new ArgumentException("Value type is incompatible");
                //}
                _data = value;
                OnPropertyChanged("Data");
            }
        }

        public List<string> Types { get; set; }
        public List<Object> Datas { get; set; }

        public Value(string type, object data)
        {
            if (!(data is IComparable))
            {
                throw new ArgumentException("Data type is incomparable");
            }
            Type = type;
            _data = data;
        }

        public override string ToString()
        {
            if (_data != null)
            {
                return _data.ToString();
            }
            return "";
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
    }
}
