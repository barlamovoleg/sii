﻿namespace SIIVizualizator
{
	internal static class VisualizerStrings
	{
		internal static class Errors
		{
			internal static string GameDataNotLoaded = "Данные игры не были загружены до создания визуализатора. Возвращена пустышка.";
			internal static string DisposeGameException = "Disposing Game or GameContainer";
		}
	}
}
