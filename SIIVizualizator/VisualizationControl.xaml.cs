﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Common;
using SIICore;
using SIICore.Data;
using SIICore.Models;
using SIIGame2D;

namespace SIIVizualizator
{
	public partial class VisualizationControl : ISiiGameVisualizer
	{
		public bool IsPlaying { get; private set; }
		public bool IsAttached { get; private set; }
		public bool IsDataSet { get; private set; }
		public double CurrentSpeed { get; private set; }
		public event Action<IVizualizedObject> ObjectSelected;

		public IGameData GameData
		{
			get { return _gameData; }
		}
		private IGameData _gameData;

		private ContentControl _grpahicHost;
		private GameMain _game;

		public VisualizationControl()
		{
			InitializeComponent();
		}

		public void OnPreviewMouseMove(object sender, MouseEventArgs mouseEventArgs)
		{
			if (_game != null)
			{
				var m =mouseEventArgs.GetPosition(this);
			}
		}

		public void Dispose()
		{
			try
			{
				_game.Destryoed = true;
				_game.Exit();
				_game.Dispose();
				RemoveData();
				Detach();
			}
			catch (Exception e)
			{
				VisualizerLogger.Logger.LogException(VisualizerStrings.Errors.DisposeGameException, e);
			}
		}

		public void SetData(IGameData data)
		{
			_gameData = data;
			IsDataSet = true;
		}

		public void RemoveData()
		{
			_gameData = null;
			IsDataSet = false;
		}

		public void Attach(IGraphicsHostService<ContentControl> host)
		{
			_grpahicHost = host.GraphicHost;
			_grpahicHost.Content = this;
		}

		public void Detach()
		{
			_grpahicHost.Content = null;
		}

		public void Initialize()
		{
			_game = new GameMain(XnaControl, _gameData, VisualizerLogger.Logger);
			_game.ObjectSelected += GameOnObjectSelected;
		}

		private IVizualizedObject selectedVizualizedObject;
		private void GameOnObjectSelected(IVizualizedObject vizualizedObject)
		{
			selectedVizualizedObject = vizualizedObject;
			if (ObjectSelected != null)
				ObjectSelected(vizualizedObject);

		}

		public void Play()
		{
			IsPlaying = true;
			XnaControl.Run();
		}

		public void Stop()
		{
			IsPlaying = false;
		}

		public void OneStep()
		{
            DefaultVizualizedObject.HISTORY.Add(null);

			var actors = _gameData.Actors;
			foreach (var actor in actors)
			{
				var actionFrame = InferenceEngine.GetFrame(actor.FrameData, GetAroundFrames(actor));
                if (actionFrame != null)
                {
                    ApplyAction(actionFrame.Item1, actor.FrameData, actionFrame.Item2);
                    var act = new DefaultVizualizedObject(0, 0, actionFrame.Item1);
                    act.FrameData.slots[0].DefaultValue.Data += ' ' + actor.FrameData.VizName;
                    act.FrameData.slots[1].DefaultValue.Data += ' ' + actionFrame.Item2.VizName;
                    
                    DefaultVizualizedObject.HISTORY.Add(act);
                }
			}			

            foreach (var g in _gameData.Actors)
                DefaultVizualizedObject.HISTORY.Add(g.Copy());
            foreach (var g in _gameData.Objects)
            {
                DefaultVizualizedObject.HISTORY.Add(g.Copy());
            }

            if (selectedVizualizedObject != null)
                GameOnObjectSelected(selectedVizualizedObject);
		}

		public void ChangePlaySpeed(double speed)
		{

		}

		private List<SIICore.Data.Frame> GetAroundFrames(IVizualizedObject target)
		{
			var result = new List<IVizualizedObject>();
			var logicObjects = new List<IVizualizedObject>();
			logicObjects.AddRange(_gameData.Objects);
			logicObjects.AddRange(_gameData.Actors);
			foreach (var obj in logicObjects)
			{
				if (obj == target)
					continue;

				if (InRange(obj, target))
					result.Add(obj);
			}
			return Sort(result, target);
		}

		private List<SIICore.Data.Frame> Sort(List<IVizualizedObject> objects, IVizualizedObject target)
		{
			var result = new List<SIICore.Data.Frame>();

			var x = target.X;
			var y = target.Y;
			var left = GetInXY(objects, x - 1, y);
			if (left != null)
				result.Add(left.FrameData);
			left = GetInXY(objects, x - 1, y - 1);
			if (left != null)
				result.Add(left.FrameData);
			left = GetInXY(objects, x, y - 1);
			if (left != null)
				result.Add(left.FrameData);
			left = GetInXY(objects, x + 1, y - 1);
			if (left != null)
				result.Add(left.FrameData);
			left = GetInXY(objects, x + 1, y);
			if (left != null)
				result.Add(left.FrameData);
			left = GetInXY(objects, x + 1, y + 1);
			if (left != null)
				result.Add(left.FrameData);
			left = GetInXY(objects, x, y + 1);
			if (left != null)
				result.Add(left.FrameData);
			left = GetInXY(objects, x - 1, y + 1);
			if (left != null)
				result.Add(left.FrameData);

			return result;
		}

		private IVizualizedObject GetInXY(IEnumerable<IVizualizedObject> objects, int x, int y)
		{
			return objects.FirstOrDefault(vizualizedObject => vizualizedObject.X == x && vizualizedObject.Y == y);
		}

		private void ApplyAction(SIICore.Data.Frame action, SIICore.Data.Frame actor, SIICore.Data.Frame target)
		{
			MLVActionHelper.ApplyAction(_gameData, actor, action, target);
		}

		private bool InRange(IVizualizedObject oneObj, IVizualizedObject secondObj)
		{
			return Math.Abs(oneObj.X - secondObj.X) < 2 && Math.Abs(oneObj.Y - secondObj.Y) < 2;
		}

		public void ClearBrush()
		{
			_game.ClearBrush();
		}

		public void SetDeleteBrush()
		{
			_game.SetDeleteBrush();
		}

		public void SetBrush(IVizualizedObject brush)
		{
			_game.SetBrush(brush);
		}
	}
}
