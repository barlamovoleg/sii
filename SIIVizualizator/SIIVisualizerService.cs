﻿using System;
using System.Windows.Controls;
using Common.Visualizer;
using SIICore;
using SIICore.Data;

namespace SIIVizualizator
{
// ReSharper disable once InconsistentNaming
	public class SIIVisualizerService : ICoreGameVisualizerService
	{
		public Guid Key
		{
			get { return new Guid("{EC0C01AF-1565-4EBF-B550-6D72E6701A99}"); }
		}

		public bool IsGameDataLoaded { get; private set; }

		private IGameData _gameData;
		private VisualizationControl _lastVisualizator;

		public ISiiGameVisualizer CreateVisualizer()
		{
			if (!IsGameDataLoaded)
			{
				VisualizerLogger.Logger.LogError(VisualizerStrings.Errors.GameDataNotLoaded);
				return new DumyVisualizer();
			}

			if (_lastVisualizator != null)
				_lastVisualizator.Dispose();

			_lastVisualizator = new VisualizationControl();
			_lastVisualizator.SetData(_gameData);
			_lastVisualizator.Initialize();
			return _lastVisualizator;
		}

		public void LoadGameData(IGameData gameData)
		{
			_gameData = gameData;
			IsGameDataLoaded = true;
		}

		IVisualizer<IGameData, ContentControl> IVisualizerService<IGameData, ContentControl>.CreateVisualizer(ContentControl visualizerHost)
		{
			return CreateVisualizer();
		}

		public void Dispose()
		{
		}
	}
}
