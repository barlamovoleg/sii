﻿using Common.Logger;
using SIICore;

namespace SIIVizualizator
{
	internal static class VisualizerLogger
	{
		static VisualizerLogger()
		{
			Logger = CoreLoggers.GetLogger("Visualizer");
		}

		internal static ILogger Logger { get; private set; }
	}
}
