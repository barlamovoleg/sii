﻿using System;
using System.Collections.Generic;
using System.Linq;
using SIICore;
using SIICore.Data;
using SIICore.Models;

namespace SIIVizualizator
{
	internal static class MLVActionHelper
	{
		private static IGameData _gameData;

		internal static void ApplyAction(IGameData gameData, Frame actor, Frame action, Frame targetFrame)
		{
			var last = action.slots[action.slots.Count - 1];
			var lexinaStroka = last.DefaultValue.Data.ToString();
			var fignia = ResultHelper.getResults(lexinaStroka);
			foreach (Result figushka in fignia)
			{
				var frameId = DomainHelper.getFrameId(figushka.Frame);
				var slotId = DomainHelper.getFrameId(figushka.Slot);
				var operation = figushka.Operation;
				var value = figushka.Value;

				var rep = ActiveRepositoryProvider.ActiveRepository;
				var frames = rep.GetAll();
				var type = frames.FirstOrDefault(frame => frame.Id == frameId);
				if (type == null)
					throw new Exception();

				try
				{
					ApplyActionToFrame(GetType(type) == Frame.SYSTEM_FRAME_ACTOR ? actor : targetFrame, slotId, operation, value);
				}
				catch (Exception e)
				{
					
				}
			}
		}

		private static void ApplyActionToFrame(Frame actor, string slotId, string operation, string value)
		{
			var targetSlot = actor.slots.FirstOrDefault(slot => slot.Id == slotId);
			if (targetSlot == null)
				throw new Exception();

			var slotType = DomainHelper.getDomainType(targetSlot.DefaultValue.Type);
			if (slotType != DomainTypes.NumberType)
				throw new Exception();

			var d = int.Parse(value);
			if (operation == "-")
				d *= -1;

			targetSlot.DefaultValue.Data = (int.Parse(targetSlot.DefaultValue.Data.ToString()) + d);
		}

		private static string GetType(Frame frame)
		{
			if (frame.Type == Frame.FrameType.System)
				return frame.Name;

			return GetType(frame.Parent);
		}
	}
}
