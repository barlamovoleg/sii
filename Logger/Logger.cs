﻿using System;
using System.IO;
using Common;
using Common.Logger;

namespace Logger
{
	internal class Logger : ILogger
	{
		public event Action<ErrorLoggedEventArgs> ErrorLogged;

		public string Name { get; private set; }

		internal const string LOG_RESOLUTION = ".txt";
		private readonly string _loggerFilePath;

		private readonly object _fileLocker = new object();

		public Logger(string name, string path)
		{
			Name = name;
			_loggerFilePath = Path.Combine(path, name) + LOG_RESOLUTION;
			try
			{
				File.Create(_loggerFilePath).Close();
			}
			catch (Exception exception)
			{
				throw new AppException(string.Format(StaticStrings.Exceptions.CreatingFileForLogger, _loggerFilePath, name), exception,
					"{98C2CE5D-B9B6-4A78-92A5-AE46D2E83739}");
			}
		}

		public void Dispose()
		{
			
		}

		public void LogInfo(string info)
		{
			LogMessage(info, LogMessageType.Info);
		}

		public void LogError(string error)
		{
			LogMessage(CommonStrings.Error + error, LogMessageType.Error);
		}

		public void LogException(string message, Exception exception)
		{
			LogMessage(CommonStrings.Exception + message + ": " + Environment.NewLine + exception, LogMessageType.Exception);
		}

		public void LogMessage(string message, LogMessageType messageType)
		{
			try
			{
				FileHelper.WriteLineAsync(DateTime.Now.ToString("hh:mm:ss.fff") + ':' + Environment.NewLine + message + Environment.NewLine,
					_loggerFilePath, _fileLocker);
			}
			catch (Exception exception)
			{
				throw new AppException(string.Format(StaticStrings.Exceptions.LogWrite, Name), exception,
					"{34305ACD-8402-4604-85F4-15C45E125F18}");
			}

			if (messageType == LogMessageType.Error || messageType == LogMessageType.Exception ||
				messageType == LogMessageType.ResultNegative)
			{
				RaiseErrorLogged(message, messageType);
			}
		}

		private void RaiseErrorLogged(string message, LogMessageType messageType)
		{
			var handler = ErrorLogged;
			if (handler != null)
				handler(new ErrorLoggedEventArgs(message, messageType));
		}
	}
}
