﻿namespace Logger
{
	internal static class StaticStrings
	{
		internal static class Exceptions
		{
			internal static string CreatingLogger
			{
				get { return "Во время создания логгера произошла ошибка. Был возвращен логер - заглушка."; }
			}

			internal static string CreatingFileForLogger
			{
				get { return "Can not create the file {0} for logger {1}."; }
			}

			internal static string CreatingDirectoryForLogManager
			{
				get { return "Can not create the directory {0} for logManager."; }
			}

			internal static string LogWrite
			{
				get { return "error when logger {0} trying to write the log string to a file"; }
			}

			internal static string CantOpenErrorLog
			{
				get { return "Не удалось открыть лог ошибок по пути: '{0}'.\nПричина:{1}"; }
			}
		}

		internal static class Messages
		{
			internal static string LogManagerCreated
			{
				get { return "Создан менеджер логирования."; }
			}

			internal static string LoggerCreated
			{
				get { return "Логгер {0} создан."; }
			}
		}

		internal static string ErrorLogName = "ErrorLog";
	}
}
