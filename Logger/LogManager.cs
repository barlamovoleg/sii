﻿using System;
using System.Collections.Generic;
using System.IO;
using Common;
using Common.Logger;

namespace Logger
{
	public class LogManager : ILoggerService
	{
		public bool HasErrors { get; private set; }

		public Guid Key
		{
			get { return new Guid("{EBB391A9-29FC-4D9E-B210-25A8DFB5D4CF}"); }
		}

		private const string LogFolderNameAddition = "Log ";
		private readonly List<ILogger> _loggers;
		private readonly IAppInfo _appInfo;

		private readonly ILogger _systemLogger;
		private readonly string _logFolerPath;
		private ILogger _errorLogger;

		public LogManager(IAppInfo appInfo, string systemLogName)
		{
			_loggers = new List<ILogger>();
			_appInfo = appInfo;

			_logFolerPath = CreateLogsDirectory(Path.Combine(appInfo.AppPath, LogFolderNameAddition + appInfo.AppName));

			_systemLogger = GetLogger(systemLogName);
			_systemLogger.LogInfo(StaticStrings.Messages.LogManagerCreated);
		}

		public void Dispose()
		{
			foreach (var logger in _loggers)
				logger.Dispose();

			_loggers.Clear();
		}

		public ILogger GetLogger(string loggerName)
		{
			foreach (var logger in _loggers)
			{
				if (logger.Name == loggerName)
					return logger;
			}

			var newLogger = CreateLogger(loggerName);
			newLogger.ErrorLogged += NewLoggerOnErrorLogged;

			_loggers.Add(newLogger);
			return newLogger;
		}

		public void OpenLog(LogFilter logFilter)
		{
			var path = Path.Combine(_logFolerPath, StaticStrings.ErrorLogName + Logger.LOG_RESOLUTION);
			try
			{
				System.Diagnostics.Process.Start("notepad.exe", path);
			}
			catch (Exception e)
			{
				_systemLogger.LogError(string.Format(StaticStrings.Exceptions.CantOpenErrorLog, path, e));
			}
		}

		private string CreateLogsDirectory(string path)
		{
			var now = DateTime.Now;
			var dirPath = Path.Combine(path, now.ToString("yy-MM-dd") + ' ' + GetTime(now));
			try
			{
				Directory.CreateDirectory(dirPath);
			}
			catch (Exception e)
			{
				throw new AppException(string.Format(StaticStrings.Exceptions.CreatingDirectoryForLogManager, dirPath), e,
					"{01175C90-3080-4C6C-A4FB-6ED7C39532DA}");
			}
			return dirPath;
		}

		private ILogger CreateLogger(string name)
		{
			try
			{
				var logger = new Logger(name, _logFolerPath);
				if (_systemLogger != null)
					_systemLogger.LogInfo(string.Format(StaticStrings.Messages.LoggerCreated, name));
				return logger;
			}
			catch (Exception e)
			{
				if (_systemLogger != null)
					_systemLogger.LogException(StaticStrings.Exceptions.CreatingLogger, e);
				return new ConsoleLogger(name);
			}
		}

		private string GetTime(DateTime dateTime)
		{
			return dateTime.Hour.ToString("00-") + dateTime.Minute.ToString("00-") + dateTime.Second.ToString("00");
		}

		private void NewLoggerOnErrorLogged(ErrorLoggedEventArgs eventArgs)
		{
			if (_errorLogger == null)
			{
				_errorLogger = CreateLogger(StaticStrings.ErrorLogName);
				_loggers.Add(_errorLogger);
			}
			_errorLogger.LogMessage(eventArgs.Message, eventArgs.MessageType);
			HasErrors = true;
		}
	}
}
