﻿using XnaCoreAdapted.Integration;

// ReSharper disable once CheckNamespace
namespace XnaWpfIntegration
{
	internal interface IXnaGamePresenterInternal
	{
		IXnaGameContainer XnaGameHostContainer { get; }
		void AttachGame(XnaGame game);
	}
}
