﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Media;
using System.Windows.Threading;
using XnaCoreAdapted.Integration;
using XnaWpfIntegration.Helpers;

// ReSharper disable DoNotCallOverridableMethodsInConstructor
namespace XnaWpfIntegration.Adapters
{
	public abstract class WpfXnaBaseAdapter : FormXnaContainerAdapter
	{
		protected abstract event RoutedEventHandler ControlActivated;
		protected abstract event RoutedEventHandler ControlDeactivated;
		protected abstract event RoutedEventHandler Loaded;
		protected abstract event SizeChangedEventHandler SizeChangedWpf;
		protected override abstract event CancelEventHandler Closing;
		protected override event EventHandler ApplicationIdle;
		protected override event EventHandler ResizeBegin;
		protected override event EventHandler ResizeEnd;
		protected override event EventHandler Activated;
		protected override event EventHandler Deactivated;

		protected override Icon WindowIcon
		{
			set { Icon = value.ToImageSource(); }
		}

		protected override FormWindowState WindowState
		{
			get { return WindowStateInternal.ToFormWindowState(); }
			set { WindowStateInternal = value.ToFormWindowState(); }
		}

		protected override FormBorderStyle FormBorderStyle
		{
			get
			{
				if (ResizeMode == ResizeMode.NoResize && WindowStyle == WindowStyle.None)
					return FormBorderStyle.None;
				if (ResizeMode == ResizeMode.CanMinimize)
					return FormBorderStyle.FixedSingle;
				return FormBorderStyle.Sizable;
			}
			set
			{
				if (value == FormBorderStyle.None)
				{
					ResizeMode = ResizeMode.NoResize;

					if (WindowStyle != WindowStyle.None)
						_lastWindowStyle = WindowStyle;
					
					WindowStyle = WindowStyle.None;
					return;
				}

				if (LogicHelper.EqualsOr(value, FormBorderStyle.Fixed3D, FormBorderStyle.FixedDialog,
					FormBorderStyle.FixedSingle, FormBorderStyle.FixedToolWindow))
				{
					ResizeMode = ResizeMode.CanMinimize;
					if (_lastWindowStyle != WindowStyle.None)
						WindowStyle = _lastWindowStyle;
					return;
				}

				ResizeMode = ResizeMode.CanResize;
				if (_lastWindowStyle != WindowStyle.None)
					WindowStyle = _lastWindowStyle;
			}
		}

		protected abstract ResizeMode ResizeMode { get; set; }
		protected abstract WindowStyle WindowStyle { get; set; }
		protected abstract override Rectangle RestoreBounds { get; }
		protected abstract ImageSource Icon { get; set; }
		protected abstract WindowState WindowStateInternal { get; set; }
		protected abstract override bool TopMost { get; set; }
		protected abstract override bool MaximizeBox { get; set; }

		private const int RESIZE_UPDATE_INTERVAL = 100;
		private WindowStyle _lastWindowStyle;
		private bool _isResizing;
		private bool _runned;
		private readonly DispatcherTimer _resizeTimer;
		private readonly DispatcherTimer _applicationTimer;
		private bool _closed;
		private IntPtr _lastFocusedHwnd;

		protected WpfXnaBaseAdapter(DispatcherObject dispatcherObject)
		{
			if (dispatcherObject == null)
				throw new ArgumentNullException("dispatcherObject");
			
			_lastWindowStyle = WindowStyle.None;
			_resizeTimer = new DispatcherTimer(new TimeSpan(0, 0, 0, 0, RESIZE_UPDATE_INTERVAL), DispatcherPriority.Normal, ResizeTimerCallBack, dispatcherObject.Dispatcher) { IsEnabled = false };
			_applicationTimer = new DispatcherTimer(DispatcherPriority.ApplicationIdle, dispatcherObject.Dispatcher) { IsEnabled = false };
			_applicationTimer.Tick += ApplicationTimerOnTick;
			ControlActivated += (sender, args) => RaiseActivated();
			ControlDeactivated += (sender, args) => RaiseDeactivated();
			MouseEnter += OnMouseEnter;
			MouseLeave += OnMouseLeave;
			Loaded += OnLoaded;
			_lastFocusedHwnd = Handle;
		}

		protected override void Dispose(bool disposing)
		{
			_resizeTimer.Stop();
			_applicationTimer.Tick -= ApplicationTimerOnTick;
			_applicationTimer.Stop();

			MouseEnter -= OnMouseEnter;
			MouseLeave -= OnMouseLeave;

			ApplicationIdle = null;
			ResizeBegin = null;
			ResizeEnd = null;
			Activated = null;
			Deactivated = null;

			base.Dispose(disposing);
		}

		public sealed override void Close()
		{
			_applicationTimer.Stop();
			_resizeTimer.Stop();
			if (!_closed)
			{
				_closed = true;
				ClosesInternal();
			}
		}

		internal void InitializeInternal()
		{
			Initialize();
		}

		protected override void Run()
		{
			if (_runned)
				return;

			_runned = true;
			_applicationTimer.Start();
		}

		protected abstract void ClosesInternal();

		private void OnMouseLeave(object sender, EventArgs eventArgs)
		{
			SetFocus(_lastFocusedHwnd);
		}

		private void OnMouseEnter(object sender, EventArgs eventArgs)
		{
			_lastFocusedHwnd = GetFocus();
			SetFocus(Handle);
		}

		private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
		{
			Loaded -= OnLoaded;
			_lastWindowStyle = WindowStyle;
			SizeChangedWpf += OnSizeChangedWpf;
		}

		private void ResizeTimerCallBack(object sender, EventArgs eventArgs)
		{
			_resizeTimer.IsEnabled = false;
			RaiseEndResize();
		}

		private void OnSizeChangedWpf(object sender, SizeChangedEventArgs sizeChangedEventArgs)
		{
			if (!_isResizing)
				RaiseBeginResize();

			_isResizing = true;
			_resizeTimer.IsEnabled = true;
			_resizeTimer.Stop();
			_resizeTimer.Start();
		}

		private void RaiseBeginResize()
		{
			var handler = ResizeBegin;
			if (handler != null)
				handler(this, new EventArgs());
		}

		private void RaiseEndResize()
		{
			var handler = ResizeEnd;
			if (handler != null)
				handler(this, new EventArgs());
		}

		private void RaiseApplicationIdle()
		{
			var handler = ApplicationIdle;
			if (handler != null)
				handler(this, new EventArgs());
		}

		private void ApplicationTimerOnTick(object sender, EventArgs eventArgs)
		{
			RaiseApplicationIdle();
		}

		private void RaiseActivated()
		{
			var handler = Activated;
			if (handler != null)
				handler(this, new EventArgs());
		}

		private void RaiseDeactivated()
		{
			var handler = Deactivated;
			if (handler != null)
				handler(this, new EventArgs());
		}

		[DllImport("user32", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
		private static extern IntPtr SetFocus(IntPtr hwnd);

		[DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
		private static extern IntPtr GetFocus();
	}
}
