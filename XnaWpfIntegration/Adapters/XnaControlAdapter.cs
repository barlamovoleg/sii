﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace XnaWpfIntegration.Adapters
{
	internal class XnaControlAdapter : WpfXnaBaseAdapter
	{
		internal event RoutedEventHandler Closed;

		protected override event RoutedEventHandler ControlActivated;
		protected override event RoutedEventHandler ControlDeactivated;
		protected override event RoutedEventHandler Loaded;
		protected override event SizeChangedEventHandler SizeChangedWpf;
		protected override event CancelEventHandler Closing;

		protected override bool TopMost { get { return false; } set { } }
		protected override bool MaximizeBox { get { return false; } set { } }
		protected override ImageSource Icon { get; set; }
		protected override WindowState WindowStateInternal { get; set; }
		protected override ResizeMode ResizeMode { get; set; }
		protected override WindowStyle WindowStyle { get; set; }
		protected override Rectangle RestoreBounds
		{
			get
			{
				var loc = new System.Windows.Point(Location.X, Location.Y);
				var screenLoc = _xnaControlContainer.PointToScreen(loc);
				return new Rectangle((int)screenLoc.X, (int)screenLoc.Y, Width, Height);
			}
		}

		private bool _isActive;
		private Window _ownerdWindow;
		private readonly UserControl _xnaControlContainer;

		public XnaControlAdapter(UserControl xnaControlContainer)
			: base(xnaControlContainer)
		{
			_xnaControlContainer = xnaControlContainer;

			_xnaControlContainer.Loaded += ContainerOnLoaded;
			_xnaControlContainer.SizeChanged += RaiseSizeChangedWpf;
			_xnaControlContainer.MouseDown += ContainerOnMouseDown;
			_xnaControlContainer.GotFocus += ContainerOnGotFocus;
			_xnaControlContainer.LostFocus += ContainerOnLostFocus;
			_xnaControlContainer.Unloaded += RaiseClosing;
		}

		protected override void Dispose(bool disposing)
		{
			_xnaControlContainer.Loaded -= ContainerOnLoaded;
			_xnaControlContainer.SizeChanged -= RaiseSizeChangedWpf;
			_xnaControlContainer.MouseDown -= ContainerOnMouseDown;
			_xnaControlContainer.GotFocus -= ContainerOnGotFocus;
			_xnaControlContainer.LostFocus -= ContainerOnLostFocus;
			_xnaControlContainer.Unloaded -= RaiseClosing;

			ControlActivated = null;
			ControlDeactivated = null;
			Loaded = null;
			SizeChangedWpf = null;
			Closing = null;
			Closed = null;

			base.Dispose(disposing);
		}

		protected override void ClosesInternal()
		{
			RaiseClosed(this, new RoutedEventArgs());
		}

		private void ContainerOnLostFocus(object sender, RoutedEventArgs routedEventArgs)
		{
			if (!_isActive)
				return;

			_isActive = false;
			RaiseControlDeactivated(routedEventArgs);
		}

		private void ContainerOnGotFocus(object sender, RoutedEventArgs routedEventArgs)
		{
			if (_isActive)
				return;

			_isActive = true;
			RaiseControlActivated(routedEventArgs);
		}

		private void ContainerOnMouseDown(object sender, MouseButtonEventArgs mouseButtonEventArgs)
		{
			if (!_xnaControlContainer.IsFocused)
				_xnaControlContainer.Focus();
		}

		private void ContainerOnLoaded(object sender, RoutedEventArgs routedEventArgs)
		{
			if (_ownerdWindow != null)
			{
				_ownerdWindow.Activated -= OwnerdWindowOnActivated;
				_ownerdWindow.Deactivated -= OwnerdWindowOnDeactivated;
			}

			_ownerdWindow = Helpers.VisualTreeHelper.FindAncestor<Window>(_xnaControlContainer);
			if (_ownerdWindow != null)
			{
				_ownerdWindow.Activated += OwnerdWindowOnActivated;
				_ownerdWindow.Deactivated += OwnerdWindowOnDeactivated;
			}

			RaiseLoaded(sender, routedEventArgs);
		}

		private void OwnerdWindowOnActivated(object sender, EventArgs eventArgs)
		{
// ReSharper disable once CoVariantArrayConversion
			var focused = Helpers.LogicHelper.EqualsOr(FocusManager.GetFocusedElement(_xnaControlContainer), new[] {null, _xnaControlContainer});

			if (focused && !_isActive)
				ContainerOnGotFocus(sender, new RoutedEventArgs());
		}

		private void OwnerdWindowOnDeactivated(object sender, EventArgs eventArgs)
		{
			ContainerOnLostFocus(sender, new RoutedEventArgs());
		}

		private void RaiseLoaded(object sender, RoutedEventArgs e)
		{
			var handler = Loaded;
			if (handler != null)
				handler(sender, e);
		}

		private void RaiseClosing(object sender, RoutedEventArgs e)
		{
			var handler = Closing;
			if (handler != null)
				handler(sender, new CancelEventArgs());
		}

		private void RaiseClosed(object sender, RoutedEventArgs e)
		{
			var handler = Closed;
			if (handler != null)
				handler(sender, e);
		}

		private void RaiseSizeChangedWpf(object sender, SizeChangedEventArgs e)
		{
			var handler = SizeChangedWpf;
			if (handler != null)
				handler(sender, e);
		}

		private void RaiseControlActivated(RoutedEventArgs e)
		{
			RoutedEventHandler handler = ControlActivated;
			if (handler != null)
				handler(this, e);
		}

		private void RaiseControlDeactivated(RoutedEventArgs e)
		{
			RoutedEventHandler handler = ControlDeactivated;
			if (handler != null)
				handler(this, e);
		}
	}
}
