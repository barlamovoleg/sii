﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using XnaWpfIntegration.Helpers;

namespace XnaWpfIntegration.Adapters
{
	internal class XnaWindowAdapter : WpfXnaBaseAdapter
	{
		internal event Action<bool> MaximizeBoxChanged;

		protected override event RoutedEventHandler ControlActivated;
		protected override event RoutedEventHandler ControlDeactivated;
		protected override event RoutedEventHandler Loaded;
		protected override event SizeChangedEventHandler SizeChangedWpf;
		protected override event CancelEventHandler Closing;

		protected override bool TopMost
		{
			get { return _containerWindow.Topmost; }
			set { _containerWindow.Topmost = value; }
		}

		protected override bool MaximizeBox
		{
			get { return _maximizeBox; }
			set
			{
				_maximizeBox = value;
				RaiseMaximizeBoxChanged(value);
			}
		}

		protected override ImageSource Icon
		{
			get { return _containerWindow.Icon; }
			set { _containerWindow.Icon = value; }
		}

		protected override WindowState WindowStateInternal
		{
			get { return _containerWindow.WindowState; }
			set { _containerWindow.WindowState = value; }
		}

		protected override ResizeMode ResizeMode
		{
			get { return _containerWindow.ResizeMode; }
			set { _containerWindow.ResizeMode = value; }
		}

		protected override WindowStyle WindowStyle
		{
			get { return _containerWindow.WindowStyle; }
			set { _containerWindow.WindowStyle = value; }
		}

		protected override Rectangle RestoreBounds
		{
			get { return _containerWindow.RestoreBounds.ToRectangle(); }
		}

		private bool _isActive;
		private bool _maximizeBox;
		private readonly Window _containerWindow;

		public XnaWindowAdapter(Window containerWindow)
			: base(containerWindow)
		{
			if (containerWindow == null) 
				throw new ArgumentNullException("containerWindow");

			_containerWindow = containerWindow;

			_containerWindow.Loaded += RaiseLoaded;
			_containerWindow.SizeChanged += RaiseSizeChangedWpf;
			_containerWindow.MouseDown += ContainerOnMouseDown;
			_containerWindow.Activated += ContainerOnGotFocus;
			_containerWindow.Deactivated += ContainerOnLostFocus;
			_containerWindow.Unloaded += RaiseClosing;

			_maximizeBox = _containerWindow.ResizeMode != ResizeMode.NoResize;
		}

		protected override void Dispose(bool disposing)
		{
			_containerWindow.Loaded -= RaiseLoaded;
			_containerWindow.SizeChanged -= RaiseSizeChangedWpf;
			_containerWindow.MouseDown -= ContainerOnMouseDown;
			_containerWindow.Activated -= ContainerOnGotFocus;
			_containerWindow.Deactivated -= ContainerOnLostFocus;
			_containerWindow.Unloaded -= RaiseClosing;

			ControlActivated = null;
			ControlDeactivated = null;
			Loaded = null;
			SizeChangedWpf = null;
			Closing = null;
			MaximizeBoxChanged = null;

			base.Dispose(disposing);
		}

		protected override void ClosesInternal()
		{
			_containerWindow.Close();
		}

		private void ContainerOnLostFocus(object sender, EventArgs eventArgs)
		{
			if (!_isActive)
				return;

			_isActive = false;
			RaiseControlDeactivated(new RoutedEventArgs());
		}

		private void ContainerOnGotFocus(object sender, EventArgs eventArgs)
		{
			if (_isActive)
				return;

			_isActive = true;
			RaiseControlActivated(new RoutedEventArgs());
		}

		private void ContainerOnMouseDown(object sender, MouseButtonEventArgs mouseButtonEventArgs)
		{
			if (!_containerWindow.IsFocused)
				_containerWindow.Focus();
		}

		private void RaiseLoaded(object sender, RoutedEventArgs e)
		{
			var handler = Loaded;
			if (handler != null)
				handler(sender, e);
		}

		private void RaiseClosing(object sender, RoutedEventArgs e)
		{
			var handler = Closing;
			if (handler != null)
				handler(sender, new CancelEventArgs());
		}

		private void RaiseSizeChangedWpf(object sender, SizeChangedEventArgs e)
		{
			var handler = SizeChangedWpf;
			if (handler != null)
				handler(sender, e);
		}

		private void RaiseMaximizeBoxChanged(bool value)
		{
			var handler = MaximizeBoxChanged;
			if (handler != null)
				handler(value);
		}

		private void RaiseControlActivated(RoutedEventArgs e)
		{
			RoutedEventHandler handler = ControlActivated;
			if (handler != null)
				handler(this, e);
		}

		private void RaiseControlDeactivated(RoutedEventArgs e)
		{
			RoutedEventHandler handler = ControlDeactivated;
			if (handler != null)
				handler(this, e);
		}
	}
}
