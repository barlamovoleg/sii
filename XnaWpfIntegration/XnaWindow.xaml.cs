﻿using System;
using System.Windows;
using System.Windows.Forms.Integration;
using XnaCoreAdapted.Integration;
using XnaWpfIntegration.Adapters;

namespace XnaWpfIntegration
{
	public partial class XnaWindow : IXnaGamePresenter, IXnaGamePresenterInternal
	{
		public bool IsGameAttached { get; private set; }
		internal XnaGame AttachedGame { get; private set; }

		IXnaGameContainer IXnaGamePresenterInternal.XnaGameHostContainer
		{
			get { return _xnaWindowAdapter; }
		}

		private readonly XnaWindowAdapter _xnaWindowAdapter;

		public XnaWindow()
		{
			InitializeComponent();
			ApplyTemplate();

			var template = Template;
			var winFormsHost = template.FindName("FormsHost", this) as WindowsFormsHost;
			if (winFormsHost == null)
				throw new Exception("WindowsForms Host has not been founded in XnaWindow Template");

			_xnaWindowAdapter = new XnaWindowAdapter(this);
			_xnaWindowAdapter.InitializeInternal();
			winFormsHost.Child = _xnaWindowAdapter;
			Closed += OnClosed;
			ResizeMode = ResizeMode.CanResize;
		}

		public void Run()
		{
			if (!IsGameAttached)
				throw new Exception("The game was not attached to the window, and therefore can not be runed.");

			AttachedGame.Run();
		}

		void IXnaGamePresenterInternal.AttachGame(XnaGame game)
		{
			if (IsGameAttached)
				throw new Exception("This window has been attached with another xna game already.");

			IsGameAttached = true;
			AttachedGame = game;
		}

		private void OnClosed(object sender, EventArgs eventArgs)
		{
			if (IsGameAttached)
				AttachedGame.Dispose();
		}
	}
}
