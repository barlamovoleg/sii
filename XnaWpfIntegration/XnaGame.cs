﻿using XnaCoreAdapted;

namespace XnaWpfIntegration
{
	public abstract class XnaGame : Game
	{
		protected XnaCoreAdapted.Core.GraphicsDeviceManager Graphics { get; private set; }

		protected XnaGame(IXnaGamePresenter xnaGameContainer)
			: base(((IXnaGamePresenterInternal)xnaGameContainer).XnaGameHostContainer)
		{
			Graphics = new XnaCoreAdapted.Core.GraphicsDeviceManager(this);
			Content.RootDirectory = "Content";

			((IXnaGamePresenterInternal)xnaGameContainer).AttachGame(this);
		}
	}
}
