﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XnaWpfIntegration
{
	public interface IXnaGamePresenter
	{
		bool IsGameAttached { get; }
		void Run();
	}
}
