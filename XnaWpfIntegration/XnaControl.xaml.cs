﻿using System;
using System.Windows.Forms.Integration;
using System.Windows.Input;
using XnaCoreAdapted.Integration;
using XnaWpfIntegration.Adapters;

namespace XnaWpfIntegration
{
	public partial class XnaControl : IXnaGamePresenter, IXnaGamePresenterInternal
	{
		public bool IsGameAttached { get; private set; }
		internal XnaGame AttachedGame { get; private set; }

		IXnaGameContainer IXnaGamePresenterInternal.XnaGameHostContainer
		{
			get { return _xnaControlAdapter; }
		}

		private readonly XnaControlAdapter _xnaControlAdapter;

		public XnaControl()  
		{
			InitializeComponent();
			ApplyTemplate();

			var template = Template;
			var winFormsHost = template.FindName("FormsHost", this) as WindowsFormsHost;
			if (winFormsHost == null)
				throw new Exception("WindowsForms Host has not been founded in XnaWindow Template");

			_xnaControlAdapter = new XnaControlAdapter(this);
			_xnaControlAdapter.InitializeInternal();
			winFormsHost.Child = _xnaControlAdapter;
		}

		public void Run()
		{
			if (!IsGameAttached)
				throw new Exception("The game was not attached to the control, and therefore can not be runed.");

			AttachedGame.Run();
		}

		void IXnaGamePresenterInternal.AttachGame(XnaGame game)
		{
			if (IsGameAttached)
				throw new Exception("This control has been attached with another xna game already.");

			IsGameAttached = true;
			AttachedGame = game;
		}
	}
}
