﻿using System;
using System.Windows;
using System.Windows.Forms;

namespace XnaWpfIntegration.Helpers
{
	internal static class WindowStateConverters
	{
		internal static FormWindowState ToFormWindowState(this WindowState windowState)
		{
			switch (windowState)
			{
				case WindowState.Maximized: { return FormWindowState.Maximized; }
				case WindowState.Minimized: { return FormWindowState.Minimized; }
				case WindowState.Normal: { return FormWindowState.Normal; }
				default: throw new Exception("invalid WpfWindowState");
			}
		}

		internal static WindowState ToFormWindowState(this  FormWindowState windowState)
		{
			switch (windowState)
			{
				case FormWindowState.Maximized: { return WindowState.Maximized; }
				case FormWindowState.Minimized: { return WindowState.Minimized; }
				case FormWindowState.Normal: { return WindowState.Normal; }
				default: throw new Exception("invalid WinFormsWindowState");
			}
		}

	}
}

