﻿using System;
using System.Linq;

namespace XnaWpfIntegration.Helpers
{
	internal static class LogicHelper
	{
		internal static bool Or(params bool[] values)
		{
			if (values == null)
				throw new ArgumentNullException("values");

			return values.Any(value => value);
		}

		internal static bool And(params bool[] values)
		{
			if (values == null)
				throw new ArgumentNullException("values");

			return values.All(value => value);
		}

		internal static bool EqualsOr<T>(T value, params T[] objects)
		{
			if (objects == null)
				throw new ArgumentNullException("objects");

			return objects.Contains(value);
		}
	}
}
