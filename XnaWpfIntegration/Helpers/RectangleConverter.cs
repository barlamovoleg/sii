﻿using System.Drawing;
using System.Windows;

namespace XnaWpfIntegration.Helpers
{
	internal static class RectangleConverter
	{
		internal static Rect ToRect(this Rectangle rectangle)
		{
			return new Rect(rectangle.Left, rectangle.Top, rectangle.Width, rectangle.Height);
		}

		internal static Rectangle ToRectangle(this Rect rectangle)
		{
			return new Rectangle((int)rectangle.Left, (int)rectangle.Top, (int)rectangle.Width, (int)rectangle.Height);
		}
	}
}
