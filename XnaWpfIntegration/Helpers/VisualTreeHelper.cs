﻿using System;
using System.Windows;
using System.Windows.Media;

namespace XnaWpfIntegration.Helpers
{
	internal class VisualTreeHelper
	{
		public static T FindAncestor<T>(FrameworkElement control) where T : Visual
		{
			if (control == null)
				throw new ArgumentNullException("control");

			var parent = control.Parent as FrameworkElement;
			if (parent == null)
				return null;

			var casted = parent as T;
			if (casted != null)
				return casted;

			return FindAncestor<T>(parent);
		}
	}
}
