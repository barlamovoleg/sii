﻿using System;
using System.Runtime.Serialization;
using Common;

// ReSharper disable once CheckNamespace
namespace SIIGame2D
{
	internal class GameException : AppException
	{
		public GameException(string message, string key) : base(message, key)
		{
		}

		public GameException(string message, Exception innerException, string key) : base(message, innerException, key)
		{
		}

		protected GameException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
