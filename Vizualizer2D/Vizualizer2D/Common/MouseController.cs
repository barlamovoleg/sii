﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using SIIGame2D;

// ReSharper disable once CheckNamespace
namespace Externals
{
	public sealed class MouseController
	{
		/// <summary>
		/// Gets the state of the mouse.
		/// </summary>
		/// <value>
		/// The state of the mouse.
		/// </value>
		public MouseState MouseState { get { return _mouseState; } }
		private MouseState _mouseState;

		/// <summary>
		/// Gets previus the state of the mouse.
		/// </summary>
		/// <value>
		/// The previus state of the mouse.
		/// </value>
		public MouseState PreviusMouseState { get { return _previusMouseState; } }
		private MouseState _previusMouseState;

		/// <summary>
		/// Gets or sets the mouse position.
		/// </summary>
		/// <value>
		/// The position.
		/// </value>
		public Point Position
		{
			get { return new Point(_mouseState.X, _mouseState.Y); }
			set { Mouse.SetPosition(value.X, value.Y); }
		}

		/// <summary>
		/// Gets a value indicating whether mouse left button is pressed.
		/// </summary>
		/// <value>
		///   <c>true</c> if mouse left button pressed; otherwise, <c>false</c>.
		/// </value>
		public bool LeftButton { get { return _mouseState.LeftButton == ButtonState.Pressed; } }
		/// <summary>
		/// Gets a value indicating whether mouse right button is pressed.
		/// </summary>
		/// <value>
		///   <c>true</c> if mouse right button pressed; otherwise, <c>false</c>.
		/// </value>
		public bool RightButton { get { return _mouseState.RightButton == ButtonState.Pressed; } }

		/// <summary>
		/// Gets a value indicating whether mouse left button clicking.
		/// </summary>
		/// <value>
		///   <c>true</c> if mouse left button clicking; otherwise, <c>false</c>.
		/// </value>
		public bool LeftButtonClicking
		{
			get
			{
				bool prev = _previusMouseState.LeftButton == ButtonState.Pressed;
				bool now = _mouseState.LeftButton == ButtonState.Pressed;
				return now && !prev;
			}
		}
		/// <summary>
		/// Gets a value indicating whether mouse right button clicking.
		/// </summary>
		/// <value>
		///   <c>true</c> if mouse right button clicking; otherwise, <c>false</c>.
		/// </value>
		public bool RightButtonClicking
		{
			get
			{
				bool prev = _previusMouseState.RightButton == ButtonState.Pressed;
				bool now = _mouseState.RightButton == ButtonState.Pressed;
				return now && !prev;
			}
		}
		/// <summary>
		/// Gets a value indicating whether mouse left button clicked.
		/// </summary>
		/// <value>
		///   <c>true</c> if mouse left button clicking; otherwise, <c>false</c>.
		/// </value>
		public bool LeftButtonClicked
		{
			get
			{
				bool prev = _previusMouseState.LeftButton == ButtonState.Pressed;
				bool now = _mouseState.LeftButton == ButtonState.Pressed;
				return !now && prev;
			}
		}
		/// <summary>
		/// Gets a value indicating whether mouse right button clicked.
		/// </summary>
		/// <value>
		///   <c>true</c> if mouse right button clicking; otherwise, <c>false</c>.
		/// </value>
		public bool RightButtonClicked
		{
			get
			{
				bool prev = _previusMouseState.RightButton == ButtonState.Pressed;
				bool now = _mouseState.RightButton == ButtonState.Pressed;
				return !now && prev;
			}
		}

		public int MouseWheelValue
		{
			get { return _mouseWheelValue; }
		}

		private int _mouseWheelValue;

		internal MouseController()
		{
			_previusMouseState = Mouse.GetState();
			_mouseState = Mouse.GetState();
		}

		internal void OnMouseWheelChange(int delta)
		{
			_mouseWheelValue += delta;
		}

		/// <summary>
		/// Start of the update input iterate.
		/// </summary>
		/// <param name="gameTime">The game time.</param>
		internal void UpdateStart(GameTime gameTime)
		{
			_mouseState = Mouse.GetState();
		}

		/// <summary>
		/// End of the update input iterate.
		/// </summary>
		/// <param name="gameTime">The game time.</param>
		internal void UpdateEnd(GameTime gameTime)
		{
			_previusMouseState = _mouseState;
		}
	}
}
