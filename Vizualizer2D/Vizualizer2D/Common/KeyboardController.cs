﻿using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

// ReSharper disable once CheckNamespace
namespace Externals
{
	public sealed class KeyboardController
	{
		/// <summary>
		/// Gets the state of the keyboard.
		/// </summary>
		/// <value>
		/// The state of the keyboard.
		/// </value>
		public KeyboardState KeyboardState { get { return _keyboardState; } }
		private KeyboardState _keyboardState;

		/// <summary>
		/// Gets previus the state of the keyboard.
		/// </summary>
		/// <value>
		/// The previus state of the previus keyboard.
		/// </value>
		public KeyboardState PreviusKeyboardState { get { return _previusKeyboardState; } }
		private KeyboardState _previusKeyboardState;

		
		internal KeyboardController()
		{
			_previusKeyboardState = Keyboard.GetState();
			_keyboardState = Keyboard.GetState();
		}

		internal void UpdateStart(GameTime gameTime)
		{
			_keyboardState = Keyboard.GetState();
		}

		internal void UpdateEnd(GameTime gameTime)
		{
			_previusKeyboardState = _keyboardState;
		}

		/// <summary>
		/// Determines whether keyboard key pressed.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns>
		///   <c>true</c> if key pressed; otherwise, <c>false</c>.
		/// </returns>
		public bool IsKeyPressed(Keys key)
		{
			return _keyboardState.IsKeyDown(key) && !_previusKeyboardState.IsKeyDown(key);
		}

		/// <summary>
		/// Determines whether keyboard keys pressed.
		/// </summary>
		/// <param name="keys">The keys.</param>
		/// <returns>
		///   <c>true</c> if key pressed; otherwise, <c>false</c>.
		/// </returns>
		public bool IsKeyPressed(params Keys[] keys)
		{
			return keys.All(key => _keyboardState.IsKeyDown(key) && !_previusKeyboardState.IsKeyDown(key));
		}

		/// <summary>
		/// Determines whether keyboard key has been upped.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns>
		///   <c>true</c> if key pressed; otherwise, <c>false</c>.
		/// </returns>
		public bool IsKeyHasBeenUpped(Keys key)
		{
			return _keyboardState.IsKeyUp(key) && _previusKeyboardState.IsKeyDown(key);
		}

		/// <summary>
		/// Determines whether keyboard keys has been upped.
		/// </summary>
		/// <param name="keys">The keys.</param>
		/// <returns>
		///   <c>true</c> if key pressed; otherwise, <c>false</c>.
		/// </returns>
		public bool IsKeyHasBeenUpped(params Keys[] keys)
		{
			return keys.All(key => _keyboardState.IsKeyUp(key) && _previusKeyboardState.IsKeyDown(key));
		}

		/// <summary>
		/// Determines whether keyboard key pressing now.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns>
		///   <c>true</c> if key pressing now; otherwise, <c>false</c>.
		/// </returns>
		public bool IsKeyPressing(Keys key)
		{
			return _keyboardState.IsKeyDown(key);
		}

		/// <summary>
		/// Determines whether keyboard key pressing now.
		/// </summary>
		/// <param name="keys">The keys.</param>
		/// <returns>
		///   <c>true</c> if key pressing now; otherwise, <c>false</c>.
		/// </returns>
		public bool IsKeyPressing(params Keys[] keys)
		{
			return keys.All(key => _keyboardState.IsKeyDown(key));
		}

		/// <summary>
		/// Determines whether keyboard key not pressed now.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns>
		///   <c>true</c> if key not pressed now; otherwise, <c>false</c>.
		/// </returns>
		public bool IsKeyUp(Keys key)
		{
			return _keyboardState.IsKeyUp(key);
		}

		/// <summary>
		/// Determines whether keyboard key not pressed now.
		/// </summary>
		/// <param name="keys">The keys.</param>
		/// <returns>
		///   <c>true</c> if key not pressed now; otherwise, <c>false</c>.
		/// </returns>
		public bool IsKeyUp(params Keys[] keys)
		{
			return keys.All(key => _keyboardState.IsKeyUp(key));
		}
	}
}
