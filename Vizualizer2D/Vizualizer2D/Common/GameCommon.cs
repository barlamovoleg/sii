﻿using System;
using Common.Logger;
using Externals;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

// ReSharper disable once CheckNamespace
namespace SIIGame2D
{
	internal static class GameCommon
	{
		internal static bool IsActive { get; private set; }
		internal static ILogger Logger { get; set; }
		internal static ILogger WorldLogger { get; set; }
		internal static Random Seed { get; set; }

		internal static KeyboardController KeyboardController { get; private set; }
		internal static MouseController MouseController { get; private set; }
		internal static GraphicsDevice GraphicsDevice { get; private set; }

		internal static void UpdateStart(GameTime gameTime, bool isActive)
		{
			IsActive = isActive;
			KeyboardController.UpdateStart(gameTime);
			MouseController.UpdateStart(gameTime);
		}

		internal static void UpdateEnd(GameTime gameTime)
		{
			KeyboardController.UpdateEnd(gameTime);
			MouseController.UpdateEnd(gameTime);
		}

		internal static void Initialize(GraphicsDevice graphicsDevice, MouseController mouseController,
			KeyboardController keyboardController)
		{
			GraphicsDevice = graphicsDevice;
			MouseController = mouseController;
			KeyboardController = keyboardController;
		}
	}
}
