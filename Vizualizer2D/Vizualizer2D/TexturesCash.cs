﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;

namespace SIIGame2D
{
	internal static class TexturesCash
	{
		public static Dictionary<string, Texture2D> _cashedTextures = new Dictionary<string, Texture2D>();

		public static void Clear()
		{
			_cashedTextures.Clear();
		}
	}
}
