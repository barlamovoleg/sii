﻿using System;
using Microsoft.Xna.Framework;

// ReSharper disable once CheckNamespace
namespace SIIGame2D.Geometry
{
	public struct PointF : IEquatable<PointF>
	{
		/// <summary>
		/// Gets the zero PointF.
		/// </summary>
		public static PointF Zero
		{
			get { return new PointF(0, 0); }
		}

		public float X;
		public float Y;

		public PointF(float x, float y)
		{
			X = x;
			Y = y;
		}

		/// <summary>
		/// To the vector2.
		/// </summary>
		/// <returns></returns>
		public Vector2 ToVector2()
		{
			return new Vector2(X, Y);
		}

		/// <summary>
		/// Returns a hash code for this instance.
		/// </summary>
		/// <returns>
		/// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
		/// </returns>
		public override int GetHashCode()
		{
			unchecked
			{
				// ReSharper disable NonReadonlyMemberInGetHashCode
				// ReSharper disable NonReadonlyFieldInGetHashCode
				return (X.GetHashCode() * 397) ^ Y.GetHashCode();
			}
		}

		/// <summary>
		/// Указывает, равен ли текущий объект другому объекту того же типа.
		/// </summary>
		/// <param name="other">Объект, который требуется сравнить с данным объектом.</param>
		/// <returns>
		/// true, если текущий объект равен параметру <paramref name="other"/>, в противном случае — false.
		/// </returns>
		public bool Equals(PointF other)
		{
			return X.Equals(other.X) && Y.Equals(other.Y);
		}

		/// <summary>
		/// Determines whether the specified <see cref="System.Object"/> is equal to this instance.
		/// </summary>
		/// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
		/// <returns>
		///   <c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.
		/// </returns>
		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			return obj is PointF && Equals((PointF)obj);
		}

		/// <summary>
		/// Returns a <see cref="System.String"/> that represents this instance.
		/// </summary>
		/// <returns>
		/// A <see cref="System.String"/> that represents this instance.
		/// </returns>
		public override string ToString()
		{
			return string.Format("({0}, {1})", X, Y);
		}

		/// <summary>
		/// Implements the operator !=.
		/// </summary>
		/// <param name="a">A.</param>
		/// <param name="b">The b.</param>
		/// <returns>
		/// The result of the operator.
		/// </returns>
		public static bool operator !=(PointF a, PointF b)
		{
			return !(a == b);
		}

		/// <summary>
		/// Implements the operator ==.
		/// </summary>
		/// <param name="a">A.</param>
		/// <param name="b">The b.</param>
		/// <returns>
		/// The result of the operator.
		/// </returns>
		public static bool operator ==(PointF a, PointF b)
		{
			return a.Equals(b);
		}
	}
}
