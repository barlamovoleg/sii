﻿using Microsoft.Xna.Framework;

// ReSharper disable once CheckNamespace
namespace SIIGame2D.Geometry
{
	public static class GeometryHelper
	{
		/// <summary>
		/// Convert point to vector2D.
		/// </summary>
		/// <param name="point">The point.</param>
		/// <returns></returns>
		public static Vector2 PointToVector2(Point point)
		{
			return new Vector2(point.X, point.Y);
		}

		/// <summary>
		/// Convert Vector2D to point.
		/// </summary>
		/// <param name="vector">The vector.</param>
		/// <returns></returns>
		public static Point Vector2ToPoint(Vector2 vector)
		{
			return new Point(MathApp.Round(vector.X), MathApp.Round(vector.Y));
		}

		/// <summary>
		/// Get the sum of tow points.
		/// </summary>
		/// <param name="point1">The point1.</param>
		/// <param name="point2">The point2.</param>
		/// <returns></returns>
		public static Point PointSum(Point point1, Point point2)
		{
			return new Point(point1.X + point2.X, point1.Y + point2.Y);
		}

		/// <summary>
		/// Get the subtraction of tow points.
		/// </summary>
		/// <param name="point1">The point1.</param>
		/// <param name="point2">The point2.</param>
		/// <returns></returns>
		public static Point PointSub(Point point1, Point point2)
		{
			return new Point(point1.X - point2.X, point1.Y - point2.Y);
		}

		/// <summary>
		/// Gets the bordering rectangle for figure.
		/// </summary>
		/// <param name="figure">The figure.</param>
		/// <returns></returns>
		public static Rectangle GetBorderingRectangle(IFigure2D figure)
		{
			return new Rectangle(figure.Left, figure.Top, figure.Width, figure.Height);
		}

		/// <summary>
		/// Gets the left top of the rectangle.
		/// </summary>
		/// <param name="rectangle">The rectangle.</param>
		/// <returns></returns>
		public static Point GetLeftTop(Rectangle rectangle)
		{
			return new Point(rectangle.Left, rectangle.Top);
		}

		/// <summary>
		/// Gets the left top of the figure.
		/// </summary>
		/// <param name="figure">The figure.</param>
		/// <returns></returns>
		public static Point GetLeftTop(IFigure2D figure)
		{
			return new Point(figure.Left, figure.Top);
		}

		/// <summary>
		/// Gets the left top of the rectangle.
		/// </summary>
		/// <param name="rectangle">The rectangle.</param>
		/// <returns></returns>
		public static Vector2 GetLeftTopVector(Rectangle rectangle)
		{
			return new Vector2(rectangle.Left, rectangle.Top);
		}

		/// <summary>
		/// Gets the left top of the figure.
		/// </summary>
		/// <param name="figure">The figure.</param>
		/// <returns></returns>
		public static Vector2 GetLeftTopVector(IFigure2D figure)
		{
			return new Vector2(figure.Left, figure.Top);
		}

		public static float DistanceBetweenPoints(Vector2 point1, Vector2 point2)
		{
			return (point2 - point1).Length();
		}

		public static float DistanceBetweenPoints(Point point1, Point point2)
		{
			return DistanceBetweenPoints(PointToVector2(point1), PointToVector2(point2));
		}
	}
}
