﻿using Microsoft.Xna.Framework;

// ReSharper disable once CheckNamespace
namespace SIIGame2D.Geometry
{
	public interface IFigure2D
	{
		/// <summary>
		/// Gets the left.
		/// </summary>
		int Left { get; }
		/// <summary>
		/// Gets the top.
		/// </summary>
		int Top { get; }
		/// <summary>
		/// Gets the width.
		/// </summary>
		int Width { get; }
		/// <summary>
		/// Gets the height.
		/// </summary>
		int Height { get; }
		/// <summary>
		/// Gets the center.
		/// </summary>
		Point Center { get; }
		/// <summary>
		/// Determines whether location contains the specified point.
		/// </summary>
		/// <param name="point">The point.</param>
		/// <returns>
		///   <c>true</c> if contains; otherwise, <c>false</c>.
		/// </returns>
		bool Contains(Point point);
		/// <summary>
		/// Determines whether location contains the specified point.
		/// </summary>
		/// <param name="x">The x.</param>
		/// <param name="y">The y.</param>
		/// <returns>
		///   <c>true</c> if contains; otherwise, <c>false</c>.
		/// </returns>
		bool Contains(int x, int y);
		/// <summary>
		/// Determines whether location contains the specified location.
		/// </summary>
		/// <param name="location">The location.</param>
		/// <returns>
		///   <c>true</c> if contains; otherwise, <c>false</c>.
		/// </returns>
		bool Contains(IFigure2D location);
		/// <summary>
		/// Moves location to current position + offset.
		/// </summary>
		/// <param name="offset">The offset.</param>
		void Move(Vector2 offset);
		/// <summary>
		/// Moves location to point.
		/// </summary>
		/// <param name="point">The point.</param>
		void MoveTo(Point point);
		/// <summary>
		/// Gets the coordinates.
		/// </summary>
		/// <returns></returns>
		Point[] GetCoordinates();
		/// <summary>
		/// Create figure from coordinates.
		/// </summary>
		/// <param name="points">The points.</param>
		/// <returns></returns>
		IFigure2D FromCoordinates(params Point[] points);
	}
}
