﻿using System;
using Microsoft.Xna.Framework;

// ReSharper disable once CheckNamespace
namespace SIIGame2D.Geometry
{
	/// <summary>
	/// Ax + By + C = 0
	/// </summary>
	public class Line2D
	{
		/// <summary>
		/// Gets or sets the rotation.
		/// </summary>
		/// <value>
		/// The rotation.
		/// </value>
		/// <exception cref="NotImplementedException"></exception>
		public float Rotation
		{
			get
			{
				if (IsVertical)
					return (float)Math.PI / 2;

				return (float)Math.Atan(Inclination);
			}
			set { Inclination = (float)Math.Tan(value); }
		}

		/// <summary>
		/// Gets or sets the inclination.
		/// y = kx + b. Inclination = k.
		/// </summary>
		/// <value>
		/// The inclination.
		/// </value>
		public float Inclination
		{
			get { return -A / B; }
			set { A = -value * B; }
		}

		/// <summary>
		/// Gets or sets the shift.
		/// y = kx + b. Shift = b.
		/// </summary>
		/// <value>
		/// The shift.
		/// </value>
		public float Shift
		{
			get { return -C / B; }
			set { C = -value * B; }
		}

		/// <summary>
		/// Gets a value indicating whether this line is vertical.
		/// </summary>
		/// <value>
		/// <c>true</c> if this line is vertical; otherwise, <c>false</c>.
		/// </value>
		public bool IsVertical { get { return Math.Abs(B) < float.Epsilon; } }
		/// <summary>
		/// Gets a value indicating whether this line is horizontal.
		/// </summary>
		/// <value>
		/// <c>true</c> if this line is horizontal; otherwise, <c>false</c>.
		/// </value>
		public bool IsHorizontal { get { return Math.Abs(A) < float.Epsilon; } }

		public float A;
		public float B;
		public float C;

		/// <summary>
		/// Initializes a new instance of the <see cref="Line2D"/> class.
		/// Ax + By + C = 0.
		/// </summary>
		/// <param name="a">a.</param>
		/// <param name="b">The b.</param>
		/// <param name="c">The c.</param>
		public Line2D(float a, float b, float c)
		{
			A = a;
			B = b;
			C = c;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Line2D"/> class.
		/// y = kx + b. k = inclination. b = shift.
		/// </summary>
		/// <param name="inclination">The inclination.</param>
		/// <param name="shift">The shift.</param>
		public Line2D(float inclination, float shift)
		{
			B = 1;
			Inclination = inclination;
			Shift = shift;
		}

		/// <summary>
		/// Determines whether location contains the specified point.
		/// </summary>
		/// <param name="point">The point.</param>
		/// <returns>
		///   <c>true</c> if contains; otherwise, <c>false</c>.
		/// </returns>
		public bool Contains(PointF point)
		{
			return Contains(point.X, point.Y);
		}

		/// <summary>
		/// Determines whether location contains the specified point.
		/// </summary>
		/// <param name="point">The point.</param>
		/// <returns>
		///   <c>true</c> if contains; otherwise, <c>false</c>.
		/// </returns>
		public bool Contains(Point point)
		{
			return Contains(point.X, point.Y);
		}

		/// <summary>
		/// Determines whether location contains the specified point.
		/// </summary>
		/// <param name="x">The x.</param>
		/// <param name="y">The y.</param>
		/// <returns>
		///   <c>true</c> if contains; otherwise, <c>false</c>.
		/// </returns>
		public bool Contains(float x, float y)
		{
			return Math.Abs(A * x + B * y + C) < float.Epsilon;
		}

		/// <summary>
		/// Determines whether location contains the specified point.
		/// </summary>
		/// <param name="x">The x.</param>
		/// <param name="y">The y.</param>
		/// <returns>
		///   <c>true</c> if contains; otherwise, <c>false</c>.
		/// </returns>
		public bool Contains(int x, int y)
		{
			return MathApp.Round(A * x + B * y + C) == 0;
		}

		/// <summary>
		/// Moves location to current position + offset.
		/// </summary>
		/// <param name="offset">The offset.</param>
		public void Move(Vector2 offset)
		{
			if (!IsVertical)
			{
				float x = offset.X;
				float y = -C / B + offset.Y;

				C = -A * x - B * y;
			}
			else
				C += offset.X;

		}

		/// <summary>
		/// Moves location to point.
		/// </summary>
		/// <param name="point">The point.</param>
		public void MoveTo(Point point)
		{
			if (!IsVertical)
			{
				float x = point.X;
				float y = point.Y;

				C = -A * x - B * y;
			}
			else
				C = point.X;
		}

		/// <summary>
		/// Equalses the specified other.
		/// </summary>
		/// <param name="other">The other.</param>
		/// <returns></returns>
		public bool Equals(Line2D other)
		{
			return Math.Abs(A - other.A) < float.Epsilon && Math.Abs(B - other.B) < float.Epsilon &&
				   Math.Abs(C - other.C) < float.Epsilon;
		}

		/// <summary>
		/// Adds the rotation.
		/// </summary>
		/// <param name="angle">The angle.</param>
		public void AddRotation(float angle)
		{
			Rotation += angle;
		}

		/// <summary>
		/// Add to rotation 90 degres.
		/// </summary>
		public void RotateTo90()
		{
			AddRotation(MathHelper.ToRadians(90));
		}

		/// <summary>
		/// Add to rotation -90 degres.
		/// </summary>
		public void RotateToMinus90()
		{
			AddRotation(MathHelper.ToRadians(-90));
		}

		/// <summary>
		/// Add to rotation 180 degres.
		/// </summary>
		public void RotateTo180()
		{
			AddRotation(MathHelper.ToRadians(180));
		}

		/// <summary>
		/// Add to rotation -180 degres.
		/// </summary>
		public void RotateToMinus180()
		{
			AddRotation(MathHelper.ToRadians(-180));
		}

		/// <summary>
		/// Resets the rotation.
		/// </summary>
		public void ResetRotation()
		{
			Rotation = 0;
		}

		/// <summary>
		/// Determines whether this line is normal of another line.
		/// </summary>
		/// <param name="line">The line.</param>
		/// <returns></returns>
		public bool IsNormalTo(Line2D line)
		{
			return Math.Abs(A * line.A + B * line.B) < float.Epsilon;
		}

		/// <summary>
		/// Determines whether this line is parallel to another line.
		/// </summary>
		/// <param name="line">The line.</param>
		/// <returns></returns>
		public bool IsParallel(Line2D line)
		{
			return Math.Abs(A / line.A - B / line.B) < float.Epsilon;
		}

		/// <summary>
		/// Gets the point of intersection this line and another line.
		/// </summary>
		/// <param name="line">The line.</param>
		/// <returns></returns>
		public PointF GetIntersection(Line2D line)
		{
			if (IsParallel(line))
				return new PointF(float.NaN, float.NaN);

			if (Math.Abs(line.A) < float.Epsilon)
				return line.GetIntersection(this);

			float y = (A * line.C / line.A - C) / (B - line.B * A / line.A);
			float x = -(line.B * y + line.C) / line.A;

			return new PointF(x, y);
		}

		/// <summary>
		/// Determines whether this line is intersect with another line.
		/// </summary>
		/// <param name="line">The line.</param>
		/// <returns></returns>
		public bool IsIntersect(Line2D line)
		{
			return !IsParallel(line);
		}

		/// <summary>
		/// Create line from angle and shift. 
		/// </summary>
		/// <param name="angle">The angle.</param>
		/// <param name="shift">The shift.</param>
		/// <returns></returns>
		public static Line2D FromAngle(float angle, float shift = 0)
		{
			var inclination = (float)Math.Tan(MathHelper.ToRadians(angle));
			return new Line2D(inclination, shift);
		}

		/// <summary>
		/// Create line from the two points.
		/// </summary>
		/// <param name="point1">The point1.</param>
		/// <param name="point2">The point2.</param>
		/// <returns></returns>
		public static Line2D FromTwoPoints(PointF point1, PointF point2)
		{
			var inclination = (point2.Y - point1.Y) / (point2.X - point1.X);
			var shift = (point1.X * point1.Y - point1.X * point2.Y) / (point2.X - point1.X) + point1.Y;

			return new Line2D(inclination, shift);
		}

		/// <summary>
		/// Create line from the vector2.
		/// </summary>
		/// <param name="vector">The vector.</param>
		/// <returns></returns>
		public static Line2D FromVector2(Vector2 vector)
		{
			return FromTwoPoints(new PointF(0, 0), new PointF(vector.X, vector.Y));
		}

		/// <summary>
		/// Create line from the point and angle.
		/// </summary>
		/// <param name="point">The point.</param>
		/// <param name="angle">The angle in radians.</param>
		/// <returns></returns>
		public static Line2D FromPointAngle(PointF point, float angle)
		{
			if (Math.Abs(MathHelper.ToDegrees(angle) - 90) < float.Epsilon)
				return new Line2D(1, 0, -point.X);

			var inclination = (float)Math.Tan(angle);
			var shift = point.Y - inclination * point.X;

			return new Line2D(inclination, shift);
		}

		/// <summary>
		/// Create line from the normal and point.
		/// </summary>
		/// <param name="normal">The normal.</param>
		/// <param name="point">The point.</param>
		/// <returns></returns>
		public static Line2D FromNormalAndPoint(Line2D normal, PointF point)
		{
			return new Line2D(normal.B, -normal.A, -normal.B * point.X + normal.A * point.Y);
		}
	}
}
