﻿using System;
using Microsoft.Xna.Framework;

// ReSharper disable once CheckNamespace
namespace SIIGame2D.Geometry
{
	public static class Translations2D
	{
		/// <summary>
		/// Rotates the point around point.
		/// </summary>
		/// <param name="center">The center.</param>
		/// <param name="point">The point.</param>
		/// <param name="alpha">The alpha.</param>
		/// <returns></returns>
		public static Point RotatePointAroundPoint(Point center, Point point, float alpha)
		{
			var o = center;
			var nx = MathApp.Round(Math.Cos(alpha) * (point.X - o.X) - Math.Sin(alpha) * (point.Y - o.Y) + o.X);
			var ny = MathApp.Round(Math.Cos(alpha) * (point.Y - o.Y) + Math.Sin(alpha) * (point.X - o.X) + o.Y);
			return new Point(nx, ny);
		}

		/// <summary>
		/// Rotates the vector2 around point.
		/// </summary>
		/// <param name="center">The center.</param>
		/// <param name="point">The point.</param>
		/// <param name="alpha">The alpha.</param>
		/// <returns></returns>
		public static Vector2 RotateVector2AroundPoint(Vector2 center, Vector2 point, float alpha)
		{
			var o = center;
			var nx = (float)(Math.Cos(alpha) * (point.X - o.X) - Math.Sin(alpha) * (point.Y - o.Y) + o.X);
			var ny = (float)(Math.Cos(alpha) * (point.Y - o.Y) + Math.Sin(alpha) * (point.X - o.X) + o.Y);
			return new Vector2(nx, ny);
		}

		/// <summary>
		/// Rotates the figure around point.
		/// </summary>
		/// <typeparam name="TFigure">The type of the figure.</typeparam>
		/// <param name="center">The center.</param>
		/// <param name="figure">The figure.</param>
		/// <param name="alpha">The alpha.</param>
		/// <returns></returns>
		public static TFigure RotateFigureAroundPoint<TFigure>(Point center, TFigure figure, float alpha) where TFigure : IFigure2D
		{
			var coords = figure.GetCoordinates();
			var rotatedCoords = new Point[coords.Length];

			for (int i = 0; i < coords.Length; i++)
				rotatedCoords[i] = RotatePointAroundPoint(center, coords[i], alpha);

			return (TFigure)figure.FromCoordinates(rotatedCoords);
		}

		/// <summary>
		/// Translates the coordinates.
		/// Factor is the ratio metrics.
		/// </summary>
		/// <param name="point">The point.</param>
		/// <param name="factor">The factor.</param>
		/// <returns></returns>
		public static Point TranslateCoordinates(Point point, Vector2 factor)
		{
			return new Point(MathApp.Round(point.X * factor.X), MathApp.Round(point.Y * factor.Y));
		}

		/// <summary>
		/// Translates the coordinates from metric1 to metric2.
		/// </summary>
		/// <param name="point">The point.</param>
		/// <param name="metric1">The metric1.</param>
		/// <param name="metric2">The metric2.</param>
		/// <returns></returns>
		public static Point TranslateCoordinates(Point point, Vector2 metric1, Vector2 metric2)
		{
			var factor = new Vector2(metric2.X / metric1.X, metric2.Y / metric1.Y);
			return TranslateCoordinates(point, factor);
		}

		/// <summary>
		/// Translates the coordinates.
		/// Factor is the ratio metrics.
		/// </summary>
		/// <param name="vector">The vector.</param>
		/// <param name="factor">The factor.</param>
		/// <returns></returns>
		public static Vector2 TranslateCoordinates(Vector2 vector, Vector2 factor)
		{
			return new Vector2(vector.X * factor.X, vector.Y * factor.Y);
		}

		/// <summary>
		/// Translates the coordinates from metric1 to metric2.
		/// </summary>
		/// <param name="vector">The vector.</param>
		/// <param name="metric1">The metric1.</param>
		/// <param name="metric2">The metric2.</param>
		/// <returns></returns>
		public static Vector2 TranslateCoordinates(Vector2 vector, Vector2 metric1, Vector2 metric2)
		{
			var factor = new Vector2(metric2.X / metric1.X, metric2.Y / metric1.Y);
			return TranslateCoordinates(vector, factor);
		}

		/// <summary>
		/// Rotates the vector2. (Radians)
		/// </summary>
		/// <param name="vector">The vector.</param>
		/// <param name="radians"></param>
		/// <returns></returns>
		public static Vector2 RotateVector2(Vector2 vector, float radians)
		{
			var cosRadians = (float)Math.Cos(radians);
			var sinRadians = (float)Math.Sin(radians);

			return new Vector2(
				vector.X * cosRadians - vector.Y * sinRadians,
				vector.X * sinRadians + vector.Y * cosRadians);
		}
	}
}
