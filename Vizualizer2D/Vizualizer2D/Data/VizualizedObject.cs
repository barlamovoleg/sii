﻿using System;
using System.IO;
using System.Threading;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using SIICore.Data;
using GameTime = XnaCoreAdapted.GameTime;

namespace SIIGame2D.Data
{
	internal class VizualizedObject
	{
		public bool Selected { get; set; }

		public Vector2 Position
		{
			get
			{
				return new Vector2(_dataModel.X, _dataModel.Y);
			}
		}
		public IVizualizedObject Model { get { return _dataModel; } }
		private readonly IVizualizedObject _dataModel;
		private Texture2D _texture;

		public VizualizedObject(IVizualizedObject dataModel)
		{
			_dataModel = dataModel;
			_dataModel.IconUpdated += DataModelOnIconUpdated;
		}

		private void DataModelOnIconUpdated()
		{
			LoadContent();
		}

		public void LoadContent()
		{
			if (string.IsNullOrWhiteSpace(_dataModel.IconPath))
				return;

			if (TexturesCash._cashedTextures.ContainsKey(_dataModel.IconPath))
			{
				_texture = TexturesCash._cashedTextures[_dataModel.IconPath];
				return;
			}

			try
			{
				using (var fileStream = new FileStream(_dataModel.IconPath, FileMode.Open))
				{
					_texture = Texture2D.FromStream(GameCommon.GraphicsDevice, fileStream);
					TexturesCash._cashedTextures.Add(_dataModel.IconPath, _texture);
				}
			}
			catch (Exception e)
			{
				Thread.Sleep(500);
				using (var fileStream = new FileStream(_dataModel.IconPath, FileMode.Open))
				{
					_texture = Texture2D.FromStream(GameCommon.GraphicsDevice, fileStream);
					TexturesCash._cashedTextures.Add(_dataModel.IconPath, _texture);
				}
			}
		}

		public void Draw(SpriteBatch spriteBatch, GameTime gameTime, Rectangle rectangle)
		{
			if (_texture != null)
			{
				Color clr = Color.White;
				if (Selected)
					clr = Color.Gold;

				spriteBatch.Draw(_texture, rectangle, clr);
			}
		}
	}
}
