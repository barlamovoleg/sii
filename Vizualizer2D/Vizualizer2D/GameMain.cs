﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Remoting.Messaging;
using Common.Logger;
using Externals;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using SIICore.Data;
using SIIGame2D.Data;
using XnaWpfIntegration;
using GameTime = XnaCoreAdapted.GameTime;

namespace SIIGame2D
{
	public class GameMain : XnaGame
	{
		public event Action<IVizualizedObject> ObjectSelected;

		private readonly Camera _camera;
		private readonly IGameData _gameData;
		private readonly IList<VizualizedObject> _objects;
		private IVizualizedObject _brush;
		private Texture2D _cursorTexture;
		private Texture2D _deleteTexture;
		private Texture2D _origCursorTexture;
		private bool _isDeleteBrush;
		private ILogger _logger;
		private SpriteBatch _spriteBatch;
		private VizualizedObject _selectedObject;
		
		private bool _hideMouse;

		public bool Destryoed;

		private Point _mousePosition;

		public GameMain(IXnaGamePresenter gamePresenter, IGameData gameData, ILogger visualizerLogger)
			: base(gamePresenter)
		{
			_logger = visualizerLogger;
			_gameData = gameData;
			_objects = new List<VizualizedObject>();
			var center = new Vector2(_gameData.Width / 2, _gameData.Height / 2);
			_camera = new Camera(center);
			IsMouseVisible = true;
			_hideMouse = false;
		}

		public void ClearBrush()
		{
			_brush = null;
			_isDeleteBrush = false;
			_hideMouse = true;
			_cursorTexture = null;
		}

		public void SetDeleteBrush()
		{
			_brush = null;
			_isDeleteBrush = true;
			_hideMouse = true;
			_cursorTexture = _deleteTexture;
		}

		public void SetBrush(IVizualizedObject brush)
		{
			_brush = brush;
			_isDeleteBrush = false;
			_hideMouse = true;
			if (TexturesCash._cashedTextures.ContainsKey(brush.IconPath))
				_cursorTexture = TexturesCash._cashedTextures[brush.IconPath];
		}

		protected override void Initialize()
		{
			IList<IVizualizedObject> objectModels = _gameData.VizualizedObjects;
			foreach (IVizualizedObject model in objectModels)
			{
				_objects.Add(new VizualizedObject(model));
			}

			base.Initialize();
		}

		protected override void LoadContent()
		{
			GameCommon.Initialize(GraphicsDevice, new MouseController(), new KeyboardController());

			_spriteBatch = new SpriteBatch(GraphicsDevice);

			using (var fileStream = new FileStream("delete.png", FileMode.Open))
			{
				_deleteTexture = Texture2D.FromStream(GameCommon.GraphicsDevice, fileStream);
			}
			using (var fileStream = new FileStream("cursor.png", FileMode.Open))
			{
				_origCursorTexture = Texture2D.FromStream(GameCommon.GraphicsDevice, fileStream);
			}

			foreach (VizualizedObject obj in _objects)
				obj.LoadContent();

			foreach (var obj in _gameData.CreatableObjects)
			{
				var path = obj.IconPath;
				if (string.IsNullOrWhiteSpace(path))
					continue;

				if (!TexturesCash._cashedTextures.ContainsKey(path))
				{
					using (var fileStream = new FileStream(path, FileMode.Open))
					{
						TexturesCash._cashedTextures.Add(path, Texture2D.FromStream(GameCommon.GraphicsDevice, fileStream));
					}

				}

			}

			base.LoadContent();
		}

		protected override void UnloadContent()
		{
			foreach (var txt in TexturesCash._cashedTextures.Values)
			{
				txt.Dispose();
			}
			TexturesCash.Clear();

			base.UnloadContent();
		}

		protected override void Update(GameTime gameTime)
		{
			if (Destryoed)
				return;

			GameCommon.UpdateStart(gameTime, IsActive);
			_mousePosition = new Point((int)(GameCommon.MouseController.Position.X / 1.08), (int)(GameCommon.MouseController.Position.Y / 1));
			_camera.Update(gameTime);

			ClickUpdate(gameTime);

			Point mPos = GameCommon.MouseController.Position;
			if (GameCommon.GraphicsDevice.Viewport.Bounds.Contains(mPos))
			{
				IsMouseVisible = !_hideMouse;
			}

			GameCommon.UpdateEnd(gameTime);
			base.Update(gameTime);
		}

		protected override void Draw(GameTime gameTime)
		{
			if (Destryoed)
				return;

			GraphicsDevice.Clear(Color.Black);

			_spriteBatch.Begin(SpriteSortMode.Deferred, null, SamplerState.PointClamp, null, null);

			var viewPort = new Vector4(
				_camera.Viewport.Left,
				_camera.Viewport.Top,
				_camera.Viewport.Width + _camera.SizeOffset.X,
				_camera.Viewport.Height + _camera.SizeOffset.Y);

			if (viewPort.Z * viewPort.W > float.Epsilon)
				DrawObjects(gameTime, viewPort);

			DrawBrush(gameTime, viewPort);

			_spriteBatch.End();

			base.Draw(gameTime);
		}

		private void ClickUpdate(GameTime gameTime)
		{
			if (!GameCommon.MouseController.LeftButtonClicked)
				return;

			if (!GameCommon.GraphicsDevice.Viewport.Bounds.Contains(_mousePosition))
				return;

			var viewPort = new Vector4(
				_camera.Viewport.Left,
				_camera.Viewport.Top,
				_camera.Viewport.Width + _camera.SizeOffset.X,
				_camera.Viewport.Height + _camera.SizeOffset.Y);

			int screenWidth = GameCommon.GraphicsDevice.Viewport.Width;
			int screenHeight = GameCommon.GraphicsDevice.Viewport.Height;

			float cellWidth = screenWidth / viewPort.Z;
			float cellHeight = screenHeight / viewPort.W;

			float xStart = viewPort.X;
			float yStart = viewPort.Y;

			double x = Math.Round((_mousePosition.X + _camera.Offset.X) / cellWidth) + xStart;
			double y = Math.Round((_mousePosition.Y + _camera.Offset.Y) / cellHeight) + yStart;

			if (_brush == null && !_isDeleteBrush)
			{
				if (!InRect(new Vector2((int)x, (int)y), new Vector2(0, 0), new Vector2(_gameData.Width, _gameData.Height)))
					return;

				VizualizedObject temp = null;
				foreach (var vizualizedObject in _objects)
				{
					if ((int)vizualizedObject.Position.X == (int)x && (int)vizualizedObject.Position.Y == (int)y)
					{
						temp = vizualizedObject;
						if (vizualizedObject.Model.FrameData.IsObject())
							break;
					}
				}

				if (temp != null)
				{
					if (_selectedObject != null)
						_selectedObject.Selected = false;

					temp.Selected = true;
					_selectedObject = temp;

					if (ObjectSelected != null)
						ObjectSelected(temp.Model);
				}

				return;
			}

			if (InRect(new Vector2((int)x, (int)y), new Vector2(0, 0), new Vector2(_gameData.Width, _gameData.Height)))
				if (_isDeleteBrush)
					Delete((int)x, (int)y);
				else
					AddObject((int)x, (int)y);
		}

		private void Delete(int x, int y)
		{
			for (int i = 0; i < _gameData.Actors.Count; i++)
			{
				var actor = _gameData.Actors[i];
				if (actor.X == x && actor.Y == y)
				{
					_gameData.Actors.RemoveAt(i);
					var id = _gameData.VizualizedObjects.IndexOf(actor);
					_gameData.VizualizedObjects.RemoveAt(id);
					_objects.RemoveAt(id);
					return;
				}
			}
			for (int i = 0; i < _gameData.Objects.Count; i++)
			{
				var obj = _gameData.Objects[i];
				if (obj.X == x && obj.Y == y)
				{
					_gameData.Objects.RemoveAt(i);
					var id = _gameData.VizualizedObjects.IndexOf(obj);
					_gameData.VizualizedObjects.RemoveAt(id);
					_objects.RemoveAt(id);
					return;
				}
			}
			for (int i = 0; i < _gameData.VizualizedObjects.Count; i++)
			{
				var obj = _gameData.VizualizedObjects[i];
				if (obj.X == x && obj.Y == y)
				{
					_gameData.VizualizedObjects.RemoveAt(i);
					_objects.RemoveAt(i);
					return;
				}
			}
		}

		private void AddObject(int x, int y)
		{
			var created = _gameData.AddObjectAs(_brush, x, y);
			if (created == null)
				return;

			var vizualized = new VizualizedObject(created);
			vizualized.LoadContent();
			_objects.Add(vizualized);
		}

		private void DrawObjects(GameTime gameTime, Vector4 viewport)
		{
			int screenWidth = GameCommon.GraphicsDevice.Viewport.Width;
			int screenHeight = GameCommon.GraphicsDevice.Viewport.Height;

			float cellWidth = screenWidth / viewport.Z;
			float cellHeight = screenHeight / viewport.W;

			float xStart = viewport.X;
			float yStart = viewport.Y;
			float width = viewport.Z + xStart;
			float height = viewport.W + yStart;

			foreach (VizualizedObject obj in _objects)
			{
				Vector2 pos = obj.Position;
				if (InRect(pos, new Vector2(xStart, yStart), new Vector2(width, height)))
				{
					float displayX = (pos.X - xStart - _camera.Offset.X) * cellWidth;
					float displayY = (pos.Y - yStart - _camera.Offset.Y) * cellHeight;
					var rec = new Rectangle((int)displayX, (int)displayY, (int)cellWidth, (int)cellHeight);
					obj.Draw(_spriteBatch, gameTime, rec);
				}
			}
		}

		private void DrawBrush(GameTime gameTime, Vector4 viewport)
		{
			if (_brush == null && !_isDeleteBrush)
			{
				var rect = new Rectangle(_mousePosition.X, _mousePosition.Y, 12, 12);
				_spriteBatch.Draw(_origCursorTexture, rect, Color.White);
				return;
			}

			int screenWidth = GameCommon.GraphicsDevice.Viewport.Width;
			int screenHeight = GameCommon.GraphicsDevice.Viewport.Height;

			float cellWidth = screenWidth / viewport.Z;
			float cellHeight = screenHeight / viewport.W;

			var x = Math.Round(_mousePosition.X / cellWidth) * cellWidth;
			var y = Math.Round(_mousePosition.Y / cellHeight) * cellHeight;
			var rec = new Rectangle((int)x, (int)y, (int)cellWidth, (int)cellHeight);

			var color = Color.White;
			if (!_isDeleteBrush)
				color = Color.FromNonPremultiplied(new Vector4(1, 1, 1, 0.3f));

			if (_cursorTexture != null)
				_spriteBatch.Draw(_cursorTexture, rec, color);
		}

		private bool InRect(Vector2 pos, Vector2 leftTop, Vector2 widthHeight)
		{
			return pos.X >= leftTop.X && pos.X <= widthHeight.X && pos.Y >= leftTop.Y && pos.Y <= widthHeight.Y;
		}
	}
}