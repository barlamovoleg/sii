﻿using Microsoft.Xna.Framework;
using SIIGame2D.Geometry;

namespace SIIGame2D
{
	internal class Camera
	{
		public float Height { get; private set; }
		public Vector2 Position;

		public Rectangle Viewport
		{
			get
			{
				return new Rectangle((int)_gameViewport.X,
									 (int)_gameViewport.Y,
									 (int)_gameViewport.Z,
									 (int)_gameViewport.W);
			}
		}

		public Vector2 Offset
		{
			get
			{
				var difX = _gameViewport.X - (int)_gameViewport.X;
				var difY = _gameViewport.Y - (int)_gameViewport.Y;
				return new Vector2(difX, difY);
			}
		}

		public Vector2 SizeOffset
		{
			get
			{
				var difX = _gameViewport.Z - (int)_gameViewport.Z;
				var difY = _gameViewport.W - (int)_gameViewport.W;
				return new Vector2(difX, difY);
			}
		}

		private float _velocityY;
		private readonly float _stopFactor;

		private Vector2 _lastMouseState;
		private bool _pressed;
		private int _lastMouseScroll;

		private Vector4 _gameViewport;

		private bool _initialized;
		private int _screenWidth;
		private int _screenHeight;

		private const int MinHeight = 4;

		private float Size
		{
			get { return Height * 2; }
		}

		public Camera(Vector2 position)
		{
			_stopFactor = 0.005f;
			Position = position;
			Height = 10;
			_gameViewport = new Vector4(Position.X - Size / 2, Position.Y - Size / 2, Size, Size);
		}

		public void Update(GameTime gameTime)
		{
			_screenWidth = GameCommon.GraphicsDevice.Viewport.Width;
			_screenHeight = GameCommon.GraphicsDevice.Viewport.Height;

			_gameViewport = new Vector4(Position.X - Size / 2, Position.Y - Size / 2, Size, Size);

			Height += _velocityY;
			if (Height < MinHeight)
				Height = MinHeight;

			var sign = MathApp.Sign(-_velocityY);
			_velocityY += _stopFactor * sign;
			if (MathApp.Sign(_velocityY) == sign)
				_velocityY = 0;

			if (GameCommon.IsActive)
				UpdateControl(gameTime);
		}

		private void UpdateControl(GameTime gameTime)
		{
			if (!_initialized)
			{
				_initialized = true;
				_lastMouseScroll = GameCommon.MouseController.MouseState.ScrollWheelValue;

			}

			if (GameCommon.MouseController.RightButton)
			{
				if (!_pressed)
				{
					_lastMouseState = new Vector2(GameCommon.MouseController.Position.X, GameCommon.MouseController.Position.Y);
					_pressed = true;
					return;
				}

				var curPoint = new Vector2(GameCommon.MouseController.Position.X, GameCommon.MouseController.Position.Y);
				var dif = curPoint - _lastMouseState;

				var screenMetric = new Vector2(_screenWidth, _screenHeight);
				var gameMetric = new Vector2(_gameViewport.Z, _gameViewport.W);
				var difInGame = Translations2D.TranslateCoordinates(dif, screenMetric, gameMetric);
				Position -= difInGame;
				_lastMouseState = new Vector2(GameCommon.MouseController.Position.X, GameCommon.MouseController.Position.Y);
			}
			else
				_pressed = false;

			var scroll = GameCommon.MouseController.MouseState.ScrollWheelValue;
			var diffScroll = scroll - _lastMouseScroll;
			if (diffScroll != 0)
			{
				if (MathApp.Sign(diffScroll) == MathApp.Sign(_velocityY))
					_velocityY = 0;

				_velocityY -= diffScroll*0.0003f;
			}

			_lastMouseScroll = GameCommon.MouseController.MouseState.ScrollWheelValue;
		}
	}
}
