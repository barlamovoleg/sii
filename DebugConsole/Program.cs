﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIICore.Services;
using SIICore.Models;
using Common.Repository;
using SIICore.Data;

namespace DebugConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            var frameService = new FrameRepositoryService(null, null, null);

            var repo = frameService.GetRepository("debug2.0");

            var frameSystem = new SIICore.Data.Frame("System", SIICore.Data.Frame.FrameType.System);
            repo.Add(frameSystem);

            var frameObject = new SIICore.Data.Frame("Объект(sys)", SIICore.Data.Frame.FrameType.System);
            repo.Add(frameObject);
            repo.AddRelation(frameObject, new SIICore.Data.Slot("Координата X", new SIICore.Data.Value(DomainHelper.getInnerType(0), 0)));
            repo.AddRelation(frameObject, new SIICore.Data.Slot("Координата Y", new SIICore.Data.Value(DomainHelper.getInnerType(0), 0)));
            repo.AddRelation(frameObject, new SIICore.Data.Slot("Иконка", new SIICore.Data.Value(DomainHelper.getInnerType(""), "")));

            var frameActor = new SIICore.Data.Frame("Актор(sys)", SIICore.Data.Frame.FrameType.System);
            repo.Add(frameActor);
			repo.AddRelation(frameActor, new SIICore.Data.Slot("Координата X", new SIICore.Data.Value(DomainHelper.getInnerType(0), 0)));
			repo.AddRelation(frameActor, new SIICore.Data.Slot("Координата Y", new SIICore.Data.Value(DomainHelper.getInnerType(0), 0)));
			repo.AddRelation(frameActor, new SIICore.Data.Slot("Иконка", new SIICore.Data.Value(DomainHelper.getInnerType(""), "")));

            var frameAction = new SIICore.Data.Frame("Действие(sys)", SIICore.Data.Frame.FrameType.System);
            repo.Add(frameAction);
            repo.AddRelation(frameAction, new SIICore.Data.Slot("Субъект", new SIICore.Data.Value(DomainHelper.getInnerType(frameActor), DomainHelper.getInnerType(frameActor))));
            repo.AddRelation(frameAction, new SIICore.Data.Slot("Объект", new SIICore.Data.Value(DomainHelper.getInnerType(frameObject), DomainHelper.getInnerType(frameObject))));
            repo.AddRelation(frameAction, new SIICore.Data.Slot("Результат", new SIICore.Data.Value(DomainHelper.getInnerType(""), "")));

			var frameCell = new SIICore.Data.Frame("Ячейка(sys)", SIICore.Data.Frame.FrameType.System);
			repo.Add(frameCell);
			repo.AddRelation(frameCell, new SIICore.Data.Slot("Координата X", new SIICore.Data.Value(DomainHelper.getInnerType(0), 0)));
			repo.AddRelation(frameCell, new SIICore.Data.Slot("Координата Y", new SIICore.Data.Value(DomainHelper.getInnerType(0), 0)));
			repo.AddRelation(frameCell, new SIICore.Data.Slot("Иконка", new SIICore.Data.Value(DomainHelper.getInnerType(""), "")));

            ((IRepositoryRelated<Frame, Frame>)repo).AddRelation(frameSystem, frameObject);
            ((IRepositoryRelated<Frame, Frame>)repo).AddRelation(frameSystem, frameActor);
            ((IRepositoryRelated<Frame, Frame>)repo).AddRelation(frameSystem, frameAction);
			((IRepositoryRelated<Frame, Frame>)repo).AddRelation(frameSystem, frameCell);

            //var frame1 = new SIICore.Data.Frame("Фрейм1");
            //repo.Add(frame1);

            //((IRepositoryRelated<Frame, Frame>)repo).AddRelation(frameObject, frame1);
            //((IRepositoryRelated<Frame, Frame>)repo).RemoveRelation(frameObject, frame1);
            //repo.AddRelation(frame1, new Slot("asdajsldk", new Value(DomainHelper.getInnerType(
            var frameList = repo.GetAll();
        }
    }
}
