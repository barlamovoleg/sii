﻿namespace GUI.Loading
{
	internal interface ISelectedGameNameProvider
	{
		string SelectedName { get; }
	}
}
