﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Common.Repository;
using GUI.Loading;
using GUISDK;
using SIICore;
using SIICore.Data;

// ReSharper disable once CheckNamespace
namespace GUI
{
	internal class LoadControlModel : BaseModel
	{
		public event Action GameLoaded;
		public ICommand LoadGameCommand { get; private set; }
		public ICommand RemoveGameCommand { get; private set; }

		public ObservableCollection<string> GameNames { get; private set; }

		private readonly IRepositoryRelatedService<Frame, Slot> _repositoryService;
		private readonly ISelectedGameNameProvider _selectedGameNameProvider;

		public LoadControlModel(IRepositoryRelatedService<Frame, Slot> repositoryService, ISelectedGameNameProvider selectedGameNameProvider)
		{
			LoadGameCommand = new RelayCommand(OnCreateCommand);
			RemoveGameCommand = new RelayCommand(OnRemoveGameCommand);
			GameNames = new ObservableCollection<string>();

			_repositoryService = repositoryService;
			_selectedGameNameProvider = selectedGameNameProvider;

			Initialize();
		}

		private void Initialize()
		{
			var names = _repositoryService.GetRepositoryNames();
			GameNames.Clear();
			foreach (var name in names)
				GameNames.Add(name);
		}

		private void OnCreateCommand(object o)
		{
			var name = _selectedGameNameProvider.SelectedName;
			var newRepositry = _repositoryService.GetRepository(name);
			ActiveRepositoryProvider.SetNewRepository(newRepositry);
			RaiseGameLoaded();
		}

		private void OnRemoveGameCommand(object o)
		{
			var name = _selectedGameNameProvider.SelectedName;
			_repositoryService.RemoveRepository(name);
			//TODO если этот активен то изменить на нул
		}

		private void RaiseGameLoaded()
		{
			var handler = GameLoaded;
			if (handler != null)
				handler();
		}
	}
}
