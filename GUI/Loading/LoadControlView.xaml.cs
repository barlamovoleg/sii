﻿using System;
using System.Windows.Controls;
using GUISDK;

// ReSharper disable once CheckNamespace
namespace GUI
{
	internal partial class LoadControlView : IModelPresenter<LoadControlModel>
	{
		public event Action<string> SelectedGameNameChanged;

		public LoadControlView()
		{
			InitializeComponent();
		}

		public void SetModel(LoadControlModel dataModel)
		{
			DataContext = dataModel;
		}

		private void ListView_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			var listView = sender as ListView;
			if (listView == null)
				return;

			var selectedName = listView.SelectedItem.ToString();
			RaiseSelectedGameNameChanged(selectedName);
		}

		private void RaiseSelectedGameNameChanged(string name)
		{
			var handler = SelectedGameNameChanged;
			if (handler != null)
				handler(name);
		}
	}
}
