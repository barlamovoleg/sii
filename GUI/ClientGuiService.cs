﻿using System;
using System.Windows;
using Common;
using Common.Repository;
using GUI.Common;
using SIICore;
using SIICore.Data;

// ReSharper disable once CheckNamespace
namespace GUI
{
	public class ClientGuiService : IServiceRunable, IServiceCloseable, IServiceRequiring
	{
		public event Action<CloseReason> Closed;
		public event Action<AppClosingEventArgs> Closing;

		Guid IService.Key
		{
			get { return new Guid("54466261-FD60-4BCA-9690-A9D20419FCE0"); }
		}

		Type[] IServiceRequiring.RequiredServices
		{
			get { return new[] { typeof(IGraphicsHostService<Window>), typeof(IRepositoryRelatedService<Frame, Slot>) }; }
		}

		private IGraphicsHostService<Window> _graphicsHostService;
		private IRepositoryRelatedService<Frame, Slot> _repositoryService;
		private ClientStartController _mainGuiController;
		private VizualizersFabric _vizualizersFabric;

		public void Dispose()
		{
			GUIcommon.Logger.LogInfo(StaticStrings.ClientGuiService.ServiceDisposeStart);
			using (_mainGuiController)
			{
				//dispose
			}
			_graphicsHostService = null;
			GUIcommon.Logger.LogInfo(StaticStrings.ClientGuiService.ServiceDisposeEnd);
		}

		public void Run()
		{
			GUIcommon.Logger.LogInfo(StaticStrings.ClientGuiService.TryRunService);
			Initialize();

			_mainGuiController = new ClientStartController(_vizualizersFabric, _repositoryService);
			_mainGuiController.CloseRequested += OnCloseRequested;

			_graphicsHostService.GraphicHost.Content = _mainGuiController.GetView();
			_graphicsHostService.Display();
			GUIcommon.Logger.LogInfo(StaticStrings.ClientGuiService.ServiceRunned);
		}

		public void OnCloseRequest(AppClosingEventArgs closingEventArgs)
		{
			if (closingEventArgs.CloseReason == CloseReason.User ||
				closingEventArgs.CloseReason == CloseReason.Unknown)
			{
				var canClose = _mainGuiController == null || _mainGuiController.OnCloseUserRequest();
				closingEventArgs.CancelClose = !canClose;
			}
		}

		private void Initialize()
		{
			MessageBoxProvider.SetGraphicHost(_graphicsHostService);
			_vizualizersFabric = new VizualizersFabric();
		}

		public void Close(CloseReason closeReason)
		{
			_graphicsHostService.GraphicHost.Content = null;
			RaiseClosed(closeReason);
		}

		private void OnCloseRequested()
		{
			var closingEventArgs = new AppClosingEventArgs(CloseReason.User);
			RaiseClosing(closingEventArgs);
			if (!closingEventArgs.CancelClose)
				Close(CloseReason.User);
		}

		private void RaiseClosing(AppClosingEventArgs closingEventArgs)
		{
			var handler = Closing;
			if (handler != null)
				handler(closingEventArgs);
		}

		private void RaiseClosed(CloseReason closeReason)
		{
			var handler = Closed;
			if (handler != null)
				handler(closeReason);
		}

		void IServiceRequiring.PassRequiredServices(IService[] requirements)
		{
			_graphicsHostService = (IGraphicsHostService<Window>)requirements[0];
			_repositoryService = (IRepositoryRelatedService<Frame, Slot>)requirements[1];
		}
	}
}
