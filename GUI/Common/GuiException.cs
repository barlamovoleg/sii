﻿using System;
using System.Runtime.Serialization;
using Common;

namespace GUI.Common
{
	internal class GuiException : AppException
	{
		public GuiException(string message, string key)
			: base(message, key)
		{
		}

		public GuiException(string message, Exception innerException, string key)
			: base(message, innerException, key)
		{
		}

		protected GuiException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
	}
}
