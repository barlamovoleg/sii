﻿using System.Windows;
using Common;


// ReSharper disable once CheckNamespace
namespace GUI
{
	public static class MessageBoxProvider
	{
		internal static void SetGraphicHost(IGraphicsHostService<Window> graphicHost)
		{
			_currentGraphicHost = graphicHost;
		}

		public static void ShowInfo(string message, string title = null, bool withIcon = true, Window owner = null)
		{
			var realOwner = owner ?? MainWindow;
			var realTitle = title ?? "Информация";

			if (withIcon)
				MessageBox.Show(realOwner, message, realTitle, MessageBoxButton.OK, MessageBoxImage.Information);
			else
				MessageBox.Show(realOwner, message, realTitle, MessageBoxButton.OK);
		}

		public static void ShowError(string message, string title = null, Window owner = null)
		{
			var realOwner = owner ?? MainWindow;
			var realTitle = title ?? "Ошибка";
			MessageBox.Show(realOwner, message, realTitle, MessageBoxButton.OK, MessageBoxImage.Error);
		}

		public static void ShowWarning(string message, string title = null, Window owner = null)
		{
			var realOwner = owner ?? MainWindow;
			var realTitle = title ?? "Предупреждение";
			MessageBox.Show(realOwner, message, realTitle, MessageBoxButton.OK, MessageBoxImage.Warning);
		}

		public static bool ShowQuestion(string question, string title = null, bool withIcon = true,  Window owner = null)
		{
			var realOwner = owner ?? MainWindow;
			var realTitle = title ?? "Вопрос";

			var result = withIcon ? MessageBox.Show(realOwner, question, realTitle, MessageBoxButton.YesNo, MessageBoxImage.Question)
				                  : MessageBox.Show(realOwner, question, realTitle, MessageBoxButton.YesNo);

			return result == MessageBoxResult.Yes;
		}

		private static Window MainWindow
		{
			get { return _currentGraphicHost.GraphicHost; }
		}
		private static IGraphicsHostService<Window> _currentGraphicHost;
	}
}
