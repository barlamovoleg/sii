﻿using Common.Logger;
using SIICore;

namespace GUI.Common
{
	// ReSharper disable once InconsistentNaming
	internal static class GUIcommon
	{
		static GUIcommon()
		{
			Logger = CoreLoggers.GetLogger(StaticStrings.GuiLoggerName);
		}

		internal static ILogger Logger { get; private set; }
	}
}
