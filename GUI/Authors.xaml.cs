﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace GUI
{
	public partial class Authors
	{
		private static readonly string[] _authorsNames =
		{
			"Барламов Олег",
			"Белоусов Алексей",
			"Жуков Александр",
		};

		private readonly Random _random;

		public Authors()
		{
			InitializeComponent();

			_random = new Random(Guid.NewGuid().GetHashCode());
			Loaded += OnLoaded;
		}

		private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
		{
			var names = new List<string>();
			names.AddRange(_authorsNames);

			var name = names[_random.Next(names.Count)];
			names.Remove(name);
			FirstAuthor.Text = name;

			name = names[_random.Next(names.Count)];
			names.Remove(name);
			SecondAuthor.Text = name;

			name = names[_random.Next(names.Count)];
			names.Remove(name);
			ThirdAuthor.Text = name;
		}
	}
}
