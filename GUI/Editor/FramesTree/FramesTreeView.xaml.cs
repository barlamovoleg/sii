﻿using GUISDK;

// ReSharper disable once CheckNamespace
namespace GUI.Editor
{
	internal partial class FramesTreeView : IModelPresenter<FramesTreeModel>
	{
		public FramesTreeView()
		{
			InitializeComponent();
		}

		public void SetModel(FramesTreeModel dataModel)
		{
			DataContext = dataModel;
		}
	}
}
