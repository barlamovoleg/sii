﻿using GUISDK;

// ReSharper disable once CheckNamespace
namespace GUI.Editor
{
	internal partial class FramesHierarchyView : IModelPresenter<FramesHierarchyEditorModel>
	{
		public FramesHierarchyView()
		{
			InitializeComponent();
		}

		public void SetModel(FramesHierarchyEditorModel dataModel)
		{
			DataContext = dataModel;
		}
	}
}
