﻿using GUISDK;

// ReSharper disable once CheckNamespace
namespace GUI.Editor
{
	internal partial class FrameEditorView : IModelPresenter<FrameEditorModel>
	{
		public FrameEditorView()
		{
			InitializeComponent();
		}

		public void SetModel(FrameEditorModel dataModel)
		{
			DataContext = dataModel;
		}
	}
}
