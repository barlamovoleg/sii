﻿using System;
using System.Collections.Generic;
using System.Windows;
using GUI.Editor;
using GUISDK;
using GUISDK.Components;
using SIICore;
using SIICore.Data;
using Common.Repository;

// ReSharper disable once CheckNamespace
namespace GUI
{
	internal class EditorModel : BaseModel, IDisposable
	{
        private Action<Node> NodeCreated;
        private Action<Node, Node> SubnodeCreated;
        private Action<Node> NodeDeleted;
        private Func<Node, Node, bool> LinkCreated;
        private Action<Node, Node> LinkDeleted;
        private Action<Frame> ActiveFrameChanged;

		public FrameworkElement LeftPanelView
		{
			get { return _framesTreeView; }
		}

		public FrameworkElement BottomPanelView
		{
			get { return _frameEditorView; }
		}

		public FrameworkElement CentralContentView
		{
			get { return _framesHierarchyView; }
		}

		private readonly FrameEditorModel _frameEditorModel;
		private readonly FrameEditorView _frameEditorView;
		private readonly FramesTreeModel _framesTreeModel;
		private readonly FramesTreeView _framesTreeView;
		private readonly FramesHierarchyEditorModel _framesHierarchyEditorModel;
		private readonly FramesHierarchyView _framesHierarchyView;

        private List<Node> nodes = new List<Node>();

		public EditorModel()
		{
			_frameEditorModel = new FrameEditorModel();
			_framesTreeModel = new FramesTreeModel();
			_framesHierarchyEditorModel = new FramesHierarchyEditorModel();

			_frameEditorView = new FrameEditorView();
			_frameEditorView.SetModel(_frameEditorModel);

			_framesTreeView = new FramesTreeView();
			_framesTreeView.SetModel(_framesTreeModel);

			_framesHierarchyView = new FramesHierarchyView();
			_framesHierarchyView.SetModel(_framesHierarchyEditorModel);

            _framesTreeView.treeView.nodes = nodes;
            _framesHierarchyView.diagram.nodes = nodes;
            _frameEditorView.dataGrid.nodes = nodes;

            _framesTreeView.treeView.NodeCreated += NewNode;
            _framesHierarchyView.diagram.NodeCreated += NewNode;
            _framesTreeView.treeView.SubnodeCreated += NewSubnode;
            _framesHierarchyView.diagram.SubnodeCreated += NewSubnode;
            _framesTreeView.treeView.NodeDeleted += DeleteNode;
            _framesHierarchyView.diagram.NodeDeleted += DeleteNode;
            _framesHierarchyView.diagram.LinkCreated += NewLink;
            _framesHierarchyView.diagram.LinkDeleted += DeleteLink;
            _framesTreeView.treeView.SelectedNodeChanged += ChangeActiveFrame;
            _framesHierarchyView.diagram.SelectedNodeChanged += ChangeActiveFrame;
            _frameEditorView.dataGrid.SlotCreated += NewSlot;
            _frameEditorView.dataGrid.SlotDeleted += DeleteSlot;
            _frameEditorView.dataGrid.FrameUpdated += UpdateFrame;
            _framesHierarchyView.diagram.FrameUpdated += UpdateFrame;
            _framesTreeView.treeView.FrameUpdated += UpdateFrame;

            NodeCreated += _framesHierarchyView.diagram.NewNode;
            NodeCreated += _framesTreeView.treeView.NewNode;
            SubnodeCreated += _framesHierarchyView.diagram.NewSubnode;
            SubnodeCreated += _framesTreeView.treeView.NewSubnode;
            NodeDeleted += _framesHierarchyView.diagram.DeleteNode;
            NodeDeleted += _framesTreeView.treeView.DeleteNode;
            LinkCreated += _framesTreeView.treeView.NewLink;
            LinkCreated += _framesHierarchyView.diagram.NewLink;
            LinkDeleted += _framesTreeView.treeView.DeleteLink;
            LinkDeleted += _framesHierarchyView.diagram.DeleteLink;

            ActiveRepositoryProvider.ActiveRepositoryChanged += LoadNodes;
            if (ActiveRepositoryProvider.IsRepositoryReady)
                LoadNodes();
		}

        private Node GetNode(Frame frame)
        {
            return nodes.Find(item => item.FrameNode == frame);
        }

        private void LoadNodes()
        {
            ActiveFrameChanged -= _frameEditorView.dataGrid.LoadSlots;

            _framesTreeView.treeView.Clear();
            _framesHierarchyView.diagram.Clear();
            nodes.Clear();
            var frames = ActiveRepositoryProvider.ActiveRepository.GetAll();
            foreach (Frame frame in frames)
            {
                var node = new Node(frame);
                nodes.Add(node);
                NodeCreated(node);
                node.IsSelected = true;
            }
            foreach (Frame frame in frames)
            {
                if (frame.Parent != null)
                    NewLink(GetNode(frame.Parent), GetNode(frame));
            }

            ActiveFrameChanged += _frameEditorView.dataGrid.LoadSlots;
            foreach (Frame frame in frames)
            {
                ActiveFrameChanged(frame);
            }
        }

        private void NewNode()
        {
            var node = new Node();
            nodes.Add(node);
            ActiveRepositoryProvider.ActiveRepository.Add(node.FrameNode);
            NodeCreated(node);
            node.IsSelected = true;     
        }

        private void NewSubnode(List<Node> selectedNodes)
        {
            foreach (Node parent in selectedNodes)
            {
                var child = new Node();
                nodes.Add(child);
                ActiveRepositoryProvider.ActiveRepository.Add(child.FrameNode);
                ((IRepositoryRelated<Frame, Frame>)ActiveRepositoryProvider.ActiveRepository).AddRelation(parent.FrameNode, child.FrameNode);
                SubnodeCreated(parent, child);
                child.IsSelected = true;
            }
        }

        private void DeleteNode(List<Node> selectedNodes)
        {
            foreach(Node node in selectedNodes)
            {
                if (node != null)
                {
                    if (ActiveRepositoryProvider.ActiveRepository.Delete(node.FrameNode))
                    {
                        nodes.Remove(node);
                        NodeDeleted(node);
                    }
                }
            }
        }

        private bool NewLink(Node parent, Node child)
        {
            bool isLinkCreated = LinkCreated(parent, child);
            if (isLinkCreated)
            {
                if (child.FrameNode.Parent == null)
                    ((IRepositoryRelated<Frame, Frame>)ActiveRepositoryProvider.ActiveRepository).AddRelation(parent.FrameNode, child.FrameNode);
                return true;
            }
            return false;
        }

        private bool DeleteLink(Node parent, Node child)
        {   
            if (((IRepositoryRelated<Frame, Frame>)ActiveRepositoryProvider.ActiveRepository).RemoveRelation(parent.FrameNode, child.FrameNode))
            {
                var slotNames = new List<string>();
                foreach (var slot in parent.FrameNode.slots)
                    slotNames.Add(slot.Name);
                DeleteChildSlots(child.FrameNode, slotNames);
                LinkDeleted(parent, child);
                return true;
            }
            return false;
        }

        private void DeleteChildSlots(Frame frame, List<string> slotNames)
        {
            var repo = ActiveRepositoryProvider.ActiveRepository;
            var slotForDelete = frame.slots.FindAll(x => slotNames.Contains(x.Name));
            foreach (var slot in slotForDelete)
            {
                repo.RemoveRelation(frame, slot);
            }
            foreach (var child in frame.children)
            {     
                DeleteChildSlots(child, slotNames);
            }
        }

        private void ChangeActiveFrame(Node node)
        {
            if (ActiveFrameChanged == null)
                return;

            if (node != null)
                ActiveFrameChanged(node.FrameNode);
            else
                ActiveFrameChanged(null);
        }

        private void NewSlot(Frame frame, Slot slot)
        {
            ActiveRepositoryProvider.ActiveRepository.AddRelation(frame, slot);
        }

        private void DeleteSlot(Frame frame, Slot slot)
        {
            ActiveRepositoryProvider.ActiveRepository.RemoveRelation(frame, slot);
        }

        private void UpdateFrame(Frame frame)
        {
            ActiveRepositoryProvider.ActiveRepository.Update(frame);
        }

		public void Dispose()
		{

		}
	}
}
