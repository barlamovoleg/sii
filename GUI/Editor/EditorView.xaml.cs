﻿using GUISDK;

// ReSharper disable once CheckNamespace
namespace GUI
{
	internal partial class EditorView : IModelPresenter<EditorModel>
	{
		public EditorView()
		{
			InitializeComponent();
		}

		public void SetModel(EditorModel dataModel)
		{
			DataContext = dataModel;
		}
	}
}
