﻿using GUISDK;

// ReSharper disable once CheckNamespace
namespace GUI
{
	internal partial class CreateControlView : IModelPresenter<CreateControlModel>
	{
		public CreateControlView()
		{
			InitializeComponent();
		}

		public void SetModel(CreateControlModel dataModel)
		{
			DataContext = dataModel;
		}
	}
}
