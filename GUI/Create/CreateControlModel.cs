﻿using System;
using System.Linq;
using System.Windows.Input;
using Common.Repository;
using GUISDK;
using SIICore;
using SIICore.Data;

// ReSharper disable once CheckNamespace
namespace GUI
{
	internal class CreateControlModel : BaseModel
	{
		public event Action GameCreated;

		public string Name
		{
			get { return NotifyPropertyGet(() => Name); }
			set { NotifyPropertySet(() => Name, value); }
		}

		public ICommand CreateCommand { get; private set; }
		public ICommand ResetCommand { get; private set; }

		private readonly IRepositoryRelatedService<Frame, Slot> _repositoryService;

		public CreateControlModel(IRepositoryRelatedService<Frame, Slot> repositoryService)
		{
			CreateCommand = new RelayCommand(OnCreateCommand);
			ResetCommand = new RelayCommand(OnResetCommand);

			_repositoryService = repositoryService;
		}

		private void OnCreateCommand(object o)
		{
			var names = _repositoryService.GetRepositoryNames();
			if (names.Contains(Name))
			{
				MessageBoxProvider.ShowError("Игра с таким именем уже существует.");
				return;
			}
			var newRepositry = _repositoryService.GetRepository(Name);
			ActiveRepositoryProvider.SetNewRepository(newRepositry);
			Reset();
			RaiseGameCreated();
		}

		private void OnResetCommand(object o)
		{
			Reset();
		}

		private void Reset()
		{
			Name = string.Empty;
		}

		private void RaiseGameCreated()
		{
			var handler = GameCreated;
			if (handler != null)
				handler();
		}
	}
}
