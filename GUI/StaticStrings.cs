﻿// ReSharper disable once CheckNamespace
namespace GUI
{
	internal static class StaticStrings
	{
		internal static class ExceptionsErrors
		{
			/// <summary>
			/// Error in dispose main client control
			/// </summary>
			internal static string MainDisposeException = "Error in dispose main client control";
		}

		internal static class ClientGuiService
		{
			/// <summary>
			/// Запуск Gui сервиса
			/// </summary>
			internal static string TryRunService = "Запуск Gui сервиса";
			/// <summary>
			/// Gui сервис успешно запущен
			/// </summary>
			internal static string ServiceRunned = "Gui сервис успешно запущен";
			/// <summary>
			/// Начало Dispose метода gui сервиса.
			/// </summary>
			internal static string ServiceDisposeStart = "Начало Dispose метода gui сервиса.";
			/// <summary>
			/// Конец Dispose метода gui сервиса.
			/// </summary>
			internal static string ServiceDisposeEnd = "Конец Dispose метода gui сервиса.";
		}

		internal static class SidePanel
		{
			/// <summary>
			/// Создание кнопок боковой панели
			/// </summary>
			internal static string ButtonsCreateStart = "Создание кнопок боковой панели";
			/// <summary>
			/// Создание кнопок боковой панели окончено
			/// </summary>
			internal static string ButtonsCreateEnd = "Создание кнопок боковой панели окончено";
			/// <summary>
			/// Контент кнопки '{0}' возвращен
			/// </summary>
			internal static string ButtonContentShow = "Запрос контента кнопки '{0}'";
		}

		/// <summary>
		/// GUILog
		/// </summary>
		internal static string GuiLoggerName = "GUILog";
	}
}
