﻿using GUISDK;
using SIICore.Data;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace GUI.Game
{
	internal partial class LogViewerView : IModelPresenter<LogViewerModel>
	{
		public LogViewerView()
		{
			InitializeComponent();
            tvHistory.ItemsSource = DefaultVizualizedObject.HISTORY;
		}

		public void SetModel(LogViewerModel dataModel)
		{
			DataContext = dataModel;
		}

		public void Show(IVizualizedObject vizualizedObject)
		{
            for (int i = 0; i < DefaultVizualizedObject.HISTORY.Count; i++)
            {
                if (DefaultVizualizedObject.HISTORY[i] != null)
                    DefaultVizualizedObject.HISTORY[i].IsExpanded = DefaultVizualizedObject.HISTORY[i].FrameData.VizName == vizualizedObject.FrameData.VizName;
            }
		}
	}
}
