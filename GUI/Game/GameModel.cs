﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Common;
using GUI.Game;
using GUISDK;
using SIICore;
using SIICore.Data;
using SIIVizualizator;

// ReSharper disable once CheckNamespace
namespace GUI
{
	internal class GameModel : BaseModel, IGraphicsHostService<ContentControl>
	{
		public FrameworkElement LeftPanelView
		{
			get { return _parametersViewerView; }
		}

		public FrameworkElement BottomPanelView
		{
			get { return _logViewerView; }
		}

		public FrameworkElement CentralContentView
		{
			get { return _vizualizerContainer; }
		}

		Guid IService.Key
		{
			get { return new Guid("{1902D605-BE61-4BF3-9873-94F9526A0DEE}"); }
		}

		ContentControl IGraphicsHostService<ContentControl>.GraphicHost
		{
			get { return _vizualizerContainer; }
		}

		private readonly LogViewerModel _logViewerModel;
		private readonly LogViewerView _logViewerView;
		private readonly ParametersViewerModel _parametersViewerModel;
		private readonly ParametersViewerView _parametersViewerView;

		private readonly VizualizersFabric _vizualizersFabric;
		private readonly ContentControl _vizualizerContainer;
		private ISiiGameVisualizer _vizualizer;

		public GameModel(VizualizersFabric vizualizersFabric)
		{
			_vizualizersFabric = vizualizersFabric;
			_vizualizerContainer = new ContentControl();
			_vizualizerContainer.PreviewMouseMove += VizualizerContainerOnPreviewMouseMove;

			_logViewerModel = new LogViewerModel();
			_logViewerView = new LogViewerView();
			_logViewerView.SetModel(_logViewerModel);

			_parametersViewerModel = new ParametersViewerModel();
			_parametersViewerModel.ReloadVizualizatorRequest += UpdateVizualizer;
			_parametersViewerModel.OneStepRequested += ParametersViewerModelOnOneStepRequested;
			_parametersViewerModel.SystemBrushSelectedEmpty += ParametersViewerModelOnSystemBrushSelectedEmpty;
			_parametersViewerView = new ParametersViewerView();
			_parametersViewerView.ModeChanged += ModeChanged;
			_parametersViewerView.InGameEditor.SelectedBrushChanged += SelectedBrushChanged;
			_parametersViewerView.SetModel(_parametersViewerModel);

			UpdateVizualizer();
		}

		private void ModeChanged()
		{
			_parametersViewerView.InGameEditor.ClearSelection();
			_vizualizer.ClearBrush();
		}

		private void ParametersViewerModelOnSystemBrushSelectedEmpty(bool isEmptyBrush)
		{
			_parametersViewerView.InGameEditor.ClearSelection();
			if (isEmptyBrush)
				_vizualizer.ClearBrush();
			else
				_vizualizer.SetDeleteBrush();
		}

		private void SelectedBrushChanged(IVizualizedObject vizualizedObject)
		{
			_vizualizer.SetBrush(vizualizedObject);
		}

		private void ParametersViewerModelOnOneStepRequested()
		{
			_vizualizer.OneStep();
		}

		public void Dispose()
		{
			using (_vizualizer)
			{
				//Dispose
			}
			_parametersViewerModel.Dispose();
		}

		private void UpdateVizualizer()
		{
			var gameData = GameDataFabric.GetGameData();
			_vizualizersFabric.SetGameData(gameData);
			_parametersViewerModel.ClearData();
			FillVizualizer();
			_parametersViewerModel.SetGameData(gameData);
			_parametersViewerView.InGameEditor.Reload();
		}

		private void VizualizerContainerOnPreviewMouseMove(object sender, MouseEventArgs mouseEventArgs)
		{
			var viz = (_vizualizer as VisualizationControl);
			if (viz == null)
				return;

			viz.OnPreviewMouseMove(sender, mouseEventArgs);
		}

		private void FillVizualizer()
		{
            DefaultVizualizedObject.HISTORY.Clear();
			_vizualizer = _vizualizersFabric.CreateVizualizer();
			_vizualizer.ObjectSelected += VizualizerOnObjectSelected;
			_vizualizer.Attach(this);
			_vizualizer.Play();
		}

		private void VizualizerOnObjectSelected(IVizualizedObject vizualizedObject)
		{
			_logViewerView.Show(vizualizedObject);
			if (_parametersViewerView.GameShell.Visibility != Visibility.Visible)
				return;

			_parametersViewerView.GameShell.Show(vizualizedObject);
		}

		void IGraphicsHostService<ContentControl>.Display()
		{
		}
	}
}
