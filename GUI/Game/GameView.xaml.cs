﻿using GUISDK;

// ReSharper disable once CheckNamespace
namespace GUI
{
	internal partial class GameView : IModelPresenter<GameModel>
	{
		public GameView()
		{
			InitializeComponent();
		}

		public void SetModel(GameModel dataModel)
		{
			DataContext = dataModel;
		}
	}
}
