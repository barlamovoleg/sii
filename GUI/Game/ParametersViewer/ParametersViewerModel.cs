﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using GUISDK;
using SIICore.Data;

// ReSharper disable once CheckNamespace
namespace GUI.Game
{
	internal class ParametersViewerModel : BaseModel, IDisposable
	{
		public event Action ReloadVizualizatorRequest;
		public event Action OneStepRequested;

		public event Action<bool> SystemBrushSelectedEmpty;

		public ICommand ReloadCommand
		{
			get { return NotifyPropertyGet(() => ReloadCommand); }
			private set { NotifyPropertySet(() => ReloadCommand, value); }
		}

		public ICommand OneStepCommand
		{
			get { return NotifyPropertyGet(() => OneStepCommand); }
			private set { NotifyPropertySet(() => OneStepCommand, value); }
		}

		public ICommand ClearBrushCommand { get; private set; }
		public ICommand DeleteBrushCommand { get; private set; }

		public ObservableCollection<IVizualizedObject> CreatableObjects { get; private set; }
		private IGameData _gameData;

		public ParametersViewerModel()
		{
			ReloadCommand = new RelayCommand(OnReloadCommand);
			OneStepCommand = new RelayCommand(OnOneStepCommand);
			ClearBrushCommand = new RelayCommand(OnClearBrushCommand);
			DeleteBrushCommand = new RelayCommand(OnDeleteBrushCommand);
			CreatableObjects = new ObservableCollection<IVizualizedObject>();
		}

		private void OnClearBrushCommand(object o)
		{
			if (SystemBrushSelectedEmpty != null)
				SystemBrushSelectedEmpty(true);
		}

		private void OnDeleteBrushCommand(object o)
		{
			if (SystemBrushSelectedEmpty != null)
				SystemBrushSelectedEmpty(false);
			Mouse.OverrideCursor = Cursors.Arrow;
		}

		public void ClearData()
		{
			_gameData = null;
			CreatableObjects.Clear();
			GC.Collect();
		}

		public void SetGameData(IGameData gameData)
		{
			_gameData = gameData;
			CreatableObjects.Clear();
			foreach (var obj in _gameData.CreatableObjects)
				CreatableObjects.Add(obj);
		}

		public void Dispose()
		{
			CreatableObjects.Clear();
			ReloadVizualizatorRequest = null;
		}

		private void OnReloadCommand(object o)
		{
			RaiseReloadVizualizatorRequest();
		}

		private void OnOneStepCommand(object o)
		{
			var handler = OneStepRequested;
			if (handler != null)
				handler();
		}

		private void RaiseReloadVizualizatorRequest()
		{
			var handler = ReloadVizualizatorRequest;
			if (handler != null)
				handler();
		}
	}
}
