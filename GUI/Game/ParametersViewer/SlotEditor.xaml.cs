﻿using System.ComponentModel;
using SIICore.Data;

namespace GUI.Game.ParametersViewer
{
	public partial class SlotEditor : INotifyPropertyChanged
	{
		private Slot _slot;
		public Slot Slot
		{
			get { return _slot; }
			set
			{
				_slot = value;
				OnPropertyChanged("Slot");
			}
		}

		public SlotEditor()
		{
			InitializeComponent();
		}


		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged(string propertyName)
		{
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
