﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using GUI.Game.ParametersViewer;
using SIICore.Data;
using SIICore.Models;
using Frame = SIICore.Data.Frame;

// ReSharper disable once CheckNamespace


namespace GUI.Game
{
	public partial class GameShell : INotifyPropertyChanged
	{
		protected virtual void OnPropertyChanged(string propertyName)
		{
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null)
				handler(this, new PropertyChangedEventArgs(propertyName));
		}
		public event PropertyChangedEventHandler PropertyChanged;

		private Frame _currentFrame;
		public Frame CurrentFrame
		{
			get { return _currentFrame; }
			set
			{
				_currentFrame = value;
				StackPanel.Visibility = value == null ? Visibility.Collapsed : Visibility.Visible;
				TextBlock.Visibility = value == null ? Visibility.Visible : Visibility.Collapsed;
				OnPropertyChanged("CurrentFrame");
			}
		}

		public GameShell()
		{
			InitializeComponent();
		}

		public void Show(IVizualizedObject vizualizedObject)
		{
			StackPanel.Children.Clear();
			CurrentFrame = vizualizedObject.FrameData;

			var title = new TextBlock();
			title.HorizontalAlignment = HorizontalAlignment.Center;
			title.Margin = new Thickness(0,0,0,20);
			title.Text = CurrentFrame.Name;
			title.Style = (Style)Resources["BlackMediumTextBlockStyle"];
			StackPanel.Children.Add(title);

			foreach (var slot in CurrentFrame.slots)
			{
				if (DomainHelper.getDomainType(slot.DefaultValue.Type) != DomainTypes.NumberType)
					continue;

				var slotEditor = new SlotEditor();
				slotEditor.HorizontalAlignment = HorizontalAlignment.Stretch;
				slotEditor.Margin = new Thickness(0,5,0,5);
				slotEditor.Slot = slot;

				StackPanel.Children.Add(slotEditor);
			}
		}
	}
}
