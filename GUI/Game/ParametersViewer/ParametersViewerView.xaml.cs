﻿using System;
using GUISDK;

// ReSharper disable once CheckNamespace
namespace GUI.Game
{
	internal partial class ParametersViewerView : IModelPresenter<ParametersViewerModel>
	{
		public event Action ModeChanged;

		public ParametersViewerView()
		{
			InitializeComponent();
			RadioButton.ValueChanged += RadioButtonOnValueChanged;
		}

		private void RadioButtonOnValueChanged()
		{
			var handler = ModeChanged;
			if (handler != null)
				handler();
		}

		public void SetModel(ParametersViewerModel dataModel)
		{
			DataContext = dataModel;
		}
	}
}
