﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using SIICore.Data;


// ReSharper disable once CheckNamespace
namespace GUI.Game
{
	public partial class InGameEditor
	{
		public Action<IVizualizedObject> SelectedBrushChanged;

		private bool _flag;
		public InGameEditor()
		{
			InitializeComponent();

			Loaded += OnLoaded;

		}

		private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
		{
			Button.Command.Execute(null);
			_flag = true;
		}

		public void Reload()
		{
			if (_flag)
				Button.Command.Execute(null);
		}

		public void ClearSelection()
		{
			ListBox.SelectedIndex = -1;
		}

		private void Selector_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			var listBox = (ListBox)sender;
			var selected = listBox.SelectedItem as IVizualizedObject;
			if (selected == null)
				return;
			RaiseSelectedBrushChanged(selected);
			Mouse.OverrideCursor = Cursors.Arrow;
		}

		private void RaiseSelectedBrushChanged(IVizualizedObject brush)
		{
			var handler = SelectedBrushChanged;
			if (handler != null)
				handler(brush);
		}
	}
}
