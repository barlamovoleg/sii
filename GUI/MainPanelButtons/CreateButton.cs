﻿using System;
using System.Windows;
using Common.Repository;
using GUI.Common;
using GUISDK.Components;
using SIICore.Data;

// ReSharper disable once CheckNamespace
namespace GUI
{
	internal class CreateButton : SidePanelButtonModel
	{
		public event Action GameCreated;

		private CreateControlView _createControlView;
		private CreateControlModel _createControlModel;

		private readonly IRepositoryRelatedService<Frame, Slot> _repositoryService;

		public CreateButton(IRepositoryRelatedService<Frame, Slot> repositoryService)
		{
			_repositoryService = repositoryService;
			Title = "Создать";
		}

		protected override FrameworkElement GetContent()
		{
			if (_createControlView != null)
				return _createControlView;

			return CreateContent();
		}

		private FrameworkElement CreateContent()
		{
			GUIcommon.Logger.LogInfo(string.Format(StaticStrings.SidePanel.ButtonContentShow, Title));
			_createControlModel = new CreateControlModel(_repositoryService);
			_createControlModel.GameCreated += RaiseGameCreated;
			_createControlView = new CreateControlView();
			_createControlView.SetModel(_createControlModel);
			
			return _createControlView;
		}

		private void RaiseGameCreated()
		{
			var handler = GameCreated;
			if (handler != null)
				handler();
		}
	}
}
