﻿using System.Windows;
using GUI.Common;
using GUISDK.Components;
using SIICore;

// ReSharper disable once CheckNamespace
namespace GUI
{
	internal class EditorButton : SidePanelButtonModel
	{
		private EditorView _editorView;
		private EditorModel _editorModel;

		public EditorButton()
		{
			Title = "Редактор";
			IsVisible = ActiveRepositoryProvider.IsRepositoryReady;
			ActiveRepositoryProvider.ActiveRepositoryChanged += ActiveRepositoryProviderOnActiveRepositoryChanged;
		}

		protected override FrameworkElement GetContent()
		{
			GUIcommon.Logger.LogInfo(string.Format(StaticStrings.SidePanel.ButtonContentShow, Title));

			if (_editorView != null)
				return _editorView;

			return CreateContent();
		}

		protected override void DisposeInternal()
		{
			base.DisposeInternal();

			ActiveRepositoryProvider.ActiveRepositoryChanged -= ActiveRepositoryProviderOnActiveRepositoryChanged;
			using (_editorModel)
			{
				//Dispose
			}
		}

		private FrameworkElement CreateContent()
		{
			_editorModel = new EditorModel();
			_editorView = new EditorView();
			_editorView.SetModel(_editorModel);
			return _editorView;
		}

		private void ActiveRepositoryProviderOnActiveRepositoryChanged()
		{
			IsVisible = ActiveRepositoryProvider.IsRepositoryReady;
		}
	}
}
