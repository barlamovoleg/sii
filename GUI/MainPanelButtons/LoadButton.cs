﻿using System;
using System.Windows;
using Common.Repository;
using GUI.Common;
using GUI.Loading;
using GUISDK.Components;
using SIICore.Data;

// ReSharper disable once CheckNamespace
namespace GUI
{
	internal class LoadButton : SidePanelButtonModel, ISelectedGameNameProvider
	{
		public event Action GameLoaded;
		public string SelectedName { get; private set; }

		private LoadControlView _loadControlView;
		private LoadControlModel _loadControlModel;

		private readonly IRepositoryRelatedService<Frame, Slot> _repositoryService;

		public LoadButton(IRepositoryRelatedService<Frame, Slot> repositoryService)
		{
			Title = "Загрузить";
			_repositoryService = repositoryService;
		}

		protected override FrameworkElement GetContent()
		{
			GUIcommon.Logger.LogInfo(string.Format(StaticStrings.SidePanel.ButtonContentShow, Title));
			return CreateContent();
		}

		private FrameworkElement CreateContent()
		{
			_loadControlModel = new LoadControlModel(_repositoryService, this);
			_loadControlModel.GameLoaded += RaiseGameLoaded;
			_loadControlView = new LoadControlView();
			_loadControlView.SelectedGameNameChanged += LoadControlViewOnSelectedGameNameChanged;
			_loadControlView.SetModel(_loadControlModel);
			return _loadControlView;
		}

		private void RaiseGameLoaded()
		{
			var handler = GameLoaded;
			if (handler != null)
				handler();
		}

		private void LoadControlViewOnSelectedGameNameChanged(string name)
		{
			SelectedName = name;
		}
	}
}
