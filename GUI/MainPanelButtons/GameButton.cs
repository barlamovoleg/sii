﻿using System.Windows;
using GUI.Common;
using GUISDK.Components;
using SIICore;

// ReSharper disable once CheckNamespace
namespace GUI
{
	internal class GameButton : SidePanelButtonModel
	{
		private GameView _gameView;
		private GameModel _gameModel;

		private readonly VizualizersFabric _vizualizersFabric;

		public GameButton(VizualizersFabric vizualizersFabric)
		{
			_vizualizersFabric = vizualizersFabric;
			Title = "Визуализация";
			ActiveRepositoryProvider.ActiveRepositoryChanged += ActiveRepositoryProviderOnActiveRepositoryChanged;
			IsVisible = _vizualizersFabric.IsServiceExist && ActiveRepositoryProvider.IsRepositoryReady;
		}

		protected override FrameworkElement GetContent()
		{
			GUIcommon.Logger.LogInfo(string.Format(StaticStrings.SidePanel.ButtonContentShow, Title));

			if (_gameView != null)
				return _gameView;

			return CreateContent();
		}

		protected override void DisposeInternal()
		{
			base.DisposeInternal();

			ActiveRepositoryProvider.ActiveRepositoryChanged -= ActiveRepositoryProviderOnActiveRepositoryChanged;
			using (_gameModel)
			{
				//Dispose
			}
		}

		private FrameworkElement CreateContent()
		{
			_gameModel = new GameModel(_vizualizersFabric);
			_gameView = new GameView();
			_gameView.SetModel(_gameModel);
			return _gameView;
		}

		private void ActiveRepositoryProviderOnActiveRepositoryChanged()
		{
			IsVisible = ActiveRepositoryProvider.IsRepositoryReady;
		}
	}
}
