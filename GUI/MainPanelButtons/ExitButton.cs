﻿using System;
using System.Windows;
using GUI.Common;
using GUISDK.Components;

// ReSharper disable once CheckNamespace
namespace GUI
{
	internal class ExitButton : SidePanelButtonModel
	{
		public event Action CloseRequested;

		public ExitButton()
		{
			Title = "Выход";
		}

		protected override FrameworkElement GetContent()
		{
			GUIcommon.Logger.LogInfo(string.Format(StaticStrings.SidePanel.ButtonContentShow, Title));
			RaiseCloseRequested();
			return null;
		}

		private void RaiseCloseRequested()
		{
			var handler = CloseRequested;
			if (handler != null)
				handler();
		}
	}
}
