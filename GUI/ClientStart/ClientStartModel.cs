﻿using System;
using System.Collections.ObjectModel;
using Common.Repository;
using GUI.Common;
using GUISDK;
using GUISDK.Components;
using SIICore;
using SIICore.Data;

// ReSharper disable once CheckNamespace
namespace GUI
{
	internal class ClientStartModel : BaseModel, IDisposable
	{
		public event Action GameCreated;
		public event Action CloseRequested;

		public ObservableCollection<SidePanelButtonModel> SidePanelTopButtons { get; private set; }
		public ObservableCollection<SidePanelButtonModel> SidePanelBottomButtons { get; private set; }

		private readonly VizualizersFabric _vizualizersFabric;
		private readonly IRepositoryRelatedService<Frame, Slot> _repositoryService;

		public ClientStartModel(VizualizersFabric vizualizersFabric, IRepositoryRelatedService<Frame, Slot> repositoryService)
		{
			_vizualizersFabric = vizualizersFabric;
			_repositoryService = repositoryService;

			SidePanelTopButtons = new ObservableCollection<SidePanelButtonModel>();
			SidePanelBottomButtons = new ObservableCollection<SidePanelButtonModel>();

			CreateMainButtons();
		}

		public void Dispose()
		{
			CloseRequested = null;
		}

		private void CreateMainButtons()
		{
			GUIcommon.Logger.LogInfo(StaticStrings.SidePanel.ButtonsCreateStart);
			var createButton = new CreateButton(_repositoryService);
			createButton.GameCreated += CreateButtonOnGameCreated;
			SidePanelTopButtons.Add(createButton);

			var loadButton = new LoadButton(_repositoryService);
			loadButton.GameLoaded += CreateButtonOnGameCreated;
			SidePanelTopButtons.Add(loadButton);

			var exitButton = new ExitButton();
			exitButton.CloseRequested += RaiseCloseRequested;
			SidePanelBottomButtons.Add(exitButton);

			var editorButton = new EditorButton();
			SidePanelTopButtons.Add(editorButton);

			var gameButton = new GameButton(_vizualizersFabric);
			SidePanelTopButtons.Add(gameButton);
			GUIcommon.Logger.LogInfo(StaticStrings.SidePanel.ButtonsCreateEnd);
		}

		private void CreateButtonOnGameCreated()
		{
			RaiseGameCreated();
		}

		private void RaiseCloseRequested()
		{
			var handler = CloseRequested;
			if (handler != null)
				handler();
		}

		private void RaiseGameCreated()
		{
			var handler = GameCreated;
			if (handler != null)
				handler();
		}
	}
}
