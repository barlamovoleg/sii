﻿using System;
using System.Windows;
using Common.Repository;
using GUI.Common;
using SIICore;
using SIICore.Data;

// ReSharper disable once CheckNamespace
namespace GUI
{
	internal class ClientStartController : IDisposable
	{
		internal event Action CloseRequested;

		private readonly ClientStartModel _clientModel;
		private readonly ClientStartView _clientView;

		public ClientStartController(VizualizersFabric vizualizersFabric, IRepositoryRelatedService<Frame, Slot> repositoryService)
		{
			_clientModel = new ClientStartModel(vizualizersFabric, repositoryService);
			_clientModel.GameCreated += ClientModelOnGameCreated;
			_clientModel.CloseRequested += ClientModelOnCloseRequested;

			_clientView = new ClientStartView();
			_clientView.SetModel(_clientModel);
		}

		public void Dispose()
		{
			try
			{
				using (_clientModel)
				{
					//Disoise
				}
				using (_clientView)
				{
					//Disoise
				}
			}
			catch (Exception exception)
			{
				throw new GuiException(StaticStrings.ExceptionsErrors.MainDisposeException, exception,
					"{B9095D0F-D857-4E6F-B400-FCAC5D082A8C}");
			}
		}

		internal FrameworkElement GetView()
		{
			return _clientView;
		}

		internal bool OnCloseUserRequest()
		{
			return MessageBoxProvider.ShowQuestion("Вы уверены что хотите выйти?", "Выход");
		}

		private void ClientModelOnGameCreated()
		{
			_clientView.SidePanel.UnselectButton();
		}

		private void ClientModelOnCloseRequested()
		{
			var canClose = OnCloseUserRequest();
			if (canClose)
				RaiseCloseRequested();
		}

		private void RaiseCloseRequested()
		{
			var handler = CloseRequested;
			if (handler != null)
				handler();
		}
	}
}
