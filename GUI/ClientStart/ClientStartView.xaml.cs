﻿using System;
using GUISDK;

// ReSharper disable once CheckNamespace
namespace GUI
{
	internal partial class ClientStartView : IModelPresenter<ClientStartModel>, IDisposable
	{
		public ClientStartView()
		{
			InitializeComponent();

			SidePanel.DefaultContent = new Authors();
		}

		public void Dispose()
		{
			SidePanel.Dispose();
		}

		public void SetModel(ClientStartModel dataModel)
		{
			DataContext = dataModel;
		}
	}
}
