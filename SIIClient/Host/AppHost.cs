﻿using System;
using System.Collections.Generic;
using System.Windows;
using Common;
using GUI;
using Logger;
using SIICore;
using SIICore.Services;
using SIIVizualizator;

// ReSharper disable once CheckNamespace
namespace SIIClient
{
	internal class AppHost : CoreAppHost
	{
		protected override IServiceCloseable MainService
		{
			get { return _mainService ?? (_mainService = new ClientGuiService()); }
		}

		protected sealed override IGraphicsHostService<Window> HostContainer
		{
			get { return _hostService ?? (_hostService = new HostWindow()); }
		}

		private readonly IAppInfo _appInfo;
		private IServiceCloseable _mainService;
		private IGraphicsHostService<Window> _hostService;

		public AppHost(IAppInfo appInfo, IAppBuilder appBuilder)
			: base(appBuilder)
		{
			_appInfo = appInfo;
			HostContainer.GraphicHost.Title = appInfo.AppFullName;
		}

		protected override IEnumerable<IService> GetServices()
		{
			var result = new List<IService>();

			try
			{
				var logManager = new LogManager(_appInfo, CommonStrings.SystemLogName);
				result.Add(logManager);

				var vizualizeService = new SIIVisualizerService();
				result.Add(vizualizeService);

                var frameService = new FrameRepositoryService(null, null, null);
                result.Add(frameService);

			}
			catch (Exception e)
			{
				LogException(e);
			}

			return result;
		}
    }
}
