﻿using System;
using System.Windows;
using Common;

// ReSharper disable once CheckNamespace
namespace SIIClient
{
	internal partial class HostWindow : IGraphicsHostService<Window>
	{
		public Guid Key
		{
			get { return new Guid("{70E1F66E-05B4-4AA3-BD6F-28131DFC81BE}"); }
		}
		public Window GraphicHost { get { return this; } }

		public HostWindow()
		{
			InitializeComponent();
		}

		public void Dispose()
		{
		}

		public void Display()
		{
			Show();
		}
	}
}
