﻿using System;
using System.Windows;
using System.Windows.Threading;
using Common;
using SIICore;

namespace SIIClient
{
	public partial class App
	{
		private CommonServiceController _serviceController;
		private IAppHost _appHost;
		private AppTerminator _appTerminator;

		private void App_OnStartup(object sender, StartupEventArgs e)
		{
			try
			{
				var appInfo = new ClientAppInfo();
				var appBuilder = new CoreAppBuilder(appInfo);
				_serviceController = new CommonServiceController();
				_appHost = new AppHost(appInfo, appBuilder);
			}
			catch (Exception exception)
			{
				throw new AppException(CommonStrings.ExceptionsAndErrors.BuildingAppExceptionMessages.PreInitializingException, exception,
					"{957889CB-9A10-48DA-BB3E-E078963BDF80}") { IsFatalException = true };
			}

			_appTerminator = new AppTerminator(_appHost, _serviceController);
			_appTerminator.Shutdown += Shutdown;

			_appHost.LogInfo(CommonStrings.ServicesMessages.TryingRegisterServices);
			_appHost.RegisterServices(_serviceController);
			_appHost.LogInfo(CommonStrings.BuildingOrRunningAppMessages.TryingHostRun);
			_appHost.RunApplication(_serviceController);
		}

		private void App_OnDispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
		{
			e.Handled = true;

			var exception = e.Exception;
			if (_appHost != null)
				_appHost.LogException(exception);

			var appException = exception as AppException;
			if (appException != null && appException.IsFatalException)
			{
				if (_appHost != null)
					_appHost.LogInfo(CommonStrings.ExceptionsAndErrors.CriticalError);

				MessageBox.Show(exception + Environment.NewLine + Environment.NewLine + CommonStrings.ExceptionsAndErrors.ApplicationWillBeClosed,
				CommonStrings.ExceptionsAndErrors.FatalErrorTitle, MessageBoxButton.OK, MessageBoxImage.Error);

				if (_appHost != null)
				{
					_appHost.DeregisterServices(_serviceController);
					_appHost.TerminateApplication(CloseReason.Exception);
				}

				Shutdown();
				return;
			}

			if (SystemConfiguration.IsDebug)
				Console.Beep(500, 500);
		}

		private void App_OnSessionEnding(object sender, SessionEndingCancelEventArgs e)
		{
			_appTerminator.AppOnSessionEnding();
		}

		private void App_OnExit(object sender, ExitEventArgs e)
		{
			_appTerminator.AppOnExit();
		}
	}
}
