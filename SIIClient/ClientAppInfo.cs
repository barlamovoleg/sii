﻿using SIICore;

namespace SIIClient
{
	internal class ClientAppInfo : CoreAppInfo
	{
		public override string AppName
		{
			get { return AppConstants.APP_NAME; }
		}
	}
}
