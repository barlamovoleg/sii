﻿using Common.Repository;
using SIICore.Data;
using System.Collections.Generic;
using System;


namespace SIICore.Services
{
	public class FrameRepositoryService : IRepositoryRelatedService<Frame, Slot>
    {
		private readonly GraphQueryHelper _graphQueryHelper;
        public FrameRepositoryService(string connectionString, string dbName, string tablename)
        {
            _graphQueryHelper = new GraphQueryHelper(connectionString, dbName, tablename);
        }

        public IRepositoryRelated<Frame, Slot> GetRepository(string name)
        {
            return new FrameRepository(name, _graphQueryHelper);
        }

        public Guid Key
        {
            get { return new Guid("573720D1-C97D-4F89-A5A7-95E63DE039CD"); }
        }

        public void Dispose()
        {

        }


        public IEnumerable<string> GetRepositoryNames()
        {
            return _graphQueryHelper.GetGraphNames();
        }

		public void RemoveRepository(string name)
		{
			_graphQueryHelper.RemoveGraph(name);
		}
    }
}
