﻿using Common.Repository;
using System;
using System.Collections.Generic;
using MongoDB.Bson;
using SIICore.Utils;
using SIICore.Models;
using System.Windows;


namespace SIICore.Data
{
    public class FrameRepository : IRepositoryRelated<Frame, Frame>, IRepositoryRelated<Frame, Slot>
    {
        private const string FRAME_TYPE_NAME = "Frame";
        private const string SLOT_TYPE_NAME = "Slot";
        private const string A_PART_OF_TYPE_NAME = "a_part_of";
        private const string IS_A_TYPE_NAME = "is_a";
        private const string IS_SUBCLASS_OF_TYPE_NAME = "subclass_of";

        private const string FRAME_TYPE = "Type";
        private const string FRAME_X = "X";
        private const string FRAME_Y = "Y";
        private const string FRAME_WIDTH = "Width";
        private const string FRAME_HEIGHT = "Height";       

        private const string SLOT_VALUE = "Value";
        private const string SLOT_DEFAULT_VALUE = "DefaultValue";
        private const string SLOT_DATA_TYPE = "DataType";
        private const string SLOT_INHERITANCE = "Inheritance";

        private string _knowledgeDBId;
        private string _frameTypeId;
        private string _slotTypeId;
		
        private string _aPartOfTypeId;
        private string _isATypeId;
        private string _isSubclassOfTypeId;

		private readonly GraphQueryHelper _graphQueryHelper;
        public FrameRepository(string name, GraphQueryHelper helper)
        {
            if (name == null)
                throw new ArgumentNullException("name");

            if (helper == null)
                throw new ArgumentNullException("helper");

            
            _graphQueryHelper = helper;
            _knowledgeDBId = _graphQueryHelper.GetOrAddGraph(name);
            _frameTypeId = _graphQueryHelper.GetVertexTypeByName(_knowledgeDBId, FRAME_TYPE_NAME);
            if (_frameTypeId == null)
				_frameTypeId = _graphQueryHelper.AddVertexType(_knowledgeDBId, FRAME_TYPE_NAME);

            _slotTypeId = _graphQueryHelper.GetVertexTypeByName(_knowledgeDBId, SLOT_TYPE_NAME);
            if (_slotTypeId == null)
                _slotTypeId = _graphQueryHelper.AddVertexType(_knowledgeDBId, SLOT_TYPE_NAME);

            _aPartOfTypeId = _graphQueryHelper.GetArcTypeByName(_knowledgeDBId, A_PART_OF_TYPE_NAME);
            if (_aPartOfTypeId == null)
                _aPartOfTypeId = _graphQueryHelper.AddArcType(_knowledgeDBId, A_PART_OF_TYPE_NAME);

            _isATypeId = _graphQueryHelper.GetArcTypeByName(_knowledgeDBId, IS_A_TYPE_NAME);
            if (_isATypeId == null)
                _isATypeId = _graphQueryHelper.AddArcType(_knowledgeDBId, IS_A_TYPE_NAME);

            _isSubclassOfTypeId = _graphQueryHelper.GetArcTypeByName(_knowledgeDBId, IS_SUBCLASS_OF_TYPE_NAME);
            if (_isSubclassOfTypeId == null)
                _isSubclassOfTypeId = _graphQueryHelper.AddArcType(_knowledgeDBId, IS_SUBCLASS_OF_TYPE_NAME);

            if (_graphQueryHelper.GetVertexByName(_knowledgeDBId, "System") == null)
            {
                CreateSystem();
            }

        }

        private void CreateSystem()
        {
            var repo = this;

            var frameSystem = new SIICore.Data.Frame("System", SIICore.Data.Frame.FrameType.System);
            frameSystem.Position = new Rect(142.5, 37.5, 54, 23);
            repo.Add(frameSystem);

            var frameObject = new SIICore.Data.Frame(Frame.SYSTEM_FRAME_OBJECT, SIICore.Data.Frame.FrameType.System);
            frameObject.Position = new Rect(67.5, 97.5, 99, 23);
            repo.Add(frameObject);
            repo.AddRelation(frameObject, new SIICore.Data.Slot(Frame.SYSTEM_SLOT_COORD_X, new SIICore.Data.Value(DomainHelper.getInnerType(0), 0)));
            repo.AddRelation(frameObject, new SIICore.Data.Slot(Frame.SYSTEM_SLOT_COORD_Y, new SIICore.Data.Value(DomainHelper.getInnerType(0), 0)));
            repo.AddRelation(frameObject, new SIICore.Data.Slot("Иконка", new SIICore.Data.Value(DomainHelper.getInnerType("file"), DomainHelper.getInnerType("file"))));

            var frameActor = new SIICore.Data.Frame(Frame.SYSTEM_FRAME_ACTOR, SIICore.Data.Frame.FrameType.System);
            frameActor.Position = new Rect(127.5, 172.5, 90, 23);
            repo.Add(frameActor);
            //repo.AddRelation(frameActor, new SIICore.Data.Slot(Frame.SYSTEM_SLOT_COORD_X, new SIICore.Data.Value(DomainHelper.getInnerType(0), 0)));
            //repo.AddRelation(frameActor, new SIICore.Data.Slot(Frame.SYSTEM_SLOT_COORD_Y, new SIICore.Data.Value(DomainHelper.getInnerType(0), 0)));
            //repo.AddRelation(frameActor, new SIICore.Data.Slot("Иконка", new SIICore.Data.Value(DomainHelper.getInnerType("file"), DomainHelper.getInnerType("file"))));

            var frameAction = new SIICore.Data.Frame(Frame.SYSTEM_FRAME_ACTION, SIICore.Data.Frame.FrameType.System);
            frameAction.Position = new Rect(187, 97.5, 117, 23);
            repo.Add(frameAction);
            repo.AddRelation(frameAction, new SIICore.Data.Slot(Frame.SYSTEM_SLOT_SUBJECT, new SIICore.Data.Value(DomainHelper.getInnerType(frameActor), DomainHelper.getInnerType(frameActor))));
            repo.AddRelation(frameAction, new SIICore.Data.Slot(Frame.SYSTEM_SLOT_OBJECT, new SIICore.Data.Value(DomainHelper.getInnerType(frameObject), DomainHelper.getInnerType(frameObject))));
            repo.AddRelation(frameAction, new SIICore.Data.Slot(Frame.SYSTEM_SLOT_RESULT, new SIICore.Data.Value(DomainHelper.getInnerType("result"), DomainHelper.getInnerType("result"))));

            var frameCell = new SIICore.Data.Frame("Ячейка(sys)", SIICore.Data.Frame.FrameType.System);
            frameCell.Position = new Rect(7.5, 172.5, 99, 23);
            repo.Add(frameCell);
            //repo.AddRelation(frameCell, new SIICore.Data.Slot(Frame.SYSTEM_SLOT_COORD_X, new SIICore.Data.Value(DomainHelper.getInnerType(0), 0)));
            //repo.AddRelation(frameCell, new SIICore.Data.Slot(Frame.SYSTEM_SLOT_COORD_Y, new SIICore.Data.Value(DomainHelper.getInnerType(0), 0)));
            //repo.AddRelation(frameCell, new SIICore.Data.Slot("Иконка", new SIICore.Data.Value(DomainHelper.getInnerType("file"), DomainHelper.getInnerType("file"))));

            ((IRepositoryRelated<Frame, Frame>)repo).AddRelation(frameSystem, frameObject);
            ((IRepositoryRelated<Frame, Frame>)repo).AddRelation(frameObject, frameActor);
            ((IRepositoryRelated<Frame, Frame>)repo).AddRelation(frameSystem, frameAction);
            ((IRepositoryRelated<Frame, Frame>)repo).AddRelation(frameSystem, frameCell);
        }

		public IEnumerable<Frame> GetAll()
        {
			var ret = new List<Frame>();

            BsonArray vertexesFrame = _graphQueryHelper.GetVertexesByType(_knowledgeDBId, _frameTypeId);
            foreach (BsonDocument vertexFrame in vertexesFrame)
            {
                BsonValue frameId = null;
                BsonValue frameName = null;

                if (!vertexFrame.TryGetValue(GraphFields.ID, out frameId) || !vertexFrame.TryGetValue(GraphFields.NAME, out frameName))
                    continue;
				var frame = new Frame(frameId.ToString(), frameName.ToString());
                BsonValue options = null;
                BsonValue type = null;
                BsonValue X = null;
                BsonValue Y = null;
                BsonValue width = null;
                BsonValue height = null;        
                if (vertexFrame.TryGetValue(GraphFields.VERTEX_OPTIONS, out options))
                {
                    if (options.AsBsonDocument.TryGetValue(FRAME_TYPE, out type))
                        frame.Type = (Frame.FrameType)type.ToInt32();
                    if (options.AsBsonDocument.TryGetValue(FRAME_X, out X) &&
                        options.AsBsonDocument.TryGetValue(FRAME_Y, out Y) &&
                        options.AsBsonDocument.TryGetValue(FRAME_WIDTH, out width) &&
                        options.AsBsonDocument.TryGetValue(FRAME_HEIGHT, out height))
                        frame.Position = new Rect(X.ToDouble(), Y.ToDouble(), width.ToDouble(), height.ToDouble());
                }
                GetSlots(frame);
                ret.Add(frame);
            }

            SetFrameRelations(ret);
            return ret;
        }

        private void GetSlots(Frame frame)
        {
            BsonArray arcsSlotFrame = _graphQueryHelper.GetArcsByVertex2AndType(_knowledgeDBId, frame.Id, _aPartOfTypeId);
            foreach (BsonDocument arc in arcsSlotFrame)
            {
                BsonValue arcId = null;
                BsonValue slotId = null;

                if (!arc.TryGetValue(GraphFields.ID, out arcId) || !arc.TryGetValue(GraphFields.ARC_VERTEX_ONE, out slotId))
                    continue;

                BsonDocument vertexSlot = _graphQueryHelper.GetVertex(_knowledgeDBId, slotId.ToString());
                BsonValue slotName = null;

                if (vertexSlot == null || !vertexSlot.TryGetValue(GraphFields.NAME, out slotName))
                    continue;

                var slot = new Slot(slotId.ToString(), slotName.ToString(), arcId.ToString());

                BsonValue options = null;
                BsonValue dataType = null;
                BsonValue defaultValue = null;
                BsonValue value = null;
                BsonValue inheritance = null;

                if (vertexSlot.TryGetValue(GraphFields.VERTEX_OPTIONS, out options))
                {
                    if (options.AsBsonDocument.TryGetValue(SLOT_DEFAULT_VALUE, out defaultValue) &&
                    options.AsBsonDocument.TryGetValue(SLOT_DATA_TYPE, out dataType))
                    {
                        string type = dataType.ToString();
                        if (DomainHelper.getDomainType(type) == DomainTypes.NumberType)
                            slot.DefaultValue = new Value(type, Convert.ChangeType(defaultValue.ToString(), 0.GetType()));
                        else
                            slot.DefaultValue = new Value(type, defaultValue.ToString());

                        //if (options.AsBsonDocument.TryGetValue(SLOT_VALUE, out value))
                        //    slot.Value = new Value(type, Convert.ChangeType(defaultValue.ToString(), type));
                    }

                    if (options.AsBsonDocument.TryGetValue(SLOT_INHERITANCE, out inheritance))
                        slot.InheritanceType = (Slot.Inheritance)inheritance.ToInt32();

                }
                frame.slots.Add(slot);
            }
        }

        private void SetFrameRelations(List<Frame> frameList)
        {
            foreach (Frame frame in frameList)
            {
                BsonArray isAArcs = _graphQueryHelper.GetArcsByVertex2AndType(_knowledgeDBId, frame.Id, _isATypeId);
                foreach (BsonDocument arc in isAArcs)
                {
                    BsonValue arcId = null;
                    BsonValue childId = null;

                    if (!arc.TryGetValue(GraphFields.ID, out arcId) || !arc.TryGetValue(GraphFields.ARC_VERTEX_ONE, out childId))
                        continue;

                    Frame child = frameList.Find(x => StringUtils.StringsEqual(x.Id, childId.ToString()));

                    child.Parent = frame;
                    frame.children.Add(child);
                }

                BsonArray isSubclassOfArcs = _graphQueryHelper.GetArcsByVertex2AndType(_knowledgeDBId, frame.Id, _isSubclassOfTypeId);
                foreach (BsonDocument arc in isSubclassOfArcs)
                {
                    BsonValue arcId = null;
                    BsonValue subclassId = null;

                    if (!arc.TryGetValue(GraphFields.ID, out arcId) || !arc.TryGetValue(GraphFields.ARC_VERTEX_ONE, out subclassId))
                        continue;

                    Frame subclass = frameList.Find(x => StringUtils.StringsEqual(x.Id, subclassId.ToString()));
                    frame.requipments.Add(subclass.Id);
                }
            }
        }

		public Frame GetByID(int id)
        {
            throw new NotImplementedException();
        }

        public void Add(Frame entity)
        {
            var options = new BsonDocument();
            options.Add(FRAME_TYPE, entity.Type);
            options.Add(FRAME_X, entity.Position.X);
            options.Add(FRAME_Y, entity.Position.Y);
            options.Add(FRAME_WIDTH, entity.Position.Width);
            options.Add(FRAME_HEIGHT, entity.Position.Height);
            entity.Id = _graphQueryHelper.AddVertex(_knowledgeDBId, entity.Name, _frameTypeId, options);
        }

        public void Update(Frame entity)
        {
            //if (entity.Type == Frame.FrameType.System)
            //    return;

            var options = new BsonDocument();
            options.Add(FRAME_TYPE, entity.Type);
            options.Add(FRAME_X, entity.Position.X);
            options.Add(FRAME_Y, entity.Position.Y);
            options.Add(FRAME_WIDTH, entity.Position.Width);
            options.Add(FRAME_HEIGHT, entity.Position.Height);
            _graphQueryHelper.UpdateVertex(_knowledgeDBId, entity.Id, entity.Name, _frameTypeId, options);
            foreach (Slot slot in entity.slots)
            {
				options = new BsonDocument();
                //options.Add(SLOT_VALUE, slot.Value.Data.ToString());
                options.Add(SLOT_DEFAULT_VALUE, slot.DefaultValue.Data.ToString());
                options.Add(SLOT_DATA_TYPE, slot.DefaultValue.Type.ToString());
                options.Add(SLOT_INHERITANCE, slot.InheritanceType);

                _graphQueryHelper.UpdateVertex(_knowledgeDBId, slot.Id, slot.Name, _slotTypeId, options);
            }
        }

        public bool Delete(Frame entity)
        {
            if (entity.Type == Frame.FrameType.System)
                return false;

			var vertexIds = new List<string>();
            vertexIds.Add(entity.Id);
			var arcIds = new List<string>();
            foreach (Slot slot in entity.slots)
            {
                vertexIds.Add(slot.Id);
                arcIds.Add(slot.ArcId);
            }
            BsonArray isA2Arcs = _graphQueryHelper.GetArcsByVertex2AndType(_knowledgeDBId, entity.Id, _isATypeId);
            BsonArray isA1Arcs = _graphQueryHelper.GetArcsByVertex1AndType(_knowledgeDBId, entity.Id, _isATypeId);

            BsonArray isSubclassOf2Arcs = _graphQueryHelper.GetArcsByVertex2AndType(_knowledgeDBId, entity.Id, _isSubclassOfTypeId);
            BsonArray isSubclassOf1Arcs = _graphQueryHelper.GetArcsByVertex1AndType(_knowledgeDBId, entity.Id, _isSubclassOfTypeId);

            isA2Arcs.AddRange(isA1Arcs);
            isA2Arcs.AddRange(isSubclassOf2Arcs);
            isA2Arcs.AddRange(isSubclassOf1Arcs);
            foreach (BsonDocument arc in isA2Arcs)
            {
                BsonValue arcId = null;
                if (arc.TryGetValue(GraphFields.ID, out arcId))
                {
                    arcIds.Add(arcId.ToString());
                }
            }
            if (entity.Parent != null)
            {
                entity.Parent.children.Remove(entity);
            }
            foreach (Frame child in entity.children)
            {
                child.Parent = null;
            }
            _graphQueryHelper.RemoveVertexes(_knowledgeDBId, vertexIds);
            _graphQueryHelper.RemoveArcs(_knowledgeDBId, arcIds);
			entity = null;
            return true;
        }

        public void AddRelation(Frame frame, Slot slot)
        {
            //if (frame.Type == Frame.FrameType.System)
            //    return;

			var options = new BsonDocument();
            
            //options.Add(SLOT_VALUE, slot.Value == null ? null : slot.Value.Data.ToString());
            options.Add(SLOT_DEFAULT_VALUE, slot.DefaultValue.Data.ToString());
            options.Add(SLOT_DATA_TYPE, slot.DefaultValue.Type.ToString());
            options.Add(SLOT_INHERITANCE, slot.InheritanceType);

            slot.Id = _graphQueryHelper.AddVertex(_knowledgeDBId, slot.Name, _slotTypeId, options);
            slot.ArcId = _graphQueryHelper.AddArc(_knowledgeDBId, slot.Id, frame.Id, _aPartOfTypeId);

            frame.slots.Add(slot);

            string frameId = DomainHelper.getFrameId(slot.DefaultValue.Type);
            if (frameId != null)
            {
                _graphQueryHelper.AddArc(_knowledgeDBId, frameId, frame.Id, _isSubclassOfTypeId);
                frame.requipments.Add(frameId);
            }
            //добавляем в дочерние фреймы тоже
            foreach (var child in frame.children)
            {
                AddRelation(child, new Slot(slot));
            }
        }

        public bool RemoveRelation(Frame frame, Slot slot)
        {
            if (frame.Type == Frame.FrameType.System)
                return false;

			var vertexIds = new List<string>();
            vertexIds.Add(slot.Id);
            _graphQueryHelper.RemoveVertexes(_knowledgeDBId, vertexIds);

            var arcIds = new List<string>();
            arcIds.Add(slot.ArcId);
            string frameId = DomainHelper.getFrameId(slot.DefaultValue.Type);
            if (frameId != null)
            {
                string arcId = _graphQueryHelper.GetArcId(_knowledgeDBId, frameId, frame.Id, _isSubclassOfTypeId);
                if (arcId != null)
                {
                    arcIds.Add(arcId);
                }
                frame.requipments.Remove(frameId);
            }
            _graphQueryHelper.RemoveArcs(_knowledgeDBId, arcIds);
            frame.slots.Remove(slot);
			slot = null;
            return true;
        }

        public void AddRelation(Frame parent, Frame child)
        {
            //if (child.Type == Frame.FrameType.System)
            //    throw new ArgumentException("Child frame can't be system");

            string arcType = _isATypeId;
            BsonArray arcs = _graphQueryHelper.GetArcsByVertex1AndType(_knowledgeDBId, child.Id, arcType);
            if (arcs.Count != 0)
                throw new ArgumentException("Child already has a parent");

            _graphQueryHelper.AddArc(_knowledgeDBId, child.Id, parent.Id, arcType);
            child.Parent = parent;
            parent.children.Add(child);

            foreach (Slot slot in parent.slots)
            {
                AddRelation(child, new Slot(slot));
            }
        }

        public bool RemoveRelation(Frame parent, Frame child)
        {
            if (child.Type == Frame.FrameType.System)
                return false;

            if (child.Parent != parent)
                return false;

            string arcType = _isATypeId;
            BsonArray isAArcs = _graphQueryHelper.GetArcsByVertex1AndType(_knowledgeDBId, child.Id, arcType);
			var arcIds = new List<string>();
            foreach (BsonDocument arc in isAArcs)
            {
                BsonValue arcId = null;
                BsonValue vertex2Id = null;
				if (arc.TryGetValue(GraphFields.ID, out arcId) && 
                    arc.TryGetValue(GraphFields.ARC_VERTEX_TWO, out vertex2Id) && 
                    StringUtils.StringsEqual(vertex2Id.ToString(), parent.Id))
                {
                    arcIds.Add(arcId.ToString());
                }
            }
            _graphQueryHelper.RemoveArcs(_knowledgeDBId, arcIds);
            child.Parent = null;
            parent.children.Remove(child);
            return true;
        }
    }
}
