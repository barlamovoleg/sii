﻿using MongoDB.Bson;
using System.Collections.Generic;
using System;
using SIICore.Utils;


namespace SIICore.Data
{
    public class GraphQueryHelper
    {
		private const string CONNECTION_string_DEFAULT = "mongodb://localhost";
		private const string DATABASE_NAME_DEFAULT = "test";
		private const string COLLECTION_NAME_DEFAULT = "graphs";

		private readonly GraphDBProvider _dbProvider;
        public GraphQueryHelper(string connectionstring, string dbName, string collectionName)
        {
            _dbProvider = new GraphDBProvider(  connectionstring ?? CONNECTION_string_DEFAULT,
                                                dbName ?? DATABASE_NAME_DEFAULT,
                                                collectionName ?? COLLECTION_NAME_DEFAULT );

        }

		public string GetOrAddGraph(string name)
        {
            BsonDocument graph = _dbProvider.TryGetByName(name);
            if (graph == null)
            {
				graph = new BsonDocument();
                graph.Add(GraphFields.NAME, name);
                _dbProvider.Save(graph);
            }

            BsonValue id = null;
            graph.TryGetValue(GraphFields.ID, out id);
            return id != null? id.ToString() : null;
        }

		public IEnumerable<string> GetGraphNames()
        {
            List<BsonDocument> graphs = _dbProvider.GetAll();
			var ret = new List<string>();//TODO var
            foreach (BsonDocument graph in graphs)
            {
                BsonValue name = null;
                if (graph.TryGetValue(GraphFields.NAME, out name))
                    ret.Add(name.ToString());
            }
            return ret;
        }

	    public void RemoveGraph(string graphName)
	    {
		    throw new NotImplementedException();
	    }

		public string AddVertexType(string graphId, string name)
        {
            BsonDocument graph = _dbProvider.GetById(graphId);

			var vertexType = new BsonDocument();
            ObjectId vertexTypeId = ObjectId.GenerateNewId();
            vertexType.Add(GraphFields.ID, vertexTypeId);
            vertexType.Add(GraphFields.NAME, name);

            BsonValue vertexTypes = null;

            if (!graph.TryGetValue(GraphFields.GRAPH_VERTEX_TYPES, out vertexTypes))
            {
                vertexTypes = new BsonArray();
                graph.Add(GraphFields.GRAPH_VERTEX_TYPES, vertexTypes);
                
            }
            vertexTypes.AsBsonArray.Add(vertexType);
            _dbProvider.Save(graph);

            return vertexTypeId.ToString();
        }

		public BsonDocument GetVertexTypeById(string graphId, string vertexTypeId)
        {
            if (string.IsNullOrEmpty(vertexTypeId))
                throw new ArgumentException("vertexTypeName is empty");

            BsonDocument graph = _dbProvider.GetById(graphId);
            BsonValue vertexTypes = null;
            graph.TryGetValue(GraphFields.GRAPH_VERTEX_TYPES, out vertexTypes);

            BsonValue ret = vertexTypes != null ? GraphDBProvider.FirstOrDefaultById(vertexTypes.AsBsonArray, vertexTypeId) : null;
            return ret != null ? ret.AsBsonDocument : null;
        }

		public string GetVertexTypeByName(string graphId, string vertexTypeName)
        {
            if (string.IsNullOrEmpty(vertexTypeName))
                throw new ArgumentException("vertexTypeName is empty");

            BsonDocument graph = _dbProvider.GetById(graphId);
            BsonValue vertexTypes = null;
            graph.TryGetValue(GraphFields.GRAPH_VERTEX_TYPES, out vertexTypes);

            BsonValue ret = vertexTypes != null ? GraphDBProvider.FirstOrDefaultByName(vertexTypes.AsBsonArray, vertexTypeName) : null;
            BsonValue id = null;
            if (ret != null)
                ret.AsBsonDocument.TryGetValue(GraphFields.ID, out id);
            return id != null ? id.ToString() : null;
        }

		public BsonArray GetVertexTypes(string graphId)
        {
            BsonDocument graph = _dbProvider.GetById(graphId);
            BsonValue vertexTypes = null;
            graph.TryGetValue(GraphFields.GRAPH_VERTEX_TYPES, out vertexTypes);

            return vertexTypes != null ? vertexTypes.AsBsonArray : null;
        }

		public void UpdateVertexType(string graphId, string vertexTypeId, string name)
        {
            if (string.IsNullOrEmpty(vertexTypeId))
                throw new ArgumentException("vertexTypeId is empty");

            BsonDocument graph = _dbProvider.GetById(graphId);
            BsonValue vertexTypes = null;

            if (!graph.TryGetValue(GraphFields.GRAPH_VERTEX_TYPES, out vertexTypes))
                return;

            BsonValue ret = GraphDBProvider.FirstOrDefaultById(vertexTypes.AsBsonArray, vertexTypeId);
            if (ret == null)
                return;

            ret[GraphFields.NAME] = name;
            _dbProvider.Save(graph);
        }

		public void RemoveVertexType(string graphId, string vertexTypeId)
        {
            if (string.IsNullOrEmpty(vertexTypeId))
                throw new ArgumentException("vertexTypeName is empty");

            BsonDocument graph = _dbProvider.GetById(graphId);
            BsonValue vertexTypes = null;

            if (!graph.TryGetValue(GraphFields.GRAPH_VERTEX_TYPES, out vertexTypes))
                return;

            BsonValue ret = GraphDBProvider.FirstOrDefaultById(vertexTypes.AsBsonArray, vertexTypeId);
            if (ret == null)
                return;

            vertexTypes.AsBsonArray.Remove(ret);
            _dbProvider.Save(graph);
        }

		public string AddArcType(string graphId, string name)
        {
            BsonDocument graph = _dbProvider.GetById(graphId);

			var arcType = new BsonDocument();
            ObjectId arcTypeId = ObjectId.GenerateNewId();
            arcType.Add(GraphFields.ID, arcTypeId);
            arcType.Add(GraphFields.NAME, name);

            BsonValue arcTypes = null;

            if (!graph.TryGetValue(GraphFields.GRAPH_ARC_TYPES, out arcTypes))
            {
                arcTypes = new BsonArray();
                graph.Add(GraphFields.GRAPH_ARC_TYPES, arcTypes);

            }
            arcTypes.AsBsonArray.Add(arcType);
            _dbProvider.Save(graph);

            return arcTypeId.ToString();
        }

		public BsonDocument GetArcTypeById(string graphId, string arcTypeId)
        {
            if (string.IsNullOrEmpty(arcTypeId))
                throw new ArgumentException("arcTypeName is empty");

            BsonDocument graph = _dbProvider.GetById(graphId);
            BsonValue arcTypes = null;
            graph.TryGetValue(GraphFields.GRAPH_ARC_TYPES, out arcTypes);

			BsonValue ret = arcTypes != null ? GraphDBProvider.FirstOrDefaultById(arcTypes.AsBsonArray, arcTypeId) : null; //TODO Оператор ??
            return ret != null ? ret.AsBsonDocument : null;
        }

		public string GetArcTypeByName(string graphId, string arcTypeName)
        {
            if (string.IsNullOrEmpty(arcTypeName))
                throw new ArgumentException("arcTypeName is empty");

            BsonDocument graph = _dbProvider.GetById(graphId);
            BsonValue arcTypes = null;
            graph.TryGetValue(GraphFields.GRAPH_ARC_TYPES, out arcTypes);

			BsonValue ret = arcTypes != null ? GraphDBProvider.FirstOrDefaultByName(arcTypes.AsBsonArray, arcTypeName) : null;//TODO Оператор ??
            BsonValue id = null;
            if (ret != null)
                ret.AsBsonDocument.TryGetValue(GraphFields.ID, out id);
            return id != null ? id.ToString() : null;
        }

		public BsonArray GetArcTypes(string graphId)
        {
            BsonDocument graph = _dbProvider.GetById(graphId);
            BsonValue arcTypes = null;
            graph.TryGetValue(GraphFields.GRAPH_ARC_TYPES, out arcTypes);

			return arcTypes != null ? arcTypes.AsBsonArray : null;
        }

		public void UpdateArcType(string graphId, string arcTypeId, string name)
        {
            if (string.IsNullOrEmpty(arcTypeId))
                throw new ArgumentException("arcTypeName is empty");

            BsonDocument graph = _dbProvider.GetById(graphId);
            BsonValue arcTypes = null;

            if (!graph.TryGetValue(GraphFields.GRAPH_ARC_TYPES, out arcTypes))
                return;

            BsonValue ret = GraphDBProvider.FirstOrDefaultById(arcTypes.AsBsonArray, arcTypeId);
            if (ret == null)
                return;

            ret[GraphFields.NAME] = name;
            _dbProvider.Save(graph);
        }

		public void RemoveArcType(string graphId, string arcTypeId)
        {
            if (string.IsNullOrEmpty(arcTypeId))
                throw new ArgumentException("arcTypeName is empty");

            BsonDocument graph = _dbProvider.GetById(graphId);
            BsonValue arcTypes = null;

            if (!graph.TryGetValue(GraphFields.GRAPH_ARC_TYPES, out arcTypes))
                return;

            BsonValue ret = GraphDBProvider.FirstOrDefaultById(arcTypes.AsBsonArray, arcTypeId);
            if (ret == null)
                return;

            arcTypes.AsBsonArray.Remove(ret);
            _dbProvider.Save(graph);
        }

		public string AddVertex(string graphId, string name, string typeId, BsonDocument options = null)
        {
            BsonDocument graph = _dbProvider.GetById(graphId);
            if (GetVertexTypeById(graphId, typeId) == null)
                throw new ArgumentException("There is no VertexType with typeId");

			var vertex = new BsonDocument();
            ObjectId vertexId = ObjectId.GenerateNewId();
            vertex.Add(GraphFields.ID, vertexId);
            vertex.Add(GraphFields.NAME, name);
            vertex.Add(GraphFields.VERTEX_TYPE, typeId);
			vertex.Add(GraphFields.VERTEX_OPTIONS, options != null ? options : new BsonDocument());//TODO для проверки на нул есть ??

            BsonValue vertexes = null;

            if (!graph.TryGetValue(GraphFields.GRAPH_VERTEXES, out vertexes))
            {
                vertexes = new BsonArray();
                graph.Add(GraphFields.GRAPH_VERTEXES, vertexes);

            }
            vertexes.AsBsonArray.Add(vertex);
            _dbProvider.Save(graph);

            return vertexId.ToString();
        }

		public BsonDocument GetVertex(string graphId, string vertexId)
        {
            if (string.IsNullOrEmpty(vertexId))
                throw new ArgumentException("vertexId is empty");

            BsonDocument graph = _dbProvider.GetById(graphId);
            BsonValue vertexes = null;
            graph.TryGetValue(GraphFields.GRAPH_VERTEXES, out vertexes);

			BsonValue ret = vertexes != null ? GraphDBProvider.FirstOrDefaultById(vertexes.AsBsonArray, vertexId) : null;//TODO для проверки на нул есть ??
			return ret != null ? ret.AsBsonDocument : null;
        }

        public BsonDocument GetVertexByName(string graphId, string name)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("name is empty");

            BsonDocument graph = _dbProvider.GetById(graphId);
            BsonValue vertexes = null;
            graph.TryGetValue(GraphFields.GRAPH_VERTEXES, out vertexes);

            BsonValue ret = vertexes != null ? GraphDBProvider.FirstOrDefaultByName(vertexes.AsBsonArray, name) : null;//TODO для проверки на нул есть ??
            return ret != null ? ret.AsBsonDocument : null;
        }

		public void UpdateVertex(string graphId, string vertexId, string name, string typeId, BsonDocument options = null)
        {
            if (string.IsNullOrEmpty(vertexId))
                throw new ArgumentException("vertexId is empty");

            if (GetVertexTypeById(graphId, typeId) == null)
                throw new ArgumentException("There is no VertexType with typeId");

            BsonDocument graph = _dbProvider.GetById(graphId);
            BsonValue vertexes = null;

            if (!graph.TryGetValue(GraphFields.GRAPH_VERTEXES, out vertexes))
                return;

            BsonValue ret = GraphDBProvider.FirstOrDefaultById(vertexes.AsBsonArray, vertexId);
            if (ret == null)
                return;

            ret[GraphFields.NAME] = name;
            ret[GraphFields.VERTEX_TYPE] = typeId;
            if (options != null)
            {
                ret[GraphFields.VERTEX_OPTIONS] = options;
            }
            _dbProvider.Save(graph);
        }

		public BsonArray GetVertexesByType(string graphId, string typeId)
        {
            if (GetVertexTypeById(graphId, typeId) == null)
                throw new ArgumentException("There is no VertexType with typeId");

            BsonDocument graph = _dbProvider.GetById(graphId);
            BsonValue vertexes = null;

			BsonArray ret = new BsonArray();//TODO var
            if (graph.TryGetValue(GraphFields.GRAPH_VERTEXES, out vertexes))
                foreach (BsonDocument vertex in vertexes.AsBsonArray)
                {
                    BsonValue type = null;
                    if (vertex.TryGetValue(GraphFields.VERTEX_TYPE, out type) && StringUtils.StringsEqual(type.ToString(), typeId))
                    {
                        ret.Add(vertex);
                    }
                }

            return ret;
        }

		public void RemoveVertexes(string graphId, List<string> vertexIds)
        {
            if (vertexIds == null)
                throw new ArgumentException("vertexIds is empty");

            if (vertexIds.Count == 0)
                return;

            BsonDocument graph = _dbProvider.GetById(graphId);
            BsonValue vertexes = null;

            if (!graph.TryGetValue(GraphFields.GRAPH_VERTEXES, out vertexes))
                return;

            BsonArray vertexesToRemove = new BsonArray();
            foreach (BsonDocument vertex in vertexes.AsBsonArray)
            {
                BsonValue id = null;
                if (vertex.TryGetValue(GraphFields.ID, out id) && vertexIds.Contains(id.ToString()))
                {
                    vertexesToRemove.Add(vertex);
                }
            }

            foreach (BsonDocument vertex in vertexesToRemove)
            {
                vertexes.AsBsonArray.Remove(vertex);
            }


            _dbProvider.Save(graph);
        }

		public string AddArc(string graphId, string vertexId1, string vertexId2, string typeId)
        {
            BsonDocument graph = _dbProvider.GetById(graphId);
            if (GetArcTypeById(graphId, typeId) == null)
                throw new ArgumentException("There is no ArcType with typeId");

            if (GetVertex(graphId, vertexId1) == null)
                throw new ArgumentException("There is no Vertex with vertexId1");

            if (GetVertex(graphId, vertexId2) == null)
                throw new ArgumentException("There is no Vertex with vertexId2");

			BsonDocument arc = new BsonDocument();//TODO var
            ObjectId arcId = ObjectId.GenerateNewId();
            arc.Add(GraphFields.ID, arcId);
            arc.Add(GraphFields.ARC_VERTEX_ONE, vertexId1);
            arc.Add(GraphFields.ARC_VERTEX_TWO, vertexId2);
            arc.Add(GraphFields.ARC_TYPE, typeId);

            BsonValue arcs = null;

            if (!graph.TryGetValue(GraphFields.GRAPH_ARCS, out arcs))
            {
                arcs = new BsonArray();
                graph.Add(GraphFields.GRAPH_ARCS, arcs);

            }
            arcs.AsBsonArray.Add(arc);
            _dbProvider.Save(graph);

            return arcId.ToString();
        }

		public BsonDocument GetArc(string graphId, string arcId)
        {
            if (string.IsNullOrEmpty(arcId))
                throw new ArgumentException("ArcId is empty");

            BsonDocument graph = _dbProvider.GetById(graphId);
            BsonValue arcs = null;
            graph.TryGetValue(GraphFields.GRAPH_ARCS, out arcs);

            BsonValue ret = arcs != null ? GraphDBProvider.FirstOrDefaultById(arcs.AsBsonArray, arcId) : null;
            return ret != null ? ret.AsBsonDocument : null;
        }

		public void UpdateArc(string graphId, string arcId, string vertexId1, string vertexId2, string typeId)
        {
            if (string.IsNullOrEmpty(arcId))
                throw new ArgumentException("ArcId is empty");

            if (GetArcTypeById(graphId, typeId) == null)
                throw new ArgumentException("There is no ArcType with typeId");

            if (GetVertex(graphId, vertexId1) == null)
                throw new ArgumentException("There is no Vertex with vertexId1");

            if (GetVertex(graphId, vertexId2) == null)
                throw new ArgumentException("There is no Vertex with vertexId2");


            BsonDocument graph = _dbProvider.GetById(graphId);
            BsonValue arcs = null;

            if (!graph.TryGetValue(GraphFields.GRAPH_ARCS, out arcs))
                return;

            BsonValue ret = GraphDBProvider.FirstOrDefaultById(arcs.AsBsonArray, arcId);

            if (ret == null)
                return;

            ret[GraphFields.ARC_VERTEX_ONE] = vertexId1;
            ret[GraphFields.ARC_VERTEX_TWO] = vertexId2;
            ret[GraphFields.ARC_TYPE] = typeId;
            _dbProvider.Save(graph);
        }

		public BsonArray GetArcsByVertex2AndType(string graphId, string vertexId2, string typeId)
        {
            if (GetArcTypeById(graphId, typeId) == null)
                throw new ArgumentException("There is no ArcType with typeId");

            if (GetVertex(graphId, vertexId2) == null)
                throw new ArgumentException("There is no Vertex with vertexId2");


            BsonDocument graph = _dbProvider.GetById(graphId);
            BsonValue arcs = null;

			var ret = new BsonArray();
            if (graph.TryGetValue(GraphFields.GRAPH_ARCS, out arcs))
                foreach (BsonDocument arc in arcs.AsBsonArray)
                {
                    BsonValue type = null;
                    BsonValue vertex2 = null;

                    if (arc.TryGetValue(GraphFields.ARC_TYPE, out type) &&
                        arc.TryGetValue(GraphFields.ARC_VERTEX_TWO, out vertex2) &&
                        StringUtils.StringsEqual(type.ToString(), typeId) && StringUtils.StringsEqual(vertex2.ToString(), vertexId2))
                    {
                        ret.Add(arc);
                    }
                }

            return ret;
        }

		public BsonArray GetArcsByVertex1AndType(string graphId, string vertexId1, string typeId)
        {
            if (GetArcTypeById(graphId, typeId) == null)
                throw new ArgumentException("There is no ArcType with typeId");

            if (GetVertex(graphId, vertexId1) == null)
                throw new ArgumentException("There is no Vertex with vertexId1");


            BsonDocument graph = _dbProvider.GetById(graphId);
            BsonValue arcs = null;

			var ret = new BsonArray();
            if (graph.TryGetValue(GraphFields.GRAPH_ARCS, out arcs))
                foreach (BsonDocument arc in arcs.AsBsonArray)
                {
                    BsonValue type = null;
                    BsonValue vertex1 = null;

                    if (arc.TryGetValue(GraphFields.ARC_TYPE, out type) &&
                        arc.TryGetValue(GraphFields.ARC_VERTEX_ONE, out vertex1) &&
                        StringUtils.StringsEqual(type.ToString(), typeId) && StringUtils.StringsEqual(vertex1.ToString(), vertexId1))
                    {
                        ret.Add(arc);
                    }
                }

            return ret;
        }

        public string GetArcId(string graphId, string vertexId1, string vertexId2, string typeId)
        {
            if (GetArcTypeById(graphId, typeId) == null)
                throw new ArgumentException("There is no ArcType with typeId");

            if (GetVertex(graphId, vertexId1) == null)
                throw new ArgumentException("There is no Vertex with vertexId1");

            if (GetVertex(graphId, vertexId2) == null)
                throw new ArgumentException("There is no Vertex with vertexId2");

            BsonDocument graph = _dbProvider.GetById(graphId);
            BsonValue arcs = null;

            var ret = new BsonArray();
            if (graph.TryGetValue(GraphFields.GRAPH_ARCS, out arcs))
                foreach (BsonDocument arc in arcs.AsBsonArray)
                {
                    BsonValue type = null;
                    BsonValue vertex1 = null;
                    BsonValue vertex2 = null;

                    if (arc.TryGetValue(GraphFields.ARC_TYPE, out type) &&
                        arc.TryGetValue(GraphFields.ARC_VERTEX_ONE, out vertex1) &&
                        arc.TryGetValue(GraphFields.ARC_VERTEX_TWO, out vertex2) &&
                        StringUtils.StringsEqual(type.ToString(), typeId) && StringUtils.StringsEqual(vertex1.ToString(), vertexId1) &&
                        StringUtils.StringsEqual(type.ToString(), typeId) && StringUtils.StringsEqual(vertex2.ToString(), vertexId2))
                    {
                        BsonValue arcId = null;
                        arc.TryGetValue(GraphFields.ID, out arcId);

                        return arcId != null ? arcId.ToString() : null;
                    }
                }

            return null;
        }

		public void RemoveArcs(string graphId, List<string> arcIds)
        {
            if (arcIds == null)
                throw new ArgumentException("arcIds is empty");

            if (arcIds.Count == 0)
                return;

            BsonDocument graph = _dbProvider.GetById(graphId);
            BsonValue arcs = null;

            if (!graph.TryGetValue(GraphFields.GRAPH_ARCS, out arcs))
                return;


            BsonArray arcsToRemove = new BsonArray();
            foreach (BsonDocument arc in arcs.AsBsonArray)
            {
                BsonValue id = null;
                if (arc.TryGetValue(GraphFields.ID, out id) &&
                    arcIds.Contains(id.ToString()))
                {
                    arcsToRemove.Add(arc);
                }
            }

            foreach (BsonDocument arc in arcsToRemove)
            {
                arcs.AsBsonArray.Remove(arc);
            }

            _dbProvider.Save(graph);
        }
    }
}