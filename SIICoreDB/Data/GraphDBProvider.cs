﻿using System;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using SIICore.Utils;


//TODO Я заметил что данный класс относится к графам только использованием GraphFields.
namespace SIICore.Data
{ //TODO Как минимум У mongoServera есть метод Disconect. Который никогда не вызывается. Считаю нужно делать данный класс Disposable. Или типа коннект даже не вызывается? Странно короче. Посмотри короче все ли рессурсы очищаются, нет ли утечек.
    internal class GraphDBProvider
    {
        protected MongoDatabase _database;
        protected MongoCollection<BsonDocument> _collection;

		public GraphDBProvider(string connectionstring, string dbName, string collectionName)
        {
			MongoClient mongoClient = new MongoClient(connectionstring);//TODO Лучше использовать var так как в правой части тип строго задан
			MongoServer mongoServer = mongoClient.GetServer();
            _database = mongoServer.GetDatabase(dbName);
            _collection = _database.GetCollection<BsonDocument>(collectionName);

            if (_collection == null)
				throw new ArgumentNullException("_collection is null");//TODO Вообще не понятный комент в эксепшоне.. ну да он нул, а почему? Предполагаю потому что коллекции с таким именем нет. И кстати в данном случае следует создать свой тип эксепшона, так как преполагается что мы будем перехватывать его в сущности которая создает данный экземпляр и выводить что то типа того что Коллекции с данным именем нет. А что если будет какое то другое исключение? Мы его тоже перехватим в catch(Exception) а проблема будет совсем в другом.. Что нибудь типа CollectionNameNotExistException.

        }

		protected virtual void BeforeSave(BsonDocument entity)//TODO Посмотрел, что эта штука вызывается при любом сохранении в БД. Круто бы было тут узнать что именно изменилось в документе. А то метод кажется бесполезным на первый взгляд. И еще, считается что в рамках класса члены лучше всего сортировать по модификаторам доступа public - protected internal - internal - protected - private. Типа по значимости члена. Чтобы значимое было выше.
        {

        }

		public static BsonValue FirstOrDefaultById(BsonArray bsonArray, string entityId)
        {
            return bsonArray.FirstOrDefault(x =>
            {
                BsonValue id;
                x.AsBsonDocument.TryGetValue(GraphFields.ID, out id);
                return id != null && StringUtils.StringsEqual(id.ToString(), entityId);
            });
        }

		public static BsonValue FirstOrDefaultByName(BsonArray bsonArray, string entityId)
        {
            return bsonArray.FirstOrDefault(x =>
            {
                BsonValue id;
                x.AsBsonDocument.TryGetValue(GraphFields.NAME, out id);
				return id != null && StringUtils.StringsEqual(id.ToString(), entityId);
            });
        }

		public BsonDocument TryGetById(string id)
        {
            ObjectId objectId;
            if (string.IsNullOrEmpty(id) || !ObjectId.TryParse(id, out objectId))
                return null;
            
            BsonDocument ret = _collection.FindOne(Query.EQ(GraphFields.ID, objectId));
            return ret;
        }

		public BsonDocument GetById(string id)
        {
            BsonDocument ret = TryGetById(id);
            if (ret == null)
                throw new ArgumentException("Threre is no Graph with Id");
            
            return ret;
        }

		public BsonDocument TryGetByName(string name)
        {
            if (string.IsNullOrEmpty(name))
                return null;

            BsonDocument ret = _collection.FindOne(Query.EQ(GraphFields.NAME, name));
            return ret;
        }

        public List<BsonDocument> GetAll()
        {
            return _collection.FindAll().ToList();
        }

        public virtual void Save(BsonDocument entity)
        {
            BsonValue id;
            entity.TryGetValue(GraphFields.ID, out id);
			BsonDocument entityInDB = id != null && !string.IsNullOrEmpty(id.ToString()) ? TryGetById(id.ToString()) : null;
            //TODO 1)Сложная строчка - Нужны локал переменки.  : null. А бальше все равно в БД созраняется 0_0.

            BeforeSave(entity);
            _collection.Save(entity);
            AfterSave(entity, entityInDB);
        }

        protected virtual void AfterSave(BsonDocument entity, BsonDocument entityInDB)
        {

        }
    }
}
