﻿namespace SIICore.Data
{
    internal class GraphFields
    {
        public static string ID = "_id";
        public static string NAME = "Name";

        public static string GRAPH_VERTEX_TYPES = "VertexTypes";
        public static string GRAPH_ARC_TYPES = "ArcTypes";
        public static string GRAPH_VERTEXES = "Vertexes";
        public static string GRAPH_ARCS = "Arcs";

        //public static string VERTEX_TYPE_NAME = "Name";
        //public static string ARC_TYPE_NAME = "Name";

        //public static string VERTEX_NAME = "Name";

        public static string VERTEX_TYPE = "idType";
        public static string VERTEX_OPTIONS = "Options";

        public static string ARC_VERTEX_ONE = "idVertex1";
        public static string ARC_VERTEX_TWO = "idVertex2";
        public static string ARC_TYPE = "idType";
    }
}
